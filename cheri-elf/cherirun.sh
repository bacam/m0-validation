#!/bin/bash

set -e

FILE="$1.mem"
ELF="`mktemp`"
shift
./mkelf "$FILE" "$ELF"
qemu-system-cheri -m 1025 -kernel "$ELF" -nographic -machine mipssim "$@"
rm -- "$ELF"
