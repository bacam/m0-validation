#!/bin/bash

#set -e

ELF="`mktemp`"
SIMREGS="`mktemp`"

./mkelf "$1.mem" "$ELF"
#timeout 10 run_cheri.native --quiet --file "$ELF" 2>&1 | grep '^DEBUG' > "$SIMREGS"
run_cheri.native --max_instruction 10000 --quiet --file "$ELF" 2>&1 | grep '^DEBUG' > "$SIMREGS"
grep '^DEBUG' "$1.reg" | diff -qwB -- "$SIMREGS" - >/dev/null
R=$?
rm -- "$ELF" "$SIMREGS"
exit $R
