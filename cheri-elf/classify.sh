#!/bin/bash

EXN="$1"
FILE="$2"

echo -n "$FILE: "

grep -q ctoptr "$FILE" && echo -n "ctoptr "
PREEXNINSTR=`head -n $EXN "$FILE" | tail -n 1`
EXNINSTR=`head -n $(($EXN + 1)) "$FILE" | tail -n 1`
echo "$PREEXNINSTR" | egrep -q ' c?[bj]'   && echo -n "cflow "
echo "$EXNINSTR"    | grep -q 'cclearregs' && echo -n "cclearregsexn "
echo
