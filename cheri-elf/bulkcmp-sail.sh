#!/bin/bash

ok=0
bad=0
for f in "$1"/test*.mem; do
  ./chericmp-sail.sh "${f%.mem}"
  if [ $? -ne 0 ]; then echo "$f"; bad=$(($bad + 1)); else ok=$(($ok + 1)); fi
done
echo "OK:  $ok"
echo "BAD: $bad"
