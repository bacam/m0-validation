#!/bin/bash

set -e

ELF="`mktemp`"
QEMUREGS="`mktemp`"

./mkelf "$1.mem" "$ELF"
timeout 10 qemu-system-cheri -m 1025 -kernel "$ELF" -display none -machine mipssim 2>&1 \
  | grep -v 'COREID\|MIPS PC\|CAP PCC' > "$QEMUREGS"
grep -v 'egisters\|Core\|MIPS PC\|CAP PCC' "$1.reg" \
  | diff -qB -- "$QEMUREGS" - >/dev/null
R=$?
rm -- "$ELF" "$QEMUREGS"
exit $R
