#include <libelf.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>

void unix_error() {
  perror("mkelf");
  exit(1);
}

void elf_error() {
  const char *s = elf_errmsg(0);
  if (s == NULL) {
    fprintf(stderr, "mkelf: terribly confused\n");
  } else {
    fprintf(stderr, "mkelf: %s\n", s);
  }
  exit(1);
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
      fprintf(stderr, "Usage: mkelf <raw binary file name> <elf output file name>\n");
      exit(1);
    }

    struct stat rawstat;
    int rawfd = open(argv[1], O_RDONLY, 0);
    if (rawfd == -1) unix_error();
    if (fstat(rawfd, &rawstat) == -1) unix_error();
    void *rawbuf = mmap(NULL, rawstat.st_size, PROT_READ, MAP_PRIVATE, rawfd, 0);
    if (rawbuf == MAP_FAILED) unix_error();

    if (elf_version(EV_CURRENT) == EV_NONE) elf_error();
    int fd = open(argv[2], O_RDWR|O_CREAT, 0666);
    if (fd == -1) unix_error();
    Elf *elf = elf_begin(fd, ELF_C_WRITE, NULL);
    if (elf == NULL) elf_error();

    Elf64_Ehdr *ehdr = elf64_newehdr(elf);
    if (ehdr == NULL) elf_error();
    ehdr->e_ident[EI_DATA] = ELFDATA2MSB;
    ehdr->e_machine = EM_MIPS;
    ehdr->e_type = ET_EXEC;
    ehdr->e_entry = 0x9000000040000000ULL;

    Elf64_Phdr *phdr = elf64_newphdr(elf, 1);
    if (ehdr == NULL) elf_error();
    phdr->p_type = PT_LOAD;
    phdr->p_vaddr = 0x9000000040000000ULL;
    phdr->p_paddr = 0x0000000040000000ULL;
    phdr->p_flags = PF_X|PF_R|PF_W;
    phdr->p_filesz = rawstat.st_size;
    phdr->p_memsz = rawstat.st_size;

    Elf_Scn *scn = elf_newscn(elf);
    if (scn == NULL) elf_error();
    Elf64_Shdr *shdr = elf64_getshdr(scn);
    if (shdr == NULL) elf_error();
    shdr->sh_type = SHT_PROGBITS;
    shdr->sh_flags = SHF_WRITE|SHF_ALLOC|SHF_EXECINSTR;
    shdr->sh_addr = 0x9000000040000000ULL;
    Elf_Data *data = elf_newdata(scn);
    if (data == NULL) elf_error();
    data->d_buf = rawbuf;
    data->d_size = rawstat.st_size;

    if (elf_update(elf, ELF_C_NULL) == -1)
      elf_error();

    phdr->p_offset = shdr->sh_offset;
    if (elf_flagphdr(elf, ELF_C_SET, ELF_F_DIRTY) == 0)
      elf_error();

    if (elf_update(elf, ELF_C_WRITE) == -1)
      elf_error();
    elf_end(elf);
    close(fd);
}
