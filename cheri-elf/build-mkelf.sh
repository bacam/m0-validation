#!/bin/sh

set -ex

gcc -Wall -g mkelf.c -o mkelf `pkg-config --cflags --libs libelf`
