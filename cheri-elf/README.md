These scripts are for running generated CHERI tests when an ELF file is
required, currently for qemu.  To use them properly requires a few changes to
qemu, but the `mkelf` tool can still be used to run an individual test:

1. Run `./build-mkelf.sh` to generate the `mkelf` executable
2. Run `./mkelf <test binary> <output filename>` to make an ELF file
3. Use `-kernel <filename` when starting qemu to load test

The `mkelf` program uses `libelf` to embed the test in a simple ELF file.  It
isn't intended to be complete or correct in any particular sense except that
I've successfully run batches of tests using it.
