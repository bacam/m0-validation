#!/bin/bash

EXN=$1
DIR=$2

ok=0
bad=0
for f in "$DIR"/test*.mem; do
  ./chericmp.sh "${f%.mem}"
  if [ $? -ne 0 ]; then
    ./classify.sh "$EXN" "${f%.mem}.instrs"
    bad=$(($bad + 1))
  else
    ok=$(($ok + 1))
  fi
done
echo "OK:  $ok"
echo "BAD: $bad"
