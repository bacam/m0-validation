#!/bin/bash

set -e

QEMUREGS="`mktemp -t qemuregs.XXXXXXXX`"
ELF="`mktemp -t qemuelf.XXXXXXXX`"
BASE="$1"
shift

./mkelf "$BASE.mem" "$ELF"
qemu-system-cheri -m 1025 -kernel "$ELF" -nographic -machine mipssim "$@" 2>&1 \
  | grep -v 'COREID\|MIPS PC\|CAP PCC' > "$QEMUREGS"
grep -v 'egisters\|Core\|MIPS PC\|CAP PCC' "$BASE.reg" \
  | diff -uBs -- "$QEMUREGS" -
rm -- "$ELF" "$QEMUREGS"
