#!/bin/bash

#set -e

FILE="$1.mem"
ELF="`mktemp`"
shift
./mkelf "$FILE" "$ELF"
run_cheri.native --file "$ELF" "$@"
rm -- "$ELF"
