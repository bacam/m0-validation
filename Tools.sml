structure Tools : Tools =
struct

local
open HolKernel Parse boolLib
in

(* String functions used in CHERI logging that cause large output or
   non-termination. *)
val string_opaque = [ASCIInumbersSyntax.num_to_dec_string_tm, ``STRCAT``,
                     wordsSyntax.word_to_hex_string_tm]

(* For general call-by-value computation, except for a given list of terms. *)
fun mk_cbv_compset non_eval =
    let val cmp = TargetBasics.compset ()
        (* The current CHERI decoder uses some HOL integers *)
        val () = intReduce.add_int_compset cmp
        val () = integer_wordLib.add_integer_word_compset cmp
        val () = 
            computeLib.add_conv (bitstringSyntax.v2w_tm, 1, bitstringLib.v2w_n2w_CONV)
                                cmp
        val () = List.app (computeLib.scrub_const cmp) string_opaque
        val () = computeLib.add_thms [wordsTheory.word_srem_def,
                                      alignmentTheory.aligned_extract,
                                      alignmentTheory.align_sub,
                                      testingTheory.myb2w_def,
                                      testingTheory.w2n8_def] cmp
        val () = List.app (fn t => (computeLib.scrub_const cmp t) handle NotFound => ()) non_eval
    in cmp
    end
fun mk_cbv_CONV non_eval =
    let val cmp = mk_cbv_compset non_eval
    in computeLib.CBV_CONV cmp
    end

fun conv_term conv tm = (snd o dest_eq o concl o conv) tm

(* We don't want to undo the rewrites during reductions that happen before we
   go to the SMT solver. *)

local open wordsSyntax in
val non_eval_terms = [word_lsl_bv_tm,
                      word_lsr_bv_tm,
                      word_asr_bv_tm,
                      word_and_tm,
                      word_lsl_tm,
                      word_lsr_tm,
                      word_asr_tm,
                      word_ror_tm,
                      word_div_tm,
                      word_sdiv_tm,
                      word_mod_tm,
                      word_srem_tm,
                      (* We can't deal with bit_field_insert in the SMT
                         translation, so they should be dealt with first,
                         but just in case we miss one don't restrict their
                         evaluation, otherwise the terms will blow up *)
                      bit_field_insert_tm,
                      ``myb2w : bool -> 'a word``,
                      ``w2n8``]@TargetBasics.smt_non_eval_terms
end

val cbv_restr_CONV = mk_cbv_CONV non_eval_terms
val cbv_restr_eval = conv_term cbv_restr_CONV

(* At other times we don't need to restrict anything. *)
val cbv_CONV = mk_cbv_CONV []
val cbv_eval = conv_term cbv_CONV


local
(* The SMT solver might return a maze of equations, so we try to arrange
   them into ``var args... = concrete value``. *)
fun build_map tms =
    let fun add (tm,m) =
            let val (l,r) = dest_eq tm
            in(* We might need to turn the eq around *)
               if is_var (fst (strip_comb l))
               then Redblackmap.insert (m, l, ref r)
               else Redblackmap.insert (m, r, ref l)
            end
    in foldl add (Redblackmap.mkDict Term.compare) tms
    end

fun follow m =
    let val max = Redblackmap.numItems m
        exception Loop
        fun follow i tm =
            if i = max then raise Loop else
            let val tm_ref = Redblackmap.find (m, tm)
            in follow_ref i tm_ref
            end handle NotFound => tm
        and follow_ref i tm_ref =
            let val tm'' = follow (i+1) (!tm_ref)
                val () = tm_ref := tm''
            in tm''
            end
        fun top (tm,tm_ref) =
            let val _ = follow_ref 0 tm_ref
            in ()
            end handle Loop => Feedback.HOL_WARNING "Tools" "follow"
                                                    ("Term recursively defined: " ^ term_to_string tm)
    in Redblackmap.app top m
    end

in
fun eqns_to_updates tms =
    let fun up [] vl _ = vl
          | up (arg::args) vl old =
            ``(^arg =+ ^(up args vl (mk_comb (old,arg)))) ^old``
        fun list_up h NONE = [h]
          | list_up h (SOME t) = h::t
        fun collect (lhs,rhs,m') =
            let val (lvar,largs) = strip_comb lhs
            in Redblackmap.update (m', lvar, list_up (up largs (!rhs) lvar))
            end
        val m = build_map tms
        val () = follow m
        val m' = Redblackmap.foldl collect (Redblackmap.mkDict Term.compare) m
        val updates = Redblackmap.foldl (fn (lhs,rhs,l) => (lhs,rhs)::l) [] m'
        fun merge (v,l) = (v,List.foldl (fn (u,us) => subst [{redex = v, residue = u}] us) v l)
    in map merge updates
    end
end

fun read_file filename =
    let val ins = TextIO.openIn filename
        val s = TextIO.inputAll ins
        val () = TextIO.closeIn ins
    in s
    end

local
    fun drop x [] = []
      | drop x (y::ys) = if x = y then drop x ys else y::ys
in
(* Merge and deduplicate two sorted lists *)
fun merge cmp [] ys = ys
  | merge cmp xs [] = xs
  | merge cmp (x::xs) (y::ys) =
    if cmp x y orelse x = y then x::(merge cmp (drop x xs) (drop x (y::ys)))
    else y::(merge cmp (x::xs) (drop y ys))
end

(* Rather indirect, but never mind. *)
fun random_word gen n zero_bits =
    let val bits = RandGen.bits gen n @ List.tabulate (zero_bits, fn _ => false)
        val hbits = bitstringSyntax.bitstring_of_bitlist bits
        val ty = wordsSyntax.mk_int_word_type (n+zero_bits)
        val word = ``v2w ^hbits : ^(ty_antiq ty)``
    in (snd o dest_eq o concl o bitstringLib.v2w_n2w_CONV) word
    end

fun bool_option name rf =
  (name,
   (fn s => 
       if s = "on" orelse s = "true" orelse s = "1" then rf := true
       else if s = "off" orelse s = "false" orelse s = "0" then rf := false
       else failwith (name ^ ": " ^ s ^ " is not a boolean value"),
    fn () => if !rf then "on" else "off"))

end

end
