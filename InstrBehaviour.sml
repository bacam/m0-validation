structure InstrBehaviour =
struct

open SymExec

fun fetch_eval hyps t = map snd (eval exception_filter [] hyps t)

val (fetch_hyps, fetch_value, instr_rw) = EvalBasics.get_fetch fetch_eval

fun next_filter filter_fn h =
    let val h = String.translate (fn #" " => "" | x => String.str x) h
        val v = 
            case StringCvt.scanString (IntInf.scan StringCvt.HEX) h of
                NONE => failwith h
              | SOME n => n
        val rs = start filter_fn
                       [(EvalBasics.fetch_tm,1,instr_rw (EvalBasics.term_for_instr v))]
                       (make_hyps (EvalBasics.std_hyp),
                        ``^(EvalBasics.next_fn) ^state_var``)
        val ths = map (fn r => (#2 r, #3 r)) rs
    in ths
    end

fun next_code_filter filter_fn q =
  let val h = fst (hd (EvalBasics.encode q))
  in next_filter filter_fn h
  end

fun next h = map snd (next_filter exception_filter h)

fun next_code q =
  let val h = fst (hd (EvalBasics.encode q))
  in next h
  end

fun next_exn h =
    List.mapPartial
       (fn (e,th) => case e of [] => NONE | _ => SOME th)
       (next_filter (K false) h)

fun next_code_exn q =
  let val h = fst (hd (EvalBasics.encode q))
  in next_exn h
  end

end
