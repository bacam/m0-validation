structure Logging : Logging =
struct

open Feedback

type outfile = {
  out : TextIO.outstream,
  name : string,
  count : int ref
}

type subtestinfo = {
  subid : int,
  successes : int,
  unables : int,
  mismatches : int,
  end_init : PolyStats.stats,
  start_sub : PolyStats.stats
}

type testinfo = {
  id : string,
  subtest : subtestinfo option ref,
  instrs : (string * string * int) list,
  exns : int list,
  cs : string ref,
  harness : string,
  locs : string list ref,
  initstats : PolyStats.stats,
  tailstats : PolyStats.stats list ref,
  smt : (Time.time * Time.time) option ref
}

type subtest_stats = {
  sequences : int,
  allsuccess : int,
  allfail : int,
  mixed : int,
  included_mismatch : int
}

type state = {
  dir : string,
  random_dir : string,
  summaryfile : TextIO.outstream,
  test_info : testinfo option ref,
  stats_info : PolyStats.stats,
  testnum : int ref,
  fail_useless_instruction : outfile,
  fail_bad_choices : outfile,
  fail_imposs_comb : outfile,
  fail_smt_unknown : outfile,
  fail_smt_unsat   : outfile,
  fail_mismatch    : outfile,
  fail_misc_exn    : outfile,
  success          : outfile,
  subtest_stats    : subtest_stats ref
}

type time = PolyStats.stats
val start_time = PolyStats.get

val separator = "   "

val s : state option ref = ref NONE

fun getstate () =
    case !s of
        NONE => failwith "Tried to use logging outside of test run"
      | SOME s' => s'

fun get_test_info () =
  case !(#test_info (getstate ())) of
      NONE => failwith "Tried to access test information when not executing a test"
    | SOME i => i

fun datetime () =
    Date.fmt "%a, %d %b %Y %H:%M:%S %Z\n" (Date.fromTimeLocal (Time.now()))

fun setup dir info_str =
    let val () =
            case !s of
                SOME _ => failwith "Tried to setup a new test run without finishing the old one"
              | _ => ()
        (* TODO: check for shell metacharacters in dir, because it's hard to
                 make shell commands work properly otherwise *)
        val () = OS.FileSys.mkDir dir
        val rdir = OS.Path.joinDirFile {dir = dir, file="random"}
        val () = OS.FileSys.mkDir rdir
        (* TODO: check this worked *)
        val _  = OS.Process.system ("(git rev-parse HEAD; git status -bs) > " ^ dir ^ "/git-status")
        val summary = TextIO.openOut (OS.Path.joinDirFile {dir = dir, file="summary"})
        val () = TextIO.output (summary, datetime ())
        val () = TextIO.output (summary, info_str)
        val () = TextIO.flushOut summary
        fun newout name = {name = name, count = ref 0,
                           out = TextIO.openOut (OS.Path.joinDirFile {dir = dir, file=name})}
    in s := SOME {dir = dir, random_dir = rdir,
                  summaryfile = summary,
                  testnum = ref 0, test_info = ref NONE,
                  stats_info = PolyStats.get (),
                  fail_useless_instruction = newout "Fail_Useless_instruction",
                  fail_bad_choices = newout "Fail_Bad_choices",
                  fail_imposs_comb = newout "Fail_Imposs_comb",
                  fail_smt_unknown = newout "Fail_SMT_Unknown",
                  fail_smt_unsat = newout "Fail_SMT_Unsat",
                  fail_mismatch = newout "Fail_Mismatch",
                  fail_misc_exn = newout "Fail_MiscExn",
                  success = newout "Success",
                  subtest_stats = ref {sequences=0,allsuccess=0,allfail=0,mixed=0,included_mismatch=0}}
    end

fun format_instrs instrs exns =
    let fun i_to_s i (hex,class,len) =
            "(\"" ^ hex ^ "\", \"" ^ class ^ "\", " ^ Int.toString len ^ "," ^
            (if Lib.mem i exns then "true" else "false") ^ ")"
        val ss = Lib.mapi i_to_s instrs
    in "[" ^ String.concatWith ", " ss ^ "]"
    end

fun save_instrs id _ [] = ()
  | save_instrs id [] _ = ()
  | save_instrs id locs instrs =
    let val filename = OS.Path.joinDirFile {dir = #dir (getstate ()), file = "test" ^ id ^ ".instrs"}
        val out = TextIO.openOut filename
        fun write (l,(h,_,_)) = TextIO.output (out, l ^ " " ^ Target.disassemble_hex h ^ "\n")
        val () = ListPair.app write (locs, instrs)
    in TextIO.closeOut out
    end

fun new_test instrs exns harness starttime =
    let val s = getstate ()
        val n = ! (#testnum s) + 1
        val sn = Int.toString n
        val () = #testnum s := n
        val pretty = map (fn (h,c,_) => h ^ " " ^ c) instrs
        val () = print ("Test " ^ sn ^ ": " ^ String.concatWith " " pretty ^ "\n")
        val () = print ("Exceptions: [" ^ String.concatWith "," (map Int.toString exns) ^ "]\n")
    in #test_info s := SOME {id=sn, subtest=ref NONE, instrs=instrs, exns=exns, cs=ref "", harness=harness, locs=ref [], initstats=starttime, tailstats=ref [], smt=ref NONE}
    end

local
   val a = ord #"a"
   fun f i l =
     if i < 26 then (chr (a+i))::l
     else f ((i div 26)-1) ((chr (a+(i mod 26)))::l)
in
fun format_subtest (SOME {subid,...}) = String.implode (f subid [])
  | format_subtest NONE = ""
end

fun filename prefix suffix default =
  case !s of
      NONE => default
    | SOME s =>
      let val {id,subtest,...} = get_test_info ()
      in OS.Path.joinDirFile {dir = #dir s, file = prefix ^ id ^ format_subtest (!subtest) ^ suffix}
      end


fun new_subtest () =
  let val ti = get_test_info ()
      val time = PolyStats.get ()
      val st = case !(#subtest ti) of
                   NONE => {subid=0,successes=0,unables=0,mismatches=0,end_init=time,start_sub=time}
                 | SOME {subid,successes,unables,mismatches,end_init,...} =>
                   {subid=subid+1,successes=successes,unables=unables,mismatches=mismatches,
                    end_init=end_init,start_sub=time}
      val () = print ("Test " ^ #id ti ^ " subtest " ^ format_subtest (SOME st) ^ "\n")
      val () = #subtest ti := SOME st
      val () = #cs ti := ""
      val () = #locs ti := []
      val () = #smt ti := NONE
      val () = #tailstats ti := []
  in ()
  end

fun record_choices choices =
    let val {cs,...} = get_test_info ()
        val () = cs := !cs ^ "[" ^ String.concatWith "," (map Int.toString choices) ^ "]"
        val () = print ("Theorem choices: " ^ !cs ^ "\n")
    in ()
    end

fun split_timing () =
    let val {tailstats,...} = get_test_info ()
        val new = PolyStats.get ()
    in tailstats := new :: !tailstats
    end

fun smt_times times =
    let val {smt,...} = get_test_info ()
    in smt := SOME times
    end

fun record_instr_locs locs' =
    let val {locs,...} = get_test_info ()
    in locs := locs'
    end

fun report_split_times init NONE l = "times:" ^ PolyStats.split_times (init::rev l)
  | report_split_times init (SOME {end_init,start_sub,...}) l =
    "times:" ^ PolyStats.split_times [init,end_init] ^ "," ^
               PolyStats.split_times (start_sub::rev l)

fun report_smt NONE = "nosmt"
  | report_smt (SOME (user,sys)) =
    "smt:" ^ Time.toString user ^ "s+" ^ Time.toString sys ^ "s"

datatype complaint =
  Useless_instruction of string
| Bad_choices of int list
| Impossible_combination
| SMT_Unknown
| SMT_Unsat
| Mismatch of string
| Misc_exn of exn

fun c_to_file c =
    let val s = getstate ()
    in case c of
           Useless_instruction _ => #fail_useless_instruction s
         | Bad_choices cs => (record_choices cs; #fail_bad_choices s)
         | Impossible_combination => #fail_imposs_comb s
         | SMT_Unknown => #fail_smt_unknown s
         | SMT_Unsat   => #fail_smt_unsat s
         | Mismatch _  => #fail_mismatch s
         | Misc_exn _  => #fail_misc_exn s
    end

fun extra_fail (Mismatch s) = s
  | extra_fail (Useless_instruction hex) = "Useless instruction: " ^ hex
  | extra_fail (Misc_exn e) = exn_to_string e
  | extra_fail _ = ""

fun fail complaint =
    let val () = split_timing ()
        val out = c_to_file complaint
        val s = extra_fail complaint
        (* Avoid extra newlines in output *)
        val s = String.toString s
        val () = #count out := !(#count out) + 1
        val {id,subtest,instrs=ins,exns,cs,harness,locs,initstats=stats,tailstats=splits,smt} = get_test_info ()
        val id = id ^ format_subtest (!subtest)
        val () = save_instrs id (!locs) ins
        val ins = format_instrs ins exns
        val locs = String.concatWith "," (!locs)
        val () = TextIO.output (#out out, (id ^ separator ^ ins ^ separator ^
                                           !cs ^ separator ^ harness ^ separator ^
                                           s ^ separator ^ locs ^ separator ^
                                           report_split_times stats (!subtest) (!splits) ^ separator ^
                                           report_smt (!smt) ^ "\n"))
        val () = print ("Failed: " ^ #name out ^ "\n")
        val () = case !subtest of
                     NONE => #test_info (getstate()) := NONE
                   | SOME {subid,successes,unables,mismatches,end_init,start_sub} =>
                     subtest :=
                     SOME (case complaint of 
                               (Mismatch _) => {subid=subid,
                                                successes=successes,
                                                unables=unables,
                                                mismatches=mismatches+1,
                                                end_init=end_init,
                                                start_sub=start_sub}
                             | _            => {subid=subid,
                                                successes=successes,
                                                unables=unables+1,
                                                mismatches=mismatches,
                                                end_init=end_init,
                                                start_sub=start_sub})
        val () =  TextIO.flushOut (#out out)
    (* Mismatches are successfully generated tests, the rest are not *)
    in case complaint of Mismatch _ => true | _ => false
    end

fun success () =
    let val () = split_timing ()
        val s = getstate ()
        val out = #success s
        val () = #count out := !(#count out) + 1
        val {id,subtest,instrs=ins,exns,cs,harness,locs,initstats=stats,tailstats=splits,smt} = get_test_info ()
        val id = id ^ format_subtest (!subtest)
        val () = save_instrs id (!locs) ins
        val ins = format_instrs ins exns
        val locs = String.concatWith "," (!locs)
        val () = TextIO.output (#out out, id ^ separator ^ ins ^ separator ^
                                          !cs ^ separator ^ harness ^ separator ^ 
                                          locs ^ separator ^
                                          report_split_times stats (!subtest) (!splits) ^ separator ^
                                          report_smt (!smt) ^ "\n")
        val () = print "OK\n"
        val () = case !subtest of
                     NONE => #test_info (getstate()) := NONE
                   | SOME {subid,successes,unables,mismatches,end_init,start_sub} =>
                     subtest := SOME {subid=subid,
                                      successes=successes+1,
                                      unables=unables,
                                      mismatches=mismatches,
                                      end_init=end_init,
                                      start_sub=start_sub}
        val () = TextIO.flushOut (#out out)
    in true
    end

fun subtests_complete () =
  let val s = getstate()
  in case !(#test_info s) of
         NONE => ()
       | SOME ti =>
         case !(#subtest ti) of
             NONE => #test_info s := NONE
           | SOME subtest =>
             let val {sequences,allsuccess,allfail,mixed,included_mismatch} = !(#subtest_stats s)
                 val (allsuccess,allfail,mixed) =
                     if #successes subtest = 0 then (allsuccess,allfail+1,mixed) else
                     if #unables subtest = 0 andalso #mismatches subtest = 0
                     then (allsuccess+1,allfail,mixed)
                     else (allsuccess,allfail,mixed+1)
                 val included_mismatch = included_mismatch + (if #mismatches subtest > 0 then 1 else 0)
                 val () = #subtest_stats s :=
                          {sequences=sequences+1,
                           allsuccess=allsuccess,
                           allfail=allfail,
                           mixed=mixed,
                           included_mismatch=included_mismatch}
             in #test_info s := NONE
             end
  end


fun record_random gen =
    let val s = getstate ()
        val {id,...} = get_test_info ()
        val out = TextIO.openOut (OS.Path.joinDirFile {dir= #random_dir s, file=id})
        val () = TextIO.output (out, RandGen.record gen)
    in TextIO.closeOut out
    end

fun print_subtest_stats (out, stats) =
  let fun p s = TextIO.output (out, s ^ "\n")
      val () = p ("Total sequences: " ^ Int.toString (#sequences stats))
      val () = p ("Seqs where all subtests OK: " ^ Int.toString (#allsuccess stats))
      val () = p ("Seqs where all subtests fail: " ^ Int.toString (#allfail stats))
      val () = p ("Seqs with mixed subtests: " ^ Int.toString (#mixed stats))
      val () = p ("Seqs whose subtests included a mismatch: " ^ Int.toString (#included_mismatch stats))
  in ()
  end

fun finish () =
    (let val s = getstate()
         val stats = PolyStats.get ()
        fun fin out = 
            let val () = TextIO.output (#summaryfile s,
                                        "Count for " ^ #name out ^ ": " ^
                                        Int.toString (!(#count out)) ^ "\n")
            in TextIO.closeOut (#out out)
            end
        val () = fin (#fail_useless_instruction s)
        val () = fin (#fail_bad_choices s)
        val () = fin (#fail_imposs_comb s)
        val () = fin (#fail_smt_unknown s)
        val () = fin (#fail_smt_unsat s)
        val () = fin (#fail_mismatch s)
        val () = fin (#fail_misc_exn s)
        val () = fin (#success s)
        val subtest_stats = !(#subtest_stats s)
        val () = if #sequences subtest_stats > 0
                 then print_subtest_stats (#summaryfile s, subtest_stats)
                 else ()
        val () = TextIO.output (#summaryfile s, datetime ())
        val () = TextIO.output (#summaryfile s, PolyStats.format_diff (#stats_info s) stats)
    in TextIO.closeOut (#summaryfile s)
    end; s := NONE)

end
