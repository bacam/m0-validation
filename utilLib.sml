structure utilLib =
struct

local
open HolKernel Parse boolLib bossLib
in

fun lit_LET_CONV tm =
  let val tm' = rand tm
  in if Literal.is_literal tm'
     then ONCE_REWRITE_CONV [boolTheory.LET_THM] tm
     else failwith "not a literal"
  end

(* Reduce a FOR loop one step when concrete bounds are known. *)
fun for_CONV tm =
    let val arg = rand tm
        val (n,m) = case pairSyntax.strip_pair arg of
                        [n,m,_] => (n,m)
                      | _ => Feedback.failwith "not a triple"
    in if numSyntax.is_numeral n andalso numSyntax.is_numeral m
       then
          (ONCE_REWRITE_CONV [state_transformerTheory.FOR_def] THENC
           ONCE_REWRITE_CONV [SIMP_CONV arith_ss [] (mk_eq (n,m))] THENC
           ONCE_REWRITE_CONV [boolTheory.COND_CLAUSES]) tm
       else Feedback.failwith "not concrete numerals"
    end


local
  open simpLib in

val FOR_ss =
    std_conv_ss {name = "FOR", pats = [``FOR (_:num # num # (num -> 'a -> unit # 'a))``], conv = for_CONV}

val lit_LET_ss =
    std_conv_ss {name = "lit LET", pats = [``LET (_:'a -> 'b) _``], conv = lit_LET_CONV}

end


fun decide tac tm = (tm via (tac THEN NO_TAC)) ORELSE (mk_neg tm via (tac THEN NO_TAC))

(* Attempt to automatically discharge bitvector comparisons. *)
local
open wordsSyntax

val cmps = [
   word_lt_tm,
   word_gt_tm,
   word_le_tm,
   word_ge_tm,
   word_ls_tm,
   word_hi_tm,
   word_lo_tm,
   word_hs_tm
]

in

fun find_cmp tm =
  let fun is_cmp tm =
        is_comb tm andalso
        let val (f,args) = strip_comb tm
        in length args = 2 andalso List.exists (same_const f) cmps
        end
  in find_term is_cmp tm
  end

fun bv_cmp_tac (hs,gl) =
  let val tm = find_cmp gl
  in ((decide wordsLib.WORD_DECIDE_TAC tm)
      THEN POP_ASSUM (fn th => REWRITE_TAC [th])) (hs,gl)
  end

end

local
   fun is_bv_eq tm =
     is_eq tm andalso
     wordsSyntax.is_word_type (type_of (lhs tm))
in
fun bv_eq_tac (hs,gl) =
  let val tm = find_term is_bv_eq gl
  in ((decide blastLib.FULL_BBLAST_TAC tm)
      THEN POP_ASSUM (fn th => REWRITE_TAC [th])) (hs,gl)
  end
end


(* Conversions to tidy up word extraction and concatenation *)
fun extract_extract_conv tm =
    let open wordsSyntax
        val (n1,m1,tm1,ty1) = dest_word_extract tm
        val (n2,m2,tm2,ty2) = dest_word_extract tm1
        val v = mk_var ("x",type_of tm2)
        val skel = mk_word_extract (n1,m1,mk_word_extract (n2,m2,v,ty2),ty1)
        val skel_th = wordsLib.WORD_CONV skel
    in INST [v |-> tm2] skel_th
    end

val extract_extract_ss = simpLib.std_conv_ss {
       name = "extract_extract",
       pats = [``(n >< m) ((n' >< m') (x : 'a word) : 'b word) : 'c word``],
       conv = extract_extract_conv
    }

fun extract_concat_conv tm =
    let open wordsSyntax Arbnum
        val mk_num = numSyntax.mk_numeral
        fun mk_extract (n,m,v,ty) = mk_word_extract (mk_num n, mk_num m, v, ty)
        val (n,m,tm0,ty0) = dest_word_extract tm
        val n = numSyntax.dest_numeral n
        val m = numSyntax.dest_numeral m
        val (tm1,tm2) = dest_word_concat tm0
        val sz1 = size_of tm1
        val sz2 = size_of tm2
        val v1 = mk_var ("x1",type_of tm1)
        val v2 = mk_var ("x2",type_of tm2)
        val skel_lhs = mk_extract (n,m, mk_word_concat (v1,v2), ty0)
        val skel1 =
            if n = sz1 + sz2 - one andalso m <= sz2
            then v1
            else mk_extract (n - sz2, m - sz2, v1, ty0)
        val skel2 =
            if n + one >= sz2 andalso m = zero
            then v2
            else mk_extract (if n < sz2 then n else sz2 - one, m, v2, ty0)
        val skel_rhs = 
            if m >= sz2 then skel1
            else if n < sz2 then skel2
            else mk_word_concat (skel1,skel2)
        val skel_th = wordsLib.WORD_DECIDE (mk_eq (skel_lhs, skel_rhs))
    in INST [v1 |-> tm1, v2 |-> tm2] skel_th
    end

val extract_concat_ss = simpLib.std_conv_ss {
       name = "extract_concat",
       pats = [``(n >< m) ((x : 'a word @@ y : 'b word) : 'c word) : 'd word``],
       conv = extract_concat_conv
    }

fun upper_bounded n =
  let open numSyntax
      val var = mk_var ("n", num)
      val upper = mk_less (var, term_of_int n)
      fun eqs m acc = if m = n then acc else eqs (m+1) ((mk_eq (var, term_of_int m))::acc)
      val disjs = list_mk_disj (eqs 0 [])
      val tm = mk_forall (var, mk_imp (upper, disjs))
  in DECIDE tm
  end

fun upper_bounded_tac tm (asl,w) =
  let open numSyntax
      val boundtm = List.find (fn asm => is_less asm andalso
                                         let val (x,y) = dest_less asm
                                         in term_eq x tm andalso is_numeral y
                                         end) asl
      val bound = int_of_term (snd (dest_less (Option.valOf boundtm)))
      val th = upper_bounded bound
  in STRUCT_CASES_TAC (UNDISCH_ALL (SPEC tm th)) (asl,w)
  end

fun bounded m n =
  let open numSyntax
      val var = mk_var ("n", num)
      val lower = mk_less (term_of_int m, var)
      val upper = mk_less (var, term_of_int n)
      fun eqs m acc = if m = n then acc else eqs (m+1) ((mk_eq (var, term_of_int m))::acc)
      val disjs = list_mk_disj (eqs (m+1) [])
      val tm = mk_forall (var, mk_imp (lower, mk_imp (upper, disjs)))
  in DECIDE tm
  end

fun bounded_tac tm (asl,w) =
  let open numSyntax
      val upboundtm = List.find (fn asm => is_less asm andalso
                                           let val (x,y) = dest_less asm
                                           in term_eq x tm andalso is_numeral y
                                           end) asl
      val lwboundtm = List.find (fn asm => is_less asm andalso
                                           let val (x,y) = dest_less asm
                                           in term_eq y tm andalso is_numeral x
                                           end) asl
      val upbound = int_of_term (snd (dest_less (Option.valOf upboundtm)))
      val lwbound = int_of_term (fst (dest_less (Option.valOf lwboundtm)))
      val th = bounded lwbound upbound
  in STRUCT_CASES_TAC (UNDISCH_ALL (SPEC tm th)) (asl,w)
  end

end
end
