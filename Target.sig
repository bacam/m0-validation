(* Basic definitions go into TargetBasics, so that Target can rely on Tools. *)

signature Target =
sig

(* Location and size of each usable memory fragment *)
val ram_details : (Arbnum.num * Arbnum.num) list

(* PC term for a given state *)
val pc : Term.term -> Term.term

val memory_term : Term.term -> Term.term
val memory_field_update : Term.term
val memory_type : Type.hol_type
val memory_element_size : int
val memory_element_ty : Type.hol_type
(* Transform word into memory_element_ty *)
val mk_memory_element : Term.term -> Term.term
val sort_addresses : Conv.conv
val sort_registers : Conv.conv
val state_type : Type.hol_type

(* State updates that we should abstract out for performance reasons *)
val abstract_updates : (string * Term.term) list

(* Clean up minor details of step theorems from stepLib *)
val fixup_th : Thm.thm -> Thm.thm

val word_size : int
val word_type : Type.hol_type
val addr_size : int
val addr_type : Type.hol_type

val code_addr_translate : Term.term -> Term.term

val basic_state : Term.term

(* [fill_in_state gen state mem] returns the [state] with the *unspecified*
   parts of memory replaced by [mem] and unspecified registers and flags
   chosen by consulting the random number source, [gen].  Assumes the free
   variables used by [basic_state] are used for unspecified values in [state].
 *)
val fill_in_state : RandGen.gen -> Term.term -> Term.term -> Term.term

(* The available options and functions to set them *)
val additional_options : (string * ((string -> unit) * (unit -> string))) list
(* Called after all options are set to check they make sense *)
val check_options : unit -> unit

(* Takes the theorem from the model, the instruction lengths, the
   counter for the number of memory locations and a function to reduce
   terms *)
val additional_constraints : Thm.thm -> int list -> int -> (Term.term -> Term.term) -> Term.term list

val smt_rewrites : Thm.thm list
val smt_simps : Thm.thm list

(* State field update functions which should be ignored in YicesTest.
   For example, memory statistics in the CHERI state can be ignored
   because they never appear in SMT problems *)
val yices_irrelevant_updates : Term.term list

(* Generate a step theorem for an instruction given in hex *)
val step : string -> Thm.thm list
val step_exn : string -> Thm.thm list

(* May depend upon whether the model's tools support them *)
val disassemble_hex : string -> string
val print_disassembled_hex : string -> unit
val encode : string quotation -> (string * int) list (* hex and instr size *)
val step_code : string quotation -> Thm.thm list
val step_code_exn : string quotation -> Thm.thm list

(* Choose which step theorem to use for each instruction (i.e., pick whether to
   take branches) *)
type compatibility_info
val choose_thms : RandGen.gen -> Thm.thm list list -> compatibility_info list * int list

(* Return which theorems for an instruction are [likely to be] compatible with the context *)
val filter_compatible : compatibility_info -> Thm.thm list -> (int * Thm.thm) list

(* Raised if choose_thms failed to make a choice.  This might not mean that
   choosing step theorems is impossible - it may have backed itself into a
   corner and not have backtracking. *)
exception Bad_choices of int list

end
