signature EvalBasics =
sig

val discard_fns : string list

(* Rewrites intended for use with the simplifier (e.g., for higher-order
   matching) *)
val eval_simp_ths : Thm.thm list

val extra_thms : Thm.thm list
val extra_defs : Thm.thm list

val fetch_tm : Term.term
val get_fetch : (Term.term list -> Term.term -> Thm.thm list) ->
                Term.term list * Term.term * (Term.term -> Thm.thm)

val inventory : {C: string list, N: int list, T: string list, Thy: string}

val junk_conds : Term.term list

val late_rws : Thm.thm list
val late_ss : simpLib.ssfrag

val mem_rws : Thm.thm list

(* Identifying processor exceptions *)
type exception_info
val is_exception : Term.term * Term.term list -> exception_info option

val next_fn : Term.term

val opaque : string list

val state_ty : Type.hol_type

val std_hyp : Term.term list

val term_for_instr : IntInf.int -> Term.term

val thy : string

val encode : string quotation -> (string * int) list

end
