structure CombineSteps :> CombineSteps =
struct

exception Combination_impossible of int

open HolKernel Parse boolLib bossLib

(* Definition for executing n steps.
   Note that we don't want the definition to be unfolded by computeLib, so use
   zDefine. *)

val NStates_def = testingTheory.NStates_def

val one_step =
    Tactical.prove (``(^(TargetBasics.nextstate) s = SOME s') ==> (NStates 1 s = s')``,
                    STRIP_TAC THEN
                    (ASM_REWRITE_TAC [numLib.num_CONV ``1:num``, NStates_def]) THEN
                    (SIMP_TAC std_ss []));

val extend_step =
    UNDISCH_ALL
    (Tactical.prove (``(^(TargetBasics.nextstate) s = SOME s') ==> (NStates n s' = NStates (SUC n) s)``,
                     SIMP_TAC std_ss [NStates_def]));

val s = mk_var ("s", Target.state_type)
val s' = mk_var ("s'", Target.state_type)
val pc_s = Target.pc s

fun note_mem th counter =
    let fun is_mem_access tm =
            if is_comb tm
            then ((term_eq (Target.memory_term s)) o rator) tm
            else false
        val tms = (concl th)::(hyp th)
        val accesses = List.concat (map (find_terms is_mem_access) tms)
    in List.foldl (fn (access,(th,counter)) =>
                      (ADD_ASSUM ``memory_address ^(numSyntax.term_of_int counter) = ^(rand access)`` th,
                       counter+1))
                  (th,counter) accesses
    end

(* Functions for combining step theorems. *)

(* Replace lefthand-side of result with NStates 1 s *)
fun last_step n th = 
    let val (th, mem_counter) = note_mem th 0
    in (th |>
           MATCH_MP one_step |>
           ADD_ASSUM ``instr_start ^(numSyntax.term_of_int n) = ^pc_s``,
        mem_counter)
    end
    handle e => raise wrap_exn "CombineSteps" "last_step" e

(* The only common free variable we want between the step theorems is the
   state, s.  Every other variable should be distinct or we'll overconstrain
   the pre-state. *)
fun freshen_free th th' =
    let val avoid = Thm.thm_frees th'
        val have = Thm.thm_frees th
        fun change v =
            if v = s then NONE else
            SOME {redex = v, residue = Term.prim_variant avoid v}
        val subs = List.mapPartial change have
    in INST subs th
    end

(* Memory updates can lead to a blow-up in the theorem size, so separate each
   one. *)
fun abstract_updates to_abstract tm =
    let fun grab (name,tm') =
            (SOME (name, rand (rand (find_term (fn t => is_comb t andalso same_const (rator t) tm') tm)))) handle HOL_ERR _ => NONE
    in List.mapPartial grab to_abstract
    end

fun abstract_memory_update n thm =
  let val vs = abstract_updates Target.abstract_updates (concl thm)
      fun extract ((name,v),thm) =
          let val var = mk_var (name ^ "_step_" ^ int_to_string n, type_of v)
              val eq_thm =
                  ISPECL [var,v] EQ_SYM |>
                  CONV_RULE (LAND_CONV (ONCE_REWRITE_CONV [GSYM markerTheory.Abbrev_def])) |>
                  UNDISCH 
          in PURE_REWRITE_RULE [eq_thm] thm
          end
  in foldl extract thm vs
  end


fun add_prev_step n new_th (tail_th, mem_counter) =
    (* Note that there is an earlier step *)
    let val new_th = freshen_free new_th tail_th
        val (new_th,mem_counter) = note_mem new_th mem_counter
        val new_th = abstract_memory_update n new_th
        (* Substitute the previous step theorem *)
        val th = tail_th |>
                 INST [s |-> s'] |>
                 (fn x => (SUBST_MATCH extend_step x) handle e => (print_thm x; raise e)) |>
                 RecordUtils.INST_record s' ((optionSyntax.dest_some o rhs o concl) new_th) |>
                 PROVE_HYP new_th |>
                 DISCH_ALL |>
                 (* Simplify things down; note that we don't want to reveal some
                    internal details here (in particular, the definition of Aligned on M0) *)
                 CONV_RULE Tools.cbv_restr_CONV |>
                 (* Treat each hypothesis and the conclusion separately,
                    otherwise unusual hypotheses like ``0w = gpr_step_6 29w``
                    cause troublesome unintentional rewrites. *)
                 UNDISCH_ALL |>
                 Conv.MAP_THM (QCONV (SIMP_CONV (arith_ss++wordsLib.WORD_ss) [combinTheory.UPDATE_EQ])) |>
                 (* Get rid of redundant updates and sort to make updates more readable *)
                 (* abstract_memory_update makes sorting the memory field pointless
                 CONV_RULE (ONCE_DEPTH_CONV Target.sort_addresses) |> *)
                 CONV_RULE (ONCE_DEPTH_CONV Target.sort_registers) |>
                 (* Enable us to add constraints based on instruction locations *)
                 ADD_ASSUM ``instr_start ^(numSyntax.term_of_int n) = ^pc_s``
        (* If the theorems are incompatible, for example because a flag is set
           to the opposite of what a branch requires, then the conclusion will
           have been simplified to true. *)
        val () = if term_eq (concl th) T then raise Combination_impossible n else ()

    in (th,mem_counter)
    end
    handle (e as HOL_ERR _) => raise wrap_exn "CombineSteps" "add_prev_step" e

fun combine_steps' n [] = (TRUTH, 0)
  | combine_steps' n [th] = last_step n th
  | combine_steps' n (th::ths) =
    add_prev_step n th (combine_steps' (n+1) ths)

fun combine_steps ths = combine_steps' 0 (map Target.fixup_th ths)

end

(* Example

val [ex_th1] = m0_stepLib.thumb_step_hex (false,false) "4010";
val [ex_th2] = m0_stepLib.thumb_step_hex (false,false) "7740";
val [ex_th3] = m0_stepLib.thumb_step_hex (false,false) "4013";

(* Longer example, but illustrates that the last step to tidy updates
   isn't general enough. *)
val ex_th = CombineSteps.combine_steps [ex_th1,ex_th2,ex_th3];
*)
