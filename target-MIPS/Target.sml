structure Target : sig
  include Target
  val use_stepLib : bool ref (* default: true *)
end =
struct

local
open HolKernel Parse boolLib bossLib mips_stepTheory

in

(* l3mips / BERI bluespec boot rom
   We leave space for the register initialisation preamble *)
val preamble_size = (* lo, hi *) 2*7*4 + (* gpr *) 31*6*4 + (* jump *) 4*2
val ram_details = [(Arbnum.fromLargeInt (0x9000000040000000+preamble_size),
                    Arbnum.fromLargeInt (2048*4-preamble_size))]

fun pc s = ``^s.PC``
fun memory_term s = ``^s.MEM``
val memory_field_update = ``mips_state_MEM_fupd``
val memory_type = ``:word64 -> word8``
val memory_element_size = 8
val memory_element_ty = ``:word8``
fun mk_memory_element w = w
val sort_addresses = updateLib.SORT_WORD_UPDATES_CONV ``:64``
val sort_registers = updateLib.SORT_WORD_UPDATES_CONV ``:5``
val state_type = ``:mips_state``

val abstract_updates = [
   ("mem", ``mips_state_MEM_fupd``)
]

(* Target specific manipulations to make to step theorems *)
fun fixup_th th =
    th |>
    (* Relatively recent problem with SH/LH; probably ought to be fixed in stepLib *)
    utilsLib.INST_REWRITE_RULE [mips_stepTheory.extract_byte,mips_stepTheory.extract_half,
                                mips_stepTheory.select_half_be]

val word_size = 64
val word_type = ``:word64``
val addr_size = word_size
val addr_type = word_type

fun code_addr_translate tm = tm : Term.term

val big_endian = true

(* TODO: fill in / generalise bits of the state as necessary *)

val basic_cp0 =
  ``<| BadVAddr := ARB;
       Cause := ARB;
       Compare := ARB;
       Config := ARB with <| BE := ^(bitstringSyntax.term_of_bool big_endian) |>;
       Count := ARB;
       Debug := ARB;
       EPC := ARB;
       EntryHi := ARB;
       EntryLo0 := ARB;
       EntryLo1 := ARB;
       ErrCtl := ARB;
       ErrorEPC := ARB;
       Index := ARB;
       LLAddr := ARB;
       PRId := ARB;
       PageMask := ARB;
       Random := ARB;
       Status := ARB with <| RE := F; EXL := F; BEV := T |>;
       Wired := ARB;
       XContext := ARB
    |>``

(* TODO: is it possible for us to set up a branch delay slot without executing
   an instruction?  Even if so, would there be any point? *)
val basic_state =
  ``<| BranchDelay := NONE;
       BranchTo := NONE;
       CP0 := ^basic_cp0;
       LLbit := ARB;  (* TODO: should we specify this? *)
       MEM := m;
       PC := pc;
       exceptionSignalled := F;
       exception := NoException;
       gpr := reg;
       (* We always use concrete values for hi and lo; if we don't then Yices
          produces slightly more complex output (e.g., I've just seen it return
          hi = lo, which we don't currently deal with). *)
       hi := SOME hi;
       lo := SOME lo
    |>``;

fun rand_regs gen =
    (* If we were to assign a non-zero value to register 0 it would be ignored
       anyway *)
    let fun rr 0 = ``0w:word64``
          | rr _ = Tools.random_word gen 64 0
        val vals = List.tabulate (32, rr)
        val hol_vals = listSyntax.mk_list (vals, ``:word64``)
    in ``\r:word5. EL (w2n r) ^hol_vals``
    end

fun fill_in_state gen s mem =
    let val regs = rand_regs gen
        val hi = Tools.random_word gen 64 0
        val lo = Tools.random_word gen 64 0
    in subst [{redex = ``reg:word5 -> word64``, residue = regs},
              {redex = ``m:word64 -> word8``, residue = mem},
              {redex = ``hi:word64``, residue = hi},
              {redex = ``lo:word64``, residue = lo}] s
    end

(* Options *)
val use_stepLib = ref true;

val additional_options =
[Tools.bool_option "use_stepLib" use_stepLib]

fun check_options () = ()

fun additional_constraints (th:Thm.thm) _ _ cbv_eval =
  let val (hy,cn) = dest_thm th
      val final_state = rhs cn
      val final_delay = cbv_eval ``^final_state.BranchDelay``
      val final_pc = cbv_eval ``^final_state.PC``
  in [``^final_delay = NONE``, ``(1 >< 0) ^final_pc = 0w : word64``]
  end

(* Part of address calculation found on MIPS with the symbolic evaluation theorems *)
val addr_calc_rw = Q.prove(
  `(8 * w2n (w:word3)) = w2n (8w : word64 * (w2w w))`,
  wordsLib.n2w_INTRO_TAC 66 THEN
  blastLib.BBLAST_TAC);

val smt_rewrites : Thm.thm list = [mipsTheory.NotWordValue_def,addr_calc_rw]
val smt_simps : Thm.thm list = []

val yices_irrelevant_updates : term list = []

(* Partially apply mips_eval_hex because it does some specialization that
   we don't want to repeat. *)
val step0 = mips_stepLib.mips_eval_hex big_endian
type code = string
fun sc_just [x] = x
  | sc_just _ = failwith "step_code: quote contains multiple strings"
val step_code0 = mips_stepLib.mips_step_code big_endian o sc_just o assemblerLib.quote_to_strings
fun step_exn _ = failwith "processor exceptions not supported"
fun step_code_exn _ = failwith "processor exceptions not supported"

fun split_branch th =
  case List.find (fn tm => is_eq tm andalso lhs tm = ``s.BranchDelay``) (hyp th) of
      NONE => [th]
    | SOME tm =>
      if optionSyntax.is_some (rhs tm)
      then let val branchdelay = optionSyntax.dest_some (rhs tm)
           in if is_var branchdelay
              then [INST [branchdelay |-> ``NONE : word64 option``] th,
                    INST [branchdelay |-> ``SOME (addr : word64)``] th]
              else [th]
           end
      else [th]

fun split_branches ths = List.concat (map split_branch ths)

fun step s =
    if !use_stepLib
    then (split_branches o step0) s
    else InstrBehaviour.next s
fun step_code c =
    if !use_stepLib
    then (split_branches o step_code0) c
    else InstrBehaviour.next_code c

fun hex s = Option.valOf (StringCvt.scanString (Int.scan StringCvt.HEX) s);

(* The disassembler needs the bytes of 32bit instructions to be split up. *)
fun breakup_hex s =
    let val n = hex s
        val b4 = Int.fmt StringCvt.HEX (n mod 256)
        val b3 = Int.fmt StringCvt.HEX (n div 256 mod 256)
        val b2 = Int.fmt StringCvt.HEX (n div 256 div 256 mod 256)
        val b1 = Int.fmt StringCvt.HEX (n div 256 div 256 div 256)
    in (b1, b2, b3, b4)
    end

fun breakup_hex_llvm_mc s =
    let val (b1,b2,b3,b4) = breakup_hex s
    in "0x" ^ b1 ^ " 0x" ^ b2 ^ " 0x" ^ b3 ^ " 0x" ^ b4 ^ "\n"
    end

fun breakup_hex_printf s =
    let val (b1,b2,b3,b4) = breakup_hex s
    in "\\x" ^ b4 ^ "\\x" ^ b3 ^ "\\x" ^ b2 ^ "\\x" ^ b1
    end

fun disassemble_hex s =
    let val i = mips.Decode (Option.valOf (BitsN.fromHexString (s,32)))
    in mips.instructionToString i
    end

fun print_disassembled_hex s =
    print (disassemble_hex s ^ "\n")

val encode = EvalBasics.encode

type compatibility_info = unit
fun filter_compatible _ _ = failwith "filter_compatible not supported on this architecture"

(* Due to branch delay slots, we don't choose whether a branch is taken while
   looking at the branch instruction itself, but on the instruction after the
   branch.  We recognise branches by looking to see if the BranchDelay part of
   the state can be filled. *)

datatype delayslot = NotDS | ContDS | BranchDS

fun what_is_the_delay_slot tm =
    if optionSyntax.is_none tm then NotDS
    else if optionSyntax.is_some tm then
       let val tm' = optionSyntax.dest_some tm
       in if optionSyntax.is_none tm' then ContDS
          else if optionSyntax.is_some tm' then BranchDS
          else Feedback.failwith ("Unexpected delay slot term: " ^ term_to_string tm)
       end
    else Feedback.failwith ("Unexpected delay slot term: " ^ term_to_string tm)

fun thm_branch_delay th =
    (* Get the conclusion's idea of the post-state delay slot, then rewrite
       with the hypotheses because it might be s.DelaySlot *)
    let val concl_ds =
            ((fn t => Tools.cbv_eval ``^t.BranchDelay``) o optionSyntax.dest_some o rhs o concl) th
    in (rhs o concl) (QCONV (REWRITE_CONV (map ASSUME (hyp th))) concl_ds)
    end

fun hyp_branch_delay th =
    (rhs o concl) (REWRITE_CONV (map ASSUME (hyp th)) ``s.BranchDelay``)

fun filter_branch_delay delay_slot ths =
    filter (fn (_,th) => (what_is_the_delay_slot o hyp_branch_delay) th = delay_slot) ths

exception Bad_choices of int list

fun choose_thms_symeval gen thmss =
    let val (end_delay_slot,choices) =
            foldl (fn (thms,(delay_slot,choices)) =>
                      let val thms = mapi (fn i => fn t => (i,t)) thms
                          val thms = filter_branch_delay delay_slot thms
                          val () = case thms of
                                       [] => (print "Bad choice: Delay slot incompatibility.\n";
                                              raise (Bad_choices (rev choices)))
                                     | _ => ()
                          val (_,(choice,th)) = RandGen.select thms gen
                          val next_delay_slot = what_is_the_delay_slot (thm_branch_delay th)
                      in (next_delay_slot,choice::choices)
                      end) (NotDS,[]) thmss
        val () = if end_delay_slot <> NotDS
                 then (print "Bad choice: Delay slot after end of sequence.\n"; raise (Bad_choices (rev choices)))
                 else ()
    in ([], rev choices)
    end

(* stepLib version *)

datatype isslot = NeverSlot | IsSlot | MaybeSlot (* branch likely instructions *) | MustBranch

fun choose_thms_stepLib gen thmss =
    let val (_,choices) =
            foldl (fn (thms,(delay_slot,choices)) =>
                      let val rs = map (rand o rhs o concl) thms
                          val delays = map (fn t => Tools.cbv_eval ``^t.BranchDelay``) rs
                          val delay = hd delays
                          val () = if List.all (curry (op =) delay) (tl delays)
                                   then ()
                                   else failwith "Step theorem with variable delay slot"
                          fun must_branch tm =
                              if optionSyntax.is_some tm
                              then optionSyntax.is_some (optionSyntax.dest_some tm)
                              else false
                          fun branch_likely tm =
                              if is_cond tm
                              then let val (_,l,r) = dest_cond tm
                                   in optionSyntax.is_none l orelse optionSyntax.is_none r
                                   end
                              else false
                          val next_delay_slot =
                              if optionSyntax.is_none delay
                              then NeverSlot
                              else if must_branch delay
                              then MustBranch
                              else if branch_likely delay
                              then MaybeSlot
                              else IsSlot
                          val choice = case delay_slot of
                                          NeverSlot => 0
                                        (* Need to allow for a single theorem when it isn't
                                           a branch delay slot *)
                                        | MaybeSlot => if length thms = 1 then 0
                                                       else snd (RandGen.select [0,2] gen)
                                        | IsSlot => 1 + IntInf.toInt (RandGen.genint gen 1)
                                        (* This will almost certainly fail if there's
                                           only one theorem, but at least gives it a
                                           chance *)
                                        | Always => if length thms > 2 then 2 else 0
                      in (next_delay_slot,choice::choices)
                      end) (NeverSlot,[]) thmss
    in ([], rev choices)
    end

fun choose_thms gen thmss =
    if !use_stepLib
    then choose_thms_stepLib gen thmss
    else choose_thms_symeval gen thmss

end

end

(*
CombineSteps.combine_steps (map (hd o mips_stepLib.mips_eval_hex false) ["00000000", "21101234", "AF011234", "12345678"]);

*)
