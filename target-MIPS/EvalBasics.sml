structure EvalBasics : EvalBasics =
struct

local
open Drule HolKernel boolLib bossLib
in

val inventory = mipsTheory.inventory
val thy = "mips"
val extra_thms = [
   mips_stepTheory.NextStateMIPS_def
]
val extra_defs = []
val state_ty = ``:mips_state``

val discard_fns = ["raise'exception", "SignalException"]
val opaque = ["NotWordValue"]@discard_fns

(* No support for exceptions.  (I've only done this for the CHERI model for
   the time being.) *)
type exception_info = unit
fun is_exception _ = NONE

local
   open mips_stepTheory wordsTheory
   val low_bit_correct = wordsLib.WORD_DECIDE ``¬(a ⊕ 7w : word3) = a``;

   (* select_*_be adapted from mips_stepTheory, which ends up producing more
      specialised versions.  We can't use REWRITE_CONV for these because we
      need to fill in the free variable in the hypothesis, but to use
      utilsLib.INST_REWRITE_CONV we need to fill in s.MEM, because it won't
      match a free variable there. *)
   val _ = List.app (fn f => f ())
                    [numLib.prefer_num, wordsLib.prefer_word, wordsLib.guess_lengths]

   open lcsymtacs
   infix \\
   val op \\ = op Tactical.THEN;

   val tac =
       wordsLib.Cases_word_value
       \\ simp []
       \\ strip_tac
       \\ blastLib.BBLAST_TAC

in

val select_byte_be = Q.prove(
   `!b:word3 a:word64.
      (7 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) = s.MEM (a + w2w (b ?? 7w))`,
   tac
   )

val select_half_be = Q.prove(
   `!b:word3 a:word64.
     ~word_bit 0 b ==>
     ((15 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 6w)) @@ s.MEM (a + w2w (b ?? 6w) + 1w)) : word16)`,
   tac
   )
val select_half_be = UNDISCH (SPEC_ALL select_half_be)

val select_word_be = Q.prove(
   `!b:word3 a:word64.
     ((1 >< 0) b = 0w:word2) ==>
     ((31 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 4w)) @@ s.MEM (a + w2w (b ?? 4w) + 1w) @@
      s.MEM (a + w2w (b ?? 4w) + 2w) @@ s.MEM (a + w2w (b ?? 4w) + 3w)) : word32)`,
   tac
   )
val select_word_be = UNDISCH (SPEC_ALL select_word_be)

val fix_addr = Q.prove(`(a : word64 && ¬7w) + w2w (((2 >< 0) a):word3) = a`,
                       blastLib.BBLAST_TAC)

val mem_rws =
    [select_byte_be, select_half_be, select_word_be, select_pc_be,
     computeLib.EVAL_RULE byte_address, address_align2,
     StoreMemory_byte, StoreMemory_doubleword, StoreMemory_half, StoreMemory_word,
     WORD_XOR_ASSOC, WORD_XOR_CLAUSES,
     low_bit_correct, fix_addr]
end

val eval_simp_ths = []

val late_rws = []
val late_ss = simpLib.rewrites []

val next_fn = ``NextStateMIPS``

val std_hyp =
    [``s.CP0.Config.BE``, ``~s.CP0.Status.RE``,
     ``~s.exceptionSignalled``, (* No MIPS exception *)
     ``s.exception = NoException``, (* No L3 exception *)
     ``s.BranchTo = NONE``]

val junk_conds = []

val fetch_tm = ``Fetch``

fun get_fetch eval =
    let val fetch =
            case eval std_hyp ``Fetch s`` of
                [th] => th
              | _ => failwith "'Fetch s' doesn't evaluate to 1 term"
        val fetched = (optionSyntax.dest_some o fst o pairSyntax.dest_pair o rhs o concl) fetch
        fun fix_instr v =
            PURE_REWRITE_RULE [ASSUME (mk_eq (fetched, v))] fetch
    in (hyp fetch, fetched, fix_instr)
    end

fun term_for_instr v = wordsSyntax.mk_wordi (Arbnum.fromLargeInt v,32)

fun encode q =
    let val lines = assemblerLib.quote_to_strings q
        fun line s =
            let val s = mips.stripSpaces s
            in if s = "" then NONE else SOME s
            end
        val instrs = List.mapPartial line lines
    in map (fn s => (mips.encodeInstruction s, 4)) instrs
    end

end
end
