(* A script to find everything that could end up in an SMT problem that the
   Yices translation doesn't support. *)

fun field_updates ty =
  let val ty_name = fst (dest_type ty)
      val fields = TypeBase.fields_of ty
      fun check_sub ty =
          if TypeBase.is_record_type ty then field_updates ty else
          let val ty' = try SOME (snd (dom_rng ty)) handle _ => NONE
          in case ty' of NONE => [] | SOME ty' => check_sub ty'
          end
      val sub = map (fn (_,ty) => check_sub ty) fields
      val ups = map (fn (field,_) => ty_name ^ "_" ^ field ^ "_fupd") fields
  in List.concat (ups::sub)
  end

(* Constants that will appear in the goals we generate here, but not (hopefully) in
   preconditions.  Some record updates can be handled by the Yices translation, but
   there's something about the structure of the ones here that it doesn't like;
   they're also unlikely to appear in preconditions, however.  *)
val ignorable = ["random_update", "user_tlb_entry", (* CHERI *)
                 "UPDATE","K","word_to_hex_string",fst (dest_const TargetBasics.nextstate)] @
                (field_updates Target.state_type);
val ignorable_terms = [``STRLEN``,``GENLIST : (num -> char) -> num -> string``,
                       ``num_to_dec_string``, ``STRCAT``, ``CHR``]
val uninteresting_updates = Target.yices_irrelevant_updates


val gen = RandGen.newgen ();

val instrs = Generate.sample_instrs gen;

(* Alternative:

fun rand_hex gen n =
    let val digits = [#"0", #"1", #"2", #"3", #"4", #"5", #"6", #"7", #"8", #"9",
                      #"a", #"b", #"c", #"d", #"e", #"f"]
      val ds = List.tabulate (n,fn _ => snd (RandGen.select digits gen))
  in String.implode ds
  end

(* Use 4 for thumb4 *)
val instrs = List.tabulate (1000,fn _ => (rand_hex gen 8, ""));
*)


fun replace_arb tm =
    let val arbs = find_terms is_arb tm
        val tys = map type_of arbs
        val tys = HOLset.listItems (HOLset.fromList Type.compare tys)
        val sub = map (fn ty => mk_arb ty |-> genvar ty) tys
    in subst sub tm
    end

fun replace_uninteresting_updates tm =
  let fun match tm = is_comb tm andalso List.exists (same_const (rator tm)) uninteresting_updates
      val upds = find_terms match tm
      val sub = map (fn tm => tm |-> mk_comb (rator tm, genvar (type_of (rand tm)))) upds
  in subst sub tm
  end

fun find_uninterp (th,name) =
    let val () = print (fst name ^ " " ^ snd name ^ "\n")
        val (hy,c) = dest_thm th
        val goal = (map replace_arb hy, replace_uninteresting_updates (replace_arb c))
        val (_,_,_,uninterp) = YicesSat.goal_to_Yices goal
                               handle e =>  (Redblackmap.mkDict Term.compare,
                                             Redblackmap.mkDict Type.compare,
                                             [],[stringSyntax.fromMLstring (exn_to_string e)])
        val uninterp = List.filter (fn tm => not (is_const tm andalso
                          mem ((fst o dest_const) tm) ignorable)) uninterp
        val uninterp = List.filter (fn tm => not
                          (List.exists (term_eq tm) ignorable_terms)) uninterp
    in (uninterp,name)
    end;

val ths = List.concat (map (fn (h,n) =>
                               (print (h ^ " " ^ n ^ "\n");
                                map (fn th => (th,(h,n)))
                                   ((Target.step h)
                                    handle e =>
                                           (print (exn_to_string e ^ "\n");[]))))
                           instrs);

val ths = map (fn (th,n) =>
                  (print (fst n ^ " " ^ snd n ^ "\n");
                   (StepSMT.trans_thm_for_test
                       (Target.fixup_th
                           (StepSMT.early_rewrites th)), n))) ths;

val uninterps = trace ("HolSmtLib",0) (map find_uninterp) ths;

val bad = List.filter (fn (l,_) => l <> []) uninterps;

(* Some useful bits and pieces *)

map (fn (a,(h,n)) => (a,h,n,Target.step h)) bad;

(* Check out ror definition in YicesSat works for at least one non-trivial example *)

YicesSat.Yices_sat ([], ``n > 5 /\ n < 32 /\ (w #>> n = (14w:word32))``);
YicesSat.Yices_sat ([], ``n > 5 /\ (w ' 0) /\ (w #>> n = (14w:word32))``);
YicesSat.Yices_sat ([], ``n > 45 /\ (w ' 0) /\ (w #>> n = (14w:word32))``);
