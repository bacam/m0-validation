structure State :> State=
struct

open HolKernel Parse boolLib
open RandGen

val background_memory = mk_var ("background_memory", Target.memory_type)

(* "Element" here means the basic unit of memory in the model, i.e., byte, word
   or double word. *)

fun random_element gen =
    let val l = RandGen.bits gen Target.memory_element_size
    in bitstringSyntax.bitlist_to_num l
    end

fun random_hol_element gen =
    let val el = random_element gen
        val w = wordsSyntax.mk_wordi (el, Target.memory_element_size)
    in Target.mk_memory_element w
    end

fun fill_memory gen ram_details =
    let val i = mk_var ("i", wordsSyntax.mk_int_word_type Target.addr_size)
        fun mk (start,len) =
            let val elements = List.tabulate (Arbnum.toInt len, fn _ => random_hol_element gen)
                val hol_elements = listSyntax.mk_list (elements, Target.memory_element_ty)
                val start = wordsSyntax.mk_wordi (start,Target.addr_size)
                val len = wordsSyntax.mk_wordi (len,Target.addr_size)
            in (``^i >= ^start /\ ^i < ^start + ^len``, ``EL (w2n (^i - ^start)) ^hol_elements``)
            end
        val areas = map mk ram_details
        val tm = foldl (fn ((cond,vls),tm) => boolSyntax.mk_cond (cond,vls,tm))
                       ``^(background_memory) ^i``
                       areas
    in mk_abs (i, tm)
    end

fun fill_memory_partial_hol gen ram_details locations =
    let fun mk (start,len) =
            let val elements = Vector.tabulate (Arbnum.toInt len, fn _ => random_element gen)
                val w_elements = Vector.map (fn i => Target.mk_memory_element 
                                                        (wordsSyntax.mk_wordi
                                                            (i, Target.memory_element_size))) elements
            in ((start,w_elements),len)
            end
        val areas = map mk ram_details
        open Arbnum
        fun find addr =
            case List.find (fn ((start,_),len) => addr >= start andalso addr - start < len) areas of
                NONE => failwith "Locations set outside of RAM areas"
              | SOME ((start,w_elements),_) => Vector.sub (w_elements, Arbnum.toInt (addr-start))
        val hol_elements =
            foldl (fn (i,m) => ``((^(wordsSyntax.mk_wordi (i, Target.addr_size)) =+
                                   ^(find i)) ^m)``)
                  background_memory locations
    in (map fst areas,hol_elements)
    end

fun fill_some_memory gen locations =
    foldl (fn (i,m) => ``((^(wordsSyntax.mk_wordi (i, Target.addr_size)) =+ ^(random_hol_element gen)) ^m)``)
          background_memory locations

fun get_tm S v = Option.valOf (subst_assoc ((equal v) o fst o dest_var) S)

fun insert_update (start,content) =
    let open Arbnum
        val len = fromInt (Vector.length content)
        fun aux [] = [(start,content)]
          | aux (m as ((start',content')::t)) =
            let val len' = fromInt (Vector.length content') in
            (* Non-interfering *)
            if start + len < start' then (start,content)::m else
            if start > start' + len' then (start',content')::(aux t) else
            (* Completely overrides *)
            if start <= start' andalso start + len > start' + len' then aux t else
            (* Restart with merged block (start > start' by above) *)
            if start + len > start' + len' then
                insert_update (start',
                               VectorSlice.concat [VectorSlice.slice (content', 0, SOME (toInt (start-start'))),
                                                   VectorSlice.full content]) t
            (* Collides with front of block *)
            else if start <= start' then
               (start,
                VectorSlice.concat [VectorSlice.full content,
                                    VectorSlice.slice (content', toInt (len-(start'-start)), NONE)])::t
            (* Embedded within (start > start' by above) *)
            else (start',
                  VectorSlice.concat [VectorSlice.slice (content', 0, SOME (toInt (start-start'))),
                   VectorSlice.full content,
                   VectorSlice.slice (content', toInt (start-start'+len), NONE)])::t
            end
    in aux
    end

(*

fun fl l = Vector.fromList l

val test = [(0,fl [0,1,2,3]), (6,fl [6,7]), (10,fl [10,11,12,13,14]), (20,fl [20]), (23,fl [23])];
insert_update (8,fl [8]) test;
insert_update (9,fl [9]) test;
insert_update (8,fl [8,9]) test;
insert_update (5,fl [5,6,7,8]) test;
insert_update (5,fl [5,6]) test;
insert_update (5,fl [5,6,7,8,9]) test;
insert_update (5,fl [5,6,7,8,99,1010]) test;
insert_update (7,fl [7,8]) test;
insert_update (7,fl [7,8,9]) test;
insert_update (7,fl [7,8,99,1010]) test;
insert_update (19,fl [19,20,21]) test;
*)

fun decompose_hol_memory m bkgd =
    case Lib.total (match_term ``((addr:^(ty_antiq Target.addr_type) =+ (value:^(ty_antiq Target.memory_element_ty))) memory)``) m of
      SOME (S,T) =>
      let val addr = get_tm S "addr"
          val addr = wordsSyntax.dest_word_literal addr
          val value = get_tm S "value"
          val m' = get_tm S "memory"
          val m' = decompose_hol_memory m' bkgd
      in insert_update (addr,Vector.fromList [value]) m'
      end
    | NONE =>
      let fun ite (i,m) =
              (* The term may have been reduced, so this isn't exactly the same as the
                 term that produced it. *)
              case Lib.total (match_term
                                 ``if ^i >= start /\ ^i < end
                                   then EL (w2n (^i + start')) content
                                   else memory : ^(ty_antiq Target.memory_element_ty)``) m of
                  SOME (S,T) =>
                  let val start = get_tm S "start"
                      val start = wordsSyntax.dest_word_literal start
                      val content = get_tm S "content"
                      val content = (fst o listSyntax.dest_list) content
                      val m' = get_tm S "memory"
                      val m' = ite (i,m')
                  in insert_update (start, Vector.fromList content) m'
                  end
                | NONE =>
                  if m = mk_comb (background_memory,i) then bkgd
                  else failwith ("Unparsable memory: " ^ term_to_string m)
      in if is_abs m
         then ite (dest_abs m)
         else if m = background_memory
         then bkgd
         else failwith ("Unparsable memory: " ^ term_to_string m)
      end

(* TODO: this shouldn't need to keep track of seen memory any more *)
fun fold_memory memory bkgd f x =
    let open Arbnum
        val memory = decompose_hol_memory memory bkgd
        fun aux ((start,content),(seen,x)) =
            let val x' =
                    Vector.foldli (fn (off,b,x) =>
                                     let val addr = start+fromInt off
                                     in if seen addr then x else f addr b x
                                     end) x content
            in (fn addr => if addr >= start andalso addr < start+fromInt (Vector.length content)
                     then true else seen addr, x')
            end
        val (_,r) = List.foldl aux (fn _ => false, x) memory
    in r
    end handle (e as HOL_ERR _) => raise (wrap_exn "State" "fold_memory" e)
end
