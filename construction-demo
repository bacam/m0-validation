#!/bin/sh

usage() {
  cat << EOF
Usage: construction-demo [-e <exns>] [-h <harness>] <instructions>

  -e <exns>      Comma separated list of instructions that should raise a
                 processor exception
  -h <harness>   Set test harness (you probably don't want to change it)
  -o <options>   Target-specific options
  <instructions> Number of instructions in test

-o options:

EOF
  cat options.txt
  echo
  exit 1

}

exceptions=""
harness="Basic Breakpoint"
options=""

while getopts e:h:o: name
do
  case $name in
  e)  exceptions="$OPTARG";;
  h)  harness="$OPTARG";;
  o)  options="$OPTARG";;
  ?)  usage;;
  esac
done

shift $(($OPTIND -1))

if [ $# -ne 1 ]; then
  usage
fi

hol << EOF
load "Test";
load "HWTest";
open Harness;

val () =
  Test.set_options "$options"
  handle e =>
    (print (General.exnMessage e ^ "\n");
     OS.Process.exit OS.Process.failure)

(* Work around https://github.com/HOL-Theorem-Prover/HOL/issues/267 *)
val () = computeLib.set_skip computeLib.the_compset combinSyntax.K_tm NONE;

(* Make displaying states and theorems that contain 8kB of memory bearable. *)
val () = max_print_depth := 200;

(* Example to generate a test case (without the full memory), but don't run it *)
val (_, instructions, [(prestate, theorem, instruction_locations, read_footprint, write_footprint)]) =
  Test.construct_test NONE (Test.Generate_exns ($1,[$exceptions])) NONE ($harness) [];

EOF
