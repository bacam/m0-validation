open HolKernel Parse boolLib bossLib lcsymtacs

val _ = new_theory "wordsExtra";

(* Counterexample for dimindex (:'b) <= dimindex (:'a + 'b) *)

val dimindex_le_counterexample = prove (
  ``dimindex (:2) > dimindex (:num + 2)``,
  simp[wordsTheory.dimindex_2]
  >> simp[fcpTheory.dimindex_def,fcpTheory.finite_sum])

val dim_sum_le = store_thm (
  "dim_sum_le",
  ``FINITE univ(:'a) /\ FINITE univ(:'b) ==> dimindex (:'b) <= dimindex (:'a + 'b)``,
  asm_simp_tac arith_ss [fcpTheory.index_sum])

val LE_MIN = prove (``x <= y ==> (MIN x y = x) /\ (MIN y x = x)``, simp[arithmeticTheory.MIN_DEF])

val word_concat_fcp = store_thm (
  "word_concat_fcp",
  ``!x : 'a word. !y : 'b word.
    FINITE univ(:'a) /\ FINITE univ(:'b) ==>
    ((x @@ y) : 'c word =
      FCP i. if i < dimindex (:'b)
             then y ' i
             else if i < dimindex (:'a) + dimindex (:'b)
             then x ' (i-dimindex (:'b))
             else F)``,

  rpt strip_tac
  >> simp_tac (arith_ss++wordsLib.WORD_EXTRACT_ss) []

  >> `dimindex (:'a + 'b) = dimindex (:'a) + dimindex (:'b)` by
     asm_simp_tac arith_ss [fcpTheory.index_sum]
  >> asm_simp_tac arith_ss [LE_MIN]

  >> simp [wordsTheory.word_or_def]
  >> rewrite_tac [fcpTheory.CART_EQ]
  >> asm_simp_tac arith_ss []
  >> rpt strip_tac
  >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp]

  >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [wordsTheory.word_lsl_def,wordsTheory.EXTRACT_ALL_BITS,fcpTheory.index_comp,wordsTheory.w2w]

  >> Cases_on `i < dimindex (:'b)`
  >| [asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp],
      Cases_on `i < dimindex (:'a) + dimindex (:'b)`
      >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp,wordsTheory.w2w]
     ])

val word_concat_msb = store_thm (
  "word_concat_msb",
  ``!x : 'a word. !y : 'b word.
    FINITE univ(:'a) /\ FINITE univ(:'b) ==>
    (dimindex (:'a) + dimindex (:'b) = dimindex (:'c)) ==>
    (word_msb ((x@@y):'c word) = word_msb x)``,

  rpt strip_tac
  >> asm_simp_tac arith_ss [wordsTheory.word_msb_def,word_concat_fcp]
  >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp,fcpTheory.DIMINDEX_GE_1]
  >> `dimindex (:'c) - (dimindex (:'b) + 1) = dimindex (:'a) - 1` by 
       asm_simp_tac arith_ss [fcpTheory.DIMINDEX_GE_1]
  >> asm_simp_tac arith_ss [])


val sw2sw_fcp = GEN ``x:'a word`` (GSYM (ISPEC ``(sw2sw (x : 'a word)) : 'c word`` fcpTheory.FCP_ETA))

val word_concat_asr = store_thm (
  "word_concat_asr",
  ``!x: 'a word. !y: 'b word. 
    FINITE univ(:'a) /\ FINITE univ(:'b) ==>
    (dimindex (:'c) = dimindex (:'a) + dimindex (:'b)) ==>
    (n = m + dimindex(:'b)) ==>
    ((x @@ y) : 'c word >> n = sw2sw (x >> m))``,

  rpt strip_tac
  >> once_rewrite_tac [sw2sw_fcp]
  >> asm_simp_tac std_ss [wordsTheory.word_asr_def]
  >> rewrite_tac [fcpTheory.CART_EQ]
  >> rpt strip_tac
  >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp,wordsTheory.sw2sw]
  
  >> `dimindex (:'a) + dimindex (:'b) <= i + (m + dimindex (:'b)) =
      dimindex (:'a) <= i + m` by asm_simp_tac arith_ss []
  
  >> asm_simp_tac arith_ss []
  
  >> COND_CASES_TAC
  
  >| [
    asm_simp_tac arith_ss [word_concat_msb]
    >> COND_CASES_TAC
    >> asm_simp_tac arith_ss []
    >> CONV_TAC (RHS_CONV (ONCE_REWRITE_CONV [wordsTheory.word_msb_def]))
    >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp,fcpTheory.DIMINDEX_GE_1]
    
    >> COND_CASES_TAC
    >> asm_simp_tac arith_ss []
    
    >> `m = 0` by asm_simp_tac arith_ss []
    >> asm_simp_tac arith_ss [wordsTheory.word_msb_def],
    
    asm_simp_tac (arith_ss++fcpLib.FCP_ss) [word_concat_fcp,fcpTheory.index_comp]
  ])

val extract_sw2sw = store_thm (
  "extract_sw2sw",
  ``dimindex(:'a) <= dimindex(:'b) ==>
    m < dimindex(:'a) ==>
    (((m >< 0) ((sw2sw (x : 'a word)) : 'b word)) : 'c word = (m >< 0) x)``,

  rpt strip_tac
  >> simp[wordsTheory.word_extract_def,wordsTheory.word_bits_def]
  >> rewrite_tac [fcpTheory.CART_EQ]
  >> rpt strip_tac
  >> simp[wordsTheory.w2w]
  >> Cases_on `i < dimindex(:'a)`
  >| [
    asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp,wordsTheory.sw2sw],
    Cases_on `i < dimindex(:'b)`
    >| [
      asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp,wordsTheory.sw2sw],
      asm_simp_tac arith_ss []
    ]
  ])

val () = wordsLib.guess_lengths ()


val word_lsl_concat = store_thm (
  "word_lsl_concat",
  ``!x : 'a word.
    FINITE univ(:'b) /\ FINITE univ(:'c) ==>
    (dimindex (:'a) = dimindex (:'b) + dimindex (:'c)) ==>
    (dimindex (:'c) = n) ==>
    ((x : 'a word) << n = (w2w x) : 'b word @@ (0w : 'c word))``,

  rpt strip_tac
  >> simp[wordsTheory.word_lsl_def,word_concat_fcp]
  >> rewrite_tac [fcpTheory.CART_EQ]
  >> rpt strip_tac
  >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp]
  >> Cases_on `i<dimindex(:'c)`
  >| [
     asm_simp_tac arith_ss [wordsTheory.word_0],
     asm_simp_tac arith_ss [wordsTheory.w2w]
  ])

val word_concat_or = store_thm (
  "word_concat_or",
  ``!x1 y1 : 'a word. !x2 y2 : 'b word.
    (x1 @@ x2) || (y1 @@ y2) = (x1 || y1) @@ (x2 || y2)``,
  wordsLib.WORD_DECIDE_TAC)

val word_concat_and = store_thm (
  "word_concat_and",
  ``!x1 y1 : 'a word. !x2 y2 : 'b word.
    FINITE univ(:'a) /\ FINITE univ(:'b) /\ (dimindex (:'c) = dimindex (:'a) + dimindex(:'b)) ==>
    ((x1 @@ x2) && (y1 @@ y2) : 'c word = (x1 && y1) @@ (x2 && y2))``,

  rpt strip_tac
  >> simp[word_concat_fcp,wordsTheory.word_and_def]
  
  >> rewrite_tac [fcpTheory.CART_EQ]
  >> rpt strip_tac
  >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp]
  
  >> `dimindex (:'a) + dimindex (:'b) = dimindex (:'a + 'b)`
     by asm_simp_tac arith_ss [fcpTheory.index_sum]
  >> `i - dimindex (:'b) < dimindex (:'a)`
     by asm_simp_tac (arith_ss) [fcpTheory.DIMINDEX_GE_1,arithmeticTheory.LESS_EQ]
  
  >> COND_CASES_TAC
  >> asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.index_comp])

val WORD_w2w_EXTRACT' = Q.store_thm(
  "WORD_w2w_EXTRACT'",
  `!w:'a word. (w2w w):'b word = (dimindex(:'b) - 1 >< 0) w`,
  SRW_TAC [fcpLib.FCP_ss] [wordsTheory.word_bits_def,wordsTheory.word_extract_def, wordsTheory.w2w]
    \\ Cases_on `i < dimindex (:'a)`
    \\ SRW_TAC [fcpLib.FCP_ss, ARITH_ss] [])

val word_1comp_concat = store_thm (
  "word_1comp_concat",
  ``!x y.
    FINITE univ(:'a) /\ FINITE univ(:'b) ==>
    (dimindex (:'c) = dimindex (:'a) + dimindex (:'b)) ==>
    (~(x : 'a word @@ y : 'b word) : 'c word = ~x @@ ~y)``,

  rpt strip_tac
  >> asm_simp_tac (std_ss++fcpLib.FCP_ss) [wordsTheory.word_1comp_def,word_concat_fcp]
  >> rpt strip_tac
  >> COND_CASES_TAC >> simp[]
  >> `i - dimindex(:'b) < dimindex(:'a)` by asm_simp_tac arith_ss []
  >> asm_simp_tac (std_ss++fcpLib.FCP_ss) [fcpTheory.FCP_BETA])

val word_extract_lsr = store_thm (
  "word_extract_lsr",
  ``!n m x. (n >< 0) (x : 'a word >>> m) : 'b word = (n+m >< m) x``,

  rpt strip_tac
  >> srw_tac [fcpLib.FCP_ss] [wordsTheory.word_lsr_def,wordsTheory.word_extract_def,fcpTheory.CART_EQ,wordsTheory.w2w,wordsTheory.word_bits_def]
  >>Cases_on `i < dimindex (:'a)`
  >> srw_tac [fcpLib.FCP_ss] [fcpTheory.DIMINDEX_GE_1]
  >> `i <= dimindex (:'a) -1` by asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.DIMINDEX_GE_1]
  >> `i + m <= dimindex (:'a) -1 = i + m < dimindex(:'a)` by asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.DIMINDEX_GE_1]
  >> asm_simp_tac std_ss [boolTheory.CONJ_ASSOC])


val word_extract_concat_join = store_thm(
  "word_extract_concat_join",
  ``!n x. FINITE univ(:'a) /\ FINITE univ(:'b) ==>
    ((((n + dimindex (:'a) + dimindex (:'b) - 1 >< n + dimindex (:'b)) x) : 'a word @@ ((n + dimindex(:'b) -1 >< n) x) : 'b word) : 'c word = (n + dimindex (:'b) + dimindex (:'a) -1 >< n) x)``,

  rpt strip_tac
  >> srw_tac [fcpLib.FCP_ss] [word_concat_fcp,wordsTheory.word_extract_def,fcpTheory.CART_EQ,wordsTheory.w2w,wordsTheory.word_bits_def]
  >> COND_CASES_TAC
  >| [
    Cases_on `i < dimindex (:'d)` >> srw_tac [fcpLib.FCP_ss] [fcpTheory.CART_EQ]
    >> `i + n <= n + dimindex(:'b) -1` by asm_simp_tac arith_ss [fcpTheory.DIMINDEX_GE_1]
    (* avoid loop in simplifier *)
    >> SPEC_TAC (``dimindex (:'a)``, ``a:num``) >> strip_tac
    >> `i + n <= n + dimindex(:'b) + a -1` by asm_simp_tac arith_ss [fcpTheory.DIMINDEX_GE_1]
    >> asm_simp_tac arith_ss []
  ,
  (* again *)
    UNDISCH_TAC ``~(i < dimindex (:'b))`` >> SPEC_TAC (``dimindex (:'b)``, ``b:num``) >> rpt strip_tac
    >> Cases_on `i < dimindex (:'a) + b`
    >| [
      asm_simp_tac std_ss []
      >> `i - b < dimindex (:'a)` by asm_simp_tac (arith_ss++fcpLib.FCP_ss) [fcpTheory.DIMINDEX_GE_1]
      >> `0 < dimindex (:'d)` by simp_tac arith_ss [INST_TYPE [alpha |-> delta] fcpTheory.DIMINDEX_GE_1]
      >> asm_simp_tac arith_ss [wordsTheory.w2w]
  
      >> Cases_on `i < dimindex (:'d)`
      >| [
        `i < b + dimindex (:'d)` by asm_simp_tac arith_ss []
        >> srw_tac [fcpLib.FCP_ss] [fcpTheory.CART_EQ]
        >> `i - b + (n + b) = i + n` by asm_simp_tac arith_ss []
        >> asm_simp_tac arith_ss []
        ,
        Cases_on `i < b + dimindex (:'d)`
        >| [
           asm_simp_tac std_ss []
           >> srw_tac [fcpLib.FCP_ss] [fcpTheory.CART_EQ]
           >> `i - b + (n + b) = i + n` by asm_simp_tac arith_ss []
           >> asm_simp_tac arith_ss []
           ,
           asm_simp_tac std_ss []
        ]
      ]
    ,
      Cases_on `i < dimindex (:'d)`
      >| [
         asm_simp_tac std_ss []
         >> srw_tac [fcpLib.FCP_ss] [fcpTheory.CART_EQ]
         >> `~(i <= dimindex (:'a) + b -1)` by asm_simp_tac arith_ss [fcpTheory.DIMINDEX_GE_1]
         >> `~(i + n <= n + b + dimindex (:'a) -1)` by asm_simp_tac arith_ss []
         >> asm_simp_tac std_ss []
       ,
         asm_simp_tac arith_ss []
      ]
    ]
  ])

val word_extract_all = store_thm (
  "word_extract_all",
  ``!w : 'a word.
    (dimindex (:'a) - 1 >< 0) w = w``,
  rw[GSYM wordsTheory.WORD_w2w_EXTRACT])

val _ = export_theory ();
