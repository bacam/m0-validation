structure Dedecoder =
struct

(* Construct a word of the form v2w [v1,v2,...] *)
fun mk_word n =
  let fun bit i = mk_var ("b" ^ Int.toString i, ``:bool``)
      val l = List.tabulate (n,bit)
      val l = listSyntax.mk_list (l,``:bool``)
      val nt = fcpSyntax.mk_int_numeric_type n
  in bitstringSyntax.mk_v2w (l,  nt)
  end

(* Match a tuple pattern from a plet with a tuple of arguments *)
fun match pat tm =
  if pairSyntax.is_pair pat 
  then if pairSyntax.is_pair tm then
          let val (pat1,pat2) = pairSyntax.dest_pair pat
              val (tm1, tm2) = pairSyntax.dest_pair tm
          in case (match pat1 tm1, match pat2 tm2) of
                 (SOME r1, SOME r2) => SOME (r1@r2)
               | _ => NONE
          end
       else NONE
  else SOME [pat |-> tm]

fun exec (model_inv : utilsLib.inventory) hy tm =
  let val model_thy = #Thy model_inv
      fun find_boolify n = DB.fetch model_thy ("boolify" ^ int_to_string n ^ "_v2w")
      val boolifies = map find_boolify (#N model_inv)
      fun simp hy tm =
        rhs (concl (QCONV (SIMP_CONV (bool_ss++wordsLib.WORD_ss) (hy@boolifies)) tm))
      fun exec hy tm =
    let fun simpexec hy' tm' =
          (* Convert constraints like v2w [...] = 1w into bit constraints *)
          let val hy' = rhs (concl (QCONV (DEPTH_CONV bitstringLib.v2w_eq_CONV) hy'))
              val hy = ASSUME hy' :: hy
          in exec hy (simp hy tm')
          end
    in if is_cond tm then
          let val (guard,t,f) = dest_cond tm
          in (simpexec guard t)@(simpexec (mk_neg guard) f)
          end
       else if pairSyntax.is_plet tm then
          let val (pat,tm',tm'') = pairSyntax.dest_plet tm
              val r' = exec hy tm'
              fun cont (hy',tm') =
                case match pat tm' of
                    SOME subs => exec hy' (subst subs tm'')
                  | NONE => failwith ("Unable to break up term " ^ term_to_string tm')
          in List.concat (map cont r')
          end
       (* The only case statement I've seen so far is for tuples, but this should
        work more generally, although it doesn't do case splits *)
       else if TypeBase.is_case tm then
          let val (_,tm1,_) = TypeBase.dest_case tm
              val rw = TypeBase.case_def_of (type_of tm1)
              val tm' = rhs (concl (ONCE_REWRITE_CONV [rw] tm))
          in exec hy (simp hy tm')
          end handle UNCHANGED => failwith ("Case term does not evaluate away: " ^ term_to_string tm)
       (* Must come after anything that is encoded as an application, e.g., let *)
       (* We assume here that the function is either a constant or a constructor,
          but do execute all of the arguments *)
       else if is_comb tm then
          let val (f,args) = strip_comb tm
              val args' = exec_args hy args
              val {Name, Thy, Ty} = dest_thy_const f
              fun cont_rw th (hy',args') =
                let val tm' = rhs (concl (ONCE_REWRITE_CONV [th] tm))
                in exec hy (simp hy tm')
                end
              fun done (hy',args') = (hy',list_mk_comb (f,args'))
          in if Thy = model_thy andalso not (TypeBase.is_constructor f) then
                let val th = DB.fetch Thy (Name ^
                                           (if String.isPrefix "boolify" Name then "_v2w" else "_def"))
                in List.concat (map (cont_rw th) args')
                end
             else map done args'
          end
       else [(hy,tm)]
    end
      and exec_args hy [] = [(hy,[])]
        | exec_args hy (h::t) =
          let val r = exec hy h
              fun cont (hy',tm') =
                let val rs = exec_args hy' t
                in map (fn (hy'',tl'') => (hy'',tm'::tl'')) rs
                end
          in List.concat (map cont r)
          end
  in exec hy tm
  end

fun exn_partition f [] = ([], [])
  | exn_partition f (h::t) =
    let val (l,r) = exn_partition f t
    in let val x = f h in (x::l,r) end handle _ => (l,h::r)
    end

fun compute_bdd_encodings model_inv tm =
  let val l = exec model_inv [] tm
      fun to_bdd (hy,instr) =
        let val bdd = SimpleBDD.term_to_bdd (list_mk_conj (map concl hy))
        in (bdd, instr)
        end
      val (good,bad) = exn_partition to_bdd l
      val () = if List.null bad then () else
               Feedback.HOL_WARNING "Dedecoder" "compute_bdd_encodings"
                 ("Couldn't handle constraints on some instructions, e.g.:" ^
                  term_to_string (snd (hd bad)))
  in good
  end

(* Produce a single term describing the decodable instructions *)
fun compute_all_encodings_term model_inv tm ignore =
  let val l = exec model_inv [] tm
      val l = List.filter (fn (_,instr) => not (exists (term_eq instr) ignore)) l
      fun to_term (hy,instr) = list_mk_conj (map concl hy)
      val terms = List.map to_term l
   in list_mk_disj terms
  end

fun compute_dnf_encodings model_inv tm =
  let val bdd_encs = compute_bdd_encodings model_inv tm
  in map (fn (bdd,instr) => (SimpleBDD.bdd_to_dnf bdd, instr)) bdd_encs
  end

end
