structure TargetBasics : TargetBasics =
struct

fun compset () = m0Lib.m0_compset [utilsLib.mk_reg_thm "m0" "PSR", m0_stepTheory.R_name_def]
val smt_non_eval_terms = [``Aligned : word32 # num -> bool``,``Align : word32 # num -> word32``]
val nextstate = ``NextStateM0``

end
