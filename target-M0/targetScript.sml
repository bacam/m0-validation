open HolKernel Parse boolLib bossLib

val _ = new_theory "target";


(* Taken from m0_stepScript because it isn't exported to the theory *)
local
   val lem =
    (SIMP_RULE (srw_ss()) [] o Q.SPECL [`v`, `32`] o
     Thm.INST_TYPE [Type.alpha |-> ``:33``]) bitstringTheory.word_index_v2w
in
   val shift32 = Q.prove(
      `!w:word32 imm.
         ((w2w w : 33 word) << imm) ' 32 = testbit 32 (shiftl (w2v w) imm)`,
      strip_tac
      \\ bitstringLib.Cases_on_v2w `w`
      \\ fs [bitstringTheory.w2v_v2w, bitstringTheory.w2w_v2w,
             bitstringTheory.word_lsl_v2w, bitstringTheory.word_index_v2w,
             lem, markerTheory.Abbrev_def])
end

val lsl_shift = store_thm (
  "lsl_shift",
  ``!w:word32 imm. testbit 32 ((w2v w) ++ replicate [F] imm) = ((w2w w : 33 word) << imm) ' 32``,
  REWRITE_TAC [bitstringTheory.shiftl_replicate_F, shift32])

val _ = export_theory ();
