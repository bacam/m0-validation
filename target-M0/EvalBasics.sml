structure EvalBasics : EvalBasics =
struct

local
open Drule HolKernel boolLib bossLib
in

val inventory = m0Theory.inventory
val thy = "m0"
val extra_thms = [
   m0_stepTheory.NextStateM0_def,
   m0_stepTheory.AddWithCarry
]
val extra_defs = []
val state_ty = ``:m0_state``

val discard_fns = ["raise'exception", "Raise", "ExceptionReturn"]
val opaque = ["Align","Aligned","AddWithCarry"]@discard_fns

(* No support for exceptions.  (I've only done this for the CHERI model for
   the time being.) *)
type exception_info = unit
fun is_exception _ = NONE
(*
local
   open m0_stepTheory wordsTheory
   val low_bit_correct = wordsLib.WORD_DECIDE ``¬(a ⊕ 7w : word3) = a``;

   (* select_*_be adapted from mips_stepTheory, which ends up producing more
      specialised versions.  We can't use REWRITE_CONV for these because we
      need to fill in the free variable in the hypothesis, but to use
      utilsLib.INST_REWRITE_CONV we need to fill in s.MEM, because it won't
      match a free variable there. *)
   val _ = List.app (fn f => f ())
                    [numLib.prefer_num, wordsLib.prefer_word, wordsLib.guess_lengths]

   open lcsymtacs
   infix \\
   val op \\ = op Tactical.THEN;

   val tac =
       wordsLib.Cases_word_value
       \\ simp []
       \\ strip_tac
       \\ blastLib.BBLAST_TAC

in

val select_byte_be = Q.prove(
   `!b:word3 a:word64.
      (7 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) = s.MEM (a + w2w (b ?? 7w))`,
   tac
   )

val select_half_be = Q.prove(
   `!b:word3 a:word64.
     ~word_bit 0 b ==>
     ((15 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 6w)) @@ s.MEM (a + w2w (b ?? 6w) + 1w)) : word16)`,
   tac
   )
val select_half_be = UNDISCH (SPEC_ALL select_half_be)

val select_word_be = Q.prove(
   `!b:word3 a:word64.
     ((1 >< 0) b = 0w:word2) ==>
     ((31 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 4w)) @@ s.MEM (a + w2w (b ?? 4w) + 1w) @@
      s.MEM (a + w2w (b ?? 4w) + 2w) @@ s.MEM (a + w2w (b ?? 4w) + 3w)) : word32)`,
   tac
   )
val select_word_be = UNDISCH (SPEC_ALL select_word_be)

val fix_addr = Q.prove(`(a : word64 && ¬7w) + w2w (((2 >< 0) a):word3) = a`,
                       blastLib.BBLAST_TAC)

val mem_rws =
    [select_byte_be, select_half_be, select_word_be, select_pc_be,
     computeLib.EVAL_RULE byte_address, address_align2,
     StoreMemory_byte, StoreMemory_doubleword, StoreMemory_half, StoreMemory_word,
     WORD_XOR_ASSOC, WORD_XOR_CLAUSES,
     low_bit_correct, fix_addr]
end
*)
local
open m0_stepTheory
in
val mem_rws = [concat16,concat32,
               extract16,extract32,
               field16,field32]
end

val eval_simp_ths = []

val late_rws = []
val late_ss = simpLib.rewrites []

val next_fn = ``NextStateM0``

val std_hyp =
    [``~s.AIRCR.ENDIANNESS``,
     ``~s.CONTROL.SPSEL``,
     ``s.exception = NoException``] (* No L3 exception *)

val junk_conds = []

val fetch_tm = ``Fetch``

(* TODO: do something sensible with the first two returned values in light of
   multiple paths *)
fun get_fetch eval =
    let val (fetch1, fetch2) =
            case eval std_hyp ``Fetch s`` of
                [th1,th2] => (th1,th2)
              | _ => failwith "'Fetch s' doesn't evaluate to 2 terms"
        val fetched = (fst o pairSyntax.dest_pair o rhs o concl)
        val (fetched1,fetched2) = (fetched fetch1, fetched fetch2)
        fun fix_instr v =
          (* M0 has 16-bit and 32-bit instructions, check which by the
             constructor used *)
          if same_const (rator v) (rator fetched1)
          then PURE_REWRITE_RULE [ASSUME (mk_eq (fetched1, v))] fetch1
          else if same_const (rator v) (rator fetched2)
          then PURE_REWRITE_RULE [ASSUME (mk_eq (fetched2, v))] fetch2
          else failwith "Couldn't find the right instruction rewrite to use"
    in (hyp fetch2, fetched2, fix_instr)
    end

fun term_for_instr v =
  if v < 65536
  then ``Thumb ^(wordsSyntax.mk_wordi (Arbnum.fromLargeInt v,16))``
  else ``Thumb2 (^(wordsSyntax.mk_wordi (Arbnum.fromLargeInt (v div 65536),16)),
                 ^(wordsSyntax.mk_wordi (Arbnum.fromLargeInt (v mod 65536),16)))``

fun encode code =
    let val hex = m0AssemblerLib.m0_code code
    in map (fn h => (h, if String.size h > 4 then 4 else 2)) hex
    end

end
end
