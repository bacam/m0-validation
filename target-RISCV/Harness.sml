structure Harness =
struct
  datatype basic_harness = Breakpoint

  datatype harness =
    Basic of basic_harness
(*  | NOP_padding_after of int * basic_harness*)

fun basic_harness_to_src Breakpoint = "Breakpoint"

fun harness_to_src (Basic b) = "(Basic " ^ basic_harness_to_src b ^ ")"
(*  | harness_to_src (NOP_padding_after (n,b)) =
    "(NOP_padding_after (" ^ Int.toString n ^ "," ^ basic_harness_to_src b ^ "))"
*)

(* No timing *)
fun basic_harness_cost Breakpoint = 0
fun harness_cost (Basic b) = basic_harness_cost b
(*  | harness_cost (NOP_padding_after (n,b)) = n + basic_harness_cost b*)

fun basic_pc_offset Breakpoint = 0

fun pc_offset (Basic b) = basic_pc_offset b
(*  | pc_offset (NOP_padding_after (n,b)) = 2*n + basic_pc_offset b*)

(* In principle we could also use information from the symbolic evaluation
   of Fetch, as we do in CHERI. *)
fun fix_instr _ m pc instr =
    [``(^m  ^pc       = ( 7 ><  0) ^instr) /\
       (^m (^pc + 1w) = (15 ><  8) ^instr) /\
       (^m (^pc + 2w) = (23 >< 16) ^instr) /\
       (^m (^pc + 3w) = (31 >< 24) ^instr)``]

(* This software breakpoint is really just to mark the end of the execution;
   it ensure the solver picks a final value for the PC that's not also in the
   middle of the sequence.  The code in HWTest will insert a hardware
   breakpoint, because the current qemu setup doesn't support software
   breakpoints. *)
fun harness_instrs h =
    map (fn instr => Tools.cbv_eval ``Encode ^instr``)
    [``System EBREAK``]

fun preamble_constraints () = []

end
