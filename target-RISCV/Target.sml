structure Target : sig
  include Target
  val use_stepLib : bool ref (* default: true *)
end =
struct

local
open HolKernel Parse boolLib bossLib riscv_stepTheory

in

fun pc s = ``^s.c_PC 0w``
fun memory_term s = ``^s.MEM8``
val memory_field_update = ``riscv_state_MEM8_fupd``
val memory_type = ``:word64 -> word8``
val memory_element_size = 8
val memory_element_ty = ``:word8``
fun mk_memory_element w = w
val sort_addresses = updateLib.SORT_WORD_UPDATES_CONV ``:64``
val sort_registers = updateLib.SORT_WORD_UPDATES_CONV ``:5``
val state_type = ``:riscv_state``

val ram_details =
    [(wordsSyntax.dest_word_literal (rhs (concl targetTheory.riscv_test_memory_start_def)),
      wordsSyntax.dest_word_literal (rhs (concl targetTheory.riscv_test_memory_length_def)))]

val abstract_updates = [
   ("mem", ``riscv_state_MEM8_fupd``)
]

(* Options *)
val use_stepLib = ref false; (* currently generating too many instructions stepLib doesn't support *)
val require_aligned = ref true; (* Add constraints to require loads/stores to be aligned *)
val amo32 = ref true; (* Add constraints to keep AMO W operands to 32 bits *)

val additional_options =
[ Tools.bool_option "use_stepLib" use_stepLib,
  Tools.bool_option "amo32" amo32,
  Tools.bool_option "require-aligned" require_aligned
]

fun proj_nth n tm =
  let val tys = pairSyntax.spine_prod (type_of tm)
      val m = length tys
  in if n < 1 orelse n > m then failwith "bad projection"
     else if n = m then funpow (n-1) pairSyntax.mk_snd tm
     else pairSyntax.mk_fst (funpow (n-1) pairSyntax.mk_snd tm)
  end

fun amo_w_constr args =
  if !amo32 then
     let val rs2 = proj_nth 5 args
     in ``((63 >< 31) (GPR ^rs2 s) = 0w : 33 word) \/ ((63 >< 31) (GPR ^rs2 s) = ~0w : 33 word)``
     end
  else T

fun align_loadstore basereg n args =
  if !require_aligned then
     let val rs1 = proj_nth basereg args
         val offs = proj_nth 3 args
         val h = numSyntax.term_of_int (n-1)
     in ``(^h >< 0) (GPR ^rs1 s + sw2sw ^offs) = 0w : ^(ty_antiq (wordsSyntax.mk_int_word_type n))``
     end
  else T

(* Extra constraints to add to particular instructions (esp to work around
   known problems), first part is instruction name, second a function from
   the operand tuple to a constraint). *)
val insn_constraints = [
   (* For AMO W instructions the model does a 64-bit operation then truncates,
      whereas simulators appear to truncate the register operand before the
      operation. *)
   ("AMOMAXU_W", amo_w_constr),
   ("AMOMAX_W",  amo_w_constr),
   ("AMOMINU_W", amo_w_constr),
   ("AMOMIN_W",  amo_w_constr),
   (* The spec allows unaligned loads and stores, but they are explicitly
      allowed to be emulated.  The model allows them directly, but spike
      traps for emulation. *)
   ("LH", align_loadstore 2 2),
   ("LHU", align_loadstore 2 2),
   ("LW", align_loadstore 2 2),
   ("LWU", align_loadstore 2 2),
   ("LD", align_loadstore 2 3),
   ("SH", align_loadstore 1 2),
   ("SW", align_loadstore 1 2),
   ("SD", align_loadstore 1 3)
]

(* Target specific manipulations to make to step theorems *)
fun fixup_th th = th

val word_size = 64
val word_type = ``:word64``
val addr_size = word_size
val addr_type = word_type

fun code_addr_translate tm = tm : Term.term

val basic_state =
  ``<| MEM8 := m;
       c_PC := \_. pc;
       exception := NoException;
       c_ReserveLoad := \_. SOME reserveload;
       c_gpr := \_. regs;
       procID := 0w;
       c_NextFetch := \_. NONE;
       c_MCSR := \_. <| mstatus := <| VM := 0w |>; mcpuid := <| ArchBase := 2w |> |>
    |>``;

fun rand_regs gen =
    (* If we were to assign a non-zero value to register 0 it would be ignored
       anyway *)
    let fun rr 0 = ``0w:word64``
          | rr _ = Tools.random_word gen 64 0
        val vals = List.tabulate (32, rr)
        val hol_vals = listSyntax.mk_list (vals, ``:word64``)
    in ``\r:word5. EL (w2n r) ^hol_vals``
    end

fun rand_addr gen =
  let val (_,(start,len)) = RandGen.select ram_details gen
      val offset = RandGen.genint gen (Arbnum.toLargeInt len-1)
  in wordsSyntax.mk_wordi (Arbnum.+ (start, Arbnum.fromLargeInt offset),64)
  end

fun fill_in_state gen s mem =
    let val regs = rand_regs gen
    in subst [{redex = ``regs:word5 -> word64``, residue = regs},
              {redex = ``reserveload:word64``, residue = rand_addr gen},
              {redex = ``m:word64 -> word8``, residue = mem}] s
    end

fun check_options () = ()

fun addr_in_mem tm accesslen =
  let fun in_area (start,len) =
        let open Arbnum
            val limit = wordsSyntax.mk_wordi(start+len-fromInt accesslen,64)
            val start = wordsSyntax.mk_wordi(start,64)
        in ``^start <=+ ^tm /\ ^tm <=+ ^limit``
        end
      val choices = map in_area ram_details
  in list_mk_disj choices
  end

fun additional_constraints (th:Thm.thm) _ _ cbv_eval =
  let val (hy,cn) = dest_thm th
      val final_state = rhs cn
      val final_pc = cbv_eval ``^final_state.c_PC 0w``
  in [``(1 >< 0) ^final_pc = 0w : word64``,
      (* To set up reserveload we'll need it to be a valid location to load a word from *)
      addr_in_mem ``reserveload:word64`` 4,
      ``(1 >< 0) (reserveload:word64) = 0w : word2``]
  end

val smt_rewrites : Thm.thm list = []
val smt_simps : Thm.thm list = []

val yices_irrelevant_updates : term list = []

fun decompose_insn tm =
  let fun aux (l,r) =
        if is_comb r then
           let val (l',r') = dest_comb r
           in if is_const l' andalso #Thy (dest_thy_const l') = "riscv"
              then aux (fst (dest_const l'),r')
              else (l,r)
           end
        else (l,r)
      val (l,r) = dest_comb tm
  in aux (fst (dest_const l), r)
  end

fun add_insn_constraint h =
  let val h = String.translate (fn #" " => "" | x => String.str x) h
      val v = 
          case StringCvt.scanString (Int.scan StringCvt.HEX) h of
              NONE => failwith h
            | SOME n => n
      val w = wordsSyntax.mk_wordii (v,32)
      val insn = Tools.cbv_eval ``Decode ^w``
      val (name,args) = decompose_insn insn
  in (ADD_ASSUM ((Lib.assoc name insn_constraints) args)) handle _ => I
  end

type code = string
fun sc_just [x] = x
  | sc_just _ = failwith "step_code: quote contains multiple strings"
(*val step_code0 = riscv_stepLib.riscv_step_code o sc_just o assemblerLib.quote_to_strings*)
fun step_code0 _ = failwith "not yet supported"
fun step_exn _ = failwith "processor exceptions not supported"
fun step_code_exn _ = failwith "processor exceptions not supported"

fun step s =
  map (add_insn_constraint s)
      (if !use_stepLib
       then [riscv_stepLib.riscv_step_hex s]
       else InstrBehaviour.next s)
fun step_code c = failwith "not yet supported"
(*    if !use_stepLib
    then step_code0 c
    else InstrBehaviour.next_code c*)

fun hex s = Option.valOf (StringCvt.scanString (Int.scan StringCvt.HEX) s);

(* The disassembler needs the bytes of 32bit instructions to be split up. *)
fun breakup_hex s =
    let val n = hex s
        val b4 = Int.fmt StringCvt.HEX (n mod 256)
        val b3 = Int.fmt StringCvt.HEX (n div 256 mod 256)
        val b2 = Int.fmt StringCvt.HEX (n div 256 div 256 mod 256)
        val b1 = Int.fmt StringCvt.HEX (n div 256 div 256 div 256)
    in (b1, b2, b3, b4)
    end

fun breakup_hex_llvm_mc s =
    let val (b1,b2,b3,b4) = breakup_hex s
    in "0x" ^ b1 ^ " 0x" ^ b2 ^ " 0x" ^ b3 ^ " 0x" ^ b4 ^ "\n"
    end

fun breakup_hex_printf s =
    let val (b1,b2,b3,b4) = breakup_hex s
    in "\\x" ^ b4 ^ "\\x" ^ b3 ^ "\\x" ^ b2 ^ "\\x" ^ b1
    end

fun disassemble_hex s =
    let val s = if String.isPrefix "0x" s then String.extract (s,2,NONE) else s
        val hex_tm = wordsSyntax.mk_word_from_hex_string (stringSyntax.fromMLstring s,``:32``)
        val tm = Tools.cbv_eval ``instructionToString (Decode ^hex_tm)``
        (* We might need some extra evaluation to get the string literal *)
        val tm = rhs (concl (EVAL tm))
    in StringCvt.padLeft #"0" 8 s ^ " " ^ stringSyntax.fromHOLstring tm
    end handle e => StringCvt.padLeft #"0" 8 s ^ " unknown\n"

fun print_disassembled_hex s =
    print (disassemble_hex s ^ "\n")

val encode = EvalBasics.encode

type compatibility_info = unit
fun filter_compatible _ _ = failwith "filter_compatible not supported on this architecture"

exception Bad_choices of int list

fun choose_thms gen thmss =
    ([], map (fn thms => IntInf.toInt (RandGen.genint gen (IntInf.fromInt (length thms - 1)))) thmss)

end

end

