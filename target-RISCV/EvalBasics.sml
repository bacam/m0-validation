structure EvalBasics : EvalBasics =
struct

local
open Drule HolKernel boolLib bossLib
in

val inventory = riscvTheory.inventory
val thy = "riscv"
val extra_thms = [
   targetTheory.matchLoadReservation_awkward,
   targetTheory.bit_field_insert_thm,
   targetTheory.riscv_test_memory_start_def,
   targetTheory.riscv_test_memory_length_def
]
val extra_defs =  [
   riscv_stepTheory.NextRISCV_def,
   riscv_stepTheory.Fetch_def,
   riscv_stepTheory.update_pc_def
]
val state_ty = ``:riscv_state``

val discard_fns = ["raise'exception", "setTrap"]
val opaque = discard_fns@["rawReadData","rawWriteData"]

(* No support for exceptions yet. *)
type exception_info = unit
fun is_exception _ = NONE

(*
local
   open mips_stepTheory wordsTheory
   val low_bit_correct = wordsLib.WORD_DECIDE ``¬(a ⊕ 7w : word3) = a``;

   (* select_*_be adapted from mips_stepTheory, which ends up producing more
      specialised versions.  We can't use REWRITE_CONV for these because we
      need to fill in the free variable in the hypothesis, but to use
      utilsLib.INST_REWRITE_CONV we need to fill in s.MEM, because it won't
      match a free variable there. *)
   val _ = List.app (fn f => f ())
                    [numLib.prefer_num, wordsLib.prefer_word, wordsLib.guess_lengths]

   open lcsymtacs
   infix \\
   val op \\ = op Tactical.THEN;

   val tac =
       wordsLib.Cases_word_value
       \\ simp []
       \\ strip_tac
       \\ blastLib.BBLAST_TAC

in

val select_byte_be = Q.prove(
   `!b:word3 a:word64.
      (7 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) = s.MEM (a + w2w (b ?? 7w))`,
   tac
   )

val select_half_be = Q.prove(
   `!b:word3 a:word64.
     ~word_bit 0 b ==>
     ((15 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 6w)) @@ s.MEM (a + w2w (b ?? 6w) + 1w)) : word16)`,
   tac
   )
val select_half_be = UNDISCH (SPEC_ALL select_half_be)

val select_word_be = Q.prove(
   `!b:word3 a:word64.
     ((1 >< 0) b = 0w:word2) ==>
     ((31 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 4w)) @@ s.MEM (a + w2w (b ?? 4w) + 1w) @@
      s.MEM (a + w2w (b ?? 4w) + 2w) @@ s.MEM (a + w2w (b ?? 4w) + 3w)) : word32)`,
   tac
   )
val select_word_be = UNDISCH (SPEC_ALL select_word_be)

val fix_addr = Q.prove(`(a : word64 && ¬7w) + w2w (((2 >< 0) a):word3) = a`,
                       blastLib.BBLAST_TAC)

val mem_rws =
    [select_byte_be, select_half_be, select_word_be, select_pc_be,
     computeLib.EVAL_RULE byte_address, address_align2,
     StoreMemory_byte, StoreMemory_doubleword, StoreMemory_half, StoreMemory_word,
     WORD_XOR_ASSOC, WORD_XOR_CLAUSES,
     low_bit_correct, fix_addr]
end
*)

val mem_rws = map (UNDISCH_ALL o SPEC_ALL) [targetTheory.rawWriteData_alt,targetTheory.rawReadData_alt]

val eval_simp_ths = []

val late_rws = []
val late_ss = simpLib.rewrites []

(* NB: this uses the simplified Next and Fetch functions from the step library *)
val next_fn = ``NextRISCV``

val std_hyp =
    [``s.procID = 0w``,
     ``(s.c_MCSR 0w).mstatus.VM = 0w``, (* No address translation *)
     ``s.c_NextFetch 0w = NONE``,
     ``(s.c_MCSR 0w).mcpuid.ArchBase = 2w``, (* RV64I, the best supported subarch in model *)
     ``s.exception = NoException`` (* No L3 exception *)
    ]

val junk_conds = []

val fetch_tm = ``riscv_step$Fetch``

fun get_fetch eval =
    let val fetch =
            case eval std_hyp ``riscv_step$Fetch s`` of
                [th] => th
              | _ => failwith "'Fetch s' doesn't evaluate to 1 term"
        (* The stepLib version of Fetch doesn't ensure instruction alignment! *)
        val fetch = ADD_ASSUM ``(1 >< 0) (s.c_PC 0w) = 0w : word2`` fetch
        val fetched = (fst o pairSyntax.dest_pair o rhs o concl) fetch
        fun fix_instr v =
            PURE_REWRITE_RULE [ASSUME (mk_eq (fetched, v))] fetch
    in (hyp fetch, fetched, fix_instr)
    end

fun term_for_instr v = wordsSyntax.mk_wordi (Arbnum.fromLargeInt v,32)

fun encode q = failwith "notyet" (*
    let val lines = assemblerLib.quote_to_strings q
        fun line s =
            let val s = mips.stripSpaces s
            in if s = "" then NONE else SOME s
            end
        val instrs = List.mapPartial line lines
    in map (fn s => (mips.encodeInstruction s, 4)) instrs
    end*)

end
end
