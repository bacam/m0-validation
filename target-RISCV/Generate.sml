structure Generate : sig 
  include Generate
  val include_sc : bool ref
end = struct

open GenerationTools RandGen

val include_sc = ref false
val additional_options = [
   Tools.bool_option "include-sc" include_sc
]

val bdd_encodings = ref NONE

fun get_bdd_encodings () =
  case !bdd_encodings of
      NONE =>
      let val () = print "Extracting instruction encodings...\n"
          val encs = Dedecoder.compute_bdd_encodings riscvTheory.inventory ``Decode ^(Dedecoder.mk_word 32)``
          val () = print "...done\n"
          val () = bdd_encodings := SOME encs
      in encs
      end
    | SOME encs => encs

fun bad_instr tm =
  exists (term_eq tm) [
   (* Undefined in some way *)
   ``UnknownInstruction``
   (* Bad idea in some way *)
   ]
  orelse term_eq (fst (strip_comb tm)) ``System``
  orelse term_eq (fst (strip_comb tm)) ``FArith``
  orelse term_eq (fst (strip_comb tm)) ``FConv``
  orelse term_eq (fst (strip_comb tm)) ``FPLoad``
  orelse term_eq (fst (strip_comb tm)) ``FPStore``
  orelse (not (!include_sc) andalso
          List.exists (Lib.can (fn pat => match_term pat tm))
                      [``AMO (SC_W x)``, ``AMO (SC_D x)``])

fun gen_one_instr gen =
  let val encs = get_bdd_encodings ()
      val encs = List.filter (not o bad_instr o snd) encs
      val (_,(bdd,instr)) = RandGen.select encs gen
      val instr = GenerationTools.describe_instr "riscv" instr
      fun bit () = hd (RandGen.bits gen 1)
      val bits = SimpleBDD.sample_bdd bit 32 bdd
      val hex = GenerationTools.to_hex bits
  in (hex,instr,4)
  end

fun gen_instrs gen n = List.tabulate (n, fn _ => gen_one_instr gen)
fun gen_instrs_exn gen n _ = gen_instrs gen n

fun sample_instrs gen =
  let fun sample (bdd,instr) =
        let val instr = GenerationTools.describe_instr "riscv" instr
            fun bit () = hd (RandGen.bits gen 1)
            val bits = SimpleBDD.sample_bdd bit 32 bdd
            val hex = GenerationTools.to_hex bits
        in (hex,instr)
        end
  in map sample (get_bdd_encodings ())
  end

fun test_instrs n =
  let val gen = RandGen.newgen ()
      val () = include_sc := true
      fun bit () = hd (RandGen.bits gen 1)
      fun merge instr hex NONE = SOME (instr,1,hex)
        | merge instr hex (SOME (_,n,_)) = SOME (instr,n+1,hex)
      fun try 0 (bdd,instr) = NONE
        | try n (bdd,instr) =
          let val rest = try (n-1) (bdd,instr)
              val bits = SimpleBDD.sample_bdd bit 32 bdd
              val hex = GenerationTools.to_hex bits
          in if Lib.can Target.step hex
             then rest
             else merge instr hex rest
          end
      fun tryn (enc,instr) =
        let val instr = GenerationTools.describe_instr "riscv" instr
            val () = print ("Trying " ^ instr ^ "\n")
        in try n (enc,instr)
        end
      val encs = get_bdd_encodings()
      val bad = List.mapPartial tryn encs
      fun print_bad (instr,n,hex) =
        print (Int.toString n ^ " failures for " ^ instr ^ ", e.g. " ^ hex ^ "\n")
  in case bad of
         [] => print ("All " ^ Int.toString (length encs) ^ " supported encodings OK\n")
       | _ =>
         let val () = print (Int.toString (length bad) ^ " encodings out of " ^
                             Int.toString (length encs) ^ " failed:\n")
             val () = List.app print_bad bad
         in ()
         end
  end

fun regs_chosen_in_instr _ = failwith "not supported"

end
