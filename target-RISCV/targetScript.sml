open HolKernel Parse boolLib bossLib lcsymtacs

val _ = new_theory "target";

(* Don't actually need this, but wanted it to justify an assumption *)
val _ = store_thm ("NextFetchNONE",
  ``!s. case NextRISCV s of SOME s' => NextFetch s' = NONE | NONE => T``,
  strip_tac
  >> rewrite_tac [riscv_stepTheory.NextRISCV_def]
  >> Cases_on `Fetch s`
  >> EVAL_TAC
  >> Q.SPEC_TAC (`Run (Decode q) r`, `x`)
  >> strip_tac
  >> Cases_on `x.exception`
  >> EVAL_TAC
  >> rewrite_tac [riscvTheory.NextFetch_def]
  >> Cases_on `x.c_NextFetch x.procID`
  >> EVAL_TAC
  >| [rewrite_tac [riscvTheory.write'PC_def]
      >> EVAL_TAC
      >> first_assum ACCEPT_TAC,
      Cases_on `x'` >> EVAL_TAC
      >> rewrite_tac [riscvTheory.write'PC_def, riscvTheory.write'NextFetch_def]
      >> EVAL_TAC
     ])

(* Original definition uses IS_SOME ... /\ THE ... =, which I could handle more
   generally, but is so horrid I'll get rid of it.  This does a three way split,
   resulting in an extra case, but I think that's reasonable.  This appears in
   the SC.[WD] instructions. *)
val _ = store_thm ("matchLoadReservation_awkward",
  ``(if ~ matchLoadReservation v state then P else Q) =
    (case ReserveLoad state of NONE => P | SOME v' => if v' <> v then P else Q)``,
  simp [riscvTheory.matchLoadReservation_def]
  >> Cases_on `ReserveLoad state`
  >> simp [])

(* Eliminate some w2n *)

val align_rw = prove (
  ``(let align = w2n ((2 >< 0) (pAddr : word64) : word3) in P (align = 0) (align * 8) : word64) =
    (let align : word128 = w2w ((2 >< 0) (pAddr : word64) : word3) in P (align = 0w) (w2n (align * 8w)))``,
  wordsLib.Cases_on_word_value `(2 >< 0) (pAddr : word64) : word3`
  >> wordsLib.WORD_EVAL_TAC)

val align_rw2 = prove (
  ``(let align = w2n ((2 >< 0) (pAddr : word64) : word3) in P (align = 0) ($+ align) (align * 8) : riscv_state) =
    (let align = (2 >< 0) (pAddr : word64) : word3 in P (align = 0w) ($+ (w2n align)) (w2n (w2w align * 8w : word64)))``,
  wordsLib.Cases_on_word_value `(2 >< 0) (pAddr : word64) : word3`
  >> wordsLib.WORD_EVAL_TAC)

val w2n_align_rw = prove (
  ``w2n (x : word3) = (if x ' 0 then 1 else 0) +
                      (if x ' 1 then 2 else 0) +
                      (if x ' 2 then 4 else 0)``,
  wordsLib.Cases_on_word_value `x : word3`
  >> wordsLib.WORD_EVAL_TAC)

val w2n_match = prove (
  ``(x : word128) << w2n (y : word64) = (x : word128) << w2n ((w2w y) : word128)``,
  rw[wordsTheory.w2n_w2w])



val bit_field_insert_thm =
   save_thm ("bit_field_insert_thm",
   utilsLib.map_conv blastLib.BBLAST_PROVE
      [
       (* For, e.g., "4D812307", "FPLoad>FLW" *)
       ``!a b. bit_field_insert 31 0 (a : word32) (b : word64) = ((63 >< 32) b) : word32 @@ a``
      ])


val _ = Define `riscv_test_memory_start = 0x80000000w : word64`
val _ = Define `riscv_test_memory_length = 0x1000w : word64`

val () = wordsLib.guess_lengths ()

open wordsExtraTheory wordsExtraLib

(* Note that the range hypotheses are to add extra constraints; they are not
   required to prove the result. *)
val rawReadData_alt = store_thm(
  "rawReadData_alt",
  ``!pAddr.
    riscv_test_memory_start <=+ pAddr /\
    pAddr <=+ riscv_test_memory_start + riscv_test_memory_length - 8w ==>
    (rawReadData pAddr =
    (\state.
      state.MEM8 (pAddr + 7w) @@
      state.MEM8 (pAddr + 6w) @@
      state.MEM8 (pAddr + 5w) @@
      state.MEM8 (pAddr + 4w) @@
      state.MEM8 (pAddr + 3w) @@
      state.MEM8 (pAddr + 2w) @@
      state.MEM8 (pAddr + 1w) @@
      state.MEM8  pAddr))``,

  rpt strip_tac
  >> simp[riscvTheory.rawReadData_def,riscvTheory.MEM_def]
  >> ABS_TAC
  >> wordsLib.Cases_on_word_value `(2 >< 0) pAddr`
  
  >> asm_simp_tac (arith_ss++wordsLib.WORD_ss) []
  
  >> CONV_TAC (REDEPTH_CONV concat_left_conv)
  >> CONV_TAC (REDEPTH_CONV concat_asr_conv)
  >> asm_simp_tac (arith_ss++wordsLib.WORD_ss) [extract_sw2sw]
  
  >> simp_tac (arith_ss++wordsLib.WORD_ss++wordsLib.WORD_CONCAT_ASSOC_ss++utilLib.extract_concat_ss) []
  
  >> `(w2w ((63 >< 3) pAddr + 1w) << 3) : word64 = (w2w ((63 >< 3) pAddr) << 3) + 8w` by blastLib.BBLAST_TAC
  >> asm_simp_tac std_ss []
  
  >> `pAddr = w2w (((63 >< 3) pAddr) : 61 word) << 3 + w2w (((2 >< 0) pAddr) : word3)` by blastLib.FULL_BBLAST_TAC
  
  >> Q.PAT_ASSUM `(2 >< 0) pAddr = x` (fn th => full_simp_tac (arith_ss++wordsLib.WORD_ss) [th])
  >> Q.PAT_ASSUM `pAddr = x` (fn th => CONV_TAC (RHS_CONV (ONCE_REWRITE_CONV [th])))
  >> simp_tac (arith_ss++wordsLib.WORD_ss) [])



val make_dword_def = Define
  `make_dword (f : num -> word8) = (f 7 @@ f 6 @@ f 5 @@ f 4 @@ f 3 @@ f 2 @@ f 1 @@ f 0) : word64`

val make_dword_lsl = prove(
  ``!f:num -> word8. !n.
    n < 9 ==>
    ((make_dword f) << (8 * n) =
     (make_dword (\i. if i < n then 0w else f (i - n))))``,

  strip_tac
  >> simp[make_dword_def]
  >> rpt (CONV_TAC (numLib.BOUNDED_FORALL_CONV REFL)
          >> simp_tac (arith_ss++wordsLib.WORD_ss++utilLib.extract_concat_ss++
                       lsl_concat_ss++wordsLib.WORD_CONCAT_ASSOC_ss)
                      [WORD_w2w_EXTRACT']))

val make_dword_and = prove (
  ``make_dword f && make_dword g = make_dword (\i. f i && g i)``,
  srw_tac [word_concat_and_ss] [make_dword_def])

val make_dword_or = prove (
  ``make_dword f || make_dword g = make_dword (\i. f i || g i)``,
  srw_tac [word_concat_or_ss] [make_dword_def])

val make_qword_def = Define
  `make_qword (f : num -> word8) =
                      (f 15 @@ f 14 @@ f 13 @@ f 12 @@ f 11 @@ f 10 @@ f 9 @@ f 8 @@
                       f 7 @@ f 6 @@ f 5 @@ f 4 @@ f 3 @@ f 2 @@ f 1 @@ f 0) : word128`

val make_qword_lsl = prove(
  ``!f:num -> word8. !n.
    n < 17 ==>
    ((make_qword f) << (8 * n) =
     (make_qword (\i. if i < n then 0w else f (i - n))))``,

  strip_tac
  >> simp[make_qword_def]
  >> rpt (CONV_TAC (numLib.BOUNDED_FORALL_CONV REFL)
          >> simp_tac (arith_ss++wordsLib.WORD_ss++utilLib.extract_concat_ss++
                       lsl_concat_ss++wordsLib.WORD_CONCAT_ASSOC_ss)
                      [WORD_w2w_EXTRACT']))

val make_qword_and = prove (
  ``make_qword f && make_qword g = make_qword (\i. f i && g i)``,
  srw_tac [word_concat_and_ss] [make_qword_def])

val make_qword_or = prove (
  ``make_qword f || make_qword g = make_qword (\i. f i || g i)``,
  srw_tac [word_concat_or_ss] [make_qword_def])

val mask_calc = prove(
  ``!nbytes:num.
    nbytes < 9 ==>
    (1w << (8 * nbytes) + -1w : word64 =
     make_dword (\i. if i < nbytes then 0xffw else 0w : word8))``,
  rewrite_tac [make_dword_def]
  >> rpt (CONV_TAC (numLib.BOUNDED_FORALL_CONV REFL) >> simp[]))

val mask_calc' = prove(
  ``!nbytes align:num.
    0 < nbytes ==>
    nbytes + align < 9 ==>
    (1w << (8 * nbytes + 8 * align) + -1w : word64 << (8 * align) =
     make_dword (\i. if i < align \/ i >= nbytes + align then 0w else 0xffw : word8))``,
rpt strip_tac
>> `nbytes < 9` by asm_simp_tac arith_ss []
>> Q.UNDISCH_TAC `nbytes + align' < 9`
>> utilLib.bounded_tac ``nbytes:num``
>> STRIP_GOAL_THEN (fn th => ONCE_REWRITE_RULE [arithmeticTheory.ADD_COMM] th |> ONCE_REWRITE_RULE [GSYM arithmeticTheory.SUB_LEFT_LESS] |> SIMP_RULE arith_ss [] |> ASSUME_TAC)
>> utilLib.upper_bounded_tac ``align':num``
>> simp[make_dword_def])

val mask_calc128 = prove(
  ``!nbytes align:num.
    0 < nbytes ==>
    nbytes < 9 ==>
    align < 9 ==>
    (w2w (1w << (8 * nbytes) + -1w : word64) : word128 << (8 * align) =
     make_qword (\i. if i < align \/ i >= nbytes + align then 0w else 0xffw : word8))``,
rpt strip_tac
>> utilLib.bounded_tac ``nbytes:num``
>> utilLib.upper_bounded_tac ``align':num``
>> simp[make_qword_def])

val read_background_64 = prove (
  ``!align nbytes:num.
  0 < nbytes ==>
  align+nbytes <= 8 ==>
  ((~((1w << (8 * nbytes) - 1w) << (8 * align)) && riscv$MEM pAddrIdx state) =
   make_dword (\i. if i < align \/ i >= align + nbytes then state.MEM8 (w2w pAddrIdx << 3 + n2w i) else 0w))``,

  rpt strip_tac
  >> `nbytes < 9` by asm_simp_tac arith_ss []
  >> `align' < 9` by asm_simp_tac arith_ss []
  >> simp[riscvTheory.MEM_def,mask_calc,make_dword_lsl]
  >> simp[make_dword_def]
  >> simp_tac (std_ss++word_concat_and_ss++word_1comp_concat_ss) []
  >> `!a b x y. (~if a then x : word8 else if b then y else x) = if a \/ ~b then ~x else ~y`
     by (rpt strip_tac >> COND_CASES_TAC >> simp[] >> COND_CASES_TAC >> simp[])
  >> asm_simp_tac std_ss []
  >> `!a x y. (~if a then 255w : word8 else y) = if ~a then ~y else 0w`
     by (rpt strip_tac >> COND_CASES_TAC >> simp[])
  >> `!a x y. (~if a then 0w : word8 else y) = if a then 255w else ~y`
     by (rpt strip_tac >> COND_CASES_TAC >> simp[])
  >> `!a x y z. (if a then x : word8 else y) && z = if a then x && z else y && z`
     by (rpt strip_tac >> COND_CASES_TAC >> simp[])
  >> `!n m : num. ~(n < m) = n >= m` by simp_tac arith_ss []
  >> asm_simp_tac (std_ss++wordsLib.WORD_ss) [])

val read_background_128 = prove (
  ``!align nbytes:num.
  0 < nbytes ==>
  nbytes < 9 ==>
  align < 9 ==>
  align+nbytes > 8 ==>
  ((~(w2w (1w << (8 * nbytes) - 1w : word64) : word128 << (8 * align)) &&
   riscv$MEM (pAddrIdx + 1w) state @@ riscv$MEM pAddrIdx state) =
   make_qword (\i. if i < align \/ i >= align + nbytes then state.MEM8 (w2w pAddrIdx << 3 + n2w i) else 0w))``,

  rpt strip_tac
  >> simp[riscvTheory.MEM_def,mask_calc128,make_qword_lsl]
  >> simp[make_qword_def]
  >> simp_tac (std_ss++word_concat_and_ss++word_1comp_concat_ss++wordsLib.WORD_CONCAT_ASSOC_ss) []
  >> `!a x y. (~if a then 0w : word8 else y) = if a then 255w else ~y`
     by (rpt strip_tac >> COND_CASES_TAC >> simp[])
  >> `!a x y z. (if a then x : word8 else y) && z = if a then x && z else y && z`
     by (rpt strip_tac >> COND_CASES_TAC >> simp[])
  >> `!n m : num. ~(n < m) = n >= m` by simp_tac arith_ss []
  >> `w2w ((pAddrIdx : 61 word) + 1w) : word64 << 3 = w2w pAddrIdx << 3 + 8w`
     by blastLib.BBLAST_TAC
  >> asm_simp_tac (std_ss++wordsLib.WORD_ss) [])


val split_data = prove(
  ``data :word64 = make_dword (\i. (7 >< 0) (data >>> (8 * i)))``,
  srw_tac [word_extract_concat_join_ss,word_extract_all_ss] [make_dword_def,word_extract_lsr])
val split_data128 = prove(
  ``w2w (data :word64) = make_qword (\i. (7 >< 0) (data >>> (8 * i)))``,
  srw_tac [word_extract_concat_join_ss,word_extract_all_ss,wordsLib.WORD_EXTRACT_ss] [make_qword_def,word_extract_lsr])

(* Note that the range hypotheses are to add extra constraints; they are not
   required to prove the result. *)
val rawWriteData_alt = store_thm (
  "rawWriteData_alt",
  ``!pAddr data nbytes.
    riscv_test_memory_start <=+ pAddr /\
    pAddr <=+ riscv_test_memory_start + riscv_test_memory_length - n2w nbytes ==>
    0 < nbytes ==>
    nbytes <= 8 ==>
    (rawWriteData (pAddr,data,nbytes) state =
      SND (FOR (nbytes-1,0,
        \i s.
          ((), s with MEM8 := (pAddr + n2w i =+ (7 >< 0) (data >>> (8*i))) s.MEM8))
      state))``,

  rpt strip_tac
  >> `nbytes < 9` by asm_simp_tac arith_ss []
  >> simp[riscvTheory.rawWriteData_def]
  >> COND_CASES_TAC
  >| [
    asm_simp_tac arith_ss [SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] (SPEC ``0:num`` read_background_64)]
    >> asm_simp_tac arith_ss [mask_calc]
    >> CONV_TAC (LHS_CONV (DEPTH_CONV (fn tm => if term_eq tm ``data:word64`` then split_data else fail())))
    >> simp_tac std_ss [make_dword_and,make_dword_or]
    >> simp_tac (srw_ss()++utilLib.extract_concat_ss) [make_dword_def,riscvTheory.write'MEM_def,combinTheory.UPDATE_APPLY,boolTheory.LET_THM]
    
    >> `w2w ((63 >< 3) pAddr) << 3 = pAddr` by blastLib.FULL_BBLAST_TAC
    >> asm_simp_tac (std_ss++wordsLib.WORD_ss) []
    >> utilLib.bounded_tac ``nbytes:num``
    >> simp_tac (srw_ss()++utilLib.FOR_ss) [state_transformerTheory.BIND_DEF]
    >> simp_tac (srw_ss()) [combinTheory.UPDATE_APPLY_IMP_ID]
  ,
    COND_CASES_TAC
    >| [
      `nbytes + w2n ((2 >< 0) pAddr) < 9` by asm_simp_tac arith_ss []
      >> asm_simp_tac arith_ss [SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] (Q.SPEC `w2n ((2 >< 0) pAddr)` read_background_64)]
      >> asm_simp_tac arith_ss [Q.SPECL [`nbytes`,`w2n ((2 >< 0) (pAddr : word64))`] mask_calc']
      >> CONV_TAC (LHS_CONV (DEPTH_CONV (fn tm => if term_eq tm ``data:word64`` then split_data else fail())))
      >> `w2n ((2><0) pAddr) < 9` by
           (wordsLib.Cases_on_word_value `(2><0) pAddr` >> simp[])
      >> asm_simp_tac std_ss [make_dword_lsl,make_dword_and,make_dword_or]
      >> CONV_TAC (RHS_CONV (DEPTH_CONV (fn tm => if term_eq tm ``pAddr:word64`` then
           blastLib.BBLAST_PROVE
           ``pAddr : word64 = w2w (((63 >< 3) pAddr) : 61 word) << 3 + w2w (((2 >< 0) pAddr) : word3)``
                  else fail())))
      >> Q.UNDISCH_TAC `nbytes + w2n ((2 >< 0) pAddr) < 9`
      >> wordsLib.Cases_on_word_value `(2 >< 0) pAddr`
      >> utilLib.bounded_tac ``nbytes:num``
      >> asm_simp_tac (arith_ss++wordsLib.WORD_ss) []
      >> simp_tac (srw_ss()++utilLib.extract_concat_ss++utilLib.FOR_ss) [make_dword_def,riscvTheory.write'MEM_def,combinTheory.UPDATE_APPLY,boolTheory.LET_THM,state_transformerTheory.BIND_DEF]
      >> simp_tac (srw_ss()++wordsLib.WORD_ss++wordsLib.WORD_ARITH_EQ_ss) [combinTheory.UPDATE_APPLY_IMP_ID,combinTheory.UPDATE_APPLY]
    ,
      `nbytes + w2n ((2 >< 0) pAddr) > 8` by asm_simp_tac arith_ss []
      >> `w2n ((2 >< 0) pAddr) < 9` by (wordsLib.Cases_on_word_value `(2 >< 0) pAddr` >> simp[])
      >> asm_simp_tac arith_ss [SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] (Q.SPEC `w2n ((2 >< 0) pAddr)` read_background_128)]
      >> asm_simp_tac arith_ss [Q.SPECL [`nbytes`,`w2n ((2 >< 0) (pAddr : word64))`] mask_calc128]
      >> CONV_TAC (LHS_CONV (DEPTH_CONV (fn tm => if term_eq tm ``w2w (data:word64) : word128`` then split_data128 else fail())))
      >> `w2n ((2 >< 0) pAddr) < 17` by asm_simp_tac arith_ss []
      >> asm_simp_tac std_ss [make_qword_lsl,make_qword_and,make_qword_or]
      >> CONV_TAC (RHS_CONV (DEPTH_CONV (fn tm => if term_eq tm ``pAddr:word64`` then
           blastLib.BBLAST_PROVE
           ``pAddr : word64 = w2w (((63 >< 3) pAddr) : 61 word) << 3 + w2w (((2 >< 0) pAddr) : word3)``
                  else fail())))
  
      >> Q.UNDISCH_TAC `nbytes + w2n ((2 >< 0) pAddr) > 8`
      >> Q.UNDISCH_TAC `(2 >< 0) pAddr <> 0w`
      >> wordsLib.Cases_on_word_value `(2 >< 0) pAddr`
      >> utilLib.bounded_tac ``nbytes:num``
      >> asm_simp_tac (arith_ss++wordsLib.WORD_ss) []
      >> simp_tac (srw_ss()++utilLib.extract_extract_ss++utilLib.extract_concat_ss++utilLib.FOR_ss++wordsLib.WORD_CONCAT_ASSOC_ss) [make_qword_def,riscvTheory.write'MEM_def,combinTheory.UPDATE_APPLY,boolTheory.LET_THM,state_transformerTheory.BIND_DEF]
      >> `w2w ((63 >< 3) pAddr + 1w) << 3 = w2w ((63 >< 3) pAddr) << 3 + 8w : word64`
         by blastLib.BBLAST_TAC
      >> asm_simp_tac (srw_ss()++wordsLib.WORD_ss++wordsLib.WORD_ARITH_EQ_ss) [combinTheory.UPDATE_APPLY_IMP_ID,combinTheory.UPDATE_APPLY]
    ]
  ])


val _ = export_theory ();
