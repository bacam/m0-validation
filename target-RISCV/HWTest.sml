structure HWTest : sig
  include HWTest
  val verbose : bool ref
end =
struct

local
open HolKernel boolLib
in

val (sram_start, sram_size) = hd Target.ram_details

type connection = GDBRemote.connection
val connect = GDBRemote.connect
val close = GDBRemote.close
fun reset _ = ()
fun cpu_id _ = ""

fun write_memory debug bgmem s =
    let val mem = State.decompose_hol_memory (Tools.cbv_eval ``^s.MEM8``) bgmem
    in List.app (fn (start, content) =>
                    GDBRemote.write_memory debug
                                     (Arbnum.toLargeInt start,
                                      (Vector.map (Word8.fromInt o wordsSyntax.uint_of_word) content)))
                mem
    end

(* Provided first mismatch only *)
fun compare_memory debug bgmem posts =
  let val modelmem = State.decompose_hol_memory (Tools.cbv_eval ``^posts.MEM8``) bgmem
      fun check (start,content) =
        let val start = Arbnum.toLargeInt start
            val targetmem =
                GDBRemote.read_memory debug (start, Vector.length content)
            fun wrongbyte (i,modelbyte) =
              Word8.toInt (Word8Vector.sub (targetmem,i)) <> wordsSyntax.uint_of_word modelbyte
        in case Vector.findi wrongbyte content of
               NONE => NONE
             | SOME (offset,_) => SOME ["mem:" ^ IntInf.fmt StringCvt.HEX (start+IntInf.fromInt offset)]
        end
  in case Lib.get_first check modelmem of
         NONE => []
       | SOME l => l
  end

val perform_load_reserve =
let val instr1 = Tools.cbv_eval ``Encode (AMO (LR_W (0w,0w,0w,1w)))``
    val instr2 = Tools.cbv_eval ``Encode (System EBREAK)``
    fun word32 v =
      let val i = wordsSyntax.uint_of_word v
          fun w x = Word8.fromInt x
      in [w (i mod 256), w ((i div 256) mod 256),
          w ((i div 0x10000) mod 256), w ((i div 0x1000000) mod 256)]
      end
in Vector.fromList (word32 instr1 @ word32 instr2)
end

fun inf_of_word w : IntInf.int = Arbnum.toLargeInt (wordsSyntax.dest_word_literal w)

(* Must come before the rest of the initialisation, because it interferes with
   the memory and registers. *)
fun setup_load_reservation debug s =
  let val addr = Tools.cbv_eval ``THE (^s.c_ReserveLoad 0w)``
      val addr = inf_of_word addr
      val codeaddr = Arbnum.toLargeInt (fst (hd Target.ram_details))
      val () = GDBRemote.write_memory debug (codeaddr, perform_load_reserve)
      val () = GDBRemote.write_registers debug false 8 [0,addr]
      val () = GDBRemote.insert_breakpoint debug 1 (codeaddr+4) "4"
      val () = GDBRemote.continue debug codeaddr
      val _ = GDBRemote.remove_breakpoint debug 1 (codeaddr+4) "4"
  in ()
  end

fun upto f n m = if n = m then () else (f n; upto f (n+1) m)

fun calculate_regs s =
  let fun reg n =
        let val tm = Tools.cbv_eval ``^s.c_gpr 0w ^(wordsSyntax.mk_wordii (n,5))``
        in (n, inf_of_word tm)
        end
  in List.tabulate (32,reg)
  end

fun regs_present l =
  let fun aux n [] = []
        | aux n ((m,h)::t) =
          if n <> m then failwith "Bad register list" else
          h::(aux (n+1) t)
  in aux 0 l
  end

fun write_registers debug regs =
  let val regs = Lib.sort (fn (n,_) => fn (m,_) => n < m) regs
      val regs = regs_present regs
  in GDBRemote.write_registers debug false 8 regs
  end

fun read_registers debug = GDBRemote.read_registers debug false 32

fun reg_name n =
  let val n_tm = wordsSyntax.mk_wordii (n,5)
      val s_tm = Tools.cbv_eval ``reg ^n_tm``
  in stringLib.fromHOLstring s_tm
  end

fun compare_regs expected readback =
  let fun look ((n,exp),read) =
        if exp = read then NONE else SOME ("reg:" ^ reg_name n)
  in List.mapPartial look (ListPair.zip (expected, readback))
  end

val verbose = ref false

fun dump_regs expected readback =
  let fun printr ((n,exp),read) =
        print ("Register " ^ Int.toString n ^ " (" ^ reg_name n ^ ") expected 0x" ^
               IntInf.fmt StringCvt.HEX exp ^ " read 0x" ^
               IntInf.fmt StringCvt.HEX read ^ "\n")
  in List.app printr (ListPair.zip (expected, readback))
  end

exception Permanent_failure of string

fun run_and_check debug bgmem s full_th harness =
  let val post = rhs (concl full_th)
      val preregs = calculate_regs s
      val postregs = calculate_regs post
      val pc = Tools.cbv_eval ``^s.c_PC 0w``
      val pc = inf_of_word pc
      val postpc = Tools.cbv_eval ``^post.c_PC 0w``
      val postpc = inf_of_word postpc
  in let
      val () = setup_load_reservation debug s
      val () = write_memory debug bgmem s
      val () = write_registers debug preregs
      val () = GDBRemote.insert_breakpoint debug 1 postpc "4"
      val () = GDBRemote.continue debug pc
      val _ = GDBRemote.remove_breakpoint debug 1 postpc "4"
      val regs = read_registers debug
      val regfail = compare_regs postregs regs
      val () = if !verbose then dump_regs postregs regs else ()
      val memfail = compare_memory debug bgmem post
      val fail = regfail@memfail
  in case fail of
         [] => (print "All OK.\n"; NONE)
       | _ =>
         let val msg = String.concatWith "," fail
             val () = print ("Failed:" ^ msg ^ "\n")
         in SOME msg
         end
  end handle (e as SysErr _) =>
      (print "Failed: system error";
       raise (Permanent_failure (exn_to_string e)))
      | GDBRemote.Timeout => 
      (* If this fails then propogate the error *)
      (print "Failed: timeout\n";
       GDBRemote.interrupt debug;
       GDBRemote.remove_breakpoint debug 1 postpc "4";
       SOME "timeout") handle _ =>
                              raise (Permanent_failure "Unable to interrupt after timeout")
  end
end

val options = []

end
