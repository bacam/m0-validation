open HolKernel Parse boolLib bossLib

val _ = new_theory "testing";

(* For CombineSteps *)

(* Definition for executing n steps.
   Note that we don't want the definition to be unfolded by computeLib, so use
   zDefine. *)

val NStates_def =
    zDefine `(NStates 0 s = s) /\
             (NStates (SUC n) s = NStates n (THE (^(TargetBasics.nextstate) s)))`;

(* For YicesSat *)
val w2n8 = Define `w2n8 (w:word8) = w2n w`

(* For StepSMT *)

(* Avoids a round-trip through num, which I think would be awkward for SMT solving *)
(*val myb2w_def = Define `(myb2w T = 1w) /\ (myb2w F = 0w)`;*)
val myb2w_def = Define `myb2w b = if b then 1w else 0w : 'a word`;

val v2w_singleton = store_thm(
       "v2w_singleton",
       ``v2w [b] = myb2w b``,
       Cases_on `b:bool` THEN EVAL_TAC)

(* Yices just eats CPU on signed mod/div of a value by itself, rewrite beforehand *)

val n_MOD_n = prove(
  ``!n. n <> 0 ==> (n MOD n = 0)``,
  Cases THEN SIMP_TAC (srw_ss()) [arithmeticTheory.LESS_0_CASES])

val word_srem_x_x = store_thm(
  "word_srem_x_x",
  ``!x : 'a word. x <> 0w ==> (word_srem x x = 0w)``,
  REPEAT STRIP_TAC
  THEN ASM_SIMP_TAC (srw_ss()) [wordsTheory.word_srem_def,wordsTheory.word_mod_def,n_MOD_n,wordsTheory.w2n_eq_0])

val n_DIV_n = prove(
  ``!n. n <> 0 ==> (n DIV n = 1)``,
  Cases THEN SIMP_TAC (srw_ss()) [arithmeticTheory.LESS_0_CASES])

val word_sdiv_x_x = store_thm(
  "word_sdiv_x_x",
  ``!x : 'a word. x <> 0w ==> (word_sdiv x x = 1w)``,
  REPEAT STRIP_TAC
  THEN ASM_SIMP_TAC (srw_ss()) [wordsTheory.word_sdiv_def,wordsTheory.word_div_def,n_DIV_n,wordsTheory.w2n_eq_0])

val _ = export_theory ();
