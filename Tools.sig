signature Tools =
sig
  (* Reduce terms using some knowledge of the model's construction; the first
     version takes a list of constants to avoid reducing *)
  val mk_cbv_compset : Term.term list -> computeLib.compset
  val mk_cbv_CONV : Term.term list -> Conv.conv
  val cbv_restr_CONV : Conv.conv
  val cbv_restr_eval : Term.term -> Term.term
  val cbv_CONV : Conv.conv
  val cbv_eval : Term.term -> Term.term

  (* String functions used in CHERI logging that cause large output or
     non-termination. *)
  val string_opaque : Term.term list

  (* Apply a conversion to a term and return just the converted term *)
  val conv_term : Conv.conv -> Term.term -> Term.term

  (* Turn ``f 3 = 5`` terms in (``f``, ``(3 =+ 5) f``) update terms. *)
  val eqns_to_updates : Term.term list -> (Term.term * Term.term) list

  val read_file : string -> string

  (* Merge and deduplicate two sorted lists (the comparison function does not
     need to be reflexive) *)
  val merge : (''a -> ''a -> bool) -> ''a list -> ''a list -> ''a list

  val random_word : RandGen.gen -> int -> int -> Term.term

  val bool_option : string -> bool ref -> string * ((string -> unit) * (unit -> string))
end
