(* Main testing code *)

structure Test =
struct

local
open HolKernel boolLib bossLib
in

local
val options =
    StepSMT.options @
    Prestate.options @
    Target.additional_options @
    Generate.additional_options @
    HWTest.options
in

fun set_options s =
  let val opts = Substring.tokens (fn #"," => true | _ => false) (Substring.full s)
      val opts = map (Substring.tokens (fn #":" => true | _ => false)) opts
      fun apply [] = failwith "Empty option"
        | apply (opt::tl) =
          let val opt = Substring.string opt
          in case assoc1 opt options of
                 NONE => failwith ("Unknown option: " ^ opt)
               | SOME (_,(f,_)) => f (case tl of [] => "on" | (vl::_) => Substring.string vl)
          end
      val () = List.app apply opts
  in Target.check_options ()
  end

fun machine_readable_options () =
  let fun fmt (name,(_,f)) = name ^ ":" ^ f ()
      val strs = map fmt options
  in String.concatWith "," strs
  end

fun option_usage () =
  let fun fmt (name,(_,f)) = name ^ ": default " ^ f () ^ "\n"
      val strs = map fmt options
  in List.app print strs
  end

end

fun report_instrs is =
    let val _ = print ("Instructions: \n")
    in List.app (fn (x,_,_) => Target.print_disassembled_hex x) is
    end

fun report_choices cs =
    let val cs = map Int.toString cs
        val s = String.concatWith "," cs
    in print ("Theorem choices: [" ^ s ^ "]\n")
    end

fun generate gen n exns =
    let val () = print "Generating instructions:\n"
        val instrs = Generate.gen_instrs_exn gen n exns
    in instrs
    end

exception Useless_instruction of string

fun choose gen instrs exns choices all_exn =
    let val () = print "Obtaining step theorems.\n"
        fun generate i (hex,d,_) =
            let val exn = Lib.mem i exns
                val step = if exn then Target.step_exn else Target.step
            in case step hex of
                [] => (print ("No " ^ (if exn then "exceptional " else "") ^
                              "step theorems for " ^ hex ^ " " ^ d ^ ".\n");
                       raise (Useless_instruction hex))
              | l => (hex,l)
            end
        val thmss = Lib.mapi generate instrs
        val () = print "Choosing step theorems.\n"
        val (compat,choices) = 
            case choices of
                NONE => Target.choose_thms gen (map snd thmss)
              | SOME cs => if all_exn
                           then failwith "Can't (yet) generate all exception behaviours when choices fixed"
                           else ([],cs)
        (* We picked a single behaviour for each instruction, but if we want
           all exceptional behaviours for an instruction then splice them in
           now. *)
        val choices =
            if all_exn then
               case exns of
                   [ ] => failwith "Can't generate all exception behaviours when there are no exceptions"
                 | [i] => let val ok = Target.filter_compatible (List.nth (compat,i))
                                                                (snd (List.nth (thmss,i)))
                              val ok = map fst ok
                              val l = List.take (choices,i)
                              val r = List.drop (choices,i+1)
                          in List.map (fn j => l@(j::r)) ok
                          end
                 | _ => failwith "Can only generate all exception behaviours for 1 position"
            else [choices]
        fun pick_out_ths choices =
          ListPair.map (fn (c,(hex,thms)) =>
                           let val th = StepSMT.early_rewrites (List.nth (thms, c))
                           in if term_eq (concl th) T
                              then raise (Useless_instruction (hex ^ " choice " ^ Int.toString c))
                              else th
                           end)
                       (choices,thmss)
        fun exn_map [] = (NONE,[])
          | exn_map (cs::css) =
            let val (e,t) = exn_map css
            in (e,(cs,pick_out_ths cs)::t) handle e' => (SOME e',t)
            end
    in
       if all_exn then
          (* TODO: record when some of the choices are impossible *)
          case exn_map choices of
              (SOME e, []) => raise e
            | (_,r) => r
       else
          map (fn cs => (cs, pick_out_ths cs)) choices
    end

fun combine ths =
    let val () = print "Combining step theorems.\n"
        val (th, mem_counter) = CombineSteps.combine_steps ths
    in (th, mem_counter)
    end

(* Instantiate all of the free variables that have a definition in the
   hypotheses; in particular the state and mem_step_n variables.
   There is some crude dependency analysis to try to keep terms
   relatively concrete, hopefully avoiding blow-ups due to alias
   checking.  (Maybe a proper topological sort would be better.)
 *)
fun instantiate_free_var_hyp th =
    let open optionSyntax
        fun extract_def tm =
            if markerSyntax.is_abbrev tm
            then let val (l,r) = markerSyntax.dest_abbrev tm
                 in [(mk_var (l, type_of r), r)]
                 end
            else if is_eq tm
            then if is_var (lhs tm)
                 then [(lhs tm, rhs tm)]
                 else if is_var (rhs tm)
                 then [(rhs tm, lhs tm)]
                 else if is_some (lhs tm) andalso is_some (rhs tm)
                 then extract_def (mk_eq (dest_some (lhs tm), dest_some (rhs tm)))
                 else if pairSyntax.is_pair (lhs tm) andalso pairSyntax.is_pair (rhs tm)
                 then let val (l1,l2) = pairSyntax.dest_pair (lhs tm)
                          val (r1,r2) = pairSyntax.dest_pair (rhs tm)
                      in (extract_def (mk_eq (l1,r1)))@(extract_def (mk_eq (l2,r2)))
                      end
                 else []
            else if is_var tm andalso type_of tm = ``:bool``
            then [(tm, T)]
            else if is_comb tm andalso rator tm = ``bool$~`` andalso is_var (rand tm)
            then [(rand tm, F)]
            else if is_conj tm
            then let val (tm1,tm2) = dest_conj tm
                 in (extract_def tm1)@(extract_def tm2)
                 end
            else []
        val defs = List.concat (map extract_def (hyp th))
        val vars = map fst defs
        val defs = List.filter (fn (_,r) => not (List.exists (fn v => mem v vars) (free_vars r))) defs
        val inst = map (fn (l,r) => {redex = l, residue = Tools.cbv_eval r}) defs
    (* I suspect this might be a little expensive, but reducing here may put
       some hypotheses into a form where they can be used *)
    in if inst = [] then th else instantiate_free_var_hyp (INST inst th |> DISCH_ALL |> Conv.CONV_RULE Tools.cbv_CONV |> UNDISCH_ALL)
    end


fun instantiate_th th s instr_start memory_addresses =
    let val th = DISCH_ALL th
(* This isn't behaving properly at the moment ----
        val cmp = Tools.mk_cbv_compset []
        val state_fields_insts = RecordUtils.record_inst_convs (mk_var ("s",Target.state_type)) s
        val () = app (fn c => computeLib.add_conv c cmp) state_fields_insts
        val th = 
            th |>
               (* Instantiating the state record and reducing to deal with the
                  projections is costly, so we rewrite the projections while
                  computing, and get rid of the state as a whole later *)
               CONV_RULE (computeLib.CBV_CONV cmp) |>
               DISCH_ALL |>
*)
         val th = 
             th |>
                (* Instantiating the state record and reducing to deal with the
                  projections is costly, so we rewrite the projections directly
                  first in INST_record. *)
               RecordUtils.INST_record (mk_var ("s",Target.state_type)) s |>
               (* Note that only the hypotheses from state_thm move, the "real"
                  ones will still be in the conclusion until we reduce it to get
                  rid of all mention of the state record. *)
               UNDISCH_ALL |>
               (* I used EVAL_CONV here previously, but this exposes details from n-bit
                  that subsequent uses of cbv_CONV won't handle *)
               CONV_RULE Tools.cbv_CONV |>


               (* Instantiate the state and get rid of hypothesis, etc by reduction *)
               INST [{redex = mk_var ("s", Target.state_type), residue = s},
                     {redex = ``instr_start:num -> ^(ty_antiq Target.word_type)``, residue = instr_start},
                     {redex = ``memory_address:num -> ^(ty_antiq Target.addr_type)``, residue = memory_addresses}] |>
               CONV_RULE Tools.cbv_CONV |>

               (* Push all the definitions in the hypotheses into the rest of
                  the theorem; most of this is now done by INST_record, above,
                  but there are still a few corner cases, such as free variables
                  generated for branch delay slot contents. *)
               UNDISCH_ALL |>
               instantiate_free_var_hyp |>
               DISCH_ALL |>
               PURE_REWRITE_RULE [markerTheory.Abbrev_def] |>

               (* Clean everything up *)
               CONV_RULE Tools.cbv_CONV |>
               CONV_RULE (ONCE_DEPTH_CONV Target.sort_addresses) |>
               CONV_RULE (ONCE_DEPTH_CONV Target.sort_registers) |>
               UNDISCH_ALL
    in if hyp th <> []
       then let val s = String.concatWith "\n" (map term_to_string (hyp th))
                val s = if String.size s > 1024 then String.substring (s,0,1020) ^ "..." else s
            in failwith ("Pre-state did not satisfy all hypotheses:\n" ^ s)
            end
       else th
    end

local
    fun aux 0 _ = []
      | aux n instr_start =
        (term_to_string (Tools.cbv_eval ``^instr_start ^(numSyntax.term_of_int (n-1))``))::
        (aux (n-1) instr_start)
in
fun instr_start_strings n instr_start =
    rev (aux n instr_start)
end

val last_random_trace = ref ""

open Harness

datatype instr_source =
    Generate of int
  | Manual of (string * string * int) list
  | Generate_exns of int * int list
  | Manual_exns of (string * string * int * bool) list
  | Manual_all_exn of (string * string * int) list * int
  | Generate_all_exn of int * int

fun split_manual_exns is =
    let val (is,exns) = 
            ListPair.unzip (Lib.mapi (fn i => fn (a,b,c,d) => ((a,b,c),if d then [i] else [])) is)
    in (length is,is,List.concat exns,false)
    end

(* Give a reasonable suffix for the test number when we generate multiple tests
   for an instruction sequence *)
local
   fun digit i = chr (ord #"a" + i)
   fun aux i l =
     if i < 26 then implode (digit i::l) else aux ((i div 26)-1) (digit (i mod 26)::l)
in

fun name_variant i =
  if i < 0 then raise Subscript
  else aux i []
end

(* There's a version of this with detailed logging in logged_test. *)
fun construct_test gen instrs choices harness constraints =
    let
        val gen = case gen of SOME g => RandGen.replay g | NONE => RandGen.newgen ()
        val (n,instrs,exns,all_exn) =
            case instrs of Manual is => (length is, is, [], false)
                         | Generate n => (n, generate gen n [], [], false)
                         | Manual_exns is => split_manual_exns is
                         | Generate_exns (n,exns) => (n, generate gen n exns, exns, false)
                         | Manual_all_exn (is,exn) => (length is, is, [exn], true)
                         | Generate_all_exn (n,exn) => (n, generate gen n [exn], [exn], true)
        val () = report_instrs instrs
        val choices = choose gen instrs exns choices all_exn
        fun combine_solve (choices, ths) =
          let val () = report_choices choices
              val (th, mem_counter) = combine ths
              val () = print "Attempting to find pre-state.\n"
              val (s, instr_start, read_footprint, write_footprint) =
                  Prestate.mk_prestate th instrs mem_counter harness constraints
      (*      val bare_th = instantiate_th th s instr_start read_footprint *)
              (* Use n+1 so that we report where the harness starts *)
              val locs = instr_start_strings (n+1) instr_start
              val () = print ("Instruction locations: " ^ (String.concatWith "," locs) ^ "\n")
          in (s, th, instr_start, read_footprint, write_footprint)
          end
        fun map_report f [] = []
          | map_report f (h::t) =
            let val h' = f h in h'::(map_report f t) end
            handle e => (print (General.exnMessage e ^ "\n"); map_report f t)
    in (gen, instrs, map_report combine_solve choices)
    end

fun run_test debug gen0 instrs choices harness constraints =
    let
        val (gen, instrs, testcases) =
            construct_test gen0 instrs choices harness constraints
        fun complete_test (s, th, instr_start, read_footprint, write_footprint) =
          let val read_footprint' = sort (curry Arbnum.<) (Prestate.extract_footprint read_footprint)
              val write_footprint' = sort (curry Arbnum.<) (Prestate.extract_footprint write_footprint)
              val footprint = Tools.merge (curry Arbnum.<) read_footprint' write_footprint'
              val (bgmem, s) = Prestate.fill_in_state gen s footprint
              val () = print "Instantiating combined step theorem with state.\n"
              val full_th = instantiate_th th s instr_start read_footprint

              val () = print "Running hardware test and comparing:\n"
              val _ = HWTest.run_and_check debug bgmem s full_th harness
                                          (* TODO: handle exceptions *)
          in ()
          end handle e => print (General.exnMessage e ^ "\n")
        val () = List.app complete_test testcases
        val () = last_random_trace := RandGen.record gen
        val () = case gen0 of NONE => RandGen.dispose gen | _ => ()
    in ()
    end

(* Simple version which takes assembly *)
fun run_test_code debug code choices harness =
    let val instrs = Target.encode code
        val instrs = map (fn (h,n) => (h, "", n)) instrs
    in run_test debug NONE (Manual instrs) choices harness
    end

fun construct_test_code code choices harness =
    let val instrs = Target.encode code
        val instrs = map (fn (h,n) => (h, "", n)) instrs
    in construct_test NONE (Manual instrs) choices harness
    end

local
    val pre_times = ref NONE
in
fun record_smt () =
    let val post_times = Posix.ProcEnv.times ()
        val pre_times = Option.valOf (!pre_times)
        val smt_times = (Time.- (#cutime post_times, #cutime pre_times),
                         Time.- (#cstime post_times, #cstime pre_times))
    in Logging.smt_times smt_times
    end

(* Helper function to make sure that early failures make it into the
   subtest statistics when appropriate *)
fun outer_fail all_exn reason =
  let val () = if all_exn then Logging.new_subtest () else ()
      val res = Logging.fail reason
      val () = Logging.subtests_complete ()
  in res
  end

fun logged_test0 debug n exns all_exn harness constraints gen =
    let
        val start_time = Logging.start_time ()
        val instrs = generate gen n exns
        val () = Logging.new_test instrs exns (harness_to_src harness) start_time
        val choices = choose gen instrs exns NONE all_exn
        val num_choices = length choices
        val () = if all_exn then print (Int.toString num_choices ^ " subtests:\n") else ()
        fun subtest (choices, ths) =
          let val () = if all_exn then Logging.new_subtest () else Logging.split_timing ()
              val () = Logging.record_choices choices
              val () = report_instrs instrs
              val (th, mem_counter) = combine ths
              val () = Logging.split_timing ()
              val () = print "Attempting to find pre-state.\n"
              (* We assume that the only active children are for the SMT solver *)
              val () = pre_times := SOME (Posix.ProcEnv.times ())
              val (s, instr_start, read_footprint, write_footprint) =
                  Prestate.mk_prestate th instrs mem_counter harness constraints
              val () = record_smt ()
              (* Use n+1 so that we report where the harness starts *)
              val () = Logging.record_instr_locs (instr_start_strings (n+1) instr_start)
              val read_footprint' = sort (curry Arbnum.<) (Prestate.extract_footprint read_footprint)
              val write_footprint' = sort (curry Arbnum.<) (Prestate.extract_footprint write_footprint)
              val footprint = Tools.merge (curry Arbnum.<) read_footprint' write_footprint'
              val (bgmem, s) = Prestate.fill_in_state gen s footprint
              val () = Logging.record_random gen
              val () = Logging.split_timing ()
              val () = print "Instantiating combined step theorem with state.\n"
              val full_th = instantiate_th th s instr_start read_footprint
              val () = Logging.split_timing ()

              val () = print "Running hardware test and comparing:\n"
          in case HWTest.run_and_check debug bgmem s full_th harness of
                 NONE => Logging.success ()
               | SOME detail => Logging.fail (Logging.Mismatch detail)
          end
          handle Useless_instruction hex => Logging.fail (Logging.Useless_instruction hex)
               | Target.Bad_choices choices => Logging.fail (Logging.Bad_choices choices)
               (* The difference between these two is rather technical and unimportant *)
               | CombineSteps.Combination_impossible _ => Logging.fail Logging.Impossible_combination
               | StepSMT.Preconditions_impossible => Logging.fail Logging.Impossible_combination
               | Prestate.SMT_Unknown => (record_smt (); Logging.fail Logging.SMT_Unknown)
               | Prestate.SMT_Unsat => (record_smt (); Logging.fail Logging.SMT_Unsat)
               | (e as HWTest.Permanent_failure s) =>
                 (print ("Permanent target failure: " ^ exn_to_string e);
                  Logging.Misc_exn e;
                  raise Interrupt)
               (* The Interrupt clause doesn't seem to have any effect, but I don't know why... *)
               | Interrupt => (Logging.fail (Logging.Misc_exn Interrupt); raise Interrupt)
               | e => Logging.fail (Logging.Misc_exn e)
        val result = foldl (fn (t,b) => subtest t orelse b) false choices
        val () = Logging.subtests_complete ()
    in result
    end
    handle Useless_instruction hex => outer_fail all_exn (Logging.Useless_instruction hex)
         | Target.Bad_choices choices => outer_fail all_exn (Logging.Bad_choices choices)
         (* The Interrupt clause doesn't seem to have any effect, but I don't know why... *)
         | Interrupt => (outer_fail all_exn (Logging.Misc_exn Interrupt); raise Interrupt)
         | e => outer_fail all_exn (Logging.Misc_exn e)
end

fun logged_test debug n exns all_exns harness constraints =
    let val gen = RandGen.newgen ()
        val ok = logged_test0 debug n exns all_exns harness constraints gen
        val () = RandGen.dispose gen
    in ok
    end

fun iter (f:'a -> bool) x 0 = ()
  | iter f x n = (let val ok = f x in iter f x (if ok then n-1 else n) end)

fun logged_run dir debug n exns all_exns harness count constraints =
    let val constraints_str = String.concatWith ", " (map term_to_string constraints)
        val () = HWTest.reset debug (* Just in case it's in a strange state before CPU id *)
        val cpu_info = HWTest.cpu_id debug
        val () = Logging.setup dir
                               ("Options: " ^ machine_readable_options () ^
                                "\nAdditional user constraints: " ^ constraints_str ^ "\n" ^
                                "CPU ID\n" ^ cpu_info)
        val () = (iter (logged_test debug n exns all_exns harness) constraints count) handle Interrupt => ()
    in Logging.finish ()
    end
end

end
