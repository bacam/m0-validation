Randomised testing of L3 instruction set models
===============================================

This code generates testcases based on randomly selected sequences of
instruction to help validate instruction set models written in
[Anthony Fox's L3 language][l3], including the cycle timing
information.  It generates a short string of instructions to execute
(possibly including branches) then uses an SMT solver to find a
prestate in which they have some meaning, if one exists.

The models it works with so far are:

* [ARM Cortex M0 with timing information](#markdown-header-running-with-an-arm-cortex-m0)
* [A plain MIPS model](#markdown-header-running-with-the-plain-mips-model)
* [CHERI](#markdown-header-running-with-cheri)

In normal use, using the M0 model performs online testing against hardware.
With the other models the tests are checked against the simulator built from
the CHERI model for sanity and stored for later use with the hardware design.

Software versions
-----------------

These are the current versions of the software dependencies for test
generation.  Note that l3mips is only required for MIPS and CHERI
testing.

* PolyML 5.7
* HOL commit `943f7618c187708bf9f660a710997c0eb9cfa5e4` from 31st July 2017
* l3mips (for MIPS and CHERI testing)
  `cd72ce3181316c0ba840af0e162d7912e68cbc16`, last change 3rd August
  2017, using L3 2017-07-24

Publications
------------

### FMICS 2014

The precise version of this code used in the FMICS 2014 paper
"Randomised Testing of a Microprocessor Model Using SMT-Solver State
Generation", and the longer 2016 journal version in Science of
Computer Programming, is tagged `fmics2014`.  Many improvements have
been made since then, improving performance and supporting more
targets.

For completeness, the commits for each piece of software used in the
runs reported in the paper are given below:

Software  | Commit
--------- | ------------------------------------------
HOL       | `15af13b06d9444864196a80169f69580a3f87884`
OpenOCD   | `30fb9dd438b8253547258d6fb02d2a4201becaf9`
this code | `41a2ef121101b9b3c4baffb82b669de5f0b27f44`

### FMCAD 2016

Recent versions of the L3 tool generates slightly different code from
the examples presented in the paper, and the `master` branch of this
code reflects this.  To reproduce the examples, the `fmcad16` branch
can be used together with the following versions of other software
packages, where HOL and l3mips are taken from their github
repositories:

Package   | Version
--------- | -----------------
PolyML    | 5.5.2
HOL       | `d8be32e26ee45d5e00eacef83885cf4966d353e8` (with record size change, see below)
Yices 1   | 1.0.39
L3        | [2016-03-04](https://bitbucket.org/bacam/m0-validation/downloads/l3-2016-03-04.tar.bz2)
MLton     | `2010060` (from Ubuntu 16.04)
l3mips    | `aa6a642c6c8c08179372d2488fee9165d0809d20`
main code | fmcad16 branch

Running with an ARM Cortex M0
-----------------------------

To get started, first get the prerequisites:

* Build a recent copy of HOL4 from [the in-development sources][hol].
* Download [Yices 1][yices], unpack it and set up the environment
  variable to let HOL4 know where it is:

      `export HOL4_YICES_EXECUTABLE=...path to yices.../bin/yices`

* (optional) To actually communicate with a device requires
  [OpenOCD](http://openocd.sourceforge.net/).  At least version 0.8.0
  is required because the previous release, 0.7.0, has some temporary
  hacks which changed the `LR` register without warning.

Then:

* Run `ln -snf target-M0 target`
* Edit the `TARGET` line in `Holmakefile` to be `M0`
* Run `Holmake`

Then the `construction-demo` script can be used to show the results of
attempting to construct a single test.  (Sometimes the random choices
made during the process can lead to a dead-end, shown as a exception.)
For example,

    ./construction-demo 5

will attempt to produce a test case that is five instructions long.

A batch run of testing can be performed using the `make-batch` script.
For example, to produce 100 tests of 7 instructions start `openocd`
then run

    ./make-batch /tmp/test-outputs 7 100

and the output can be found in `/tmp/test-outputs`.  In particular,
the `summary` file gives an overview of what happened.  Note that
because some test generation attempts fail, it may need more than
100 attempts before it finds 100 actual tests.

Finally, for interactive use in HOL, you can look at the example
code in `scratch.sml`.

I've been using an
[STM32F0-Discovery](http://www.st.com/web/catalog/tools/FM116/SC959/SS1532/LN1199/PF253215)
board.  I start OpenOCD using

    openocd -f board/stm32f0discovery.cfg

For other devices the position and amount of SRAM can be changed in `Target.sml`.

I've also tested a NXP LPC11U14 on an LPCXpresso board, using the
programmer from the ST board because the onboard LPC-Link isn't
supported.  I used the OpenOCD changes currently in code review at
<http://openocd.zylin.com/#/c/1896/16>, together with the simple board
configuration in `lpc.cfg`.

Running with the plain MIPS model
---------------------------------

To get started, first get the prerequisites:

* Build a recent copy of HOL4 from [the in-development sources][hol]
* Download [Yices 1][yices], unpack it and set up the environment
  variable to let HOL4 know where it is:

    `export HOL4_YICES_EXECUTABLE=...path to yices.../bin/yices`

* Download [L3][l3] and build it.
* (optional) Download [l3mips][l3mips], and build the simulator:

    `make CAP=p256`

Then:

* Run `ln -snf target-MIPS target`
* Edit the `TARGET` line in `Holmakefile` to be `MIPS`
* Run `Holmake`

Then the `construction-demo` script can be used to show the results of
attempting to construct a single test.  (Sometimes the random choices
made during the process can lead to a dead-end, shown as a exception.)
For example,

    ./construction-demo 5

will attempt to produce a test case that is five instructions long.

A batch run of testing can be performed using the `make-batch` script.
For example, to produce 100 tests of 7 instructions run

    ./make-batch /tmp/test-outputs 7 100

and the output can be found in `/tmp/test-outputs`.  In particular,
the `summary` file gives an overview of what happened.  The tests are
run against the simulator as a sanity check.  Note that because some
test generation attempts fail, it may need more than 100 attempts
before it finds 100 actual tests.  For each successful generation
there are three files:

1. `testn.mem` which contains a boot rom containing the test,
2. `testn.reg` with the predicted final values for the registers,
3. `testn.instrs` which containts the instructions in the tests and
   their location in memory.

Finally, for interactive use in HOL, you can look at the example
code in `scratch.sml`.

Running with CHERI
------------------

To get started, first get the prerequisites:

* Build a recent copy of HOL4 from [the in-development sources][hol].

* Download [Yices 1][yices], unpack it and set up the environment
  variable to let HOL4 know where it is:

      `export HOL4_YICES_EXECUTABLE=...path to yices.../bin/yices`

* Download [L3][l3] and build it.
* Download [the CHERI model, l3mips][l3mips] and build both the
  simulator and the HOL model:

    `make CAP=p256`
    `make hol CAP=p256`

Then:

* Run `ln -snf target-CHERI target`
* Edit the `TARGET` line in `Holmakefile` to be `CHERI` and the
  `L3MIPS` line to point to the model
* Run `Holmake`

Then the `construction-demo` script can be used to show the results of
attempting to construct a single test.  (Sometimes the random choices
made during the process can lead to a dead-end, shown as a exception.)
For example,

    ./construction-demo 5

will attempt to produce a test case that is five instructions long.

A batch run of testing can be performed using the `make-batch` script.
For example, to produce 100 tests of 7 instructions run

    ./make-batch /tmp/test-outputs 7 100

and the output can be found in `/tmp/test-outputs`.  In particular,
the `summary` file gives an overview of what happened.  The tests are
run against the simulator as a sanity check.  Note that because some
test generation attempts fail, it may need more than 100 attempts
before it finds 100 actual tests.  For each successful generation
there are three files:

1. `testn.mem` which contains a boot rom containing the test,
2. `testn.reg` with the predicted final values for the registers,
3. `testn.instrs` which containts the instructions in the tests and
   their location in memory.

Finally, for interactive use in HOL, you can look at the example
code in `scratch.sml`.

## Files

File                 | Content
-------------------- | ----------------------------------------------------------
`scratch.sml`        | Work-in-progress file for interactive use
`Test.sml`           | The high-level testing code
`Generate.sml`       | Random instruction generation plus sanity checks
`testgen`            | Script to run sanity checks for `Generate.sml`
`SymExec.sml`        | Symbolic execution library
`InstrBehaviour.sml` | Extract instruction behaviour using `SymExec`
`CombineSteps.sml`   | Combine several step theorems from the model
`State.sml`          | Library routines for M0 model states
`StepSMT.sml`        | Produce problem for SMT solver and interpret results
`YicesSat.sml`       | Adapted from HolSmtLib to provide a satisfying assignment
`OpenOCD.sml`        | Interface to OpenOCD daemon, and hence the actual hardware
`HWTest.sml`         | Runs test case on the hardware and compares the results
`Target.sml`         | Target-specific information (including some of the test harness on CHERI)
`Logging.sml`        | Logs test results for batch mode testing
`Harness.sml`        | Code defining the parts of the test harness that the SMT solver is given

The `YicesSat.sml` file is derived from Tjark Weber's HolSmtLib.
Copyright information for this file can be found in `HolSmt-COPYRIGHT`.

I've added a few notes on adapting this testing to other models in `Adapting.md`.

## Options

Some target-specific options can be supplied to the
`construction-demo` and `make-batch` scripts using the `-o` option,
which takes a comma-separated list of options and settings.  For example:

    `-o align_sp_flag:off,full_memory_option:Partial`

Option                         | Purpose
------------------------------ | ----------------------------------------------------------------
`no_self_modification`         | Disallow self modifying code (default: on)
`align_sp_flag` (ARM)          | Add constraint that stack pointer(s) are aligned (default: on)
`full_memory_option`           | Put full memory into the HOL theorem (`FullHOL`, default),
                               | use full memory in SML, put footprint into theorem (`FullSML`),
                               | only put footprint into theorem, ignore rest of memory (`Partial`)
`use_stepLib` (plain MIPS)     | Whether to use Fox's verification library to extract instruction
                               | behaviour (`on`) or the built-in symbolic execution library (`off`)
`harness` (CHERI)              | Which harness variant to use (default `ERET`; alternative `KernelCapJump`)
`cu2` (CHERI)                  | Whether CHERI instruction coprocessor interface is on (default, `on`)
`start-mode` (CHERI)           | Processor mode test sequences will start in (`user`,`kernel`,`any`)
`strict-unspec` (CHERI)        | Fill unspecified bits with zero rather than randomly (default: `off`)

[l3]: http://www.cl.cam.ac.uk/~acjf3/l3/
[l3mips]: https://github.com/acjf3/l3mips/commits/master
[hol]: https://github.com/mn200/HOL
[yices]: http://yices.csl.sri.com/old/download-yices1.shtml
