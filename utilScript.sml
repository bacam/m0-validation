open HolKernel Parse boolLib bossLib

val _ = new_theory "util";

(* Rather than use the definition for BIND, we translate it into a let so
   that symbolic execution respects the structure *)
val bind_let = Q.store_thm (
  "bind_let",
  `BIND g (f:'b -> 'a -> 'c # 'a) (x:'a) = (let (y,z) = g x in f y z)`,
  EVAL_TAC THEN REFL_TAC)

val v2w_append = Q.store_thm (
 "v2w_append",
 `FINITE univ(:'a) /\ FINITE univ(:'b) /\
  (dimindex (:'a) = LENGTH x) /\ 
  (dimindex (:'b) = LENGTH y) /\
  (dimindex (:'c) = dimindex (:'a) + dimindex (:'b)) ==>
  (v2w (x ++ y) : 'c word = v2w x : 'a word @@ v2w y : 'b word)`,
  REWRITE_TAC [bitstringTheory.word_concat_v2w_rwt]
  THEN SIMP_TAC (srw_ss()++wordsLib.WORD_ss) [])

val v2w_field_w2v_gen = store_thm (
  "v2w_field_w2v_gen",
  ``!h l w:'a word.
   (dimindex (:'b) = SUC h - l) /\ dimindex (:'b) < dimindex (:'a) ==>
   (v2w (field h l (w2v w)) = (h >< l) w : 'b word)``,
   REPEAT STRIP_TAC THEN
   ASM_SIMP_TAC (arith_ss++wordsLib.WORD_ss)
     [bitstringTheory.length_w2v, GSYM bitstringTheory.extract_v2w, bitstringTheory.v2w_w2v])

val v2w_field_w2v_id = store_thm (
  "v2w_field_w2v_id",
  ``!h w:'a word. (SUC h = dimindex (:'a)) ==> (v2w (field h 0 (w2v w)) = w)``,
  REPEAT STRIP_TAC
  THEN ASM_SIMP_TAC arith_ss [bitstringTheory.length_w2v, bitstringTheory.field_id_imp, bitstringTheory.v2w_w2v])

fun save_thms name ths =
    let fun store i th = (save_thm (name ^ Int.toString i, th); ())
        val () = Lib.appi store ths
        val names = List.tabulate (length ths, fn i => name ^ Int.toString i)
    in adjoin_to_theory
          {sig_ps = SOME(fn ppstrm =>
             let val S = (fn s => (PP.add_string ppstrm s; PP.add_newline ppstrm))
             in S "val "; S name; S " : thm list"
             end),
           struct_ps = SOME(fn ppstrm =>
             let val S = PP.add_string ppstrm
                 fun NL() = PP.add_newline ppstrm
                 fun NMs [] = ()
                   | NMs [s] = (S s; NL())
                   | NMs (s::t) = (S s; S ","; NL(); NMs t)
             in
                S "val "; S name; S " = [";                  NL();
                NMs names;
                S "];"
             end)}
    end

(* This takes a little while.  We probably don't need all of these, but it would
   also be nice to have a more general conversion. *)
local
   val bytes = List.tabulate (8,(fn i => Psyntax.mk_var ("b" ^ int_to_string i, ``:word8``)))
   fun concat_list tms =
       let fun add (tm,NONE) = SOME tm
             | add (tm,SOME tm') = SOME (wordsSyntax.mk_word_concat (tm, tm'))
           val r = foldr add NONE tms
       in Option.valOf r
       end
   fun combine n m = concat_list (List.take (List.drop (bytes,7-n), n-m+1))
   val bytes_tm = combine 7 0
   fun thm n m =
       let val (n_tm, m_tm) = (numSyntax.term_of_int (8*n+7),numSyntax.term_of_int (8*m))
       in wordsLib.WORD_DECIDE ``(^n_tm >< ^m_tm) ^bytes_tm = ^(combine n m)``
       end
   fun count f 0 = [f 0]
     | count f n = (f n)::(count f (n-1))
in val byte_selects = List.concat (count (fn n => count (thm n) n) 7)
   val () = save_thms "byte_selects" byte_selects
end;

val FST_COND = store_thm(
       "FST_COND",
       ``!x x':'a. !y y':'b. !b.
         FST (if b then (x,y) else (x',y')) = if b then x else x'``,
       REPEAT STRIP_TAC
       THEN COND_CASES_TAC
       THEN SIMP_TAC std_ss [])

val SND_COND = store_thm(
       "SND_COND",
       ``!x x':'a. !y y':'b. !b.
         SND (if b then (x,y) else (x',y')) = if b then y else y'``,
       REPEAT STRIP_TAC
       THEN COND_CASES_TAC
       THEN SIMP_TAC std_ss [])

val _ = export_theory ();
