PolyML.Compiler.debug := true;

load "Dedecoder";

val model_inv = cheriTheory.inventory

val l = Dedecoder.exec model_inv [] ``Decode ^(Dedecoder.mk_word 32)``;

val bad = List.filter (not o Lib.can (SimpleBDD.term_to_dnf o list_mk_conj o map concl o fst)) l

val enc = time (Dedecoder.compute_dnf_encodings model_inv) ``Decode ^(Dedecoder.mk_word 32)``;

val instrs = map snd enc
val instrs = Lib.sort (fn x => fn y => Term.compare (x,y) = LESS) instrs;

fun skip x [] = []
  | skip x (h::t) = if x = h then skip x t else (h::t)

fun dups []  = []
  | dups [_] = []
  | dups (h1::h2::t) = if term_eq h1 h2 then h1::(dups (skip h1 t)) else dups (h2::t)

val dupinstrs = dups instrs


(* Checking that the patterns are correct *)

fun test instr hy =
  let val input = rhs (concl (SIMP_CONV bool_ss [ASSUME hy] (Dedecoder.mk_word 32)))
      val result = rhs (concl (computeLib.CBV_CONV SymExec.full_cmp ``Decode ^input``))
      (* Concrete numerical values can appear during simplification, so normalise
         everything to bitstrings. *)
      val result = rhs (concl (QCONV (DEPTH_CONV bitstringLib.n2w_v2w_CONV) result))
      val instr = rhs (concl (QCONV (DEPTH_CONV bitstringLib.n2w_v2w_CONV) instr))
  (* We match rather than compare because the pattern might fix some of the operands *)
  in Lib.can (match_term instr) result
  end

val all_good = List.all (fn (hys,instr) => List.all (test instr) hys) enc

val bad_enc =
    let val pairs = List.concat (map (fn (hys,instr) => map (fn x => (instr,x)) hys) enc)
    in List.filter (not o uncurry test) pairs
    end


(* Sampling instructions *)

val bdd_encs = Dedecoder.compute_bdd_encodings model_inv ``Decode ^(Dedecoder.mk_word 32)``;

val gen = RandGen.newgen()

fun bit () = hd (RandGen.bits gen 1)

val example = SimpleBDD.sample_bdd bit 32 (fst (hd bdd_encs))
val hex = GenerationTools.to_hex example

fun bits_to_term bs =
  bitstringSyntax.mk_v2w 
     (listSyntax.mk_list (map (boolSyntax.lift_bool ``:bool``) bs, ``:bool``),
      ``:32``)


