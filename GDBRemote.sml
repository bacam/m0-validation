structure GDBRemote =
struct

(* Some cheating is used here:
   - we assume that all sends will be reliable, so we should always get a "+"
     ack back immediately, whereas the spec allows for dodgy serial lines and
     retransmission.
   - we assume that notifications only come when we're expecting them, and so
     we assume a single packet arrives at once
   - no runlength encoding
 *)

exception ProtocolError of string
exception Timeout

(* For RISC-V spike only supports writing single registers at a time, and
   qemu bulk reads/writes (just for RISC-V, and we could probably lift the
   restriction if we really need to). *)

type connection = {
   sock : Socket.active INetSock.stream_sock,
   use_bulk_regs : bool,
   use_bin_memory : bool option ref
}

fun close (con : connection) =
    Socket.close (#sock con)

local open Word8 val z = fromInt 0 in
fun checksum v =
  Word8VectorSlice.foldr (op +) z v
end

val dollarw = Byte.charToByte #"$"
val hashw = Byte.charToByte #"#"
val dollarv = Byte.stringToBytes "$"
val hashv = Byte.stringToBytes "#"

fun hex_byte n =
  let val s = IntInf.fmt StringCvt.HEX n
  in StringCvt.padLeft #"0" 2 s
  end

fun raw_socket_read con maxsize timeout =
  case Socket.select {rds=[Socket.sockDesc (#sock con)], wrs=[], exs=[], timeout=SOME timeout} of
      {rds=[], wrs=[], exs=[]} => raise Timeout
    | _ => Socket.recvVec (#sock con, maxsize)

fun raw_send con v =
  let val sum = checksum (Word8VectorSlice.full v)
      val sum = Byte.stringToBytes (hex_byte (Word8.toLargeInt sum))
      val v' = Word8Vector.concat [dollarv,v,hashv,sum]
      val sz = Socket.sendVec (#sock con, Word8VectorSlice.full v')
      val () = if sz = Word8Vector.length v' then () else raise ProtocolError "short write"
      val r = raw_socket_read con 1 (Time.fromSeconds 1)
  in if Byte.bytesToString r = "+" then () else
     raise ProtocolError "raw_send: bad ack"
  end

(* More than long enough for our tests! *)
val initial_timeout = Time.fromSeconds 10

val ack = Word8VectorSlice.full (Byte.stringToBytes "+")

fun raw_read con =
  let open Word8Vector
      val pkt = raw_socket_read con 4096 initial_timeout
      val () = if sub (pkt, 0) = dollarw then () else raise ProtocolError "raw_read: bad start"
      (* If we haven't seen the end of the GDB packet, keep waiting with a short timeout *)
      fun continue prev =
        case Word8Vector.findi (fn (_,0wx23) => true | _ => false) prev of
            NONE => prev::(continue (raw_socket_read con 4096 (Time.fromSeconds 1)))
          | SOME (i,_) => [prev]
      val pkt = Word8Vector.concat (continue pkt)
      val delim =
          case Word8Vector.findi (fn (_,0wx23) => true | _ => false) pkt of
              NONE => raise ProtocolError "raw_end: no terminating #"
            | SOME (i,_) => i
      val content = Word8VectorSlice.slice (pkt, 1, SOME (delim-1))
      val sum_rec_v = Word8VectorSlice.slice (pkt, delim+1, SOME 2)
      val sum_rec =
          case Word8.fromString (Byte.unpackStringVec sum_rec_v) of
              NONE => raise ProtocolError "raw_read: badly formatted checksum"
            | SOME w => w
      val sum = checksum content
      val () = if Word8Vector.length pkt <= delim+3 then ()
               else print ("raw_read: extra data sent \"" 
                           ^ Byte.unpackStringVec (Word8VectorSlice.slice (pkt, delim+3, NONE))
                           ^ "\"\n")
  in if sum_rec = sum
     then let val _ = Socket.sendVec (#sock con, ack)
          in content
          end
     else raise ProtocolError "raw_read: bad checksum"
  end

fun test_protocol s =
  let val cmd = Byte.stringToBytes "p0"
      (* Fake value for proto, as raw_* projects sock out *)
      val con = {sock = s, use_bulk_regs = false, use_bin_memory = ref NONE}
      val () = raw_send con cmd
      val r = raw_read con
      val rs = Byte.unpackStringVec r
  in rs = ""
  end

fun connect () : connection =
    let val s = INetSock.TCP.socket()
        val ent = Option.valOf (NetHostDB.getByName "localhost")
        val addr = INetSock.toAddr(NetHostDB.addr ent, 1234)
        val _ = Socket.connect (s,addr)
        (* Clear any initial traffic; e.g., spike sends a halt message immediately *)
        val _ = case Socket.select {rds=[Socket.sockDesc s],
                                    wrs=[], exs=[], timeout=SOME (Time.fromSeconds 1)} of
                    {rds=[], ...} => ()
                  | _ => (Socket.recvVecNB (s,4096); ())
    in {sock = s, use_bulk_regs = test_protocol s, use_bin_memory = ref NONE}
    end

fun parsehex v =
  Word8Vector.tabulate
     (Word8VectorSlice.length v div 2,
      fn i =>
         case Word8.fromString
                 (Byte.unpackStringVec
                     (Word8VectorSlice.subslice (v,i*2,SOME 2)))
          of NONE => raise ProtocolError "Bad hex"
           | SOME x => x)

fun read_memory_chunk con (addr,length) =
  let val addrh = IntInf.fmt StringCvt.HEX addr
      val lengthh = Int.fmt StringCvt.HEX length
      val cmd = Byte.stringToBytes ("m" ^ addrh ^ "," ^ lengthh)
      val () = raw_send con cmd
      val r = raw_read con
      val () = if Word8VectorSlice.length r >= 2 then ()
               else raise ProtocolError "read_memory: response too short"
      val () = if Word8VectorSlice.length r = 3
               then raise ProtocolError ("read_memory: error " ^ Byte.unpackStringVec r)
               else ()
  in parsehex r
  end

fun read_memory con (addr,length) =
  let val numchunks = 1 + ((length-1) div 1024)
      fun chunk i =
        let val offset = i * 1024
            val length' = if offset + 1024 > length then length mod 1024 else 1024
        in read_memory_chunk con (addr+IntInf.fromInt offset,length')
        end
      val chunks = List.tabulate (numchunks,chunk)
  in Word8Vector.concat chunks
  end

local open Word8 in
fun hex d = if d < 0w10 then 0w48 + d else if d < 0w16 then 0w87 + d else raise Subscript
end

fun formathex v =
  Word8Vector.tabulate
     (2 * VectorSlice.length v,
      fn i =>
         let val x = VectorSlice.sub (v,i div 2)
             val y = if i mod 2 = 0 then Word8.div (x,0w16) else Word8.mod (x,0w16)
         in hex y
         end)

(* This isn't currently supported by spike *)
fun write_memory_hex con (addr,content) =
  let val addrh = IntInf.fmt StringCvt.HEX addr
      val lengthh = Int.fmt StringCvt.HEX (VectorSlice.length content)
      val contenth = formathex content
      val cmd = Byte.stringToBytes ("M" ^ addrh ^ "," ^ lengthh ^ ":")
      val () = raw_send con (Word8Vector.concat [cmd, contenth])
      val r = raw_read con
      val rs = Byte.unpackStringVec r
  in if rs = "OK" then ()
     else raise ProtocolError ("write_memory: bad response " ^ rs)
  end

fun binary_encode v =
  let fun enc (0wx23,t) = 0wx7d::0wx03::t
        | enc (0wx24,t) = 0wx7d::0wx04::t
        | enc (0wx7d,t) = 0wx7d::0wx5d::t
        | enc (x,t) = x::t
  in Word8Vector.fromList (VectorSlice.foldr enc [] v)
  end

fun write_memory_bin con (addr,content) =
  let val addrh = IntInf.fmt StringCvt.HEX addr
      val lengthh = Int.fmt StringCvt.HEX (VectorSlice.length content)
      val contentb = binary_encode content
      val cmd = Byte.stringToBytes ("X" ^ addrh ^ "," ^ lengthh ^ ":")
      val () = raw_send con (Word8Vector.concat [cmd, contentb])
      val r = raw_read con
      val rs = Byte.unpackStringVec r
  in if rs = "OK" then ()
     else raise ProtocolError ("write_memory: bad response " ^ rs)
  end

fun test_bin con addr =
  let val addrh = IntInf.fmt StringCvt.HEX addr
      val cmd = Byte.stringToBytes ("X" ^ addrh ^ ",0:")
      val () = raw_send con cmd
      val r = raw_read con
      val rs = Byte.unpackStringVec r
      val ok = rs = "OK"
      val () = #use_bin_memory con := SOME ok
  in ok
  end

fun write_memory con (addr,content) =
  let val bin =
          case !(#use_bin_memory con) of
              NONE => test_bin con addr
            | SOME b => b
      val size = Vector.length content
      val chunks = 1 + ((size-1) div 1024)
      fun chunk i =
        let val offset = i*1024
            val len = if offset+1024 > size then NONE else SOME 1024
        in
           (addr+IntInf.fromInt offset,
            VectorSlice.slice (content,offset,len))
        end
      val l = List.tabulate (chunks, chunk)
  in if bin
     then List.app (write_memory_bin con) l
     else List.app (write_memory_hex con) l
  end


fun digit_hex h =
  let val h = Word8.toLargeInt h in
     if h >= 48 andalso h <= 57 then h - 48
     else if h >= 97 andalso h <= 102 then h - 87
     else if h >= 65 andalso h <= 70 then h - 55
     else raise ProtocolError "Bad hex digit"
  end

fun read_hex_val bigendian v =
  let fun aux i acc =
        let val h1 = Word8VectorSlice.sub (v,i)
            val h2 = Word8VectorSlice.sub (v,i+1)
            val byte = 16*(digit_hex h1) + digit_hex h2
            val acc = byte::acc
        in if i = 0 then acc else aux (i-2) acc
        end
      val sz = Word8VectorSlice.length v
      val () = if sz mod 2 = 0 then () else raise ProtocolError "Odd hex"
      val () = if sz >= 2 then () else raise ProtocolError "Short hex"
      val bytes = aux (sz-2) []
      val bytes = if bigendian then bytes else rev bytes
  in foldl (fn (b,v) => b+256*v) 0 bytes
  end

fun read_register con bigendian reg =
  let val regh = Int.fmt StringCvt.HEX reg
      val cmd = Byte.stringToBytes ("p" ^ regh)
      val () = raw_send con cmd
      val r = raw_read con
  in read_hex_val bigendian r
  end

local
val cmd = Byte.stringToBytes "g"
in
fun read_registers_g con bigendian =
  let val () = raw_send con cmd
      val r = raw_read con
      val sz = Word8VectorSlice.length r
      fun aux i =
        if i = sz then [] else
        let val reghex = Word8VectorSlice.subslice (r,i,SOME 16)
            val reg = read_hex_val bigendian reghex
        in reg::(aux (i+16))
        end
  in aux 0
  end
end

fun read_registers con bigendian n =
  if #use_bulk_regs con
  then List.take (read_registers_g con bigendian, n)
  else List.tabulate (n,read_register con bigendian)

fun int_padded_hex bigendian n (i : IntInf.int) =
  let fun aux 0 i acc = if i > 0 then raise Overflow else acc
        | aux n i acc = aux (n-1) (i div 256) (i mod 256 :: acc)
      val bytes = aux n i []
      val bytes = if bigendian then bytes else rev bytes
      val hexbytes = map hex_byte bytes
  in String.concat hexbytes
  end

fun write_register con bigendian n (reg,v : IntInf.int) =
  let val regh = Int.fmt StringCvt.HEX reg
      val vh = int_padded_hex bigendian n v
      val cmd = Byte.stringToBytes ("P" ^ regh ^ "=" ^ vh)
      val () = raw_send con cmd
      val r = raw_read con
      val s = Byte.unpackStringVec r
  in if s = "OK" then ()
     else raise ProtocolError "write_register: bad response"
  end

fun write_registers_G con bigendian n l =
  let val l = map (int_padded_hex bigendian n) l
      val cmd = Byte.stringToBytes ("G" ^ String.concat l)
      val () = raw_send con cmd
      val r = raw_read con
      val s = Byte.unpackStringVec r
  in if s = "OK" then ()
     else raise ProtocolError "write_registers: bad response"
  end

fun write_registers con bigendian n l =
  if #use_bulk_regs con
  then write_registers_G con bigendian n l
  else Lib.appi (fn reg => fn v => write_register con bigendian n (reg,v)) l

fun continue con addr =
  let val addrh = IntInf.fmt StringCvt.HEX addr
      val cmd = Byte.stringToBytes ("c" ^ addrh)
      val () = raw_send con cmd
      val r = raw_read con
      val s = Byte.unpackStringVec r
  in if String.size s >= 1 andalso
        (String.sub (s,0) = #"S" orelse String.sub (s,0) = #"T") then ()
     else raise ProtocolError ("continue: unexpected response \"" ^ s ^ "\"")
  end

fun insert_breakpoint con ty addr kind =
  let val tyh = Int.fmt StringCvt.HEX ty
      val addrh = IntInf.fmt StringCvt.HEX addr
      val cmd = Byte.stringToBytes ("Z" ^ tyh ^ "," ^ addrh ^ "," ^ kind)
      val () = raw_send con cmd
      val r = raw_read con
      val s = Byte.unpackStringVec r
  in if s = "OK" then ()
     else raise ProtocolError ("insert_breakpoint: unexpected response \"" ^ s ^ "\"")
  end

fun remove_breakpoint con ty addr kind =
  let val tyh = Int.fmt StringCvt.HEX ty
      val addrh = IntInf.fmt StringCvt.HEX addr
      val cmd = Byte.stringToBytes ("z" ^ tyh ^ "," ^ addrh ^ "," ^ kind)
      val () = raw_send con cmd
      val r = raw_read con
      val s = Byte.unpackStringVec r
  in if s = "OK" then true
     else if String.isPrefix "E" s then false
     else raise ProtocolError ("insert_breakpoint: unexpected response \"" ^ s ^ "\"")
  end

fun string_cmd con s =
  let val () = raw_send con (Byte.stringToBytes s)
      val r = raw_read con
  in Byte.unpackStringVec r
  end

val int = Word8VectorSlice.full (Word8Vector.fromList [Word8.fromInt 3])

fun interrupt con =
  let val _ = Socket.sendVec (#sock con, int)
      val r = raw_read con
      val s = Byte.unpackStringVec r
  in if String.size s >= 1 andalso
        (String.sub (s,0) = #"S" orelse String.sub (s,0) = #"T") then ()
     else raise ProtocolError ("continue: unexpected response \"" ^ s ^ "\"")
  end

end
