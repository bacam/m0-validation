(* Some attempts to get useful a useful prestate using an SMT solver. *)

structure StepSMT :> StepSMT =
struct

exception Preconditions_impossible

open HolKernel Parse boolLib bossLib State


(* Alternatives for some definitions that aren't supported by the SMT
   translation *)

val word_bit_rw = prove(
  ``!b w:'a word. b <= dimindex (:'a) - 1 ==> (word_bit b w = w ' b)``,
  METIS_TAC [wordsTheory.word_bit_def]);

(* Avoids a round-trip through num, which I think would be awkward for SMT solving *)
val myb2w_def = testingTheory.myb2w_def;

val w2n8_rw = prove(
  ``w2n w = w2n8 w``,
  SRW_TAC [] [YicesSat.w2n8]);

val shift_carry_rw = prove(
  ``!x : word32. testbit 32 (shiftl (w2v x) n) = if (n > 0) /\ (n <= 32) then x ' (32 - n) else F``,
  SRW_TAC [ARITH_ss]
           [bitstringTheory.testbit,bitstringTheory.shiftl_def,bitstringTheory.length_pad_right] THEN
  Q.UNABBREV_TAC `n'` THEN SRW_TAC [ARITH_ss] [listTheory.PAD_RIGHT] THEN
  Cases_on `n <= 32` THEN
  SRW_TAC [ARITH_ss] [rich_listTheory.EL_APPEND1,rich_listTheory.EL_APPEND2,
                      bitstringTheory.length_w2v,wordsTheory.dimindex_32,bitstringTheory.el_w2v]);

val lsl_rw = prove(
  ``!w : 'a word. !n : 'b word. dimindex (:'b) <= dimindex (:'a) ==> (w << w2n n = w <<~ w2w n)``,
  SRW_TAC [] [wordsTheory.word_lsl_bv_def,wordsTheory.w2n_w2w]);

val asr_rw = prove(
  ``!w : 'a word. !n : 'b word. dimindex (:'b) <= dimindex (:'a) ==> ( w >> w2n n = w >>~ w2w n)``,
  SRW_TAC [] [wordsTheory.word_asr_bv_def,wordsTheory.w2n_w2w]);

val asr_n_rw = prove(
  ``!w : 'a word. !n:num. n < dimword (:'a) ==> (w >> n = w >>~ n2w n)``,
  SRW_TAC [] [wordsTheory.word_asr_bv_def,wordsTheory.w2n_w2w]);


val lsr_rw = prove(
  ``!w : 'a word. !n : 'b word. dimindex (:'b) <= dimindex (:'a) ==> (w >>> w2n n = w >>>~ w2w n)``,
  SRW_TAC [] [wordsTheory.word_lsr_bv_def,wordsTheory.w2n_w2w]);

(* We don't currently use this because YicesSat has a definition for #>>. *)
val ror_rw = prove(
  ``!w : 'a word. !n : 'b word. dimindex (:'b) <= dimindex (:'a) ==> (w #>> w2n n = w #>>~ w2w n)``,
  SRW_TAC [] [wordsTheory.word_ror_bv_def,wordsTheory.w2n_w2w]);

local
open wordsTheory

val dimindex_plus_1 = prove(
  ``(FINITE (UNIV:'a -> bool)) ==> (dimindex (:1+'a) = 1 + dimindex (:'a))``,
  (SIMP_TAC arith_ss [fcpTheory.index_sum,dimindex_1,fcpTheory.dimindex_def]) THEN
  (SRW_TAC [] [fcpTheory.dimindex_def]) THEN
  SIMP_TAC arith_ss []);

val dimword_plus_unit = prove(
  ``(FINITE (UNIV:'a -> bool)) ==> (dimword (:unit+'a) = 2 * dimword (:'a))``,
  (SIMP_TAC arith_ss [dimword_def,fcpTheory.index_sum,dimindex_1,fcpTheory.dimindex_def]) THEN
  (SRW_TAC [] [fcpTheory.dimindex_def,arithmeticTheory.EXP_ADD]) THEN
  SIMP_TAC arith_ss []);

in
val add_with_carry_result = prove(
  ``FST (add_with_carry (a,b,c)) = a + b + myb2w c : 'a word``,
(* Cargo culted, perhaps should slim down *)
  SRW_TAC [boolSimps.LET_ss]
    [GSYM word_add_def, add_with_carry_def,
     GSYM word_add_n2w, word_sub_def, WORD_NOT, myb2w_def]);

(* Worse, I've just copied the proof for this! *)
val overflow = prove(
  ``OVERFLOW (a:'a word) b c = (word_msb a  = word_msb b) /\ (word_msb a <> word_msb (a+b+ myb2w c))``,
  SRW_TAC [boolSimps.LET_ss]
    [GSYM word_add_def, add_with_carry_def,
     GSYM word_add_n2w, word_sub_def, WORD_NOT, myb2w_def]);

val carry = prove(
  ``!a b:'a word. !c. FINITE (UNIV:'a -> bool) ==>
    (CARRY_OUT a b c =
       word_msb ((word_join (0w:1 word) a) + (word_join (0w:1 word) b) + myb2w c))``,
  Cases THEN Cases THEN STRIP_TAC THEN
  (SIMP_TAC (arith_ss++boolSimps.LET_ss) [add_with_carry_def]) THEN
  (SRW_TAC [] [myb2w_def,word_join_0,word_add_n2w,word_msb_n2w_numeric,w2w_def,
               INT_MIN_def,dimindex_plus_1,dimword_plus_unit]) THENL
  [(`n + n' + 1 < 2 * dimword (:'a)` by DECIDE_TAC),
   (`n + n' < 2 * dimword (:'a)` by DECIDE_TAC)
  ] THEN
  (SRW_TAC [] [dimword_def]) THEN
  (SIMP_TAC arith_ss []));

val carry32 = prove(
  ``!a b:word32. !c.
    (CARRY_OUT a b c =
       word_msb ((word_join (0w:1 word) a) + (word_join (0w:1 word) b) + myb2w c))``,
  SRW_TAC [] [carry]);

end

fun early_rewrites th =
  (* wordsTheory.LSL_ADD will prevent lsl_rw working *)
  let val ss = simpLib.remove_theorems [``w : 'a word << m << n``] (srw_ss())
  in DISCH_ALL th |>
     SIMP_RULE ss [word_bit_rw,add_with_carry_result,overflow,carry,
                   shift_carry_rw,
                   lsl_rw, lsr_rw, asr_rw, (*ror_rw,*)
                   w2n8_rw] |>
     UNDISCH_ALL
  end

(* Flags for additional constraints *)
val no_self_modification_flag = ref true

val options =
[Tools.bool_option "no_self_modification" no_self_modification_flag]

(* See also stateLib.write_footprint; I'm not entirely happy with that exception
   handler.  The first couple of nested functions go through any auxiliary
   mem_step_n definitions introduced by CombineSteps to prevent blow-up due
   to aliasing. *)
fun extract_writes hyps tm =
    let fun find_hyp m =
            case List.find (fn tm =>
                               is_comb tm andalso 
                               rator tm = ``Abbrev`` andalso
                               is_eq (rand tm) andalso
                               lhs (rand tm) = m) hyps of
                NONE => failwith ("Unable to find memory variable " ^ term_to_string m ^ " in hypotheses")
              | SOME tm => rhs (rand tm)
        fun extract tm =
            let val (upd,m) = combinSyntax.strip_update tm
            in if is_var m then
                   if dest_var m = ("m", Target.memory_type)
                   then upd
                   else upd@(extract (find_hyp m))
               else failwith ("Expected memory variable \"m\", got " ^ term_to_string m)
            end
    in
        (case strip_comb tm of
             (tm, [v, rst]) =>
             if tm = Target.memory_field_update
             then (extract (combinSyntax.dest_K_1 v))@(extract_writes hyps rst)
             else extract_writes hyps rst
           | _ => [])
    end handle HOL_ERR {message = "not a const", ...} => []
fun write_footprint hyps x = map fst (extract_writes hyps x)


(* To handle some things properly, in particular to provide a simple translation
   of the Aligned predicate suitable for feeding to the solver, it's easier to
   generate a new term based on informal examination of the precondition.  We
   will end up checking that the values we get from SMT solving are good anyway,
   so we don't need a formal link here.

   Note that on some targets such as RISC-V, we add some memory range
   constraints during symbolic execution so that they don't get lost;
   in particular, loads to a constant zero register would lose track of
   the memory access otherwise.
 *)

local
    val low_high_mems =
        map (fn (start,size) =>
                let val low_mem = wordsSyntax.mk_wordi (start,Target.addr_size) in
                   (low_mem,``^(low_mem) + ^(wordsSyntax.mk_wordi (size,Target.addr_size))``)
                end) Target.ram_details
    fun check_mem addr =
        list_mk_disj
           (map
               (fn (low_mem,high_mem) =>``(^addr >=+ ^(low_mem)) /\ (^addr <+ ^(high_mem))``)
               low_high_mems)
    fun check_mem_access i = check_mem ``(memory_address : num -> ^(ty_antiq Target.addr_type)) ^(numSyntax.term_of_int i)``

    fun record_write i addr = ``write_address ^(numSyntax.term_of_int i) = ^addr``
    fun check_write_address i = check_mem ``(write_address : num -> ^(ty_antiq Target.addr_type)) ^(numSyntax.term_of_int i)``

    fun mapi f l =
        let fun aux _ [] = []
              | aux n (h::t) = f n h :: aux (n+1) t
        in aux 0 l
        end
in

val record_writes = mapi record_write

fun no_self_modification instr_lengths write_num =
    let val write = ``(write_address : num -> ^(ty_antiq Target.addr_type)) ^(numSyntax.term_of_int write_num)``
        fun aux i len =
            let val i = numSyntax.term_of_int i
                val instr_start = Target.code_addr_translate ``(instr_start ^i) : ^(ty_antiq Target.word_type)``
                val len = wordsSyntax.mk_wordi (Arbnum.fromInt len,Target.addr_size)
            in ``(^write < ^instr_start) \/ (^write >= ^instr_start + ^len)``
            end
    in mapi aux instr_lengths
    end

(* NB: assumes straight-line code, no jumps *)
fun add_harness n poststate h mem_counter =
    let val instrs = Harness.harness_instrs h
        val post_pc = Tools.cbv_restr_eval (Target.pc poststate)
        val m = mk_var ("m", Target.memory_type)
        fun sizeof ins = (Arbnum.toInt (wordsSyntax.size_of ins)) div 8
        fun add (ins,(i,off,constrs)) =
            let val i' = numSyntax.term_of_int (i+n)
                val mi = numSyntax.term_of_int (i+mem_counter)
                val off' = off+sizeof ins
                val off = wordsSyntax.mk_wordii (off, Target.word_size)
                val vaddr = ``^post_pc + ^off``
                val paddr = Target.code_addr_translate vaddr
                val inject = Harness.fix_instr poststate m vaddr ins
                val check  = check_mem paddr
            in (i+1, off', ``(instr_start ^i' = ^vaddr)``::
                           ``(memory_address ^mi = ^paddr)``::
                           check::inject@constrs)
            end
        val (_,_,constrs) = foldl add (0,0,[]) instrs
    (* Return the lengths of the new instructions, plus constraints *)
    in (mem_counter + length instrs, map sizeof instrs, constrs)
    end

fun trans_simple_thm th instr_lengths mem_counter harness =
    let val (hy,cn) = dest_thm th
        val (_,rhs) = dest_eq cn
        val write_footprint = write_footprint hy rhs
        val writes = length write_footprint
        val write_recording = record_writes write_footprint
        val write_checks = List.tabulate (writes, check_write_address)
        val (mem_counter, harness_lengths, harness_conds) =
            add_harness (length instr_lengths) rhs harness mem_counter
        val no_self_mod =
            if !no_self_modification_flag
            then List.concat (List.tabulate (writes,
                     (no_self_modification (instr_lengths@harness_lengths))))
            else []
        val read_checks = List.tabulate (mem_counter, check_mem_access)
        val target_checks = Target.additional_constraints th instr_lengths mem_counter Tools.cbv_restr_eval
    in (writes, harness_conds @
                no_self_mod @
                write_recording @ write_checks @
                read_checks @ 
                target_checks @ 
                hy)
    end
    handle e => raise wrap_exn "StepSMT" "trans_simple_thm" e
end

fun simplify_thm th =
    let val th = RecordUtils.INST_record (mk_var ("s", Target.state_type)) Target.basic_state th
        (* Simplify away bit vectors (into words), Aligned and trivial propositions *)
        val th = th |>
                    DISCH_ALL |>
                    (* Immediately do some computation to get rid of projections,
                       map lookups, ... because it can be more expensive to do it
                       in SIMP_RULE *)
                    CONV_RULE Tools.cbv_restr_CONV |>
                    REWRITE_RULE Target.smt_rewrites |>
                    CONV_RULE (DEPTH_CONV bitstringLib.v2w_n2w_CONV) |>
                    (* Most of the rewrites from the list of theorems now happen
                       in early_rewrites, but are still kept here in case one is
                       exposed between there and here.  With a little care we
                       could slim this list down (probably to word_bit_rw,
                       w2n8_rw and myb2w_def). *)
                    SIMP_RULE (srw_ss()) ([word_bit_rw,add_with_carry_result,overflow,carry,
                                           shift_carry_rw,
                                           lsl_rw, lsr_rw, asr_rw, (*ror_rw,*)
                                           w2n8_rw,
                                           testingTheory.word_srem_x_x,
                                           testingTheory.word_sdiv_x_x
                                          ] @ Target.smt_simps)
    in th
    end

fun cleanup_results simp comp =
  comp (simp (arith_ss++wordsLib.WORD_ss) [],
       (* Put shift by a constant back into bit vector form, and deal with any
          remaining uses of word_bit *)
        simp arith_ss [asr_n_rw, wordsTheory.dimword_32, wordsTheory.dimword_64,
                       wordsTheory.word_bit_def,
                       testingTheory.v2w_singleton, myb2w_def])

fun trans_thm th instr_lengths mem_counter harness constraints =
    (* Instantiate the state to remove irrelevant stuff and name components
       individually *)
    let val th = simplify_thm th |>
                 UNDISCH_ALL
        val () = if concl th = ``T`` then raise Preconditions_impossible else ()
        (* Get the preconditions *)
        val (write_counter, hol_preconds) = 
            trans_simple_thm th instr_lengths mem_counter harness
        val hol_preconds = hol_preconds @ constraints
        (* Make a single term *)
        val hol_precond = List.foldl mk_conj ``T`` hol_preconds
        val (hy,cc) = (hd o fst) (cleanup_results SIMP_TAC (op THEN) ([],hol_precond))
        val preamble_conds = Harness.preamble_constraints ()
    in (hy, List.foldl mk_conj cc preamble_conds)
    end
    handle e as HOL_ERR _ => raise wrap_exn "StepSMT" "trans_thm" e

fun trans_thm_for_test th =
    th |> SIMP_RULE arith_ss [] |> simplify_thm |> cleanup_results SIMP_RULE (fn (f,g) => g o f)




fun augment_state s assignments =
    let val subs = map (fn (x,y) => {redex = x, residue = y}) assignments
    in subst subs s
    end

(* Virtual and physical addresses, resp *)
val vmap_type = ``:num -> ^(Target.word_type)``
val pmap_type = ``:num -> ^(Target.addr_type)``

fun get_map name assignments =
    let val var = List.find (equal name o fst) assignments
        val arb = mk_arb (type_of name)
    in case var of
         SOME (_,v) => subst [{redex = name, residue = arb}] v
       | NONE => arb
    end

val instr_starts = get_map (mk_var ("instr_start", vmap_type))
val memory_addresses = get_map (mk_var ("memory_address", pmap_type))
val write_addresses = get_map (mk_var ("write_address", pmap_type))

end

(* So that we can see the variable name choices and temporary files for
   debugging purposes.
Feedback.set_trace "HolSmtLib" 4;
*)
