PolyML.Compiler.debug := true;
PolyML.line_length 100;
Globals.linewidth := 100;
Globals.show_assums := true;
load "SymExec";

SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (Shift (SLL (0w,0w,0w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (ArithI (ADDIU (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (ArithI (ADDI (1w,2w,3w))) s``;
SymExec.eval SymExec.exception_filter [] EvalBasics.std_hyp ``Run (ArithI (ADDI (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (ArithI (LUI (1w,2w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (ArithR (MOVN (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (ArithR (SUBU (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (Shift (SLL (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (MultDiv (MFHI 1w)) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (MultDiv (MTHI 1w)) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (MultDiv (MUL (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (MultDiv (MULT (1w,2w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (MultDiv ($DIV (1w,2w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (Branch (BEQ (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (CACHE (1w,2w,3w)) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (CP (MFC0 (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (ERET) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (Load (LB (1w,2w,3w))) s``;
SymExec.eval SymExec.exception_filter [] EvalBasics.std_hyp ``Run (Load (LB (1w,2w,3w))) s``;
SymExec.eval SymExec.exception_filter [] EvalBasics.std_hyp ``Run (Load (LDL (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (Load (LDL (1w,2w,3w))) s``;
SymExec.eval SymExec.exception_filter [] EvalBasics.std_hyp ``Run (Store (SB (1w,2w,3w))) s``;
SymExec.eval SymExec.exception_filter [] EvalBasics.std_hyp ``Run (Store (SDL (1w,2w,3w))) s``;
SymExec.eval (K false) [] EvalBasics.std_hyp ``Run (Trap (TEQ (1w,2w))) s``;

SymExec.eval SymExec.exception_filter [] EvalBasics.std_hyp ``Fetch s``;

(* Demonstrate the conditional splitting of a pattern match term (as I needed to
   add code for it a while ago, but don't have a concrete example of where it is
   needed), see 2016-03/30.md *)

SymExec.eval (K false) [] [] 
  ``let v = if b then x else y in 
    case v of  Raw _ => (T,s:cheri_state)
             | Cap _ => (F,s with c_gpr := (K 0w))``;
                 



load "InstrBehaviour";

InstrBehaviour.next "12345678";

InstrBehaviour.next_code "sb $3,2($1)";

InstrBehaviour.next_code "beql $1, $2, 128";

SymExec.eval SymExec.exception_filter [] EvalBasics.std_hyp ``LoadMemoryCap (BYTE, vAddr, DATA, LOAD, F) s``;



map (UNDISCH_ALL o SIMP_RULE (srw_ss()) [] o DISCH_ALL)
(SymExec.eval EvalBasics.std_hyp ``SND (Run (Load (LB (1w,2w,3w))) s) with <| log := ARB |>``);

map (UNDISCH_ALL o SIMP_RULE (srw_ss()) [] o DISCH_ALL)
(SymExec.eval EvalBasics.std_hyp ``SND (Run (Store (SB (1w,2w,3w))) s) with <| log := ARB |>``);


SymExec.eval EvalBasics.std_hyp ``SND (Run (LWC2 (CHERILWC2 (CLoad (1w,2w,3w,0w,0w,0w)))) s) with <| log := ARB |>``;

SymExec.eval EvalBasics.std_hyp ``Run (LDC2 (CHERILDC2 (CLC (1w,2w,3w,0w)))) s``;



fun run_test (name,f) =
    let val () = print ("Running " ^ name ^ ": ")
    in (if f ()
        then print "OK\n"
        else print "Mismatch\n") handle _ => print "Exception\n"
    end

val tests = [
   ("simple FOR",
    fn () =>
       let val [([],th)] =
               SymExec.eval (K false) [] EvalBasics.std_hyp
                             ``FOR (2,4,\x y.((),(x+FST y,SND y))) (5,s:cheri_state)``
       in term_eq (rhs (concl th)) ``((),14:num,s:cheri_state)``
       end),
   ("open ended FOR",
    fn () =>
       (SymExec.eval (K false) [] EvalBasics.std_hyp
                      ``FOR (2,n,\x y.((),(x+FST y,SND y))) (5,s:cheri_state)``;
        false)
       handle SymExec.TooHard s => s = "Non-constant FOR term"),
   ("nested case split safety limit",
    fn () =>
       (SymExec.eval (K false) [] EvalBasics.std_hyp
                      ``FOR (0,16,\n s. if f n
                                        then ((),(1  + FST s,SND s))
                                        else ((),(FST s,SND s))) (0:num,s:cheri_state)``;
        false)
       handle SymExec.TooHard s => String.isPrefix "Too many nested case splits" s)
];

app run_test tests;






