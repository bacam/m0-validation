structure SymExec =
struct

exception EvalAssert

exception TooHard of string

open Drule Term Thm Type Conv Rewrite Lib boolSyntax Feedback simpLib

fun assert f x = assert_exn f x EvalAssert

(* I suspect that I could replace these with Q.AP_TERM *)

fun AP_FST th =
    let val (ty1, ty2) = (pairSyntax.dest_prod o type_of o lhs o concl) th
        val fst_tm = inst [alpha |-> ty1, beta |-> ty2] pairSyntax.fst_tm
    in AP_TERM fst_tm th
    end

fun AP_SND th =
    let val (ty1, ty2) = (pairSyntax.dest_prod o type_of o lhs o concl) th
        val snd_tm = inst [alpha |-> ty1, beta |-> ty2] pairSyntax.snd_tm
    in AP_TERM snd_tm th
    end

fun clean_hyps th =
    let fun clean (tm, th) =
            if is_eq tm andalso term_eq (lhs tm) (rhs tm)
            then PROVE_HYP (REFL (lhs tm)) th
            else if term_eq tm T
            then PROVE_HYP boolTheory.TRUTH th
            else th
    in foldl clean th (hyp th)
    end

(* Check that we're not using the big records feature - I need to look at
   it more closely to see if it's buggy, unsuitable, or just in some form
   I don't quite understand. *)
val () = if List.null (TypeBase.updates_of EvalBasics.state_ty)
         then (print ("\n\nThere do not appear to be any fields in the state record type.\n" ^
                      "This may be due to HOL's big records feature, try increasing the\n" ^
                      "value for Datatype.big_record_size from 30 to 50 in\n" ^
                      "HOL/examples/l3-machine-code/common/Import.sml\n\n");
               failwith "Bad state record type")
         else ()

(* Is the type a computation - in our case that means it returns a tuple with
   a state record at the end.  We also support "state option", which (AFAIK)
   doesn't appear in the model but does appear in the n-steps definitions we
   use. *)
fun carries_state_type ty =
    let val return_ty = List.last (pairSyntax.strip_prod ty)
    in return_ty = EvalBasics.state_ty
       orelse (optionSyntax.is_option return_ty
               andalso optionSyntax.dest_option return_ty = EvalBasics.state_ty)
    end

(* Replacement for broken pairSyntax.unstrip_pair, e.g.
     pairSyntax.unstrip_pair ``:('a#'b)#'c`` (pairSyntax.strip_pair ``(ARB:'a#'b,y:'c)``)
   fails. *)
fun unstrip_pair _ [] = Feedback.failwith "unstrip_pair unable"
  | unstrip_pair ty (h::t) =
    if ty = type_of h then (h,t)
    else case total pairSyntax.dest_prod ty
          of SOME (ty1, ty2) =>
             let
                val (ltm, vs1) = unstrip_pair ty1 (h::t)
                val (rtm, vs2) = unstrip_pair ty2 vs1
             in
                (pairSyntax.mk_pair (ltm, rtm), vs2)
             end
           | NONE => (h,t)

fun extract_state tm =
    let val tms = pairSyntax.strip_pair tm
        fun aux [] = Feedback.failwith "Unable to locate state term"
          | aux [s] =
            let val _ = assert (fn ty => ty = EvalBasics.state_ty) (type_of s)
                val v = genvar EvalBasics.state_ty
            in (s,v,[v])
            end
          | aux (h::t) =
            let val (s,v,t') = aux t
            in (s,v,h::t')
            end
        val (s,v,tms') = aux tms
        val (tm',left_over) = unstrip_pair (type_of tm) tms'
        val _ = assert List.null left_over
    in (s,v,tm')
    end

local open utilLib in
val word_extract_concat_CONV =
    SIMP_CONV (boolSimps.bool_ss++extract_extract_ss++extract_concat_ss++wordsExtraLib.word_extract_concat_join_ss) []
end

(* Conversion that gathers up exploded "v2w"s into concatentations, e.g.
     v2w [word_bit 7 x.ExcCode; word_bit 6 x.ExcCode; ...
     = x.ExcCode @@ ...
   to make the register theorems nicer. *)
local
   open wordsSyntax numSyntax Arbnum

   fun mk n m v =
       let val size = fcpSyntax.mk_numeric_type (n-m+one)
       in if m = zero andalso size = wordsSyntax.dest_word_type (type_of v)
          then v
          else mk_word_extract (mk_numeral n, mk_numeral m, v, size)
       end

fun mk_word_concat_list [] = failwith "empty word list"
  | mk_word_concat_list [tm] = tm
  | mk_word_concat_list (tm::tms) = mk_word_concat (tm, mk_word_concat_list tms)

fun dest_word_bit tm =
    if not (is_comb tm) then NONE else
    let val (f,args) = strip_comb tm
    in
       if fst (dest_const f) = "word_bit"
       then case args of
                [n,v] => if is_numeral n
                         then SOME (dest_numeral n, v)
                         else NONE
              | _ => failwith "Bad word_bit"
       else NONE
    end

fun mk_rvec l = bitstringSyntax.mk_v2w (listSyntax.mk_list (rev l, bool),
                                       fcpSyntax.mk_int_numeric_type (length l))

fun start [] = []
  | start (tm::tms) =
    case dest_word_bit tm of
        NONE => vec [tm] tms
      | SOME (n,v) => combine n n v tms
and vec acc [] = [mk_rvec acc]
  | vec acc (tm::tms) =
    case dest_word_bit tm of
        NONE => vec (tm::acc) tms
      | SOME (n,v) => (mk_rvec acc)::(combine n n v tms)
and combine n m v [] = [mk n m v]
  | combine n m v (tm::tms) =
    case dest_word_bit tm of
           NONE => (mk n m v)::(vec [tm] tms)
         | SOME (m',v') => if term_eq v v' andalso m' = m-one
                           then combine n m' v tms
                           else (mk n m v)::(combine m' m' v' tms)
in
fun v2w_list_conv tm =
    if bitstringSyntax.is_v2w tm then
    let val (l,_) = listSyntax.dest_list (rand tm)
        val len = length l
        val () = if Int.< (len,2) then failwith "no point" else ()
        val l' = start l
    in if length l' = 1 andalso bitstringSyntax.is_v2w (hd l')
       then failwith "No better"
       else let val rh = mk_word_concat_list l'
            in blastLib.BBLAST_PROVE (mk_eq (tm,rh))
            end
    end
    else failwith "Not v2w"
end


(* The reg'... definitions aren't great for symbolic use, but utilsLib can
   already give us nice versions, especially if we use the conversion above
   to gather the bits of fields together. *)
local
  val consts = #C EvalBasics.inventory
  val reg_consts = List.filter (String.isPrefix "reg'") consts
  val _ = assert (List.all (String.isSuffix "_def")) reg_consts
in
  val regs = map (fn s => String.substring (s,4,String.size s - 8)) reg_consts
  val reg_thms = map (utilsLib.mk_reg_thm EvalBasics.thy) regs
(* Recent versions of L3 provide this directly
  val reg_thms = map (CONV_RULE ((QCONV o STRIP_QUANT_CONV o RHS_CONV) v2w_list_conv)) reg_thms
*)
end

(* I tried to use computeLib.scrub_const to get rid of the definitions for terms
   in opaque, but it seems to be too late by then: it has already noticed and
   reduced the terms in other definitions.  Instead, build the compset from scratch.

   We make two versions, one without let so that we can handle it specially, and a
   full one that we can use when we're no longer interested in a term.
 *)

local open wordsSyntax in
  val words_opaque = [word_sdiv_tm, word_srem_tm, word_div_tm, word_mod_tm, word_1comp_tm,
                      add_with_carry_tm]
end

local open wordsTheory in
  (* There are some generally undesirable rewrites involving n2w that we should
     avoid; I've already encountered the first doing
       0w && n2w n = n2w (BITWISE 64 $/\ 0 n) *)
  val words_scrub_thms = [word_and_n2w,word_or_n2w,word_nor_n2w,word_xnor_n2w,word_xor_n2w]
end

(* Use UNCURRY rather than pair_CASE because it has the state value as the last
   argument, which we assume is the case throughout.  We only do this for state
   and allow other pairs to be split up by introducing new hypotheses. *)
val pair_CASE_UNCURRY =
    INST_TYPE [gamma |-> EvalBasics.state_ty]
              (REWRITE_RULE [GSYM pairTheory.UNCURRY] pairTheory.pair_CASE_def);

val extra_thms = EvalBasics.extra_thms @ [
   pair_CASE_UNCURRY,
   pairTheory.UNCURRY_DEF,
   optionTheory.IS_NONE_EQ_NONE, optionTheory.NOT_IS_SOME_EQ_NONE,
   boolTheory.DE_MORGAN_THM,
   combinTheory.UPDATE_EQ,
   utilTheory.FST_COND,
   utilTheory.SND_COND
]

(* These theorems push conditionals down into the state, but we only really want
   that for map fields, such as memory and registers.  Doing it for other fields
   can delay necessary case splits until quite late in the process.

   Hence we filter the theorems down to the generic ones (that do not mention
   the state) and the ones that update function fields. *)
local
  val all_cond_updates = utilsLib.mk_cond_update_thms [EvalBasics.state_ty]
  fun is_interesting th =
      let val tys = (map type_of o fst o strip_forall o concl) th
          fun nice ty = is_type ty andalso fst (dest_type ty) = "fun"
      in (not (List.exists (fn ty => ty = EvalBasics.state_ty) tys)) orelse List.exists nice tys
      end
  val if_conj = trace ("metis",0) Tactical.prove
                   (``(if b1 then if b2 then x else y else y:'a) = if b1 /\ b2 then x else y:'a``,
                    metisLib.METIS_TAC [])
in
val cond_updates = if_conj::(List.filter is_interesting all_cond_updates)
end

local
  open utilsLib Tactical computeLib Parse

  val mips_rws = theory_rewrites (reg_thms@EvalBasics.extra_defs,
                                  filter_inventory EvalBasics.opaque EvalBasics.inventory);
  (* The particular rewrites that this helped with (ones that appeared to
     return a new state, but which was projected away) no longer occur with
     L3 2016-05-19, and this doesn't work with the closest equivalents, so
     I've turned it off.

  (* For general reduction we want to specialise the list of definitions from
     the model that we use so that we avoid exposing monadic functions.
     At the moment this is providing only small improvements (and slowing down
     a few cases), so it may be worth revisiting in future. *)
  fun specialise_rw th =
      let val tm = concl th |> strip_forall |> snd |> lhs
          val ty = type_of tm
          val (args,r) = wfrecUtils.strip_fun_type ty
          val tys = wfrecUtils.strip_prod_type r
      in if List.last tys = EvalBasics.state_ty
         then if length args = 0
              (* L3 threads and returns the state through some read-only
                 functions (eg, cheriTheory.HI_def), but discards the "new"
                 state with FST when calling them, so generate specialised
                 rewrites for them to include in the compset *)
              then SOME (SPEC_ALL th |> AP_FST |> GEN_ALL)
              (* If it has arguments then it is probably read/write, so
                 don't include it at all *)
              else NONE
         else NONE (*SOME th*)
      end
  val mips_gen_rws = List.mapPartial specialise_rw mips_rws
  *)
  val mips_gen_rws = []
  val tys = theory_types EvalBasics.inventory

  (* Try to get rid of conditional guards on words, so that trivial aliasing checks
     are removed *)
  fun cond_word_CONV tm =
      let val (g,t,t') = dest_cond tm
          val g_th = wordsLib.WORD_ARITH_CONV g
          val r = rhs (concl g_th)
      in if term_eq r boolSyntax.T orelse term_eq r boolSyntax.F
         then PURE_ONCE_REWRITE_CONV [g_th] tm
         else raise UNCHANGED
      end handle UNCHANGED => Feedback.failwith "Can't convert guard"

  (* We can't reduce ARB to a pair, so we'll just have to settle for projections.
     This appears in a few places where an exception has been signalled and the
     values will eventually be thrown away. *)
  fun is_arb_like tm =
      is_arb tm orelse
      (pairSyntax.is_fst tm andalso (is_arb_like (rand tm))) orelse
      (pairSyntax.is_snd tm andalso (is_arb_like (rand tm)))

  fun UNCURRY_ARB_CONV tm =
      if is_arb_like (rand tm)
      then PURE_ONCE_REWRITE_CONV [pairTheory.UNCURRY] tm
      else failwith "uncurry not applied to something ARB-like"

  fun construct remove_let =
      let val cmp = wordsLib.words_compset ()
          val () = computeLib.scrub_thms words_scrub_thms cmp
          (* The current CHERI decoder uses some HOL integers *)
          val () = intReduce.add_int_compset cmp
          val () = integer_wordLib.add_integer_word_compset cmp
          val () = if remove_let then computeLib.scrub_const cmp let_tm else ()
          val () = List.app (computeLib.scrub_const cmp) (Tools.string_opaque@words_opaque)
          val () = add_base_datatypes cmp
          val () = add_datatypes tys cmp
          (* FOR_def expands horribly on non-concrete terms, so restrict it *)
          val () = computeLib.scrub_const cmp state_transformerSyntax.for_tm
          val () = computeLib.add_conv (state_transformerSyntax.for_tm,1,utilLib.for_CONV) cmp
          val () = computeLib.scrub_const cmp state_transformerSyntax.bind_tm
          val () = computeLib.add_thms [utilTheory.bind_let] cmp

          (* For now there are specialised rewrites for this in the CHERI version of
             EvalBasics. *)
          val () = computeLib.scrub_const cmp wordsSyntax.bit_field_insert_tm

          val () = computeLib.add_conv (boolSyntax.bool_case,3,cond_word_CONV) cmp
          val () = computeLib.add_thms (if remove_let then mips_gen_rws else mips_rws) cmp
          val () = computeLib.add_thms extra_thms cmp
          val () = 
              computeLib.add_conv (bitstringSyntax.v2w_tm, 1, bitstringLib.v2w_n2w_CONV)
                                  cmp
          val () = computeLib.add_conv (pairSyntax.uncurry_tm, 2, UNCURRY_ARB_CONV) cmp
      in cmp
      end

  fun name_arity th =
      let val (name,args) = (strip_comb o lhs o concl) th
      in (name,length args,th)
      end

in
val cmp = construct true
val full_cmp = construct false
val function_rw_info = map (name_arity o Drule.SPEC_ALL) mips_rws
end;

val byte_selects = utilTheory.byte_selects

(* Theorems and information to help build the overall result *)
val let_thm = trace ("metis",0) Tactical.prove(
       ``!u v P t. (u : 'a = v) ==> (P v = t : 'b) ==> (LET P u = t)``,
       metisLib.METIS_TAC []);

val cond_thm = trace ("metis",0) Tactical.prove(
       ``((b = b1) ==> b1 ==> (tt = t : 'a) ==> ((if b then tt else tf) = t)) /\
         ((~b = b2) ==> b2 ==> (tf = t) ==> ((if b then tt else tf) = t))``,
       metisLib.METIS_TAC []);

val comb_thm = trace ("metis",0) Tactical.prove(
       ``!x x' f t. (x : 'a = x') ==> (f x' = t : 'b) ==> (f x = t)``,
       metisLib.METIS_TAC []);

fun ASSUME_CONJ tm =
    if is_conj tm then
       let val (l,r) = dest_conj tm
           val (l,r) = (ASSUME_CONJ l, ASSUME_CONJ r)
       in CONJ l r
       end
    else ASSUME tm

fun AP_THM_L th l =
  foldl (fn (tm,th) => AP_THM th tm) th l

val update_fns = map rator (utilsLib.update_fns EvalBasics.state_ty)

val optionSyntax_is_is_some = can optionSyntax.dest_is_some

infixr 3 ANDPERHAPSC;

fun (conv1 ANDPERHAPSC conv2) t =
    let val th1 = conv1 t
    in TRANS th1 (conv2 (rhs (concl th1))) handle UNCHANGED => th1
    end

fun WHENC p conv t =
    if p t then conv t else raise UNCHANGED

(* Adapted from Conv; apply cnv on every hypothesis and the rhs of concl,
   also tweaked to allow new assumptions to be added *)
local
   fun cnvMP eqth impth =
      let
         open boolSyntax
         val impth = DISCH (lhs (concl eqth)) impth
         val tm = snd (dest_imp (concl impth))
         val imp_refl = REFL implication
      in
         UNDISCH (EQ_MP (MK_COMB (MK_COMB (imp_refl, eqth), REFL tm)) impth)
      end
in
   fun MAP_HYP_RHS_THM cnv th =
      let
         val (hyps, c) = dest_thm th
         val cth = (Conv.RHS_CONV cnv) c
         val hypths = map cnv hyps
      in
         UNDISCH_ALL (itlist cnvMP hypths (EQ_MP cth th))
      end
      handle e as (HOL_ERR _) => raise (Feedback.wrap_exn "" "MAP_HYP_RHS_THM" e)
end

fun freshen_term tm =
    let val fv = free_vars tm
        val subs = map (fn x => x |-> genvar (type_of x)) fv
    in subst subs tm
    end

(* Some ad-hoc profiling
val lastcall = ref NONE : (Time.time * term) option ref
val worstcall = ref NONE : (int * term) option ref
fun tick tm =
  let val t' = Time.now ()
      val () = case !lastcall of NONE => ()
                               | SOME (t,_) =>
                                 let val d = Time.toSeconds (Time.- (t',t)) in
                                 case !worstcall of
                                     NONE => worstcall := SOME (d,tm)
                                   | SOME (d',_) =>
                                     if d > d' then worstcall := SOME (d,tm) else ()
                                 end
  in lastcall := SOME (t',tm)
  end
*)

(* Like the hypotheses, we need to reduce these to make sure they match partly
   evaluated terms *)
val junk_conds = map (rhs o concl o computeLib.CBV_CONV cmp) EvalBasics.junk_conds;

(* To prevent blow-up, panic if we see too many nested case splits. *)
val nested_splits_limit = 10

fun chomp filter_fn extra_fn_rws nested_splits must_reduce_fn (hyps,shyps,t0) =
  let (*val () = tick t0*)
     val () = if nested_splits > nested_splits_limit
              then raise TooHard ("Too many nested case splits (" ^ Int.toString nested_splits
                                  ^ " > " ^ Int.toString nested_splits_limit ^ ")")
              else ()
      fun chomp_cases n = chomp filter_fn extra_fn_rws (nested_splits + (if n > 1 then 1 else 0))
      val chomp = chomp filter_fn extra_fn_rws nested_splits
      val () = assert (fn () => carries_state_type (type_of t0)) ()
      fun cnv cmp = (QCONV o Conv.REPEATC o Conv.CHANGED_CONV)
                       (REWRITE_CONV shyps
                        THENC computeLib.CBV_CONV cmp
                        (* Push conditionals into state updates (especially for memory) to
                           avoid quadratic case-splits.
                           Too expensive to do everywhere, so try a top-level rewrite and
                           only do the whole term if that had an effect. *)
                        THENC ((GEN_REWRITE_CONV TRY_CONV empty_rewrites cond_updates
                                ANDPERHAPSC REWRITE_CONV cond_updates)
                               (* Expose and eliminate trivial aliasing checks early to
                                  prevent quadratic blowup *)
                               ANDPERHAPSC computeLib.CBV_CONV cmp)
                        (* We can't use INST_REWRITE_CONV for hyps due to free vars with a
                           specific meaning.  We use the simplifier rather than REWRITE_CONV
                           for rewrites that need higher-order matching or are conditional. *)
                        THENC SIMP_CONV bossLib.std_ss (hyps@EvalBasics.eval_simp_ths)
                        THENC word_extract_concat_CONV
                        THENC utilsLib.INST_REWRITE_CONV (byte_selects@EvalBasics.mem_rws))

      (* When we case split on a conditional guard we want to make sure that
         the assumption we produce is in a nice form.  We return an equality
         theorem, which reduces the guard into the term that is returned,
         plus a theorem for rewriting, and a theorem for linking the guard
         term with the assumptions. *)
      fun cnv_guard tm =
          let val th = cnv full_cmp tm
              val tm = rhs (concl th)
          in if optionSyntax_is_is_some tm
             then let val tm' = optionSyntax.dest_is_some tm
                      val v = genvar (optionSyntax.dest_option (type_of tm'))
                      val hy = ASSUME ``^tm' = SOME ^v``
                      val th' = SIMP_PROVE (BasicProvers.srw_ss()) [hy] ``IS_SOME ^tm'``
                  in (th, tm, [hy], th')
                  end
             else let val th' = ASSUME_CONJ tm
                  in (th, tm, map ASSUME (hyp th'), th')
                  end
          end
      fun mk_cond t =
          let val (guard,t1,t2) = dest_cond t
              val (guard_eq, guard', guard_hyps, guard_thm)  = cnv_guard guard
              val (nguard_eq, nguard', nguard_hyps, nguard_thm)  = cnv_guard (mk_neg guard)
              val ty = type_of t1
              val cth = INST_TYPE [alpha |-> ty] cond_thm
              val cth = INST [``b:bool`` |-> guard,
                              ``b1:bool`` |-> guard',
                              ``b2:bool`` |-> nguard',
                              mk_var ("tt",ty) |-> t1,
                              mk_var ("tf",ty) |-> t2] cth
              val (th1, th2) = CONJ_PAIR cth
              val gt = MP (MP th1 guard_eq) guard_thm
              val gf = MP (MP th2 nguard_eq) nguard_thm
              (* We might have managed to resolve the guard to T or F, so we
                 don't need the F cases; we can drop them by returning ARB *)
              (* TODO: get rid of the T hypothesis which appears when this
                       happens *)
              fun good_hyps th =
                  List.all (fn tm => tm <> boolSyntax.F) (hyp th)
              val t1 = if List.all good_hyps guard_hyps then t1 else mk_arb ty
              val t2 = if List.all good_hyps nguard_hyps then t2 else mk_arb ty
          in (t1,t2,guard_hyps,nguard_hyps,gt,gf)
          end
                   
      val thm = cnv cmp t0
      val t = rhs (concl thm)

      fun function_rw t =
          let val (f,args) = strip_comb t
              val arity = length args
              val (_,a,th) =
                  case
                     List.find (fn (f',a',_) => same_const f f' andalso arity >= a')
                               (extra_fn_rws@function_rw_info)
                   of NONE => failwith "no rewrite"
                    | SOME x => x
              val th1 = if arity > a then AP_THM_L th (List.drop (args,a)) else th
              val th2 = utilsLib.INST_REWRITE_CONV1 th1 t
              val th3 = MAP_HYP_RHS_THM (cnv cmp) th2
          in (th3, rhs (concl th3))
          end

      val (thm,t) = if must_reduce_fn andalso term_eq t t0
                    then function_rw t handle HOL_ERR _ => (thm,t)
                    else (thm,t)

      (* We need to apply an extra shove to reduce terms like
         `if IE /\ ~EXL /\ ~ERL then ... else ...` when there's the negated
          hypothesis `~IE \/ EXL \/ ERL`. TODO: do we need something similar
          in cnv_guard? *)
      fun ifneg_CONV tm =
          let val (g,t,t') = dest_cond tm
              val th = SIMP_PROVE boolSimps.bool_ss hyps (mk_neg g)
          in ONCE_REWRITE_CONV [th] tm
          end
      val ifneg_ss = std_conv_ss {name = "ifneg", pats=[``if b then x:'a else y:'a``], conv=ifneg_CONV}

      (* For simplifying terms once we stop exploring; also allowed to deal with let terms *)
      fun final_simp t = (cnv full_cmp
                          THENC (SIMP_CONV (boolSimps.bool_ss++ifneg_ss++EvalBasics.late_ss) hyps
                                 ANDPERHAPSC cnv full_cmp)
                          (* This catches cases of memory updates that were
                             partially rewritten before, but need more to expose
                             and eliminate the aliasing checks.  TODO: make the
                             general conversion handle this more appropriately *)
                          THENC REWRITE_CONV cond_updates
                          (* Expose and eliminate trivial aliasing checks early to
                             prevent quadratic blowup *)
                          THENC computeLib.CBV_CONV full_cmp
                          (* Disabled for CHERI because it exposes HOL definitions
                             for (e.g.) word_1comp *)
                          (*THENC report_conv wordsLib.WORD_ARITH_CONV*)) t
      fun no_more t =
          (* Attempt to do any "late" rewrites here, those which
             may introduce hypotheses that need to be kept simple. *)
          let val th1 = utilsLib.INST_REWRITE_CONV EvalBasics.late_rws t
              val th2 = MAP_HYP_RHS_THM final_simp th1
          in [((map ASSUME (hyp th2))@hyps,[],th2)]
          end handle UNCHANGED => [(hyps,[],final_simp t)]

      (*val () = print_term t
      val () = print "\n---\n"*)
      val thms =
          if must_reduce_fn andalso term_eq t t0 then no_more t else
          if is_let t
          then let val (f,x) = dest_let t
               in if carries_state_type (type_of x) then
                     let val xs = chomp false (hyps,shyps,x)
                         val chomp = chomp_cases (length xs)
                         fun cont (hy,exs,th) =
                             let val r = rhs (concl th)
                                 val (s,v,r') = extract_state r
                                 val seqns = RecordUtils.fieldify s v (map concl shyps)
                                 val shyps' = map ASSUME seqns
                                 val ts = chomp false (hy,shyps'@shyps,mk_comb (f,r'))
                                 fun fixup (hy',exs',th') =
                                     let val th' = MAP_HYP_RHS_THM (cnv cmp) (INST [v |-> s] th')
                                     in (hy', exs@exs', MP (MP (ISPECL [x,r,f,rhs (concl th')] let_thm) th) th')
                                     end
                             in map fixup ts
                             end
                     in List.concat (map cont xs)
                     end
                  else
                     (* No state in first subterm, so reduce it, rebuild the whole term,
                        use GEN_BETA_CONV to deal with terms of pair type that we aren't
                        reduced to pair terms, then package everything up. *)
                     let val xs = no_more x
                         fun cont (hy,exs,x) =
                           let val t' = mk_comb (f,rhs (concl x))
                               val bth = PairedLambda.GEN_BETA_CONV t'
                               val t'' = rhs (concl bth)
                               val ts = chomp false (hy,shyps,t'')
                           in (x,exs,bth,ts)
                           end
                         val ts = map cont xs
                     in List.concat
                           (map (fn (xth,exs,bth,ts') =>
                                    map (fn (hy,exs',tth) =>
                                            (hy,exs@exs',MP (MP (ISPECL [x,rhs (concl xth),f,rhs (concl tth)] let_thm) xth) (TRANS bth tth))) ts') ts)
                     end
               end
          else if pairSyntax.is_pair t
          then let val (l,r) = pairSyntax.dest_pair t
                   val l' = final_simp l
                   val _ = assert carries_state_type (type_of r)
                   val rs = chomp false (hyps,shyps,r)
               in map (fn (hy,exs,r) => (hy,exs,PairRules.MK_PAIR (l',r))) rs
               end
          else if pairSyntax.is_snd t
          then let val t = pairSyntax.dest_snd t
                   val _ = assert carries_state_type (type_of t)
                   val ts = chomp false (hyps,shyps,t)
                   val do_snd =
                       CONV_RULE (RHS_CONV (PURE_ONCE_REWRITE_CONV [pairTheory.SND]))
               in map (fn (hy,exs,t) => (hy,exs,do_snd (AP_SND t))) ts
               end
          (* There are some things we just don't want to touch, e.g., UARTs.
             Introduce a hypothesis that gets rid of them once we have a
             reasonable term for the variables involved. *)
          else if is_cond t
                  andalso List.exists (fn tm => Lib.can (match_term tm) (#1 (dest_cond t)))
                                      junk_conds
          then let val hy = ASSUME (mk_neg (#1 (dest_cond t)))
               in chomp true (hy::hyps,shyps, t)
               end
          else if is_cond t
          then let val (t1,t2,ht,hf,gt,gf) = mk_cond t
                   val ts1 = chomp false (ht@hyps, shyps, t1)
                   val ts1 = map (fn (hy,exs,th) => (hy,exs,MP (INST [mk_var ("t",type_of t1) |-> rhs (concl th)] gt) th)) ts1
                   val ts2 = chomp false (hf@hyps, shyps, t2)
                   val ts2 = map (fn (hy,exs,th) => (hy,exs,MP (INST [mk_var ("t",type_of t2) |-> rhs (concl th)] gf) th)) ts2
               in ts1@ts2
          end
          else if TypeBase.is_case t
          then let val (_,v,cs) = TypeBase.dest_case t
                   (* Reduce the term that we're matching on, since we can use
                      the full compset on it now (TODO: should we beware of a
                      state record appearing here?) *)
                   val thm = cnv full_cmp v
                   val v' = rhs (concl thm)
                   val thm' = ONCE_REWRITE_CONV [thm] t
                   val t' = rhs (concl thm')
               in
                  (* If the term we're matching is a conditional, then break
                     it up and try again *)
                  if is_cond v'
                  then let val (t1,t2,ht,hf,gt,gf) = mk_cond v'
                           val th1 = cnv full_cmp t1
                           val th2 = cnv full_cmp t2
                           val gt = MATCH_MP gt th1
                           val gf = MATCH_MP gf th2
                           fun rw th = TRANS thm' (ONCE_REWRITE_CONV [th] t') 
                           val thm1 = rw gt
                           val thm2 = rw gf
                           val ts1 = chomp false (ht@hyps, shyps, rhs (concl thm1))
                           val ts2 = chomp false (hf@hyps, shyps, rhs (concl thm2))
                           fun fix thm (h,exs,thm') = (h,exs,TRANS thm thm')
                       in (map (fix thm1) ts1)@(map (fix thm2) ts2)
                       end
                       (* If the term we're matching is now a constructor we can
                          try again because the case will probably reduce now *)
                  else if TypeBase.is_constructor v' orelse
                          (is_comb v' andalso TypeBase.is_constructor (fst (strip_comb v'))) then
                     let val rs = chomp false (hyps, shyps, t')
                         fun fix (h,exs,thm'') = (h,exs,TRANS thm' thm'')
                     in map fix rs
                     end
                  else (* Otherwise a case split is all we can do *)
                     let fun branch (c,_) =
                             let val hy = ASSUME (mk_eq (v',freshen_term c))
                                 val thm'' = TRANS thm' (REWRITE_CONV [hy] t')
                                 val t'' = rhs (concl thm'')
                                 val rs = chomp false (hy::hyps,shyps,t'')
                                 fun fix (hyp',exs,thm''') =
                                     (hyp',exs,TRANS thm'' thm''')
                             in map fix rs
                             end
                     in List.concat (map branch cs)
                     end
               end
          else if is_comb t
          then let val (f,args) = strip_comb t
                   val f_name = (fst o dest_const) f
               (* Discard cases we don't want *)
               in if mem f_name EvalBasics.discard_fns
                  then []
                  else if filter_fn (f,args)
                  then []
                  else if same_const f state_transformerSyntax.for_tm
                  (* The case with constant bounds is done by for_CONV in cmp *)
                  then raise (TooHard "Non-constant FOR term")
                  (* For state updates we want to check the term that's updated
                     for interesting content. *)
                  else
                     let val exs = case EvalBasics.is_exception (f,args) of
                                              NONE => []
                                            | SOME ex => [ex]
                         val (f,arg) = dest_comb t
                     in if carries_state_type (type_of arg)
                        then
                           let val xs = chomp false (hyps,shyps,arg)
                               val chomp = chomp_cases (length xs)
                               (* TODO: really ought to update shyps here! *)
                               (* Insist that the term reduce to prevent loops *)
                               val ts = map (fn (hy,exs',x) => (x,exs',chomp true (hy, shyps, mk_comb (f,rhs (concl x))))) xs
                           in List.concat (map (fn (xth,exs',ts') =>
                                                   map (fn (hy,exs'',tth) =>
                                                           (hy,exs@exs'@exs'',MP (MP (ISPECL [arg,rhs (concl xth),f,rhs (concl tth)] comb_thm) xth) tth)) ts') ts)
                           end
                        else no_more t
                     end

               end
          (* ARB isn't a useful next state - it appears when a case split
             becomes an if-then-else tower *)
          else if is_arb t
          then []
          else no_more t
(*
fun warn_tm tm = if term_size tm > 100 then 
                 let val () = print_term t
                     val () = print "\n --- giving ---\n"
                     val () = print_term tm
                     val () = print "\n === \n"
                 in ()
                 end else ()
fun warn thm = List.app warn_tm (Thm.hyps thm)
val () = List.app (fn (_,thm) => warn thm) thms
*)
  in map (fn (hy,exs,th) => (hy,exs,
                         let val th' = TRANS thm (assert (fn th => term_eq (lhs (concl th)) t) th)
                                       handle e => (Parse.print_thm thm; Parse.print_thm th; raise e)
                         in th'
                         end
         )) thms
  end;

(* To be certain that hypotheses will match terms during evaluation we need to
   reduce them, too.  (Especially for projections of large records, e.g., for
   CHERI.) *)
fun make_hyps tms =
    let val tms = map (rhs o concl o computeLib.CBV_CONV cmp) tms
    in map ASSUME tms
    end

(* TODO: maybe try to extract this from the term?  Or at least check that it
   is present?  Would like to avoid accidentally using a different name and
   not realising why it doesn't work. *)
val state_var = mk_var ("s", EvalBasics.state_ty)

fun start filter_fn inst_rws (hyps, tm) =
    let val s = genvar EvalBasics.state_ty
        val shyps = map ASSUME (RecordUtils.base_fields s state_var)
        val rs = chomp filter_fn inst_rws 0 false (hyps, shyps, subst [state_var |-> s] tm)
        fun finish (hy,exs,th) =
            let val hy = map (INST [s |-> state_var]) hy
                val th = INST [s |-> state_var] th
                val th = clean_hyps th
                (* This is too expensive to apply all the time, but is OK at the end *)
                val th = Conv.MAP_THM (QCONV (DEPTH_CONV v2w_list_conv)) th
            in (hy, exs, th)
            end
        val ths = map finish rs
    (* If something has come up false (e.g., because of the way that address
       translation is currently rigged on CHERI) then get rid of it. *)
    in List.filter (List.all (not o same_const boolSyntax.F) o hyp o #3) ths
    end

fun eval filter_fn inst_rws hyps t =
    let val ty = type_of t
    in if carries_state_type ty
       then map (fn (h,e,t) => (e,t)) (start filter_fn inst_rws (make_hyps hyps, t))
       else Feedback.failwith "Term does not include state"
    end

val exception_filter = Option.isSome o EvalBasics.is_exception

end
