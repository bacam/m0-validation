structure wordsExtraLib =
struct

local open HolKernel Parse boolLib bossLib lcsymtacs wordsTheory wordsExtraTheory in

fun concat_left_conv tm =
  let val xyzt = wordsSyntax.dim_of tm
      val (x,yz) = wordsSyntax.dest_word_concat tm
      val (y,z) =  wordsSyntax.dest_word_concat yz
      val xt = wordsSyntax.dim_of x
      val yt = wordsSyntax.dim_of y
      val zt = wordsSyntax.dim_of z
      val xs = fcpSyntax.dest_numeric_type xt
      val ys = fcpSyntax.dest_numeric_type yt
      val zs = fcpSyntax.dest_numeric_type zt
      val xyt = fcpSyntax.mk_numeric_type (Arbnum.+ (xs,ys))
      val yzt = fcpSyntax.mk_numeric_type (Arbnum.+ (ys,zs))
      val th = GSYM wordsTheory.word_concat_assoc
      val th = INST_TYPE [alpha |-> xt, beta |-> yt, gamma |-> zt,
                          delta |-> xyt, etyvar |-> yzt, ftyvar |-> xyzt] th
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPECL [x, y, z] th
  end

fun concat_asr_conv tm =
  let val (xy,n) = wordsSyntax.dest_word_asr tm
      val (x,y) = wordsSyntax.dest_word_concat xy
      val xt = wordsSyntax.dim_of x
      val yt = wordsSyntax.dim_of y
      val xyt = wordsSyntax.dim_of xy
      val nn = numSyntax.dest_numeral n
      val mn = Arbnum.- (nn, fcpSyntax.dest_numeric_type yt)
      val th = INST_TYPE [alpha |-> xt, beta |-> yt, gamma |-> xyt] word_concat_asr
      val th = INST [``n:num`` |-> n, ``m:num`` |-> numSyntax.mk_numeral mn] th
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPECL [x,y] th
  end


fun lsl_concat_conv tm =
  let val (x,n) = wordsSyntax.dest_word_lsl tm
      val xt = wordsSyntax.dim_of x
      val nn = numSyntax.dest_numeral n
      val xs' = Arbnum.- (fcpSyntax.dest_numeric_type xt, nn)
      val xt' = fcpSyntax.mk_numeric_type xs'
      val nt = fcpSyntax.mk_numeric_type nn
      val th = INST_TYPE [alpha |-> xt, beta |-> xt', gamma |-> nt] word_lsl_concat
      val th = INST [``n:num`` |-> n] th
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPECL [x] th
  end

val lsl_concat_ss = simpLib.std_conv_ss {
       name = "lsl_concat",
       pats = [``x : 'a word << n``],
       conv = lsl_concat_conv
}

fun word_concat_or_conv tm =
  let val (tm1,tm2) = wordsSyntax.dest_word_or tm
      val (x1,x2) = wordsSyntax.dest_word_concat tm1
      val (y1,y2) = wordsSyntax.dest_word_concat tm2
      val dtm = wordsSyntax.dim_of tm
      val d1 = wordsSyntax.dim_of x1
      val d2 = wordsSyntax.dim_of x2
      val th = INST_TYPE [alpha |-> d1, beta |-> d2, gamma |-> dtm] word_concat_or
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPECL [x1,y1,x2,y2] th
  end

val word_concat_or_ss = simpLib.std_conv_ss {
       name = "word_concat_or",
       pats = [``(x1 : 'a word @@ x2 : 'b word) : 'c word || (y1 : 'a word @@ y2 : 'b word)``],
       conv = word_concat_or_conv
    }

fun word_concat_and_conv tm =
  let val (tm1,tm2) = wordsSyntax.dest_word_and tm
      val (x1,x2) = wordsSyntax.dest_word_concat tm1
      val (y1,y2) = wordsSyntax.dest_word_concat tm2
      val dtm = wordsSyntax.dim_of tm
      val d1 = wordsSyntax.dim_of x1
      val d2 = wordsSyntax.dim_of x2
      val th = INST_TYPE [alpha |-> d1, beta |-> d2, gamma |-> dtm] word_concat_and
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPECL [x1,y1,x2,y2] th
  end

val word_concat_and_ss = simpLib.std_conv_ss {
       name = "word_concat_and",
       pats = [``(x1 : 'a word @@ x2 : 'b word) : 'c word && (y1 : 'a word @@ y2 : 'b word)``],
       conv = word_concat_and_conv
    }

fun word_1comp_concat_conv tm =
  let val xy = wordsSyntax.dest_word_1comp tm
      val (x,y) = wordsSyntax.dest_word_concat xy
      val dtm = wordsSyntax.dim_of tm
      val dx = wordsSyntax.dim_of x
      val dy = wordsSyntax.dim_of y
      val th = INST_TYPE [alpha |-> dx, beta |-> dy, gamma |-> dtm] word_1comp_concat
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPECL [x,y] th
  end

val word_1comp_concat_ss = simpLib.std_conv_ss {
       name = "word_1comp_concat",
       pats = [``~(x : 'a word @@ y : 'b word) : 'c word``],
       conv = word_1comp_concat_conv
    }

fun word_extract_concat_join_conv tm =
  let val (ex,ey) = wordsSyntax.dest_word_concat tm
      val (hx,lx,x,xt) = wordsSyntax.dest_word_extract ex
      val (hy,ly,y,yt) = wordsSyntax.dest_word_extract ey
      val () = if term_eq x y then ()
               else failwith "different words"
      val dtm = wordsSyntax.dim_of tm
      val dx = wordsSyntax.dim_of x
      val th = INST_TYPE [alpha |-> xt, beta |-> yt, gamma |-> dtm, delta |-> dx] word_extract_concat_join
      val th = SPEC ly th
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPEC x th
  end

val word_extract_concat_join_ss = simpLib.std_conv_ss {
       name = "word_extract_concat_join",
       pats = [``(((m >< n) (x : 'a word)) : 'b word @@ ((m' >< n') x) : 'c word) : 'd word``],
       conv = word_extract_concat_join_conv
    }

fun word_extract_all_conv tm =
  let val (h,l,x,t) = wordsSyntax.dest_word_extract tm
      val th = INST_TYPE [alpha |-> t] word_extract_all
      val th = SIMP_RULE (arith_ss++wordsLib.WORD_ss) [] th
  in SPEC x th
  end

val word_extract_all_ss = simpLib.std_conv_ss {
       name = "word_extract_all",
       pats = [``((m >< 0) (x : 'a word)) : 'b word``],
       conv = word_extract_all_conv
    }

end
end
