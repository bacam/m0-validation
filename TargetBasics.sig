signature TargetBasics =
sig

(* Step function of the model *)
val nextstate : Term.term

(* Basic compset for the model, used by Tools *)
val compset : unit -> computeLib.compset

val smt_non_eval_terms : Term.term list

end
