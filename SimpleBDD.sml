(* After Harrison, Handbook of Practical Logic and Automated Reasoning, 2009, Sec 2.11
   Also see http://www.cl.cam.ac.uk/~jrh13/atp/

   This uses a fixed ordering with integer variables (corresponding to the bits in an
   instruction that are being decoded).  The main use is to calculate a reasonable DNF.
 *)

structure SimpleBDD = struct

type node = int * int * int
datatype bdd = BDD of (node -> int) * (int -> node) * int

fun expand_node (BDD (_,expand,_)) n =
  if n >= 0 then (expand n) handle _ => (~1,1,1)
  else let val (p,l,r) = expand (~n)
       in (p,~l,~r)
       end handle _ => (~1,~1,~1)

fun update a b f = fn a' => if a = a' then b else f a'

fun lookup_unique (bdd as BDD (unique,expand,n)) node =
  (bdd, unique node)
  handle _ =>
    (BDD (update node n unique, update n node expand, n+1), n)
                            
fun mk_node bdd (s,l,r) =
  if l = r then (bdd, l)
  else if l >= 0 then lookup_unique bdd (s,l,r)
  else let val (bdd',n) = lookup_unique bdd (s,~l,~r)
       in (bdd',~n)
       end

val empty_bdd = BDD (fn _ => raise Subscript, fn _ => raise Subscript, 2)

fun order p1 p2 = (p2 = ~1 andalso p1 <> ~1) orelse p1 < p2

fun thread s g (f1,x1) (f2,x2) =
  let val (s', y1) = f1 s  x1
      val (s'',y2) = f2 s' x2
  in g s'' (y1,y2)
  end

fun bdd_and (bddcomp as (bdd,comp)) (m1,m2) =
  if m1 = ~1 orelse m2 = ~1 then (bddcomp,~1)
  else if m1 = 1 then (bddcomp,m2)
  else if m2 = 1 then (bddcomp,m1)
  else
     (bddcomp, comp (m1,m2)) handle _ =>
     (bddcomp, comp (m2,m1)) handle _ =>
     let val (p1,l1,r1) = expand_node bdd m1
         val (p2,l2,r2) = expand_node bdd m2
         val (p,lpair,rpair) =
             if p1 = p2 then (p1,(l1,l2),(r1,r2))
             else if order p1 p2 then (p1,(l1,m2),(r1,m2))
             else (p2,(m1,l2),(m1,r2))
         val ((bdd',comp'), (lnew,rnew)) =
             thread bddcomp (fn s => fn z => (s,z)) (bdd_and, lpair) (bdd_and, rpair)
         val (bdd'',n) = mk_node bdd' (p,lnew,rnew)
     in ((bdd'', update (m1,m2) n comp'), n)
     end

fun bdd_or bdc (m1,m2) =
  let val (bdc1,n) = bdd_and bdc (~m1,~m2)
  in (bdc1,~n)
  end

fun bdd_imp bdc (m1,m2) = bdd_or bdc (~m1,m2)

fun bdd_iff bdc (m1,m2) = thread bdc bdd_or (bdd_and, (m1,m2)) (bdd_and, (~m1,~m2))

fun atom_of_var v =
  let val (name,ty) = dest_var v
  in if ty <> ``:bool`` then failwith ("Non-boolean variable: " ^ name)
     else if String.isPrefix "b" name
     then string_to_int (String.extract (name, 1, NONE))
     else failwith ("Variable name not of form bnnn: " ^ name)
  end

fun mkbdd (bddcomp as (bdd,comp)) fm =
  if term_eq fm boolSyntax.F then (bddcomp,~1)
  else if term_eq fm boolSyntax.T then (bddcomp,1)
  else if is_var fm then let val (bdd',n) = mk_node bdd (atom_of_var fm,1,~1) in ((bdd',comp),n) end
  else if is_neg fm then let val (bddcomp',n) = mkbdd bddcomp (dest_neg fm) in (bddcomp',~n) end
  else if is_conj fm then thread bddcomp bdd_and (mkbdd,lhand fm) (mkbdd,rand fm)
  else if is_disj fm then thread bddcomp bdd_or (mkbdd,lhand fm) (mkbdd,rand fm)
  else if is_imp fm then thread bddcomp bdd_imp (mkbdd,lhand fm) (mkbdd,rand fm)
  else if is_eq fm andalso type_of (lhand fm) = ``:bool`` then thread bddcomp bdd_iff (mkbdd,lhand fm) (mkbdd,rand fm)
  else failwith ("Unsupported term: " ^ term_to_string fm)

fun bddtaut fm = snd (mkbdd (empty_bdd, fn _ => raise Subscript) fm) = 1

fun term_to_bdd tm = mkbdd (empty_bdd, fn _ => raise Subscript) tm

fun var_of_atom a = mk_var ("b" ^ int_to_string a, ``:bool``)

fun bdd_to_term bdd n =
  case expand_node bdd n of
      (~1,1,1) => T
    | (~1,~1,~1) => F
    | (~1,_,_) => failwith "Shouldn't happen"
    | (p,l,r) => mk_disj (mk_conj (var_of_atom p, bdd_to_term bdd l),
                          mk_conj (mk_neg (var_of_atom p), bdd_to_term bdd l))

fun bdd_to_dnf (bdd, n) =
  let fun aux n =
        case expand_node bdd n of
            (~1,1,1) => [[]]
          | (~1,~1,~1) => []
          | (~1,_,_) => failwith "Shouldn't happen"
          | (p,l,r) =>
            let val v = var_of_atom p
                val nv = mk_neg v
            in (map (fn t => v::t) (aux l)) @ (map (fn t => nv::t) (aux r))
            end
      val l = aux n
  in List.mapPartial (Lib.total list_mk_conj) l
  end

fun term_to_dnf tm =
  let val ((bdd,_),n) = mkbdd (empty_bdd, fn _ => raise Subscript) tm
  in bdd_to_dnf (bdd, n)
  end

fun term_to_bdd tm =
  let val ((bdd,_),n) = mkbdd (empty_bdd, fn _ => raise Subscript) tm
  in (bdd, n)
  end

fun fill f m n = if m < n then f () :: fill f (m+1) n else []

fun sample_bdd random size (bdd, n) =
  let fun aux last n acc =
        case expand_node bdd n of
            (~1,1,1) => rev (fill random (last+1) size @ acc)
          | (~1,~1,~1) => raise Empty
          | (~1,_,_) => failwith "Shouldn't happen"
          | (p,l,r) =>
            let val skipped = fill random (last+1) p
                val bit = random ()
            in (aux p (if bit then l else r) (bit::skipped@acc))
               handle Empty =>
               (aux p (if bit then r else l) (not bit::skipped@acc))
            end
  in aux 0 n []
  end

datatype bit = Zero | One | Arbitrary

fun bdd_to_dnf_lists size (bdd, n) =
  let fun aux last n acc =
        case expand_node bdd n of
            (~1,1,1) => [rev (fill (fn () => Arbitrary) (last+1) size @ acc)]
          | (~1,~1,~1) => []
          | (~1,_,_) => failwith "Shouldn't happen"
          | (p,l,r) =>
            let val skipped = fill (fn () => Arbitrary) (last+1) p
            in (aux p l (One::skipped@acc)) @ (aux p r (Zero::skipped@acc))
            end
  in aux 0 n []
  end

fun bitchar Zero = #"0"
  | bitchar One  = #"1"
  | bitchar Arbitrary = #"x"

fun bdd_to_dnf_bit_strings size bdd =
  let val lists = bdd_to_dnf_lists size bdd
  in List.map (String.implode o (List.map bitchar)) lists
  end

fun term_to_dnf_bit_strings size tm = bdd_to_dnf_bit_strings size (term_to_bdd tm)

fun sample random size tm =
  let val ((bdd,_),n) = mkbdd (empty_bdd, fn _ => raise Subscript) tm
  in sample_bdd random size (bdd, n)
  end

end
