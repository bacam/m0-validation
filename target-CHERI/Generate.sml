structure Generate : sig
             include Generate
             val find_bad_instr : string -> bool list
             val respect_delay_slots : bool ref
             datatype sc_choice = SCAnywhere | AnySCAfterLL | SCMatchingLL
             val store_conditionals : sc_choice ref
             val autoextract : bool ref
             val compare_formats : unit -> unit
          end = struct

open GenerationTools RandGen

datatype instr_frag =
   Lit of int list
 | Imm of int
 | Unspec of int
 | Reg
 (* Not zero register *)
 | RegNZ
 (* Not link register, 31 *)
 | RegNL
 (* Offset in bytes from a given register *)
 | RegOffset
 (* Offset in words from the PC (actually, probably the address of the delay slot) *)
 | PCOffset
 | JumpTarget
 (* Hardware register *)
 | HWR
 | CP0
 (* Capabilities *)
 | Cap
 | CPtrCmpOp
 (* Behaviour on invalid hint fields seems to vary, restrict to official ones *)
 | JumpHint

val reg_list = List.tabulate (32, fn i => i)

fun mk_reg (g,rs) =
    let val selection = reg_list@ !rs
        val r = hd (select_n g selection 1)
        val () = update_rs rs [r]
    in to_n_bits 5 (IntInf.fromInt r)
    end
fun mk_regnz (g,rs) =
    let val selection = List.filter (fn i => i > 0) (reg_list@ !rs)
        val r = hd (select_n g selection 1)
        val () = update_rs rs [r]
    in to_n_bits 5 (IntInf.fromInt r)
    end
fun mk_regnl (g,rs) =
    let val selection = List.filter (fn i => i < 31) (reg_list@ !rs)
        val r = hd (select_n g selection 1)
        val () = update_rs rs [r]
    in to_n_bits 5 (IntInf.fromInt r)
    end

val cap_list = List.tabulate (32, fn i => i)

fun mk_cap (g,rs) =
    let val selection = cap_list@ !rs
        val r = hd (select_n g selection 1)
        val () = update_rs rs [r]
    in to_n_bits 5 (IntInf.fromInt r)
    end

(* FIXME: work for all areas of sram (within reason) *)
val sram_start = fst (hd Target.ram_details)
val sram_size = snd (hd Target.ram_details)

fun mk_offset g dv =
    let val max_magnitude = (Arbnum.toLargeInt sram_size) div dv
        val off = genint g (2*max_magnitude) - max_magnitude
    in signed_to_n_bits 16 off
    end

(* Jump targets are only a 26-bit subsequence of the bits of the
   target address. *)

val jump_low = ((Arbnum.toLargeInt sram_start) mod (IntInf.pow (2,28))) div 4

fun mk_jump_target g =
    let val pos = genint g ((Arbnum.toLargeInt sram_size) div 4)
    in to_n_bits 26 (jump_low + pos)
    end

(* The CP0 registers we're currently happy about playing with.  Largely
   based on what we initialise in Target.sml.  TODO: allow more *)
val cp0_valid =
    map (fn (reg,sel) => (to_n_bits 5 reg) @ (to_n_bits 8 0) @ (to_n_bits 3 sel))
    [(4,2), (* UsrLocal *)
     (*(12,0), Status register - disabled because some bits are undefined *)
     (14,0), (* EPC *)
     (30,0)] (* ErrorEPC *)

val strict_unspec = ref false

(* TODO: we could also allow larger values for RegOffset, hoping that the
   SMT solver will choose appropriate base registers, but I still need to
   think about that. *)

fun mk_frag (g,rs) (Lit l)    = tbl l
  | mk_frag (g,rs) (Imm n)    = bits g n
  | mk_frag (g,rs) (Unspec n) = if !strict_unspec then to_n_bits n 0 else bits g n
  | mk_frag (g,rs) Reg        = mk_reg (g,fst rs)
  | mk_frag (g,rs) RegNZ      = mk_regnz (g,fst rs)
  | mk_frag (g,rs) RegNL      = mk_regnl (g,fst rs)
  | mk_frag (g,rs) RegOffset  = mk_offset g 1
  | mk_frag (g,rs) PCOffset   = mk_offset g 4
  | mk_frag (g,rs) JumpTarget = mk_jump_target g
  (* The hardware register list is CHERI specific, does not include kill sim,
     reset or dump stats.  Or count (2) for now.  3 can vary between targets
     without problem, so we can't use it. *)
  | mk_frag (g,rs) HWR        = to_n_bits 5 (snd (select [0(*,2*),29(* not in qemu, not standard ,30*)] g))
  | mk_frag (g,rs) CP0        = snd (select cp0_valid g)
  | mk_frag (g,rs) Cap        = mk_cap (g,snd rs)
  | mk_frag (g,rs) CPtrCmpOp  = to_n_bits 3 (genint g 6)
  | mk_frag (g,rs) JumpHint   = to_n_bits 5 (snd (select [0,0x10] g))

fun frag_size (Lit l)    = length l
  | frag_size (Imm n)    = n
  | frag_size (Unspec n) = n
  | frag_size Reg        = 5
  | frag_size RegNZ      = 5
  | frag_size RegNL      = 5
  | frag_size RegOffset  = 16
  | frag_size PCOffset   = 16
  | frag_size JumpTarget = 26
  | frag_size HWR        = 5
  | frag_size CP0        = 16
  | frag_size Cap        = 5
  | frag_size CPtrCmpOp  = 3
  | frag_size JumpHint   = 5

fun fmt_size (_,(_,fmt,s)) = (foldl (op +) 0 (map frag_size fmt), s)

fun mk_instr g fmt = List.concat (map (mk_frag g) fmt)

(* This is based on the decoder from the simulator version of the L3 model, by
   cut, paste, replace and partitioning.

   NB: a lot of the RegNZ are places where I think the processor does have a
   defined instruction, but we don't get a step theorem for it. *)

val normal_instrs : (int * (bool * instr_frag list * string)) list =
     map (fn (is,s) => (1,(true,is,s))) (* Preserves linked load bit *)
     [([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [0,0,0,0,0,0]], "SLL"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [0,0,0,0,1,0]], "SRL"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [0,0,0,0,1,1]], "SRA"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,0,0,1,0,0]], "SLLV"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,0,0,1,1,0]], "SRLV"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,0,0,1,1,1]], "SRAV"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,0,1,0,1,0]], "MOVZ"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,0,1,0,1,1]], "MOVN"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], Reg, Lit [0,0,0,0,0,0,1,0,0,0,0]], "MFHI"),
      ([Lit [0,0,0,0,0,0], Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1]], "MTHI"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], Reg, Lit [0,0,0,0,0,0,1,0,0,1,0]], "MFLO"),
      ([Lit [0,0,0,0,0,0], Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,1]], "MTLO"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,1,0,1,0,0]], "DSLLV"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,1,0,1,1,0]], "DSRLV"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,1,0,1,1,1]], "DSRAV"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0]], "MULT"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,1]], "MULTU"),
      ([Lit [0,0,0,0,0,0], Reg, RegNZ, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,0]], "DIV"),
      ([Lit [0,0,0,0,0,0], Reg, RegNZ, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1]], "DIVU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0]], "DMULT"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1]], "DMULTU"),
      ([Lit [0,0,0,0,0,0], Reg, RegNZ, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0]], "DDIV"),
      ([Lit [0,0,0,0,0,0], Reg, RegNZ, Lit [0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1]], "DDIVU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,0,0,0]], "ADD"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,0,0,1]], "ADDU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,0,1,0]], "SUB"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,0,1,1]], "SUBU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,1,0,0]], "AND"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,1,0,1]], "OR"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,1,1,0]], "XOR"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,0,1,1,1]], "NOR"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,1,0,1,0]], "SLT"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,1,0,1,1]], "SLTU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,1,1,0,0]], "DADD"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,1,1,0,1]], "DADDU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,1,1,1,0]], "DSUB"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,1,0,1,1,1,1]], "DSUBU"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [1,1,1,0,0,0]], "DSLL"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [1,1,1,0,1,0]], "DSRL"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [1,1,1,0,1,1]], "DSRA"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [1,1,1,1,0,0]], "DSLL32"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [1,1,1,1,1,0]], "DSRL32"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Reg, Reg, Imm 5, Lit [1,1,1,1,1,1]], "DSRA32"),
      ([Lit [0,0,1,1,1,1,0,0,0,0,0], Reg, Imm 16], "LUI"),
      ([Lit [0,1,1,1,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]], "MADD"),
      ([Lit [0,1,1,1,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]], "MADDU"),
      ([Lit [0,1,1,1,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0]], "MSUB"),
      ([Lit [0,1,1,1,0,0], Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1]], "MSUBU"),
      ([Lit [0,1,1,1,0,0], Reg, Reg, Reg, Lit [0,0,0,0,0,0,0,0,0,1,0]], "MUL"),
      ([Lit [0,0,1,0,0,0], Reg, Reg, Imm 16], "ADDI"),
      ([Lit [0,0,1,0,0,1], Reg, Reg, Imm 16], "ADDIU"),
      ([Lit [0,0,1,0,1,0], Reg, Reg, Imm 16], "SLTI"),
      ([Lit [0,0,1,0,1,1], Reg, Reg, Imm 16], "SLTIU"),
      ([Lit [0,0,1,1,0,0], Reg, Reg, Imm 16], "ANDI"),
      ([Lit [0,0,1,1,0,1], Reg, Reg, Imm 16], "ORI"),
      ([Lit [0,0,1,1,1,0], Reg, Reg, Imm 16], "XORI"),
      ([Lit [0,1,1,0,0,0], Reg, Reg, Imm 16], "DADDI"),
      ([Lit [0,1,1,0,0,1], Reg, Reg, Imm 16], "DADDIU"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], Imm 5, Lit [0,0,1,1,1,1]], "SYNC"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Imm 10, Lit [1,1,0,0,0,0]], "TGE"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Imm 10, Lit [1,1,0,0,0,1]], "TGEU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Imm 10, Lit [1,1,0,0,1,0]], "TLT"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Imm 10, Lit [1,1,0,0,1,1]], "TLTU"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Imm 10, Lit [1,1,0,1,0,0]], "TEQ"),
      ([Lit [0,0,0,0,0,0], Reg, Reg, Imm 10, Lit [1,1,0,1,1,0]], "TNE"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,1,0,0,0], Imm 16], "TGEI"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,1,0,0,1], Imm 16], "TGEIU"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,1,0,1,0], Imm 16], "TLTI"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,1,0,1,1], Imm 16], "TLTIU"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,1,1,0,0], Imm 16], "TEQI"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,1,1,1,0], Imm 16], "TNEI"),
      ([Lit [0,1,1,1,1,1,0,0,0,0,0], Reg, HWR, Lit [0,0,0,0,0,1,1,1,0,1,1]], "RDHWR"),
      ([Lit [1,1,0,0,0,0], Reg, Reg, RegOffset], "LL"),
      ([Lit [1,1,0,1,0,0], Reg, Reg, RegOffset], "LLD"),
      ([Lit [0,1,0,0,0,0,0,0,0,0,0], Reg, CP0], "MFC"),
      ([Lit [0,1,0,0,0,0,0,0,0,0,1], Reg, CP0], "DMFC"),
      ([Lit [0,1,0,0,0,0,0,0,1,0,0], Reg, CP0], "MTC"),
      ([Lit [0,1,0,0,0,0,0,0,1,0,1], Reg, CP0], "DMTC"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Imm 15, Lit [0,0,1,1,0,0]], "SYSCALL"),
      ([Lit [0,0,0,0,0,0,0,0,0,0,0], Imm 15, Lit [0,0,1,1,0,1]], "BREAK")]
     @
     map (fn (is,s) => (1,(false,is,s))) (* Clears linked load bit *)
     [([Lit [1,0,0,0,0,0], Reg, Reg, RegOffset], "LB"),
      ([Lit [1,0,0,0,0,1], Reg, Reg, RegOffset], "LH"),
      ([Lit [1,0,0,0,1,1], Reg, Reg, RegOffset], "LW"),
      ([Lit [1,0,0,1,0,0], Reg, Reg, RegOffset], "LBU"),
      ([Lit [1,0,0,1,0,1], Reg, Reg, RegOffset], "LHU"),
      ([Lit [1,0,0,1,1,1], Reg, Reg, RegOffset], "LWU"),
      ([Lit [1,0,1,0,0,0], Reg, Reg, RegOffset], "SB"),
      ([Lit [1,0,1,0,0,1], Reg, Reg, RegOffset], "SH"),
      ([Lit [1,0,1,0,1,1], Reg, Reg, RegOffset], "SW"),
      ([Lit [1,1,0,1,1,1], Reg, Reg, RegOffset], "LD"),
      ([Lit [1,1,1,1,1,1], Reg, Reg, RegOffset], "SD"),
      ([Lit [0,1,1,0,1,0], Reg, Reg, RegOffset], "LDL"),
      ([Lit [0,1,1,0,1,1], Reg, Reg, RegOffset], "LDR"),
      ([Lit [1,0,0,0,1,0], Reg, Reg, RegOffset], "LWL"),
      ([Lit [1,0,0,1,1,0], Reg, Reg, RegOffset], "LWR"),
      ([Lit [1,0,1,0,1,0], Reg, Reg, RegOffset], "SWL"),
      ([Lit [1,0,1,1,0,0], Reg, Reg, RegOffset], "SDL"),
      ([Lit [1,0,1,1,0,1], Reg, Reg, RegOffset], "SDR"),
      ([Lit [1,0,1,1,1,0], Reg, Reg, RegOffset], "SWR")]

(* Use a heavy weighting as they can only occur after a linked load
   Record the corresponding load so that we can be strict about matching
   them up *)
val store_conditional_instrs : (string * (int * (bool * instr_frag list * string))) list =
     map (fn (ll,(is,s)) => (ll,(10,(false,is,s)))) (* Clears linked load bit *)
     [("LL" ,([Lit [1,1,1,0,0,0], Reg, Reg, RegOffset], "SC")),
      ("LLD", ([Lit [1,1,1,1,0,0], Reg, Reg, RegOffset], "SCD"))]

(* Instructions that should not occur in a delay slot *)
val non_delay_slot_instrs : (int * (bool * instr_frag list * string)) list =
     (1,(false,[Lit [0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0]], "ERET"))::
     map (fn (is,s) => (1,(true,is,s))) (* Preserves linked load bit *)
     [([Lit [0,0,0,0,0,0], Reg, Lit [0,0,0,0,0,0,0,0,0,0], JumpHint, Lit [0,0,1,0,0,0]], "JR"),
      ([Lit [0,0,0,0,0,0], Reg, Lit [0,0,0,0,0], Reg, JumpHint, Lit [0,0,1,0,0,1]], "JALR"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,0,0,0,0], PCOffset], "BLTZ"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,0,0,0,1], PCOffset], "BGEZ"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,0,0,1,0], PCOffset], "BLTZL"),
      ([Lit [0,0,0,0,0,1], Reg, Lit [0,0,0,1,1], PCOffset], "BGEZL"),
      ([Lit [0,0,0,0,0,1], RegNL, Lit [1,0,0,0,0], PCOffset], "BLTZAL"),
      ([Lit [0,0,0,0,0,1], RegNL, Lit [1,0,0,0,1], PCOffset], "BGEZAL"),
      ([Lit [0,0,0,0,0,1], RegNL, Lit [1,0,0,1,0], PCOffset], "BLTZALL"),
      ([Lit [0,0,0,0,0,1], RegNL, Lit [1,0,0,1,1], PCOffset], "BGEZALL"),
      ([Lit [0,0,0,0,1,0], JumpTarget], "J"),
      ([Lit [0,0,0,0,1,1], JumpTarget], "JAL"),
      ([Lit [0,0,0,1,0,0], Reg, Reg, PCOffset], "BEQ"),
      ([Lit [0,0,0,1,0,1], Reg, Reg, PCOffset], "BNE"),
      ([Lit [0,0,0,1,1,0], Reg, Lit [0,0,0,0,0], PCOffset], "BLEZ"),
      ([Lit [0,0,0,1,1,1], Reg, Lit [0,0,0,0,0], PCOffset], "BGTZ"),
      ([Lit [0,1,0,1,0,0], Reg, Reg, PCOffset], "BEQL"),
      ([Lit [0,1,0,1,0,1], Reg, Reg, PCOffset], "BNEL"),
      ([Lit [0,1,0,1,1,0], Reg, Lit [0,0,0,0,0], PCOffset], "BLEZL"),
      ([Lit [0,1,0,1,1,1], Reg, Lit [0,0,0,0,0], PCOffset], "BGTZL")]

val difficult_instrs : (int * (bool * instr_frag list * string)) list =
     (* non-delay-slot *)
     (map (fn (is,s) => (1,(true,is,s))) (* Preserves linked load bit *)
     [([Lit [0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]], "TLBR"),
      ([Lit [0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0]], "TLBWI"),
      ([Lit [0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0]], "TLBWR"),
      ([Lit [0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0]], "TLBP"),
      (* A little non-determinstic (just checks address translation in model, complete
         nop on qemu *)
      ([Lit [1,0,1,1,1,1], Imm 5, Imm 5, RegOffset], "CACHE"),
      (* Don't generate WAIT - it has implementation defined semantics and will
         actually wait for something on qemu! *)
      ([Lit [0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0]], "WAIT")
     ])


val unsupported_instrs : (int * (bool * instr_frag list * string)) list =
     []

val cheri_instrs : (int * (bool * instr_frag list * string)) list =
  map (fn (is,s) => (1,(true,is,s))) (* Preserves linked load bit *)
  (map (fn (f,s) => (Lit [0,1,0,0,1,0] :: f,s)) (* CP2 instructions (0x12) *)
   [([Lit [0,0,0,0,0], Reg, Cap, Unspec 5, Lit [0,0,0,0,1,0]], "CGet(CGetBase(Reg, Cap))"),
    ([Lit [0,1,1,0,1], Reg, Cap, Unspec 5, Lit [0,0,0,0,1,0]], "CGet(CGetOffset(Reg, Cap))"),
    ([Lit [0,0,0,0,0], Reg, Cap, Unspec 5, Lit [0,0,0,0,1,1]], "CGet(CGetLen(Reg, Cap))"),
    ([Lit [0,0,0,0,0], Reg, Cap, Unspec 5, Lit [0,0,0,1,0,1]], "CGet(CGetTag(Reg, Cap))"),
    ([Lit [0,0,0,0,0], Reg, Cap, Unspec 5, Lit [0,0,0,1,1,0]], "CGet(CGetSealed(Reg, Cap))"),
    ([Lit [0,0,0,0,0], Reg, Cap, Unspec 5, Lit [0,0,0,0,0,0]], "CGet(CGetPerm(Reg, Cap))"),
    ([Lit [0,0,0,0,0], Reg, Cap, Unspec 5, Lit [0,0,0,0,0,1]], "CGet(CGetType(Reg, Cap))"),
    ([Lit [0,0,0,0,0], Cap, Lit [0,0,0,0,0, 1,1,1,1,1, 1,1,1,1,1,1]], "CGet(CGetPCC(cd))"),
    ([Lit [0,0,0,0,0], Cap, Reg, Lit [0,0,1,1,1, 1,1,1,1,1,1]], "CGet(CGetPCCSetOffset(cd,rs))"),
    ([Lit [0,0,0,0,0], Reg, Lit [0,0,0,0,0], Unspec 5, Lit [0,0,0,1,0,0]], "CGet(CGetCause(Reg))"),
    ([Lit [0,0,1,0,0], Lit [0,0,0,0,0], Lit [0,0,0,0,0], Reg, Unspec 3, Lit [1,0,0]], "CSet(CSetCause(Reg))"),
    ([Lit [0,0,0,0,1], Cap, Cap, Reg, Unspec 6], "CSet(CSetBounds(cd, cb, rt))"),
    ([Lit [0,0,0,0,0], Cap, Cap, Reg, Lit [0,0,1,0,0,1]], "CSet(CSetBoundsExact(cd, cb, rt))"),
    ([Lit [0,1,1,0,1], Cap, Cap, Reg, Unspec 3, Lit [0,0,0]], "CSet(CIncOffset(cd, cb, rt))"),
    (* split regset because only bottom two bits relevant for now;
       TODO: ought to be able to generate more once we want to test exceptions;
       note that there's no register biasing here for the moment *)
    ([Lit [0,1,1,1,1], Lit [0,0,0], Imm 2, Imm 16], "CSet(CClearRegs(regset, mask))"),
    ([Lit [0,0,1,0,0], Cap, Cap, Unspec 8, Lit [1,0,1]], "CSet(CClearTag(Cap, Cap))"),
    ([Lit [0,0,1,0,0], Cap, Cap, Reg, Unspec 3, Lit [0,0,0]], "CSet(CAndPerm(Cap, Cap, Reg))"),
    ([Lit [0,1,1,0,1], Cap, Cap, Reg, Unspec 3, Lit [0,0,1]], "CSet(CSetOffset(Cap, Cap, Reg))"),
    ([Lit [0,0,0,0,0], Reg, Cap, Cap, Lit [0,0,1,0,1,0]], "CSub(rd, cb, ct)"),
    ([Lit [0,1,0,1,1], Cap, Unspec 5, Reg, Unspec 3, Lit [0,0,0]], "CCheck(CCheckPerm(Cap, Reg))"),
    ([Lit [0,1,0,1,1], Cap, Cap, Unspec 8, Lit [0,0,1]], "CCheck(CCheckType(Cap, Cap))"),
    ([Lit [0,0,1,0,0], Cap, Cap, Reg, Unspec 3, Lit [1,1,1]], "CSet(CFromPtr(Cap, Cap, Reg))"),
    ([Lit [0,1,1,0,0], Reg, Cap, Cap, Unspec 6], "CGet(CToPtr(Reg, Cap, Cap))"),
    ([Lit [0,0,0,1,0], Cap, Cap, Cap, Unspec 6], "CSeal(Cap, Cap, Cap)"),
    ([Lit [0,0,0,1,1], Cap, Cap, Cap, Unspec 6], "CUnseal(Cap, Cap, Cap)"),
    ([Lit [0,0,0,0,0], Cap, Cap, Reg, Lit [0,1,1,1,0,0]], "CMOVN(cd, cb, rt)"),
    ([Lit [0,0,0,0,0], Cap, Cap, Reg, Lit [0,1,1,0,1,1]], "CMOVZ(cd, cb, rt)")
   ])@
  (* Similar, but with higher weighting so that each Op is treated as a half-instruction *)
  map (fn (is,s) => (3,(true,is,s))) (* Preserves linked load bit *)
  (map (fn (f,s) => (Lit [0,1,0,0,1,0] :: f,s)) (* CP2 instructions (0x12) *)
    [([Lit [0,1,1,1,0], Reg, Cap, Cap, Unspec 3, CPtrCmpOp], "CPtrCmp(Reg, Cap, Cap, t)")])@

  map (fn (is,s) => (1,(false,is,s))) (* Clears linked load bit *)
  (map (fn (f,s) => (Lit [1,1,0,0,1,0] :: f,s)) (* CP2 load woReg instructions (0x32) *)
   [([Reg, Cap, Reg, Imm 8, Lit [0], Imm 2], "CLoad(Reg, Cap, Reg, offset, 0b0, t)"),
    ([Reg, Cap, Reg, Imm 8, Lit [1,0,0]], "CLoad(Reg, Cap, Reg, offset, 0b1, 0b0,0)"),
    ([Reg, Cap, Reg, Imm 8, Lit [1,0,1]], "CLoad(Reg, Cap, Reg, offset, 0b1, 0b0,1)"),
    ([Reg, Cap, Reg, Imm 8, Lit [1,1,0]], "CLoad(Reg, Cap, Reg, offset, 0b1, 0b1,0)")])@
  map (fn (is,s) => (1,(true,is,s))) (* Sets linked load bit *)
  (map (fn (f,s) => (Lit [0,1,0,0,1,0,1,0,0,0,0] :: f,s)) (* CP2 *)
   [([Reg, Cap, Unspec 7, Lit [1], Imm 1, Lit [0,0]], "CLLx(Reg, Cap, s00)"),
    ([Reg, Cap, Unspec 7, Lit [1], Imm 1, Lit [0,1]], "CLLx(Reg, Cap, s01)"),
    ([Reg, Cap, Unspec 7, Lit [1], Imm 1, Lit [1,0]], "CLLx(Reg, Cap, s10)"),
    ([Reg, Cap, Unspec 7, Lit [1,0,1,1]], "CLLx(Reg, Cap, 011)"),
    ([Reg, Cap, Unspec 7, Lit [1,1,1,1]], "CLLC(Cap, Cap)")])@
(*
  map (fn (is,s) => (1,(true,is,s))) (* Sets linked load bit *)
  (map (fn (f,s) => (Lit [1,1,0,0,1,0] :: f,s)) (* CP2 load woReg instructions (0x32) *)
   [([Reg, Cap, Reg, Imm 8, Lit [1,1,1]], "CLLD(Reg, Cap, Reg, offset)")])@
*)  
  map (fn (is,s) => (1,(false,is,s))) (* Clears linked load bit *)
  (map (fn (f,s) => (Lit [1,1,0,1,1,0] :: f,s)) (* CP2 load double instructions (0x36) *)
    [([Cap, Cap, Reg, Imm 11], "LDC2(CHERILDC2(CLC(cd, Cap, Reg, offset)))")])@
  
  (* Weight it because it is equivalent to 4 MIPS patterns *)
  map (fn (is,s) => (4,(false,is,s))) (* Clears linked load bit *)
  (map (fn (f,s) => (Lit [1,1,1,0,1,0] :: f,s)) (* CP2 store woReg instructions (0x3a) *)
    [([Reg, Cap, Reg, Imm 8, Lit [0], Imm 2], "SWC2(CHERISWC2(CStore(rs, Cap, Reg, offset, t)))")])@
  
  map (fn (is,s) => (1,(false,is,s))) (* Clears linked load bit *)
  (map (fn (f,s) => (Lit [1,1,1,1,1,0] :: f,s)) (* CP2 store double instructions (0x3e) *)
    [([Cap, Cap, Reg, Imm 11], "SDC2(CHERISDC2(CSC(Cap, Cap, Reg, offset)))")])

val cheri_non_delay_slot_instrs : (int * (bool * instr_frag list * string)) list =
  map (fn (is,s) => (1,(true,is,s))) (* Preserves linked load bit *)
  (map (fn (f,s) => (Lit [0,1,0,0,1,0] :: f,s)) (* CP2 instructions (0x12) *)
   [([Lit [0,1,0,0,0], Unspec 5, Cap, Unspec 11], "CJR(Cap)"),
    ([Lit [0,0,1,1,1], Cap, Cap, Unspec 11], "CJALR(Cap, Cap)"),
    ([Lit [0,1,0,0,1], Cap, Imm 16], "CBTU(Cap, offset)"),
    ([Lit [0,1,0,1,0], Cap, Imm 16], "CBTS(Cap, offset)"),
    (* Only selectors 0 and 1 are defined at the moment *)
    ([Lit [0,0,1,0,1], Cap, Cap, Lit [0,0,0,0,0,0,0,0,0,0], Lit [0]], "CCall(Cap, Cap)"),
    ([Lit [0,0,1,0,1], Cap, Cap, Lit [0,0,0,0,0,0,0,0,0,0], Lit [1]], "CCall1(Cap, Cap)"),
    ([Lit [0,0,1,1,0], Unspec 21], "CReturn")])

(* Again, high weight because they need a linked load first *)
val cheri_store_conditional_instrs : (string * (int * (bool * instr_frag list * string))) list =
  map (fn (ll,(is,s)) => (ll,(10,(false,is,s)))) (* Clears linked load bit *)
  (map (fn (ll,(f,s)) => (ll,(Lit [0,1,0,0,1,0,1,0,0,0,0] :: f,s))) (* CP2 *)
    [("CLLx(Reg, Cap, s00)", ([Reg, Cap, Reg, Unspec 2, Lit [0,0,0,0]], "CSCx(Reg, Cap, Reg, 00)))")),
     ("CLLx(Reg, Cap, s01)", ([Reg, Cap, Reg, Unspec 2, Lit [0,0,0,1]], "CSCx(Reg, Cap, Reg, 01)))")),
     ("CLLx(Reg, Cap, s10)", ([Reg, Cap, Reg, Unspec 2, Lit [0,0,1,0]], "CSCx(Reg, Cap, Reg, 10)))")),
     ("CLLx(Reg, Cap, 011)", ([Reg, Cap, Reg, Unspec 2, Lit [0,0,1,1]], "CSCx(Reg, Cap, Reg, 11)))")),
     ("CLLC(Cap, Cap)", ([Cap, Cap, Reg, Unspec 2, Lit [0,1,1,1]], "CSCC(Cap, Cap, Reg)"))])
(* @
  map (fn (is,s) => (10,(false,is,s))) (* Clears linked load bit *)
  (map (fn (f,s) => (Lit [1,1,1,0,1,0] :: f,s)) (* CP2 store woReg instructions (0x3a) *)
    [([Reg, Cap, Reg, Imm 8, Lit [1,1,1]], "SWC2(CHERISWC2(CSCD(rs, Cap, Reg, offset)))")])
*)

val cheri_difficult_instrs : (int * (bool * instr_frag list * string)) list =
  map (fn (is,s) => (1,(true,is,s))) (* Preserves linked load bit *)
  (map (fn (f,s) => (Lit [0,1,0,0,1,0] :: f,s)) (* CP2 instructions (0x12) *)
   [([Lit [0,0,1,0,0], Unspec 18, Lit [1,1,0]], "DumpCapReg")])

fun strip_ll (l : (string * 'a) list) : 'a list = map snd l

val all_cheri_instrs = cheri_instrs @ cheri_non_delay_slot_instrs @
                       strip_ll cheri_store_conditional_instrs @ cheri_difficult_instrs

val all_supported_instrs = normal_instrs @ strip_ll store_conditional_instrs @
                           non_delay_slot_instrs @ difficult_instrs @
                           all_cheri_instrs
val all_instrs = all_supported_instrs @ unsupported_instrs

val all_store_conditional = store_conditional_instrs @ cheri_store_conditional_instrs

val always_usable_instrs = normal_instrs @ cheri_instrs
(* See also store_conditional_instrs *)
val usable_non_delay_slot_instrs = non_delay_slot_instrs @ cheri_non_delay_slot_instrs
val usable_instrs = always_usable_instrs @ usable_non_delay_slot_instrs

(* Instructions that never signal an exception *)
val no_exception_instrs =
 ["SLL", "SRL", "SRA", "SLLV", "SRLV", "SRAV", "MOVZ", "MOVN",
  "MFHI", "MTHI", "MFLO", "MTLO", "DSLLV", "DSRLV", "DSRAV", "MULT",
  "MULTU", "DIV", "DIVU", "DMULT", "DMULTU", "DDIV", "DDIVU", "ADDU",
  "SUBU", "AND", "OR", "XOR", "NOR", "SLT", "SLTU", "DADDU", "DSUBU",
  "DSLL", "DSRL", "DSRA", "DSLL32", "DSRL32", "DSRA32", "LUI", "MADD",
  "MADDU", "MSUB", "MSUBU", "MUL", "ADDIU", "SLTI", "SLTIU", "ANDI",
  "ORI", "XORI", "DADDIU", "SYNC", "WAIT", "JR", "JALR",
  "BLTZ", "BGEZ", "BLTZL", "BGEZL", "BLTZAL", "BGEZAL", "BLTZALL",
  "BGEZALL", "J", "JAL", "BEQ", "BNE", "BLEZ", "BGTZ", "BEQL", "BNEL",
  "BLEZL", "BGTZL", "DumpCapReg" ]

(* Instructions which always signal an exception *)
fun only_exception_instrs () =
    ["SYSCALL","BREAK"] @
    (if !Target.cu2 then ["CCall(Cap, Cap)","CReturn"]
     else map (fn (_,(_,_,s)) => s) all_cheri_instrs)

fun descl [] = "<none>"
  | descl l = String.concatWith "\n  " l

fun find_bad_instr s =
    let val (_,(_,fmt,_)) = case List.find (fn (_,(_,_,s')) => s' = s) all_instrs of
                                NONE => failwith ("Unable to find format for " ^ s)
                              | SOME x => x
        val gen = RandGen.newgen ()
        fun try () =
            let val i = mk_instr (gen, (ref [], ref [])) fmt
            in let val _ = Target.step (to_hex i)
               in try ()
               end handle _ => i
            end
    in try ()
    end

(* TODO: check delay slot instruction lists *and* delay_slot_must_follow *)

  fun test_instrs n =
      let val sizes = map fmt_size all_instrs
          val bad = List.mapPartial (fn (32,_) => NONE | (_,s) => SOME s) sizes
          val gen = RandGen.newgen ()
          fun tell_time ts =
              let val ts = map Time.toReal ts
                  val ts = Lib.sort (curry (op >)) ts
                  val len = length ts
                  val mean = (foldl (op +) 0.0 ts) / (Real.fromInt len)
                  val median = if len mod 2 = 1 then List.nth (ts,len div 2)
                               else (List.nth (ts,len div 2 - 1) + List.nth (ts,len div 2)) / 2.0
              in print ("mean: " ^ Real.toString mean ^
                        " median: " ^ Real.toString median ^
                        " max: " ^ Real.toString (hd ts) ^ "\n")
              end
          fun mk_thms exn fmt =
              let val i = mk_instr (gen, (ref [], ref [])) fmt
                  val h = to_hex i
                  val init_stats = PolyStats.get ()
                  val thms = (if exn then Target.step_exn h else Target.step h) handle _ => []
                  val time = PolyStats.time_diff init_stats (PolyStats.get())
              in ((h,thms),time)
              end
          fun mk_n_thms exn (_,(ll,fmt,s)) =
              let val () = print (s ^ " ")
                  val (thms,ts) = unzip (List.tabulate (n,fn _ => mk_thms exn fmt))
                  val () = tell_time ts
              in (ll,fmt,s,thms)
              end
          val supported = map (mk_n_thms false) all_supported_instrs
          val supported_exn = map (mk_n_thms true) all_supported_instrs
          val unsupported = map (mk_n_thms false) unsupported_instrs
          val unsupported_exn = map (mk_n_thms true) unsupported_instrs
          fun try f 0 fmt = (NONE,0,[])
            | try f n fmt =
              let val i = mk_instr (gen, (ref [], ref [])) fmt
                  val h = to_hex i
                  val (eg,m,ts) = try f (n-1) fmt
                  val init_stats = PolyStats.get ()
                  val r = f h
                  val time = PolyStats.time_diff init_stats (PolyStats.get())
                  val count = (SOME h,m+1,time::ts)
                  val ignore = (eg,m,time::ts)
              in if r then ignore else count
              end
          fun tryn f (_,(_,fmt,s)) =
              let val () = print (s ^ " ")
                  val (eg,bad,ts) = try f n fmt
              in if bad = 0 then NONE else
                 SOME (s ^ " x" ^ Int.toString bad ^ " (e.g. " ^ Option.valOf eg ^ ")")
              end
          open boolSyntax Thm Term
          val updates =
              (map (fst o strip_comb o lhs o snd o strip_forall o concl) o TypeBase.updates_of)
                 ``:cheri_state``
          fun is_updates tm =
              is_var tm orelse
              let val (f,args) = strip_comb tm
                  val tail = List.last args
              in List.exists (term_eq f) updates andalso is_updates tail
              end
          val only_exception_instrs = only_exception_instrs ()
          fun check_supported s (_,ths) =
              (List.all (is_updates o rand o rhs o concl) ths andalso
               (mem s only_exception_instrs orelse length ths > 0))
              handle _ => false
          fun check_exn_supported s (_,ths) =
              (List.all (is_updates o rand o rhs o concl) ths andalso
               (mem s no_exception_instrs orelse length ths > 0))
              handle _ => false
          fun check f (ll,fmt,s,ins_thms_pairs) =
              (case List.filter (f ll s) ins_thms_pairs of
                  [] => NONE
                | (l as ((h,_)::_)) => SOME (s ^ " x" ^ Int.toString (length l) ^ " (e.g. " ^ h ^ ")"))
          fun check_llbit ll (_,ths) =
              let val get =
                      rhs o concl o computeLib.EVAL_CONV o
                      (fn tm => ``^tm.c_state.c_LLbit``) o rand o rhs o concl
                  val rh = map get ths
              in if ll
                 then List.all (fn tm => term_eq ``s.c_state.c_LLbit`` tm
                                         orelse term_eq ``SOME T`` tm) rh
                 else List.all (fn tm => term_eq ``NONE : bool option`` tm
                                         orelse term_eq ``SOME F`` tm) rh
              end
          fun roundtrip h =
              let val n = Arbnum.fromHexString h
                  val w = wordsSyntax.mk_wordi (n,32)
                  val i = Tools.cbv_eval ``Decode ^w``
                  val r = Tools.cbv_eval ``^i = Decode (Encode ^i)``
              in term_eq T r
              end
          fun stringroundtrip h =
              let val instr = mips.Decode (Option.valOf (BitsN.fromHexString (h,32)))
                  val s = mips.instructionToString instr
              in mips.OK instr = mips.instructionFromString s
              end
          val bad_unsupported = List.mapPartial (check (fn _ => fn s => not o (check_supported s))) supported
          val bad_exn_unsupported = List.mapPartial (check (fn _ => fn s => not o (check_exn_supported s))) supported_exn
          val oops_supported = List.mapPartial (check (fn _ => check_supported)) unsupported
          val oops_exn_supported = List.mapPartial (check (fn _ => check_exn_supported)) unsupported_exn
          val bad_llbit = List.mapPartial (check (fn ll => fn _ => not o check_llbit ll)) supported
          val roundtrip = List.mapPartial (tryn roundtrip) all_instrs
          val () = print "\n"
          val stringtrip = List.mapPartial (tryn stringroundtrip) all_instrs
          val () = print "\n"
      in print ("Bad instruction length:\n  " ^ descl bad ^ "\n" ^
                "Unsupported instructions:\n  " ^ descl bad_unsupported ^ "\n" ^
                "Unsupported instructions (exn):\n  " ^ descl bad_exn_unsupported ^ "\n" ^
                "Unexpectedly supported instructions:\n  " ^ descl oops_supported ^ "\n" ^
                "Unexpectedly supported instructions (exn):\n  " ^ descl oops_exn_supported ^ "\n" ^
                "Badly classified LLbit:\n  " ^ descl bad_llbit ^ "\n" ^
                "Fails decode / encode / decode round trip:\n  " ^ descl roundtrip ^ "\n" ^
                "Fails decode / ToString / FromString round trip:\n  " ^ descl stringtrip ^ "\n")
      end

  datatype llbit_status = NoLL | AllLL | OnlyLL of (int * (bool * instr_frag list * string))

fun pll NoLL = "NoLL"
  | pll AllLL = "AllLL"
  | pll (OnlyLL (_,(_,_,s))) = "Only " ^ s

  fun pick_one_instr (gen,rs) =
    let val only_exception_instrs = only_exception_instrs ()
    in fn delay_slot => fn llbit_allowed => fn exn =>
      let val instrs = if delay_slot then always_usable_instrs else usable_instrs
(*val () = print (pll llbit_allowed ^ "\n")*)
          val instrs = case llbit_allowed of 
                           NoLL => instrs
                         | AllLL => strip_ll store_conditional_instrs @
                                    strip_ll cheri_store_conditional_instrs @
                                    instrs
                         (* If we're restricted to one SC instruction, increase
                            the weighting *)
                         | OnlyLL (w,i) => (3*w,i)::instrs
          fun exn_filter (_,(_,_,s)) =
              not (mem s (if exn then no_exception_instrs else only_exception_instrs))
          val instrs = filter exn_filter instrs
          val (_,(llbitmask,fmt,s)) = GenerationTools.weighted_select instrs gen
          val masked_llbit_allowed = if llbitmask then llbit_allowed else NoLL
          val new_llbit_allowed = 
              if exn then masked_llbit_allowed else
              case Lib.assoc1 s all_store_conditional of
                  NONE => masked_llbit_allowed
                | SOME (_,i) => OnlyLL i
(*val () = print (s ^ "\n")
val () = print (pll masked_llbit_allowed ^ "\n")
val () = print (pll new_llbit_allowed ^ "\n")*)
          val i = mk_instr (gen,rs) fmt
      in ((to_hex i,s,4),new_llbit_allowed)
      end
    end
  fun gen_one_instr_normal gen = fst (pick_one_instr (gen, (ref [], ref [])) false AllLL false)

  val respect_delay_slots = ref true
  datatype sc_choice = SCAnywhere | AnySCAfterLL | SCMatchingLL
  val store_conditionals = ref SCMatchingLL

  val sc_option =
      ("storeconditionals",
       (fn "anywhere" => store_conditionals := SCAnywhere
        | "anyafterll" => store_conditionals := AnySCAfterLL 
        | "matching" => store_conditionals := SCMatchingLL
        | s => failwith (s ^ " is not 'anywhere', 'afteranyll', or 'matching'"),
        fn () => case !store_conditionals of
                     SCAnywhere => "anywhere"
                   | AnySCAfterLL => "anyafterll"
                   | SCMatchingLL => "matching"))

  fun update_sc SCAnywhere _ = AllLL
    | update_sc AnySCAfterLL NoLL = NoLL
    | update_sc AnySCAfterLL _ = AllLL
    | update_sc SCMatchLL x = x
  val update_sc = fn x => update_sc (!store_conditionals) x

  (* This doesn't include the branch likely instructions, where the next
     instruction executed might or might *not* be in a delay slot; we leave
     it to the model to reject cases where it is and we put control flow in *)
  val delay_slot_must_follow =
      ["JR", "JALR", "BLTZ", "BGEZ", "BLTZAL", "BGEZAL", "J", "JAL",
       "BEQ", "BNE", "BLEZ", "BGTZ",
       "CJR(Cap)", "CJALR(Cap, Cap)", "CBTU(Cap, offset)", "CBTS(Cap, offset)"]

  fun gen_instrs_exn_normal gen n exns =
      let val rs = (ref [], ref [])
          val pick = pick_one_instr (gen,rs)
          fun mk m delayslot llbit_allowed acc =
              if m = n then rev acc else
              let val exn = (mem m exns)
                  val ((h,s,sz),llbit_allowed') =
                      pick
                         (!respect_delay_slots andalso delayslot)
                         (update_sc llbit_allowed)
                         exn
                  val delayslot' = (not exn) andalso
                                   List.exists (fn s' => s = s') delay_slot_must_follow
              in mk (m+1) delayslot' llbit_allowed' ((h,s,sz)::acc)
              end
      in mk 0 false NoLL []
      end
  fun gen_instrs_normal gen n = gen_instrs_exn_normal gen n []
  fun sample_instrs_normal gen =
      map (fn (_,(_,fmt,s)) => (to_hex (mk_instr (gen, (ref [], ref [])) fmt),s)) usable_instrs
  fun regs_chosen_in_instr _ = Feedback.fail ()

val undefined_instruction_terms = [
   ``ReservedInstruction``,
   ``Unpredictable``,
   ``COP1 ()``,
   ``COP2 (CHERICOP2 UnknownCapInstruction)``]

val bdd_encodings = ref NONE
fun bad_instr tm =
  exists (term_eq tm) (
   (* Undefined in some way *)
   undefined_instruction_terms @ [
   (* Bad idea in some way *)
   ``COP2 (CHERICOP2 DumpCapReg)``,
   ``TLBP``,
   ``TLBR``,
   ``TLBWI``,
   ``TLBWR``])
  orelse term_eq (fst (strip_comb tm)) ``CP``

fun get_bdd_encodings () =
  case !bdd_encodings of
      NONE =>
      let val () = print "Extracting instruction encodings...\n"
          val encs = Dedecoder.compute_bdd_encodings cheriTheory.inventory ``Decode ^(Dedecoder.mk_word 32)``
          val encs = List.filter (not o bad_instr o snd) encs
          val () = print "...done\n"
          val () = bdd_encodings := SOME encs
      in encs
      end
    | SOME encs => encs

fun gen_one_instr_auto gen =
  let val encs = get_bdd_encodings ()
      val (_,(bdd,instr)) = RandGen.select encs gen
      val instr = GenerationTools.describe_instr "cheri" instr
      fun bit () = hd (RandGen.bits gen 1)
      val bits = SimpleBDD.sample_bdd bit 32 bdd
      val hex = GenerationTools.to_hex bits
  in (hex,instr,4)
  end

fun gen_instrs_auto gen n = List.tabulate (n, fn _ => gen_one_instr_auto gen)
fun gen_instrs_exn_auto gen n _ = gen_instrs_auto gen n

fun sample_instrs_auto gen =
  let fun sample (bdd,instr) =
        let val instr = GenerationTools.describe_instr "cheri" instr
            fun bit () = hd (RandGen.bits gen 1)
            val bits = SimpleBDD.sample_bdd bit 32 bdd
            val hex = GenerationTools.to_hex bits
        in (hex,instr)
        end
  in map sample (get_bdd_encodings ())
  end


fun term_of_bit i 0 = mk_neg (mk_var ("b" ^ Int.toString i, bool))
  | term_of_bit i _ = mk_var ("b" ^ Int.toString i, bool)

fun term_of_frag i frag =
  case frag of
    Lit l => list_mk_conj (Lib.mapi (fn j => term_of_bit (i+j)) l)
  | RegNZ => list_mk_disj (List.tabulate (5,fn j => term_of_bit (i+j) 1))
  | RegNL => list_mk_disj (List.tabulate (5,fn j => term_of_bit (i+j) 0))
  | _ => T

fun term_of_frags l =
  let fun aux i [] = T
        | aux i [frag] = term_of_frag i frag
        | aux i (h::t) = mk_conj (term_of_frag i h, aux (i+frag_size h) t)
  in aux 0 l
  end

(* Compare the encodings found by processing the decode function against the
   manually written generator above.  To run this, start HOL; load "Generate";
   then Generate.compare_formats();. *)
fun compare_formats () =
  let val model_term =
          Dedecoder.compute_all_encodings_term
             cheriTheory.inventory
             ``Decode ^(Dedecoder.mk_word 32)``
             undefined_instruction_terms
      val formats_term = list_mk_disj (List.map (fn (_,(_,frags,_)) => term_of_frags frags) all_instrs)
      val model_not_in_formats = SimpleBDD.term_to_dnf_bit_strings 32
                                    (mk_conj (model_term, mk_neg formats_term))
      val () = print ("DNF of encodings in model's decoder but not in formats:\n" ^
                      String.concatWith "\n" model_not_in_formats ^ "\n")
      val formats_not_in_model = SimpleBDD.term_to_dnf_bit_strings 32
                                    (mk_conj (mk_neg model_term, formats_term))
      val () = print ("DNF of encodings in formats but not model's decoder:\n" ^
                      String.concatWith "\n" formats_not_in_model ^ "\n")
  in ()
  end

val autoextract = ref false

fun gen_one_instr gen =
  if !autoextract then gen_one_instr_auto gen else gen_one_instr_normal gen
fun gen_instrs gen =
  if !autoextract then gen_instrs_auto gen else gen_instrs_normal gen
fun gen_instrs_exn gen =
  if !autoextract then gen_instrs_exn_auto gen else gen_instrs_exn_normal gen
fun sample_instrs gen =
  if !autoextract then sample_instrs_auto gen else sample_instrs_normal gen

val additional_options =
 [Tools.bool_option "extract-encodings" autoextract,
  Tools.bool_option "strict-unspec" strict_unspec,
  sc_option]

end
