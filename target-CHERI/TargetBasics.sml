structure TargetBasics : TargetBasics =
struct

fun compset () =
    let val cmp = utilsLib.theory_compset ([], cheriTheory.inventory)
    in cmp
    end
val smt_non_eval_terms : Term.term list = []
val nextstate = EvalBasics.next_fn

end
