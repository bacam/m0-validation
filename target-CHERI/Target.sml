structure Target : sig
  include Target
  val bootmem_size : IntInf.int
  val preamble_size : IntInf.int
  val handler_vaddr : IntInf.int
  val handler_offset : IntInf.int
  val handler_size : IntInf.int
  val instr_fetch_value : Term.term
  val instr_fetch_hyps : Term.term list
  val cu2 : bool ref
  datatype startmode = StartUser | StartKernel | StartAny
  val start_mode : startmode ref
  datatype HarnessCode =
    Word of IntInf.int
  | Double of IntInf.int
  | Instr of term
  | SetCapTags
  | CapFile
  | RegFile
  | SpecialRegs
  | CapCause
  | LoadCapFile
  | LoadRegFile
  | Label of string
  | Using of string * (IntInf.int -> HarnessCode)
  | UsingL of string * (IntInf.int -> HarnessCode list)
  | Padding of IntInf.int (* in words *)
  val preamble : HarnessCode list
  val preamble_labels : (string, IntInf.int) Redblackmap.dict
  val harness_type_code : unit -> IntInf.int
  val pcc_source_cap_num : unit -> IntInf.int
end =
struct

local
open HolKernel Parse boolLib bossLib 

in

(* There's a constant called "c" in the CHERI model, which is a pain because
   it occasionally gets used for a free variable. *)
val _ = hide "c"

datatype HarnessType =
  KernelCapJump
| ERET_ERL
| ERET_EXL

val harness_type = ref ERET_EXL
fun harness_type_code () : IntInf.int = 
  case !harness_type of
      KernelCapJump => 0
    | ERET_ERL => 1
    | ERET_EXL => 2

val cu2 = ref true

val zero = Arbnum.zero
val li = Arbnum.fromLargeInt

(* Note that we don't work with the usual 37bit physical addresses, but the
   35bit addresses that the the_MEM field maps to capability-sized chunks.  The
   translation is done in the model, so we don't really see the 37bit ones.
 *)
fun to_psize addr = li (addr div 32)
fun to_paddr addr =
    let val addr =
            if addr >= 0xFFFFFFFF80000000 andalso
               addr <  0xFFFFFFFFA0000000
            then addr mod 0x10000000000 - 0xFF80000000
            else addr mod 0x10000000000
    in to_psize addr
    end

fun code_addr_translate tm =
    Tools.cbv_eval ``if ^tm >= 0xFFFFFFFF80000000w : word64 /\ ^tm < 0xFFFFFFFFA0000000w
                     then (39 >< 5) ^tm - 0x7FC000000w (* 0xFF80000000 div 32 *)
                     else (39 >< 5) ^tm : 35 word``

(* We need some capability register to be the same as the PCC for the harness
   code.  When using a capability jump it can be any general purpose capability
   register, but when we use ERET to jump to the code it has to be EPCC (31). *)
fun pcc_source_cap_num () : IntInf.int =
    case !harness_type of
        KernelCapJump => 19
      | ERET_ERL => 31
      | ERET_EXL => 31

val handler_vaddr = 0xffffffff80000000 + 0x180
val handler_size = 0x180


(* <--- Harness code for HWTest.  Here because we need to know its size, and
   HWTest needs information from here *)

datatype HarnessCode =
  Word of IntInf.int
| Double of IntInf.int
| Instr of term
| SetCapTags
| CapFile
| RegFile
| SpecialRegs
| CapCause
| LoadCapFile
| LoadRegFile
| Label of string
| Using of string * (IntInf.int -> HarnessCode)
| UsingL of string * (IntInf.int -> HarnessCode list)
| Padding of IntInf.int (* in words *)


(* Jump over the capability register values and
           register values and set $31 to point to them *)
val caps_size = 8 * 32
val regs_size = 2*34 (* $1 is done separately *)

(* Note that we assume that sizes can be calculated from labels present in the
   code, and not external data added later. *)

fun harness_calc code =
  let fun aux (c,(lbls,n)) =
        let fun l i = (lbls,n+i)
            fun f (Word _)       = l 1
              | f (Double _)     = l 2
              | f (Instr _)      = l 1
              | f SetCapTags     = l 2
              | f CapFile        = l caps_size
              | f RegFile        = l regs_size
              | f SpecialRegs    = l 0 (* 8 counted in RegFile *)
              | f CapCause       = l 2
              | f LoadCapFile    = l 31
              | f LoadRegFile    = l 30
              | f (Label s)      = (Redblackmap.insert (lbls, s, 4*n), n)
              | f (Using (s,g))  = f (g (Redblackmap.find (lbls,s) handle Not_found => 0))
              | f (UsingL (s,g)) = foldl aux (lbls,n) (g (Redblackmap.find (lbls,s) handle Not_found => 0))
              | f (Padding n)    = l n
        in f c
        end
      val (lbls,words) = foldl aux (Redblackmap.mkDict String.compare, 0) code
  in (lbls, 4 * words)
  end

(* We subsitute a dummy zero for any unknown value while calculating the size,
   but we have to be careful not to attempt to construct an invalid term. *)
fun mkimm v = wordsSyntax.mk_wordi (Arbnum.fromLargeInt (if v < 0 then 0 else v),16)
fun statusflags ksu erl exl =
  let val erl = case !harness_type of KernelCapJump =>  erl
                                    | ERET_ERL => 4 (* Will be cleared by ERET *)
                                    | ERET_EXL => 0
      val exl = case !harness_type of KernelCapJump =>  exl
                                    | ERET_ERL => exl
                                    | ERET_EXL => 2 (* Will be cleared by ERET *)
      val cu2 = if !cu2 then 0x40000000 else 0
      (* CU1 must be on so that the register dumping instruction will work *)
      val cu1 = 0x10000000
      val uxsxkx = 0xe0
      val bev = 0 (* handlers in RAM *)
  in cu1 + cu2 + 8 * ksu + erl + exl + uxsxkx + bev
  end
(* Set IE off; EXL, ERL, KSU as given; UX, SX, KX on (as per CHERI model,
   but not on qemu by default, and not specified); CU1, CU2, BEV as appropriate *)
val statusflags_mask = 0xffffffffffffffff - 0x504000ff

local
  val i = ref 0
in
  fun align_to_cap () =
    let val n = !i
        val () = i := n+1
        val l = "align" ^ Int.toString n
    in [Label l,
        Using (l, fn offset => Padding ((31 - ((offset-1) mod 32)) div 4))]
    end
end

fun imm_load r v =
  let val r = wordsSyntax.mk_wordii (r,5)
      val (h0,v)  = (v mod 65536, v div 65536)
      val (h1,v)  = (v mod 65536, v div 65536)
      val (h2,h3) = (v mod 65536, v div 65536)

  in [Instr ``ArithI (LUI (^r,^(mkimm h3)))``,
      Instr ``ArithI (ORI (^r,^r,^(mkimm h2)))``,
      Instr ``Shift (DSLL (^r,^r,16w))``,
      Instr ``ArithI (ORI (^r,^r,^(mkimm h1)))``,
      Instr ``Shift (DSLL (^r,^r,16w))``,
      Instr ``ArithI (ORI (^r,^r,^(mkimm h0)))``]
  end

(* Some of the instructions in the preamble are given as constants because they
   predate encoding support, and I'm too lazy to rewrite them all. *)

val preamble = [
        (* Use a branch with link to grab the address *)
        Instr ``Branch (BGEZAL (0w,1w))``,

        (* Enable CHERI coprocessor access (CU2), keep original value in $13 for later *)
        Instr ``CP (MFC0 (13w,12w,0w))``, (* mfc0        $13, c0_status *)
        (* remember that branch? Sets $31 to here *) Label "addr from branch",
        Word 0x3C024000, (* lui $2,0x4000 *)
        Instr ``ArithR (OR (13w,2w,1w))``,
        Word 0x40816000, (* mtc0        $1, c0_status *)

        Instr ``CP (MFC0 (2w,16w,0w))``, (* Config into $2 *)
        Instr ``ArithI (DADDI (0w,3w,~7w))``,
        Instr ``ArithR (AND (2w,3w,2w))``,
        Instr ``ArithI (ORI (2w,2w,3w))``,  (* Set K0 to 3 *)
        Instr ``CP (MTC0 (2w,16w,0w))``,

        SetCapTags,

        (* Set up pointer to reg values / end of cap values *)
        Using ("reg_data_start", fn reg_data_start =>
          Using ("addr from branch", fn addr_from_branch =>
            Word (0x67FE0000 + (reg_data_start - addr_from_branch)))), (* daddiu $30, $31, ... *)

        (* Set up pointer to cap values *)
        Using ("cap_data_start", fn cap_data_start =>
          Using ("addr from branch", fn addr_from_branch =>
            Instr ``ArithI (DADDIU (31w,31w,^(mkimm (cap_data_start - addr_from_branch))))``)),

        (* Copy handlers *)
        Using ("reg_data_start", fn reg_data_start =>
          Using ("handler_offset", fn handler_offset =>
            Word (0x67C10000 + handler_offset - reg_data_start))), (* daddiu $1, $30, ... (source) *)

        Using ("reg_data_start", fn reg_data_start =>
          Using ("handler location offset", fn loc_offset =>
            Word (0xDFC20000 + (loc_offset - reg_data_start)))), (* ld          $2, ...($30) *)
        Word (0x60430000 + handler_size), (* daddi $3,$2,handler_size *)
        Word 0x8C240000, (* lw $4,0($1) *)
        Word 0xAC440000, (* sw $4,0($2) *)
        Word 0x60420004, (* daddi $2,$2,4 *)
        Word 0x1443fffc, (* bne $2,$3,0xfffc *)
        Word 0x60210004, (* daddi $1,$1,4  (delay slot) *)

        (* Set C0 offset to make reading its proper value back later easy *)
        Word 0x49a007c1, (* csetoffset  c0, c0, $31 *)

        Label "cap_setup",
        (* Assume c3 is default cap throughout,
           $31 points to data for cap,
           $30 contains address at end,
           $29 contains tag bits
           $1 - $7, $28, c1, c2 used internally *)

        (* Check tag bit; don't rebuild cap if it's clear so that we keep the reserved
           bits. *)
        Word 0x33BC0001,  (* andi $28,$29,1 *)
        Word 0x13800010,  (* beq $28, $0,16 *)
        Word 0x001DE87A,  (* dsrl $29, $29, 1  (delay slot) *)

        Word 0xD823f800,  (* clc         c1, c3, $31, 0  *)
        Word 0x48010802,  (* cgetbase    $1, c1 *)
        Word 0x49a20802,  (* cgetoffset  $2, c1 *)
        Word 0x48030803,  (* cgetlen     $3, c1 *)
        (* Tag is separate *)
        Word 0x48050806,  (* cgetsealed  $5, c1 *)
        Word 0x48060800,  (* cgetperm    $6, c1 *)
        Word 0x48070801,  (* cgettype    $7, c1 *)

        Word 0x49a21841,  (* csetoffset  c2, c3, $1 *)
        Word 0x482210c0,  (* csetbounds  c2, c2, $3 *)
        Word 0x49a21081,  (* csetoffset  c2, c2, $2 *)
        
        Word 0x10A00003,  (* beq $5,$0,3 *)
        
        (* delay slot *)
        Word 0x48821180,  (* candperm    c2, c2, $6 *)

        (* Put type in offset for cseal *)
        Word 0x49a119c1,  (* csetoffset  c1, c3, $7 *)
        Word 0x48421040,  (* cseal       c2, c2, c1 *)

        Word 0xF843f800,  (* csc         c2, c3, $31, 0 *)
        Word 0x67FF0020,  (* daddiu $31,$31,32 *)

        Label "cap_branch",
        Using ("cap_setup", fn cap_setup => Using ("cap_branch", fn cap_branch =>
          Word (0x17DF0000 + (0x10000 - (cap_branch - cap_setup) div 4 - 1)))), (* bne $30, $31, -... *)
        Word 0,  (* delay slot nop *)

        LoadCapFile,

        CapCause,

        (* Read special regs; remember that we've set C0's offset to point at the cap regs *)
        Word (0xDC000000 + 0x10000 + 32*32), (* ld $1,1024($0) *)
        Word 0x00200013, (* mtlo $1 *)
        Word (0xDC000000 + 0x10000 + 32*32 + 8), (* ld $1,1032($0) *)
        Word 0x00200011, (* mthi $1 *)
        Word (0xDC000000 + 0x10000 + 32*32 + 2*8), (* ld $1,...($0) *)
        Word 0x40a17000, (* dmtc0       $1, c0_epc *)
        Word (0xDC000000 + 0x10000 + 32*32 + 3*8), (* ld $1,...($0) *)
        Word 0x40a1f000, (* dmtc0       $1, c0_errorepc *)

        (* Set up Status register in $1, using original value from $13 earlier *)
        UsingL ("status flags", fn floff => [UsingL ("cap_data_start", fn base =>
          [Instr ``Load (LD (0w,2w,^(mkimm (floff - base))))``,
           Instr ``Load (LD (0w,3w,^(mkimm (floff - base + 8))))``])]),
        Instr ``ArithR (AND (2w,13w,1w))``,
        Instr ``ArithR (OR (3w,1w,1w))``,

        (* Set up TLB to map a bit of user space to the test case, should match
           targetScript.user_tlb_entry.  Note that we're relying on full TLB 
           initialisation not being required by CHERI. *)
        Instr ``CP (DMTC0 (0w, 0w, 0w))``, (* Index 0 *)
        Instr ``ArithI (LUI (2w, 0x100w))``,
        Instr ``ArithI (ADDI (2w, 2w, 0x1Fw))``,
        Instr ``CP (DMTC0 (2w, 2w, 0w))``, (* EntryLo0 *)
        Instr ``ArithI (ADDI (2w, 2w, 0x40w))``,
        Instr ``CP (DMTC0 (2w, 3w, 0w))``, (* EntryLo1 *)
        Instr ``CP (DMTC0 (0w, 5w, 0w))``, (* PageMask *)
        Instr ``ArithI (LUI (2w, 0x4000w))``,
        Instr ``CP (DMTC0 (2w, 10w, 0w))``, (* EntryHi *)
        Instr ``TLBWI``,

        (* Read GPR *)
        LoadRegFile,

        UsingL ("harness type", fn 0 => (* KernelCapJump *)
            [Word 0x40816000, (* mtc0        $1, c0_status *)
             UsingL ("r1", fn r1 => imm_load 1 r1),
             (* cjr c<pcc_source_cap_num> *)
             Using ("pcc_source_cap_num",
                    fn cap => Word (0x49000000 + 0x800 * cap)),
             (* delay slot:  clc         c0, c0, $0, 0 *)
             Word 0xd8000000]

          | _ => (* ERET_* *)
            [Word 0xd8000000, (* clc         c0, c0, $0, 0 *)
             Word 0x40816000, (* mtc0        $1, c0_status *)
             UsingL ("r1", fn r1 => imm_load 1 r1),
             Instr ``ERET``])
        ]@

        align_to_cap ()

        @[

        Label "cap_data_start",
        CapFile,

        Label "reg_data_start",

        (* Special registers *)
        SpecialRegs,
        (* GPR *)
        RegFile,

        Label "status flags",
        Double statusflags_mask,
        Using ("ksu", fn ksu => Using ("erl", fn erl => Using ("exl", fn exl =>
               Double (statusflags ksu erl exl)))),

        Label "handler location offset",
        (* Handler location *)
        Double handler_vaddr]@

        (* Round up init to a 256 bit boundary (needed due to the memory repr) *)
        align_to_cap ()


(* ---> *)

(* l3mips / BERI bluespec boot rom
   We leave space for the register initialisation preamble *)
val (preamble_labels, preamble_size) = harness_calc preamble
val start_vaddr = 0x9800000040000000+preamble_size
val bootmem_size = 2048*4
val handler_offset = bootmem_size - handler_size
val vsize = bootmem_size-preamble_size-handler_size
val ram_details = [(to_paddr start_vaddr, to_psize vsize),
                   (to_paddr handler_vaddr, to_psize handler_size)]

(* FIXME: ought to check for exceptions *)
fun pc s = ``^s.c_state.c_PC + ^s.c_pcc.base``
fun memory_term s = ``^s.the_MEM``
val memory_field_update = ``cheri_state_the_MEM_fupd``
val memory_type = ``:35 word -> DataType``
val memory_element_size = 256
val memory_element_ty = ``:DataType``
val raw = ``Raw``
fun mk_memory_element w = mk_comb (raw, w)
val sort_addresses = updateLib.SORT_WORD_UPDATES_CONV ``:64``
(* The LIST_UPDATE_ELIM_CONV works around a bug where nested list updates are broken by sorting *)
val sort_registers = updateLib.SORT_WORD_UPDATES_CONV ``:5`` THENC TOP_DEPTH_CONV updateLib.LIST_UPDATE_ELIM_CONV
val state_type = ``:cheri_state``


val abstract_updates = [
   ("mem", ``cheri_state_the_MEM_fupd``),
   ("gpr", ``cheri_state_c_gpr_fupd``)
   (* We can't include capr in this list unless we change the representation of
      the basic state so that ``EL (w2n r)`` doesn't appear, because that is not
      handled by the SMT solver. *)
]


local

(* Part of data selection for CStore found on CHERI *)
val data_calc_rw = Q.prove(
  `8 * w2n (w:word3) = w2n (8w : word64 * (w2w w))`,
  wordsLib.n2w_INTRO_TAC 66 THEN
  blastLib.BBLAST_TAC);

in

(* Target specific manipulations to make to step theorems *)
fun fixup_th th =
    th |>
    (* Make sure that the logs (which are :num -> string list updates) are
       properly cleared *)
    CONV_RULE (DEPTH_CONV updateLib.SORT_NUM_UPDATES_CONV) |>

    SIMP_RULE arith_ss [targetTheory.byte_select1,
                        targetTheory.byte_select2,
                        targetTheory.byte_select3,
                        data_calc_rw]

end

val word_size = 64
val word_type = ``:word64``
val addr_size = 35
val addr_type = ``:35 word``

val big_endian = true

val basic_cp0 =
  ``<| BadVAddr := ARB;
       Cause := ARB;
       Compare := cp0compare;
       Config := ARB with <| K0 := 3w; BE := ^(bitstringSyntax.term_of_bool big_endian) |>;
       Config6 := <| LTLB := F |>; (* No BERI/CHERI direct TLB *)
       Count := cp0count;
       Debug := ARB;
       EPC := cp0_epc;
       EntryHi := ARB;
       EntryLo0 := ARB;
       EntryLo1 := ARB;
       ErrCtl := ARB;
       ErrorEPC := cp0_errorepc;
       Index := ARB;
       LLAddr := ARB;
       PRId := ARB;
       PageMask := ARB;
       Random := ARB;
       Status := <| CU3 := F; CU2 := cp0_cu2; CU1 := F; CU0 := T;
                    FR := F; RE := F; BEV := F; IM := 0w; KX := T; SX := T; UX := T;
                    KSU := cp0_ksu; ERL := cp0_erl; EXL := cp0_exl; IE := F |>;
       Wired := ARB;
       XContext := ARB;
       UsrLocal := 0w
    |>``

val default_cap = Tools.cbv_eval ``defaultCap``

val cap_fields = TypeBase.fields_of ``:Capability``

fun mk_cap suffix =
  let fun v s ty = mk_var ("cap_" ^ s ^ suffix, ty)
  in TypeBase.mk_record (``:Capability``, map (fn (s,ty) => (s,v s ty)) cap_fields)
  end
val pcc_source_cap = mk_cap "_pcc"

(* Equalities on records don't translate well to Yices at the moment, so
   generate a constraint for each field *)
fun pcc_eq_constraints () =
  let val n = pcc_source_cap_num ()
      val s = IntInf.toString n
  in map (fn (fld,ty) => mk_eq (mk_var ("cap_" ^ fld ^ "_pcc", ty), mk_var ("cap_" ^ fld ^ s, ty)))
         cap_fields
  end

val basic_caps =
    let fun mk r = combinSyntax.mk_update (wordsSyntax.mk_wordii (r,5),
                                           (mk_cap o int_to_string) r)
        val caps = List.tabulate (32, mk)
    in foldl mk_comb ``ARB:word5 -> Capability`` caps
    end

val cap_constraints =
    let fun cap n =
          let val suffix = int_to_string n
              fun v s = mk_var ("cap_" ^ s ^ suffix, assoc s cap_fields)
              val tag = v "tag"
              val sealed = v "sealed"
              val otype = v "otype"
              val base = v "base"
              val len = v "length"
              val reserved = v "reserved"
          in ``^tag ==>
                 ((w2w ^base + w2w ^len <+ 0x10000000000000000w :65 word) /\
                 ((¬ ^sealed) ==> (^otype = 0w)) /\
                 (^reserved = 0w))`` (* we cannot play with reserved bits on real caps *)
          end
    in List.tabulate (32, cap)
    end

fun rand_caps gen =
    let fun cap n =
            let val suffix = int_to_string n
                fun v s ty = mk_var ("cap_" ^ s ^ suffix, ty)
                fun bool () = if hd (RandGen.bits gen 1) then T else F
                fun w n = Tools.random_word gen n 0
                fun field s =
                  let val ty = assoc s cap_fields
                      val var = v s ty
                      val vl = if ty = ``:bool`` then bool ()
                               else w (fcpSyntax.dest_int_numeric_type
                                          (wordsSyntax.dest_word_type ty))
                  in var |-> vl
                  end
            in [field "tag",
                field "sealed",
                field "cursor",
(* Mentioned in the constraints, SMT solver should produce valid values,
this won't.
                field "base",
                field "length",
                field "reserved",
*)
                field "otype",
                field "uperms",
                field "perms"]
            end
    in List.concat (List.tabulate (32, cap))
    end

(* Check that we didn't leave any fields out in rand_caps *)
val () = if length cap_fields <> 9 then failwith "update rand_caps and length check" else ()

val basic_state =
  ``<| c_state := <|
         c_PC := pc;
         c_BranchDelay := NONE;
         c_BranchTo := NONE;
         c_CP0 := ^basic_cp0;
         c_LLbit := NONE;
         c_exceptionSignalled := F;
         (* We always use concrete values for hi and lo; if we don't then Yices
            produces slightly more complex output (e.g., I've just seen it return
            hi = lo, which we don't currently deal with). *)
         c_hi := SOME hi_reg;
         c_lo := SOME lo_reg
       |>;
       BranchDelayPCC := NONE;
       BranchToPCC := NONE;
       CCallBranchDelay := false;
       CCallBranch := false;
       the_MEM := m;
       exception := NoException;
       c_gpr := reg;
       c_capr := ^basic_caps;
       c_pcc := ^pcc_source_cap;
       capcause := <| ExcCode := exccode; RegNum := capcausereg |>;

       c_TLB_assoc := (0w =+ SOME user_tlb_entry) (K NONE);

       procID := 0w;
       totalCore := 1;
       JTAG_UART := <| base_address := 0x000000007f000000w >>> 3 |>;
       PIC_base_address := \i. (0x7f804000w + w2w i * 0x4000w) >>> 3;
       trace_level := 0

    |>``;

fun rand_regs gen =
    (* If we were to assign a non-zero value to register 0 it would be ignored
       anyway *)
    let fun rr 0 = ``0w:word64``
          | rr _ = Tools.random_word gen 64 0
        val vals = List.tabulate (32, rr)
        val hol_vals = listSyntax.mk_list (vals, ``:word64``)
    in ``\r:word5. EL (w2n r) ^hol_vals``
    end

fun fill_in_state gen s mem =
    let val regs = rand_regs gen
        val caps = rand_caps gen
        val hi = Tools.random_word gen 64 0
        val lo = Tools.random_word gen 64 0
        val epc = Tools.random_word gen 64 0
        val errorepc = Tools.random_word gen 64 0
        val exccode = Tools.random_word gen 8 0
        val capcausereg = Tools.random_word gen 8 0
        val (exl, erl, ksu) =
            if hd (RandGen.bits gen 1)
            then (false, false, ``2w:word2``)
            else (hd (RandGen.bits gen 1), hd (RandGen.bits gen 1),
                  snd (RandGen.select [``0w:word2``,``1w:word2``,``2w:word2``] gen))
    in subst ([``reg:word5 -> word64`` |-> regs,
               mk_var ("m", memory_type) |-> mem,
               ``hi_reg:word64`` |-> hi,
               ``lo_reg:word64`` |-> lo,
               ``cp0_epc:word64`` |-> epc,
               ``cp0_errorepc:word64`` |-> errorepc,
               ``cp0_exl:bool``  |-> bitstringSyntax.term_of_bool exl,
               ``cp0_erl:bool``  |-> bitstringSyntax.term_of_bool erl,
               ``cp0_ksu:word2`` |-> ksu,
               ``exccode:word8`` |-> exccode,
               ``capcausereg:word8`` |-> capcausereg
              ]@caps) s
    end

datatype startmode = StartUser | StartKernel | StartAny
val start_mode = ref StartAny


val additional_options =
[("harness",
  (fn s =>
      if s = "KernelCapJump" then harness_type := KernelCapJump
      else if s = "ERET_ERL" then harness_type := ERET_ERL
      else if s = "ERET_EXL" then harness_type := ERET_EXL
      else (failwith ("Unknown harness type: " ^ s)),
   fn () => case !harness_type of 
                KernelCapJump => "KernelCapJump"
              | ERET_ERL => "ERET_ERL"
              | ERET_EXL => "ERET_EXL")),
 Tools.bool_option "cu2" cu2,
 ("start-mode",
  (fn s => if s = "user" then start_mode := StartUser
           else if s = "kernel" then start_mode := StartKernel
           else if s = "any" then start_mode := StartAny
           else (failwith ("Unknown start mode; " ^ s)),
   fn () =>
      case !start_mode of StartUser => "user"
                        | StartKernel => "kernel"
                        | StartAny => "any"))
]

fun check_options () =
  if !harness_type = KernelCapJump
  then if not (!cu2)
       then failwith "Can't turn CU2 (CHERI instructions) off and use KernelCapJump harness"
       else if !start_mode = StartUser
       then failwith "Can't start in user mode using KernelCapJump harness"
       else ()
  else ()

fun additional_constraints (th:Thm.thm) instr_lengths mem_counter cbv_eval =
  let val (hy,cn) = dest_thm th
      val init_state = rand (lhs cn)
      val init_pure_pc = cbv_eval ``^init_state.c_state.c_PC``
      val init_pc = cbv_eval ``^init_pure_pc + ^init_state.c_pcc.base``
      val init_mem = cbv_eval ``^init_state.the_MEM``
      val final_state = rhs cn
      val final_delay = cbv_eval ``^final_state.c_state.c_BranchDelay``
      val final_pc = cbv_eval ``^final_state.c_state.c_PC``
      (* We don't have a way to set CHERI tag bits in memory yet, so force
         them all to false *)
      val tags_off = List.tabulate (mem_counter,
                       fn n => ``case ^(init_mem) (memory_address ^(numSyntax.term_of_int n)) of Raw _ => T | _ => F``)
      (* Note that we'll rig one of the capabilities, usually c19, to be PCC, below.
         This makes the test harness easy (perhaps even possible) to write.
         I've chosen c19 because it's a general purpose cap in the calling
         conventions, so it's unlikely to be treated specially by any of
         the instructions.  Note that the ERET version of the harness has
         to use c31 (EPCC) instead. *)
      val pcc_source_cap' = mk_cap (IntInf.toString (pcc_source_cap_num ()))
      val harness_specific =
          case !harness_type of
              KernelCapJump =>
              [(* ensure that we can use CJR by setting the Global permission *)
               Tools.cbv_eval ``(getPerms ^pcc_source_cap).Global``,
               (* This harness depends on being in kernel mode *)
               ``(cp0_ksu = 0w : word2) \/ cp0_exl \/ cp0_erl``,
               (* We jump using a capability, so the PC will end up fixed to the PCC's offset *)
               Tools.cbv_eval ``getOffset ^pcc_source_cap = pc : word64``]
            | ERET_ERL => [``~(cp0_erl: bool)``,
                           ``cp0_errorepc = ^init_pure_pc``]
            | ERET_EXL => [``~(cp0_erl: bool)``,
                           ``~(cp0_exl: bool)``,
                           ``cp0_epc = ^init_pure_pc``]
      (* FIXME: it looks like Fetch has a broken alignment check (only does PC, not PC+base)
         so fix it up here for now. *)
      val pc_align =
          List.tabulate
             (length instr_lengths,
              fn n => ``(1 >< 0) ((instr_start ^(numSyntax.term_of_int n)) : word64) = 0w : word2``)
      val start =
          case !start_mode of
              StartUser => [``(cp0_ksu = 2w : word2) /\ ~cp0_exl /\ ~cp0_erl``]
            | StartKernel => [``(cp0_ksu = 0w : word2) \/ cp0_exl \/ cp0_erl``]
            | StartAny => []
  in [(* Make sure that we end up somewhere valid for the post-test
         instructions, i.e., not a delay-slot where we might jump away,
         and properly aligned in case we've just finished a jump *)
      ``^final_delay = NONE``, ``(1 >< 0) ^final_pc = 0w : word64``,
      ``cp0_cu2 = ^(lift_bool bool (!cu2))``,
      (* KSU = 3 isn't legal, but we appear to be able to set it anyway... *)
      ``cp0_ksu <> 3w : word2``] @
     pcc_eq_constraints () @
     tags_off @
     pc_align @
     harness_specific @
     cap_constraints @
     start
  end

val smt_rewrites : Thm.thm list = [cheriTheory.NotWordValue_def,
                                   targetTheory.no_arb_word_select2]
val smt_simps : Thm.thm list = [targetTheory.Capability_literal_constructor]

val yices_irrelevant_updates = [``dynamicMemStats_fupd``, ``staticMemStats_fupd``, ``memAccessStats_fupd``]

val instr_fetch_value =
    rhs (concl (REWRITE_CONV [targetTheory.no_arb_word_select2] InstrBehaviour.fetch_value))
val instr_fetch_hyps = InstrBehaviour.fetch_hyps

type code = string

val step = InstrBehaviour.next
val step_code = InstrBehaviour.next_code

val step_exn = InstrBehaviour.next_exn
val step_code_exn = InstrBehaviour.next_code_exn

fun hex s = Option.valOf (StringCvt.scanString (Int.scan StringCvt.HEX) s);

(* The disassembler needs the bytes of 32bit instructions to be split up. *)
fun breakup_hex s =
    let val n = hex s
        val b4 = Int.fmt StringCvt.HEX (n mod 256)
        val b3 = Int.fmt StringCvt.HEX (n div 256 mod 256)
        val b2 = Int.fmt StringCvt.HEX (n div 256 div 256 mod 256)
        val b1 = Int.fmt StringCvt.HEX (n div 256 div 256 div 256)
    in (b1, b2, b3, b4)
    end

fun breakup_hex_llvm_mc s =
    let val (b1,b2,b3,b4) = breakup_hex s
    in "0x" ^ b1 ^ " 0x" ^ b2 ^ " 0x" ^ b3 ^ " 0x" ^ b4 ^ "\n"
    end

fun breakup_hex_printf s =
    let val (b1,b2,b3,b4) = breakup_hex s
    in "\\x" ^ b4 ^ "\\x" ^ b3 ^ "\\x" ^ b2 ^ "\\x" ^ b1
    end

fun disassemble_hex s =
    let val s = if String.isPrefix "0x" s then String.extract (s,2,NONE) else s
        val hex_tm = wordsSyntax.mk_word_from_hex_string (stringSyntax.fromMLstring s,``:32``)
        val tm = Tools.cbv_eval ``instructionToString (Decode ^hex_tm)``
        (* We might need some extra evaluation to get the string literal *)
        val tm = rhs (concl (EVAL tm))
    in StringCvt.padLeft #"0" 8 s ^ " " ^ stringSyntax.fromHOLstring tm
    end handle e => StringCvt.padLeft #"0" 8 s ^ " unknown\n"

fun print_disassembled_hex s =
    print (disassemble_hex s ^ "\n")

val encode = EvalBasics.encode

(* Due to branch delay slots, we don't choose whether a branch is taken while
   looking at the branch instruction itself, but on the instruction after the
   branch.  We recognise branches by looking to see if the BranchDelay part of
   the state can be filled. *)

datatype delayslot = NotDS (*| ContDS*) | BranchDS | BranchPCCDS

fun what_is_the_delay_slot (tm, pcctm) =
    if optionSyntax.is_none tm then
       if optionSyntax.is_none pcctm then NotDS
       else if optionSyntax.is_some pcctm then BranchPCCDS
       else Feedback.failwith ("Unexpected PCC delay slot term: " ^ term_to_string pcctm)
    else if optionSyntax.is_some tm then
       let val tm' = optionSyntax.dest_some tm
       (* Complete delay slot modelling isn't in the CHERI model yet *)
       in (*if optionSyntax.is_none tm' then ContDS
          else if optionSyntax.is_some tm' then BranchDS
          else Feedback.failwith ("Unexpected delay slot term: " ^ term_to_string tm)*)
          BranchDS
       end
    else Feedback.failwith ("Unexpected delay slot term: " ^ term_to_string tm)

fun thm_branch_delay th =
    (* Get the conclusion's idea of the post-state delay slot, then rewrite
       with the hypotheses because it might be s.DelaySlot *)
    let val t = (optionSyntax.dest_some o rhs o concl) th
        val concl_ds = Tools.cbv_eval ``^t.c_state.c_BranchDelay``
        val concl_ds_pcc = Tools.cbv_eval ``^t.BranchDelayPCC``
    (* Use SIMP_CONV rather than REWRITE because sometimes we need to use an ==> *)
    in ((rhs o concl) (QCONV (SIMP_CONV std_ss (map ASSUME (hyp th))) concl_ds),
        (rhs o concl) (QCONV (SIMP_CONV std_ss (map ASSUME (hyp th))) concl_ds_pcc))
    end

fun opt_conv tm =
    if optionSyntax.is_none tm then SOME false
    else if optionSyntax.is_some tm then SOME true
    else NONE

(* The idea here is to see if assuming the current branch delay state makes the
   hypotheses for the theorem false. *)
fun test_delay_slot th bd bdpcc =
    (not o (term_eq F) o rhs o concl)
       (QCONV (SIMP_CONV std_ss
                  [ASSUME ``s.c_state.c_BranchDelay = ^bd``,
                   ASSUME ``s.BranchDelayPCC = ^bdpcc``]) (list_mk_conj (hyp th)))

local
val none = ``NONE           : word64 option``
val some = ``SOME dscontent : word64 option``
val nonecap = ``NONE           : (word64 # Capability) option``
val somecap = ``SOME dscontent : (word64 # Capability) option``
in
fun compatible_delay_slot NotDS       th = test_delay_slot th none nonecap
  | compatible_delay_slot BranchDS    th = test_delay_slot th some nonecap
  | compatible_delay_slot BranchPCCDS th = test_delay_slot th none somecap
end

fun filter_branch_delay delay_slot ths =
    filter (compatible_delay_slot delay_slot o snd) ths

fun cu2_ok (_,th) =
    (not o (term_eq F) o rhs o concl)
       (QCONV (SIMP_CONV std_ss [ASSUME ``s.c_state.c_CP0.Status.CU2 = ^(lift_bool bool (!cu2))``]) (list_mk_conj (hyp th)))

exception Bad_choices of int list

type compatibility_info = delayslot

fun filter_compatible delay_slot thms =
  let val thms = mapi (fn i => fn t => (i,t)) thms
      val thms = filter cu2_ok thms
      val thms = filter_branch_delay delay_slot thms
  in thms
  end

fun choose_thms gen thmss =
    let val (end_delay_slot,info,choices) =
            foldl (fn (thms,(delay_slot,info,choices)) =>
                      let val thms = filter_compatible delay_slot thms
                          val () = case thms of
                                       [] => (print "Bad choice: Delay slot incompatibility.\n";
                                              raise (Bad_choices (rev choices)))
                                     | _ => ()
                          val (_,(choice,th)) = RandGen.select thms gen
                          val next_delay_slot = what_is_the_delay_slot (thm_branch_delay th)
                      in (next_delay_slot,delay_slot::info,choice::choices)
                      end) (NotDS,[],[]) thmss
        val () = if end_delay_slot <> NotDS
                 then (print "Bad choice: Delay slot after end of sequence.\n"; raise (Bad_choices (rev choices)))
                 else ()
    in (rev info, rev choices)
    end

end

end

