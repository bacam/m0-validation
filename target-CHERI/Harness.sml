structure Harness =
struct
  datatype basic_harness = Breakpoint

  datatype harness =
    Basic of basic_harness
(*  | NOP_padding_after of int * basic_harness*)

fun basic_harness_to_src Breakpoint = "Breakpoint"

fun harness_to_src (Basic b) = "(Basic " ^ basic_harness_to_src b ^ ")"
(*  | harness_to_src (NOP_padding_after (n,b)) =
    "(NOP_padding_after (" ^ Int.toString n ^ "," ^ basic_harness_to_src b ^ "))"
*)

(* No timing for MIPS *)
fun basic_harness_cost Breakpoint = 0
fun harness_cost (Basic b) = basic_harness_cost b
(*  | harness_cost (NOP_padding_after (n,b)) = n + basic_harness_cost b*)

fun basic_pc_offset Breakpoint = 0

fun pc_offset (Basic b) = basic_pc_offset b
(*  | pc_offset (NOP_padding_after (n,b)) = 2*n + basic_pc_offset b*)

fun fix_instr s m pc instr =
    (* The information about the model's instruction Fetch function is given
       in terms of a state represented by a free variable s.  We need to
       replace that with concrete values here, and perform some reduction
       to deal with projections from the state. *)
    let val vars = free_varsl (Target.instr_fetch_value::Target.instr_fetch_hyps)
        val freshen_sub = List.mapPartial
                             (fn v => if v = ``s:cheri_state`` then NONE
                                      else SOME (v |-> genvar (type_of v)))
                             vars
        val sub = Tools.cbv_eval o 
                  (subst ([``s.c_state.c_PC + s.c_pcc.base`` |-> pc,
                           ``s.the_MEM`` |-> m,
                           ``s : cheri_state`` |-> s]@freshen_sub))
        val instr_fetch_value = sub Target.instr_fetch_value
        val hyps = map sub Target.instr_fetch_hyps
  in (mk_eq (instr_fetch_value, instr))::hyps
  end

fun harness_instrs h =
    map (fn instr => wordsSyntax.mk_wordii (instr,32))
    [ (* dump registers | dump cap registers | exit simulator *)
       0x4080d000,
       0x48800006,
       0x4080b800
    ]
(*    let val is = case h of
                     Basic Breakpoint => [0xbe00]
                   | NOP_padding_after (nops,Breakpoint) =>
                     (List.tabulate (nops,fn _ => 0xbf00))@[0xbe00]
    in map (fn i => [i mod 256, i div 256]) is
    end*)

fun preamble_constraints () = []

end
