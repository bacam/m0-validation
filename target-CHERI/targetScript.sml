open HolKernel Parse boolLib bossLib

val _ = new_theory "target";

(* Get rid of the logs after each step.  (The next step clears them anyway, but
   if we get rid of them here the memory usage appears to become more
   reasonable. *)
val NextStateCHERI_def = Define`
    NextStateCHERI s =
    let s' = Next s
    in if s'.exception = NoException then SOME s' else NONE`

(* We don't fix start values for Random and Wired, so avoid the update to Random
   in Fetch blowing up exponentially due to the repetition of Random. *)
val random_update_def = Define`
    random_update (random:word8) wired = (if random = wired then 15w else random + 255w)`

(* The normalization of arithmetic terms in the simplifier can arrange terms
   in several ways, and I haven't yet found a yet to make the simplifier apply
   this theorem to all arrangements, so we have several variants. *)
val byte_select1 = store_thm (
   "byte_select1",
   ``!w : word3. !w' : word64. ((n + 8 * w2n w) >< (8 * w2n w)) w' =
        if w = 0w then  (n >< 0) w'
   else if w = 1w then ((n + 8) >< 8) w'
   else if w = 2w then ((n + 16) >< 16) w'
   else if w = 3w then ((n + 24) >< 24) w'
   else if w = 4w then ((n + 32) >< 32) w'
   else if w = 5w then ((n + 40) >< 40) w'
   else if w = 6w then ((n + 48) >< 48) w'
   else                ((n + 56) >< 56) w'``,
   wordsLib.Cases_word_value THEN GEN_TAC THEN EVAL_TAC);

val byte_select2 = store_thm (
   "byte_select2",
   ``!w : word3. !w' : word64. ((8 * w2n w + n) >< (8 * w2n w)) w' =
        if w = 0w then  (n >< 0) w'
   else if w = 1w then ((8  + n) >< 8) w'
   else if w = 2w then ((16 + n) >< 16) w'
   else if w = 3w then ((24 + n) >< 24) w'
   else if w = 4w then ((32 + n) >< 32) w'
   else if w = 5w then ((40 + n) >< 40) w'
   else if w = 6w then ((48 + n) >< 48) w'
   else                ((56 + n) >< 56) w'``,
   wordsLib.Cases_word_value THEN GEN_TAC THEN EVAL_TAC);

val byte_select3 = store_thm (
   "byte_select3",
   ``!w : word3. !w' : word64. ((w2n w * 8 + n - 1) >< (w2n w * 8)) w' =
        if w = 0w then  (n - 1 >< 0) w'
   else if w = 1w then ((8  + n - 1) >< 8) w'
   else if w = 2w then ((16 + n - 1) >< 16) w'
   else if w = 3w then ((24 + n - 1) >< 24) w'
   else if w = 4w then ((32 + n - 1) >< 32) w'
   else if w = 5w then ((40 + n - 1) >< 40) w'
   else if w = 6w then ((48 + n - 1) >< 48) w'
   else                ((56 + n - 1) >< 56) w'``,
   wordsLib.Cases_word_value THEN GEN_TAC THEN EVAL_TAC);

val no_arb_word_select2 = store_thm (
   "no_arb_word_select2",
   ``(if w = 0w : word2 then w0 : 'a
         else if w = 1w then w1
         else if w = 2w then w2
         else if w = 3w then w3
         else ARB) = 
     (if w = 0w : word2 then w0 : 'a
         else if w = 1w then w1 : 'a
         else if w = 2w then w2 : 'a
                        else w3 : 'a)``,
     wordsLib.Cases_on_word_value `w : word2` THEN EVAL_TAC);

val no_arb_word_state1 = store_thm (
   "no_arb_word_state1",
   ``(if w = 0w : word1 then (w0,s) : 'a # 'b
     else if w = 1w     then (w1,s)
                        else ARB) = 
     ((if w = 0w : word1 then w0 : 'a
                         else w1 : 'a),s)``,
     wordsLib.Cases_on_word_value `w : word1` THEN EVAL_TAC);

val no_arb_word_state2 = store_thm (
   "no_arb_word_state2",
   ``(if w = 0w : word2 then (w0,s) : 'a # 'b
         else if w = 1w then (w1,s)
         else if w = 2w then (w2,s)
         else if w = 3w then (w3,s)
         else ARB) = 
     ((if w = 0w : word2 then w0 : 'a
          else if w = 1w then w1 : 'a
          else if w = 2w then w2 : 'a
                         else w3 : 'a),s)``,
     wordsLib.Cases_on_word_value `w : word2` THEN EVAL_TAC);

val no_arb_word_state3 = store_thm (
   "no_arb_word_state3",
   ``(if w = 0w : word3 then (w0,s) : 'a # 'b
     else if w = 1w then (w1,s)
     else if w = 2w then (w2,s)
     else if w = 3w then (w3,s)
     else if w = 4w then (w4,s)
     else if w = 5w then (w5,s)
     else if w = 6w then (w6,s)
     else if w = 7w then (w7,s)
     else ARB) = 
     ((if w = 0w : word3 then w0 : 'a
     else if w = 1w then w1 : 'a
     else if w = 2w then w2 : 'a
     else if w = 3w then w3 : 'a
     else if w = 4w then w4 : 'a
     else if w = 5w then w5 : 'a
     else if w = 6w then w6 : 'a
     else                w7 : 'a),s)``,
     wordsLib.Cases_on_word_value `w : word3` THEN EVAL_TAC);

fun record_to_literal ty =
  let val {Thy = thy, Tyop = str, ...} = dest_thy_type ty
      val constructor =
          case TypeBase.constructors_of ty of
              [tm] => tm
            | _ => failwith ("Type " ^ str ^ " should have exactly one constructor")
      val tys = fst (wfrecUtils.strip_fun_type (type_of constructor))
      val args = map genvar tys
      val tm = list_mk_comb (constructor,args)
      val th = DB.fetch thy (str ^ "_updates_eq_literal")
      val updates = TypeBase.updates_of ty
  in REWRITE_RULE (combinTheory.K_THM::updates) (SPEC tm (GSYM th))
  end

val _ = save_thm ("Capability_literal_constructor", record_to_literal ``:Capability``);

val bit_field_insert_thm =
   save_thm ("bit_field_insert_thm",
   utilsLib.map_conv blastLib.BBLAST_PROVE
      [
       ``!a b. bit_field_insert 255 192 (a: word64) (b: 257 word) =
               ((256 >< 256) b : 1 word) @@ (a @@ ((191 >< 0) b : 192 word)) : 256 word``,
       ``!a b. bit_field_insert 191 128 (a: word64) (b: 257 word) =
               ((256 >< 192) b : 65 word) @@ (a @@ ((127 >< 0) b : 128 word)) : 192 word``,
       ``!a b. bit_field_insert 127 64 (a: word64) (b: 257 word) =
               ((256 >< 128) b : 129 word) @@ (a @@ ((63 >< 0) b : 64 word)) : 128 word``,
       ``!a b. bit_field_insert 63 0 (a: word64) (b: 257 word) =
               ((256 >< 64) b : 193 word) @@ a``,
       ``!a b. bit_field_insert 2 0 (a: 3 word) (b: 40 word) =
               ((39 >< 3) b : 37 word) @@ a``,
       ``!a b c. bit_field_insert 63 1 (a : 63 word)
                   (bit_field_insert 0 0 (b : 1 word) (c : word64)) =
               a @@ b``,
       ``!a b c. bit_field_insert 63 31 (a : 33 word)
                   (bit_field_insert 30 0 (b : 31 word) (c : word64)) =
               a @@ b``,
       ``!a b c. bit_field_insert 63 24 (a : 40 word)
                   (bit_field_insert 23 0 (b : 24 word) (c : word64)) =
               a @@ b``,
       ``!a b c d. bit_field_insert 63 16 (a : 48 word)
                   (bit_field_insert 15 8 (b : 8 word)
                   (bit_field_insert 7 0 (c : 8 word) (d : word64))) =
               a @@ ((b @@ c) : 16 word)``,
       (* For, e.g., 40842002 mtc0        $4, c0_context, 0x2 *)
       ``!a b c. bit_field_insert 30 15 (a : 16 word)
                   (bit_field_insert 14 0 (b : 15 word) (c : word64)) =
               ((63 >< 31) c : 33 word) @@ (a @@ b) : 31 word``
      ])

(* LO and HI extract the value of the appropriate register if there is one, but
   return ARB if there isn't.  ARB is generally not useful because we can't
   calculate with it, so we use a couple of rewrites to force the registers to
   contain a real value.  (Note that we can't leave this to the symbolic
   execution because it doesn't play a direct role in calculating the state.) *)

val LO_rw =
  save_thm("LO_rw",
    simpLib.SIMP_PROVE bossLib.std_ss [cheriTheory.LO_def,cheriTheory.lo_def]
                       ``(s.c_state.c_lo = SOME v) ==> (LO s = v)``)

val HI_rw =
  save_thm("HI_rw",
    simpLib.SIMP_PROVE bossLib.std_ss [cheriTheory.HI_def,cheriTheory.hi_def]
                       ``(s.c_state.c_hi = SOME v) ==> (HI s = v)``)

fun cap_load_select_s size =
    let val tmsize = numSyntax.term_of_int size
        val intermediate_type = Parse.ty_antiq (wordsSyntax.mk_int_word_type size)
    in store_thm ("cap_load_select_s" ^ Int.toString size,
          ``(v2w (SignExtendBitString
                     (64, field (w2n (a:word3) * 8 + ^tmsize - 1) (w2n a * 8) (w2v w))) : word64
             = sw2sw (((w2n a * 8 + ^tmsize - 1) >< (w2n a * 8)) (w : word64) : ^intermediate_type))``,
          wordsLib.Cases_on_word_value `a:word3`
          THEN REWRITE_TAC [cheriTheory.SignExtendBitString_def]
          THEN computeLib.EVAL_TAC
          THEN blastLib.BBLAST_TAC); ()
    end;

val _ = List.app cap_load_select_s [8,16,32]

fun cap_load_select_u size =
    let val tmsize = numSyntax.term_of_int size
        val intermediate_type = Parse.ty_antiq (wordsSyntax.mk_int_word_type size)
    in store_thm ("cap_load_select_u" ^ Int.toString size,
          ``(v2w (ZeroExtendBitString
                     (64, field (w2n (a:word3) * 8 + ^tmsize - 1) (w2n a * 8) (w2v w)))) : word64
             = w2w (((w2n a * 8 + ^tmsize - 1) >< (w2n a * 8)) (w : word64) : ^intermediate_type)``,
          wordsLib.Cases_on_word_value `a:word3`
          THEN REWRITE_TAC [cheriTheory.ZeroExtendBitString_def]
          THEN computeLib.EVAL_TAC
          THEN blastLib.BBLAST_TAC); ()
    end;

val _ = List.app cap_load_select_u [8,16,32]

val cap_load_select_64 =
    store_thm ("cap_load_select_64",
               ``v2w (ZeroExtendBitString (64, field 63 0 (w2v (w:word64)))) = w``,
               REWRITE_TAC [cheriTheory.ZeroExtendBitString_def]
               THEN computeLib.EVAL_TAC
               THEN blastLib.BBLAST_TAC);


(* The mask calculation in StoreMemoryCap isn't supported by the SMT translation
   (and might be difficult for solvers).  We replace before symbolic execution
   because when the model is specialised to a particular instruction some of the
   w2ns may disappear, resulting in multiple cases to rewrite.  Here we can do
   it with a single rewrite. *)
val mask_rw = prove (
  ``(let l = 64:num - (w2n (AccessLength:word3) + 1 + w2n (loaddr : word3)) * 8 in
     let mask = 1w : word64 << (l + (w2n AccessLength + 1) * 8) − (1w << l) in
     Q mask) =
    (let mask = if w2w AccessLength + w2w loaddr < 8w : word5
                then ((1w : word64 <<~ ((w2w AccessLength + 1w) * 8w)) - 1w) <<~
                       (64w - (w2w AccessLength + 1w + w2w loaddr) * 8w)
                else (1w : word64 <<~ ((w2w AccessLength + 1w) * 8w)) - 1w in Q mask)``,
  wordsLib.Cases_on_word_value `AccessLength : word3`
  THEN wordsLib.Cases_on_word_value `loaddr : word3`
  THEN wordsLib.WORD_EVAL_TAC)

val StoreMemoryCap_alt = save_thm ("StoreMemoryCap_alt",
  Ho_Rewrite.REWRITE_RULE [mask_rw] cheriTheory.StoreMemoryCap_def)


(* NB: if this is changed then the shorten_word_in_cap rewrite below will
  need to be updated or symbolic execution will quietly get slower. *)
val user_tlb_entry_def =
  Define` user_tlb_entry = 
    <| (* Map two pages (8kB) with minimal change in address... *)
       VPN2 := 0x20000w;
       ASID := 0w;
       Mask := 0w;
       R := 0w;
       G := T;
       (* ...to the boot ROM *)
       PFN0 := 0x40000w;
       C0 := 3w;
       D0 := T;
       V0 := T;
       L0 := F;
       S0 := F;
       PFN1 := 0x40001w;
       C1 := 3w;
       D1 := T;
       V1 := T;
       L1 := F;
       S1 := F
     |>`

val v2w_addr_rw = Q.prove (
 `v2w (field 27 0 x ++ field 11 0 y) : 40 word =
  v2w (field 27 0 x) : 28 word @@ v2w (field 11 0 y) : 12 word`,
  MATCH_MP_TAC utilTheory.v2w_append
  THEN SIMP_TAC (srw_ss()) [])

(* Specialisation of AddressTranslation that limits memory accesses to areas that
   we use during tests.  Note that there is only one execution path through the
   replacement function body (for successful translations); this avoids early
   decisions about processor modes and address ranges, allowing them to be made
   by the SMT solver.

   The third area of memory is intended to cover the general exception handler
   and ccall/creturn handler, when in uncached memory (i.e., BEV is true).

   As we used a fixed TLB with one entry, we only allow TLB refill exceptions
   and not exceptions relating to invalid or read-only TLB entries.  We also
   don't try to do address exceptions, because they tend to rely on processor
   mode.
 *)
val AddressTranslation_rw = store_thm(
  "AddressTranslation_rw",
  ``!vAddr AccessType s.
    (s.c_state.c_CP0.Config.K0 = 3w) ==>
    (s.c_TLB_assoc = (0w =+ SOME user_tlb_entry) (K NONE)) ==>
    ~s.c_state.c_CP0.Config6.LTLB ==>
    (vAddr <+ 0x40000000w + 8192w) \/
    (KernelMode s /\
     ((0x9800000040000000w <=+ vAddr /\ vAddr <+ 0x9800000040000000w + 8192w) \/
      (0xffffffff80000000w + 0x180w <=+ vAddr /\ vAddr <+ 0xffffffff80000000w + 0x280w + 0x80w))) ==>
    (AddressTranslation (vAddr,AccessType) s =
     if  vAddr <+ 0x40000000w
     then (ARB, SND (SignalTLBException
                            (if AccessType = LOAD then XTLBRefillL
                             else XTLBRefillS,(CP0 s).EntryHi.ASID,
                             vAddr) s))
     else
     (if 0xffffffff80000000w + 0x180w <=+ vAddr
     then ((39 >< 0) vAddr − 0xFF80000000w,3w,F,F)
     else ((39 >< 0) vAddr,3w,F,F), s)
    )``,

    REWRITE_TAC [user_tlb_entry_def]
    THEN REPEAT STRIP_TAC
    THENL
    [`vAddr <+ 0x10000000000w` by wordsLib.WORD_DECIDE_TAC
     THEN `CheckSegment vAddr s = (NONE,T)` by
           (ASM_SIMP_TAC bool_ss [cheriTheory.CheckSegment_def]
            THEN NTAC 2 utilLib.bv_cmp_tac
            THEN ASM_SIMP_TAC bool_ss [])
     THEN Cases_on `0x40000000w <=+ vAddr`
     THEN utilLib.bv_cmp_tac
     THENL [utilLib.bv_cmp_tac, ALL_TAC]
     THEN ASM_SIMP_TAC (srw_ss()) [cheriTheory.AddressTranslation_def,boolTheory.LET_THM]
     THEN ASM_SIMP_TAC (srw_ss()) [cheriTheory.LookupTLB_def,cheriTheory.TLBAssocEntries_def,cheriTheory.TLB_assoc_def,cheriTheory.CP0_def]
     THEN ASM_SIMP_TAC (srw_ss()++utilLib.FOR_ss++utilLib.lit_LET_ss) [utilTheory.bind_let,combinTheory.UPDATE_APPLY]
     THEN utilLib.decide blastLib.FULL_BBLAST_TAC
            ``(0w :word2 = (63 >< 62) vAddr) /\ ((0x20000w : 27 word = (39 >< 13) (vAddr : word64)))``
     THEN ASM_SIMP_TAC (srw_ss()) [boolTheory.LET_THM,combinTheory.UPDATE_APPLY,cheriTheory.checkMask_def]
     THEN Cases_on `word_bit 12 (vAddr : word64)`
     THEN ASM_SIMP_TAC (srw_ss()++wordsLib.WORD_ss) [cheriTheory.check_cca_def]
     THEN ASM_SIMP_TAC (srw_ss()++wordsLib.WORD_ss) [v2w_addr_rw,
                                                     utilTheory.v2w_field_w2v_gen,
                                                     utilTheory.v2w_field_w2v_id]
     THEN blastLib.FULL_BBLAST_TAC
    ,
     `~UserMode s /\ ~SupervisorMode s` by
       FULL_SIMP_TAC (srw_ss()) [cheriTheory.UserMode_def,cheriTheory.SupervisorMode_def,cheriTheory.KernelMode_def]
     THEN ``(61 >< 59) (vAddr : word64) = 3w : word3`` via
          blastLib.FULL_BBLAST_TAC
     THEN ASM_SIMP_TAC (srw_ss()) [cheriTheory.AddressTranslation_def,cheriTheory.CheckSegment_def]
     THEN REPEAT utilLib.bv_cmp_tac
     THEN ASM_SIMP_TAC (srw_ss()) [cheriTheory.check_cca_def,boolTheory.LET_THM]
     THEN REPEAT utilLib.bv_eq_tac
    ,
     `~UserMode s /\ ~SupervisorMode s` by
       FULL_SIMP_TAC (srw_ss()) [cheriTheory.UserMode_def,cheriTheory.SupervisorMode_def,cheriTheory.KernelMode_def]
     THEN ASM_SIMP_TAC (srw_ss()) [cheriTheory.AddressTranslation_def,cheriTheory.CheckSegment_def]
     THEN REPEAT utilLib.bv_cmp_tac
     THEN ASM_SIMP_TAC (srw_ss()) [cheriTheory.check_cca_def,boolTheory.LET_THM,cheriTheory.CP0_def]
    ]);

(* Simplify the conditional for selecting a 64-bit word out of a capability
   sized chunk of memory.  Note that this is quite specific to the current
   memory set-up above. *)
val shorten_word_in_cap = store_thm(
  "shorten_word_in_cap",
  ``!x : 40 word. !y : word3.
    ((4 >< 3) ((if b then x + 0x80000000w else x) ?? w2w y) = ((4 >< 3) x) : word2) /\
    ((4 >< 3) (if b then x + 0x80000000w else x) = ((4 >< 3) x) : word2)``,
  blastLib.BBLAST_TAC)

val _ = export_theory ();

