structure EvalBasics : EvalBasics where type exception_info = Term.term =
struct

local
open Drule HolKernel boolLib bossLib targetTheory Tactical Rewrite
in

val inventory = cheriTheory.inventory
val thy = "cheri"
val extra_thms = [
   NextStateCHERI_def,
   bit_field_insert_thm,
(* Ideally we'd leave these to SymExec.full_cmp, but eta-reduction by
   computeLib removes their arguments, preventing their expansion. *)
   cheriTheory.capToBits_def,
   cheriTheory.bitsToCap_def,
(* avoid Random update in Fetch blowing up *)
   GSYM random_update_def,

   no_arb_word_state1,
   no_arb_word_state2,
   no_arb_word_state3,

   shorten_word_in_cap
]
val extra_defs = [
(* A slightly rewritten version of StoreMemoryCap that's easier to translate for
   an SMT solver *)
   StoreMemoryCap_alt
]
val state_ty = ``:cheri_state``

val discard_fns = ["raise'exception"]
val opaque = ["JTAG_UART_load","PIC_load","SignExtendBitString","LO","HI",
              "StoreMemoryCap","AddressTranslation"]@discard_fns


(* Processor exceptions, not L3 exceptions *)
val exception_fns = ["SignalException", "SignalCapException", "SignalTLBException"]

type exception_info = term
fun is_exception (f,args) =
    if mem ((fst o dest_const) f) exception_fns
    then SOME (hd args)
    else NONE


val mem_rws = [cap_load_select_64,
               cap_load_select_s8,cap_load_select_s16,cap_load_select_s32,
               cap_load_select_u8,cap_load_select_u16,cap_load_select_u32,
               UNDISCH_ALL (SPEC_ALL targetTheory.AddressTranslation_rw)]

(* This needs the simplifier's higher-order matching *)
val mem_access_state_rw =
  prove (``(case x of Cap cap => (f cap,s) | Raw raw => (g raw,s)) : 'a # cheri_state
            = ((case x of Cap cap => f cap | Raw raw => g raw),s)``,
         Cases_on `x:DataType`
         THEN EVAL_TAC);

val eval_simp_ths = [mem_access_state_rw]

val late_rws = map UNDISCH_ALL [] (* All moved below *)
local
  (* Deal with the free var in these theorems that won't be matched *)
  val LO_rw = UNDISCH_ALL targetTheory.LO_rw
  val HI_rw = UNDISCH_ALL targetTheory.HI_rw
  fun LOHI_CONV th tm =
      let val th = INST [``v:word64`` |-> genvar ``:word64``] th
      in utilsLib.INST_REWRITE_CONV [th] tm
      end
  open simpLib
in
val late_ss =
    merge_ss
       [std_conv_ss {name = "LO", pats=[``LO s``], conv=LOHI_CONV LO_rw},
        std_conv_ss {name = "HI", pats=[``HI s``], conv=LOHI_CONV HI_rw}]
end

val next_fn = ``NextStateCHERI``

val junk_conds = [``v = uart.base_address``,
                  ``s.PIC_base_address n <=+ v /\ v <+ s.PIC_base_address n + m``]

val std_hyp =
    [``s.procID = 0w``,
     ``s.c_state.c_CP0.Config.BE``, ``~s.c_state.c_CP0.Status.RE``,
     ``~s.c_state.c_exceptionSignalled``, (* No MIPS exception *)
     ``s.exception = NoException``, (* No L3 exception *)
     ``s.c_state.c_BranchTo = NONE``,
     ``s.BranchToPCC = NONE``,
     (* The next two are conditional, so need to be given to the simplifier *)
     ``(s.c_state.c_BranchDelay <> NONE) ==> (s.BranchDelayPCC = NONE)``,
     ``(s.BranchDelayPCC <> NONE) ==> (s.c_state.c_BranchDelay = NONE)``,
     ``s.totalCore = 1``,
     ``¬(s.c_state.c_CP0.Compare = s.c_state.c_CP0.Count)``,
     ``s.trace_level = 0``,
     ``¬s.c_state.c_CP0.Status.IE``]

val fetch_tm = ``Fetch``

fun get_fetch eval =
    let val fetch =
            case eval std_hyp ``Fetch s`` of
                [th] => th
              | _ => failwith "'Fetch s' doesn't evaluate to 1 term"
        val fetched = (optionSyntax.dest_some o fst o pairSyntax.dest_pair o rhs o concl) fetch
        fun fix_instr v =
            PURE_REWRITE_RULE [ASSUME (mk_eq (fetched, v))] fetch
    in (hyp fetch, fetched, fix_instr)
    end

fun term_for_instr v = wordsSyntax.mk_wordi (Arbnum.fromLargeInt v,32)

end

fun encode q =
    let val lines = assemblerLib.quote_to_strings q
        fun line s =
            let val s = mips.stripSpaces s
            in if s = "" then NONE else SOME s
            end
        val instrs = List.mapPartial line lines
    in map (fn s => (mips.encodeInstruction s, 4)) instrs
    end

end
