structure HWTest : HWTest =
struct

local
open HolKernel boolLib
in

type connection = unit
exception Permanent_failure of string

fun connect () =
  let val cmd = "scripts/checkl3mips"
      val result = OS.Process.isSuccess (OS.Process.system cmd)
  in if result then () else
     failwith "Unable to find simulator executable"
  end
fun close () = ()
fun reset () = ()
fun cpu_id () = ""

val sail_binary_caps = ref false

(* See Target.sml for the harness code *)

fun upto f n m = if n = m then () else (f n; upto f (n+1) m)
fun inf_of_word w : IntInf.int = Arbnum.toLargeInt (wordsSyntax.dest_word_literal w)

fun write_preamble out s =
    let val regs = Tools.cbv_eval ``^s.c_gpr``
        val caps = Tools.cbv_eval ``^s.c_capr``
        val count = ref 0
        fun write_word v =
            let val (b4,v) = (v mod 256, v div 256)
                val (b3,v) = (v mod 256, v div 256)
                val (b2,v) = (v mod 256, v div 256)
                val (b1,v) = (v mod 256, v div 256)
                val () = BinIO.output1 (out,Word8.fromLargeInt b1)
                val () = BinIO.output1 (out,Word8.fromLargeInt b2)
                val () = BinIO.output1 (out,Word8.fromLargeInt b3)
                val () = BinIO.output1 (out,Word8.fromLargeInt b4)
            in count := !count + 4
            end
        fun write_double v =
            let val (w2,v) = (v mod 0x100000000, v div 0x100000000)
                val (w1,v) = (v mod 0x100000000, v div 0x100000000)
                val () = write_word w1
                val () = write_word w2
            in ()
            end
        fun write_cap_tags () =
            let fun aux r acc =
                if r = 32 then acc else
                if bitstringSyntax.bool_of_term
                      (Tools.cbv_eval ``(^caps ^(wordsSyntax.mk_wordii (31-r,5))).tag``)
                then aux (r+1) (2*acc + 1)
                else aux (r+1) (2*acc)
                val tags = aux 0 0
                val () = write_word (0x3C1D0000 + tags div 0x10000) (* lui $29, ... *)
                val () = write_word (0x37BD0000 + tags mod 0x10000) (* ori $29, $29, ... *)
            in ()
            end
        fun write_cap c =
            (*let val v = Tools.cbv_eval ``reg'Capability ^c``*)
            let val v = computeLib.CBV_CONV SymExec.full_cmp ``capToBits ^c``
                val v = rhs (concl v)
                val v = inf_of_word v
                val (w8,v) = (v mod 0x100000000, v div 0x100000000)
                val (w7,v) = (v mod 0x100000000, v div 0x100000000)
                val (w6,v) = (v mod 0x100000000, v div 0x100000000)
                val (w5,v) = (v mod 0x100000000, v div 0x100000000)
                val (w4,v) = (v mod 0x100000000, v div 0x100000000)
                val (w3,v) = (v mod 0x100000000, v div 0x100000000)
                val (w2,v) = (v mod 0x100000000, v div 0x100000000)
                val (w1,v) = (v mod 0x100000000, v div 0x100000000)
                val () = List.app write_word [w7,w8,w5,w6,w3,w4,w1,w2]
            in ()
            end
        fun write_ld_reg r =
            (* We've set up C0's offset to point at the cap registers, so the base
               register can be 0 *)
            (* ld $r,r($0) *)
            let val r = IntInf.fromInt r
                val instr = 0xDC000000 + r * 0x10000 (* target *) + 32*32 (* caps *) +
                            4*8 (* special regs *) + 8*(r-2) (* offset *)
            in write_word instr
            end
        fun write_clc r =
                (*  clc cr, c0, $0, ... *)
            let val r = IntInf.fromInt r
                val clc = 0xD8000000 + (0x00200000 * r) + (2 * r)
            in write_word clc
            end

        fun write_capcause () =
            let val capcause = Tools.cbv_eval ``^s.capcause``
                val exccode = Tools.cbv_eval ``^capcause.ExcCode``
                val exccode = inf_of_word exccode
                val regnum = Tools.cbv_eval ``^capcause.RegNum``
                val regnum = inf_of_word regnum
                val () = write_word (0x34010000 + exccode * 256 + regnum) (* ori $1,$0,... *)
                val () = write_word 0x48800044 (* csetcause   $1 *)
            in ()
            end

        fun reg_val r =
            let val v = Tools.cbv_eval ``^regs ^(wordsSyntax.mk_wordii (r,5))``
                val v = inf_of_word v
            in v
            end
        fun write_reg_val r =
            let val v = reg_val r
            in write_double v
            end
        fun write_cap_val r =
            let val () = if !count mod 32 <> 0
                         then failwith "Attempted to write unaligned capability"
                         else ()
                val v = Tools.cbv_eval ``^caps ^(wordsSyntax.mk_wordii (r,5))``
            in write_cap v
            end
        val lo = inf_of_word (rand (Tools.cbv_eval ``^s.c_state.c_lo``))
        val hi = inf_of_word (rand (Tools.cbv_eval ``^s.c_state.c_hi``))
        val epc = inf_of_word (Tools.cbv_eval ``^s.c_state.c_CP0.EPC``)
        val errorepc = inf_of_word (Tools.cbv_eval ``^s.c_state.c_CP0.ErrorEPC``)
        val exl = bitstringSyntax.bool_of_term (Tools.cbv_eval ``^s.c_state.c_CP0.Status.EXL``)
        val erl = bitstringSyntax.bool_of_term (Tools.cbv_eval ``^s.c_state.c_CP0.Status.ERL``)
        val ksu = inf_of_word (Tools.cbv_eval ``^s.c_state.c_CP0.Status.KSU``)

        val () = print ("Processor mode: KSU = " ^ IntInf.toString ksu ^
                        " ERL = " ^ (if erl then "true" else "false") ^
                        " EXL = " ^ (if exl then "true" else "false") ^ "\n")

        val labels = Redblackmap.insertList (Target.preamble_labels,
                         [("ksu", ksu),
                          ("erl", if erl then 4 else 0),
                          ("exl", if exl then 2 else 0),
                          ("handler_offset", Target.handler_offset),
                          ("r1",reg_val 1),
                          ("pcc_source_cap_num", Target.pcc_source_cap_num ()),
                          ("harness type", Target.harness_type_code ())])

        fun enc t = inf_of_word (Tools.cbv_eval ``Encode ^t``)

        local open Target in
        fun write (Word i)      = write_word i
          | write (Double d)    = write_double d
          | write (Instr i)     = write_word (enc i)
          | write SetCapTags    = write_cap_tags ()
          | write CapFile       = upto write_cap_val 0 32
          | write RegFile       = upto write_reg_val 2 32
          | write SpecialRegs   = List.app write_double [lo, hi, epc, errorepc]
          | write CapCause      = write_capcause ()
          | write LoadCapFile   = upto write_clc 1 32
          | write LoadRegFile   = upto write_ld_reg 2 32
          | write (Padding n)   = upto (fn _ => write_word 0) 0 (IntInf.toInt n)
          | write (Label s)     = (assert (fn x => Redblackmap.find (labels,s) = x) (!count); ())
          | write (Using (s,f)) = write (f (Redblackmap.find (labels, s)))
          | write (UsingL (s,f)) = List.app write (f (Redblackmap.find (labels, s)))
        end

        val () = List.app write Target.preamble

    in if !count <> Target.preamble_size
       then failwith ("Wrote " ^ IntInf.toString (!count) ^
                      " bytes of preamble, but expected to write " ^
                      IntInf.toString Target.preamble_size)
       else if (!count) mod 32 <> 0
       then failwith ("Preamble size " ^ IntInf.toString (!count) ^
                      " is not a multiple of 32")
       else print ("Test preamble is "^ IntInf.toString (!count) ^ " bytes long\n")
    end

val raw_constr = ``Raw``

(* Offsets of memory regions in boot memory *)
val ram_details = ListPair.zip (map (Arbnum.toLargeInt ## Arbnum.toLargeInt) Target.ram_details,
                                [0,Target.handler_offset - Target.preamble_size])

fun write_memory out bgmem s =
    let val mem = State.decompose_hol_memory (Tools.cbv_eval ``^s.the_MEM``) bgmem
        val bootmem = Word8Array.array (IntInf.toInt Target.bootmem_size, 0wx0)
        fun bytes off 0 v = []
          | bytes off n v = (off+n-1,v mod 256)::(bytes off (n-1) (v div 256))
        fun update_dword i w =
            List.app (fn (j,b) => Word8Array.update (bootmem,32*i+j,Word8.fromLargeInt b))
                     ((bytes 24 8 (w div 0x1000000000000000000000000000000000000000000000000))@
                      (bytes 16 8 (w div 0x100000000000000000000000000000000))@
                      (bytes  8 8 (w div 0x10000000000000000))@
                      (bytes  0 8 w))
        fun update_element i tm =
            if is_comb tm andalso term_eq (rator tm) raw_constr
            then update_dword i (inf_of_word (rand tm))
            else failwith ("Word at physical address " ^ Int.fmt StringCvt.HEX i ^
                           " is not Raw: " ^ term_to_string tm)
        fun map start content =
            case 
               List.find (fn ((start',len),_) =>
                             start >= start' andalso
                             start + IntInf.fromInt (Vector.length content) <= start'+len)
                         ram_details
             of NONE => failwith "Memory out of range"
              | SOME ((start',len), offset) => offset div 32 + (start - start')
        val () = List.app (fn (start, content) => 
                              let val start = Arbnum.toLargeInt start
                                  val off = map start content in
                                 Vector.appi (fn (i,c) => update_element (i + IntInf.toInt off) c)
                                             content
                              end) mem
    in BinIO.output (out, Word8Array.vector bootmem)
    end

fun report_final_regs out th =
    let fun print s = TextIO.output (out, s)
        val () = print "Post-state registers:\n"
        val post = (rhs o concl) th
        val regs = Tools.cbv_eval ``^post.c_gpr``
        fun conv n v =
            let val s = IntInf.fmt StringCvt.HEX v
                val s = String.map Char.toLower s
            in StringCvt.padLeft #"0" n s
            end
        fun padded_reg_num r =
          (if r < 10 then
             if !sail_binary_caps
             then "0"
             else " "
          else "") ^ Int.toString r
        fun reg r =
            let val v = Tools.cbv_eval ``^regs ^(wordsSyntax.mk_wordii (r,5))``
                val v = inf_of_word v
            in print ("DEBUG MIPS REG " ^ padded_reg_num r ^ "\t0x" ^ conv 16 v ^ "\n")
            end
        fun aux f 32 = ()
          | aux f n = let val () = f n in aux f (n+1) end
        val pc = Tools.cbv_eval ``^post.c_state.c_PC``
        val pc = inf_of_word pc 
        val () = print ("DEBUG MIPS PC\t0x" ^ conv 16 pc ^ "\n")
        val () = aux reg 0
        val capregs = Tools.cbv_eval ``^post.c_capr``
        val () = print "======   Registers   ======\n"
        val () = print "Core = 0\n"
        fun cap s r =
          if !sail_binary_caps then
             let val bits = trace ("notify type variable guesses",0)
                       Term `v2w [^r.tag] :word1 @@ ^r.reserved @@ ^r.otype @@
                             ^r.uperms @@ ^r.perms @@ v2w [^r.sealed] : word1 @@
                             (^r.cursor - ^r.base) @@ ^r.base @@ ^r.length`
                 val bits = with_flag (wordsLib.notify_on_word_length_guess, false)
                                      wordsLib.inst_word_lengths bits
                 val r = Tools.cbv_eval bits
                 val v = inf_of_word r
                 val v = StringCvt.padLeft #"0" 257 (IntInf.fmt StringCvt.BIN v)
             in print (s ^ " 0b" ^ v ^ "\n")
             end
          else
            let val tagged = if bitstringSyntax.dest_b (Tools.cbv_eval ``getTag ^r``)
                             then "1" else "0"
                val sealed = if bitstringSyntax.dest_b (Tools.cbv_eval ``getSealed ^r``)
                             then "1" else "0"
                val perms  = conv  8 (inf_of_word (Tools.cbv_eval ``(^r.uperms @@ ^r.perms) : 31 word``))
                val otype  = conv  6 (inf_of_word (Tools.cbv_eval ``getType ^r``))
                val offset = conv 16 (inf_of_word (Tools.cbv_eval ``getOffset ^r``))
                val base   = conv 16 (inf_of_word (Tools.cbv_eval ``getBase ^r``))
                val length = conv 16 (inf_of_word (Tools.cbv_eval ``getLength ^r``))
            in print (s ^ "\tt:" ^ tagged ^ " s:" ^ sealed ^ " perms:0x" ^ perms ^ " type:0x" ^ otype ^
                      " offset:0x" ^ offset ^ " base:0x" ^ base ^ " length:0x" ^ length  ^ "\n")
            end
        val () = cap "DEBUG CAP PCC   " (Tools.cbv_eval ``^post.c_pcc``)
        fun capreg r =
            let val s = "DEBUG CAP REG " ^ padded_reg_num r
            in cap s (Tools.cbv_eval ``^capregs ^(wordsSyntax.mk_wordii (r,5))``)
            end
        val () = aux capreg 0
    in ()
    end

fun check testfile regfile =
  let val () = print "Checking HOL model against simulation\n"
      val cmd = "scripts/l3cmp \"" ^ testfile ^ "\" \"" ^ regfile ^ "\""
      val result = OS.Process.isSuccess (OS.Process.system cmd)
      val () = print (if result then "OK\n" else "Failed\n")
  in result
  end

fun run_and_check con bgmem s full_th harness =
    let val mem_name = Logging.filename "test" ".mem" "/tmp/test.mem"
        val () = print ("Writing test case to " ^ mem_name ^ "\n")
        val out = BinIO.openOut mem_name
        val () = write_preamble out s
        val () = write_memory out bgmem s
        val () =  BinIO.closeOut out
        val reg_name = Logging.filename "test" ".reg" "/tmp/test.reg"
        val () = print ("Dumping expected register contents to " ^ reg_name ^ "\n")
        val out = TextIO.openOut reg_name
        val () = report_final_regs out full_th
        val () = TextIO.closeOut out
        val cmp =
            if !sail_binary_caps
            then (print "Skipping simulation check because we're in sail output mode\n"; true)
            else check mem_name reg_name
    in if cmp then NONE else SOME "regs"
    end

val options = [Tools.bool_option "sail-output" sail_binary_caps]

end
end
