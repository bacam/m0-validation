structure CheriEval =
struct

open Drule Term Thm Type Conv Rewrite Lib boolSyntax Feedback

(* The reg'... definitions aren't great for symbolic use, but utilsLib can
   already give us nice versions. *)
local
  val consts = #C cheriTheory.inventory
  val reg_consts = List.filter (String.isPrefix "reg'") consts
  val _ = assert (List.all (String.isSuffix "_def")) reg_consts
in
  val regs = map (fn s => String.substring (s,4,String.size s - 8)) reg_consts
  val reg_thms = map (utilsLib.mk_reg_thm "cheri") regs
end

(* I tried to use computeLib.scrub_const to get rid of the definitions for terms
   in opaque, but it seems to be too late by then: it has already noticed and
   reduced the terms in other definitions.  Instead, build the compset from scratch.

   We make two versions, one without let so that we can handle it specially, and a
   full one that we can use when we're no longer interested in a term.
 *)

val opaque = ["NotWordValue", "raise'exception", "SignalException", "SignalCapException", "LoadMemory"]
local open wordsSyntax in
  val words_opaque = [word_sdiv_tm, word_div_tm, word_mod_tm, word_to_hex_string_tm]
end
val extra_thms = [
   (*mips_stepTheory.NextStateMIPS_def,*)
   pairTheory.UNCURRY_DEF,
   optionTheory.IS_NONE_EQ_NONE, optionTheory.NOT_IS_SOME_EQ_NONE,
   boolTheory.DE_MORGAN_THM
]

(* These theorems push conditionals down into the state, but we only really want
   that for map fields, such as memory and registers.  Doing it for other fields
   can delay necessary case splits until quite late in the process.

   Hence we filter the theorems down to the generic ones (that do not mention
   the state) and the ones that update function fields. *)
local
  val all_cond_updates = utilsLib.mk_cond_update_thms [``:cheri_state``]
  fun is_interesting th =
      let val tys = (map type_of o fst o strip_forall o concl) th
          fun nice ty = is_type ty andalso fst (dest_type ty) = "fun"
      in (not (List.exists (fn ty => ty = ``:cheri_state``) tys)) orelse List.exists nice tys
      end
in
val cond_updates = List.filter is_interesting all_cond_updates
end

local
  open utilsLib Tactical computeLib
  (* For all definitions which act on the state we add "let s = s in" to the rhs
     so that the rest of the symbolic evaluation machinery gets a chance to
     process the term before it is substituted and used. *)
  val extra_let = Q.prove(`!P Q : cheri_state -> 'a.
                           (!s. P s = Q s) ==>
                           (!s. P s = let s = s in Q s)`, metisLib.METIS_TAC []);
  val extra_let' = Q.prove(`!P Q : cheri_state -> 'a.
                            (P = \s. Q s) ==>
                            (P = \s. let s = s in Q s)`, EVAL_TAC THEN metisLib.METIS_TAC []);
  val cheri_rws = theory_rewrites (reg_thms,filter_inventory opaque cheriTheory.inventory);
  val cheri_rws = map (fn th => HO_MATCH_MP extra_let th
                        handle HOL_ERR _ => GEN_ALL (HO_MATCH_MP extra_let' (SPEC_ALL th))
                        handle HOL_ERR _ => th) cheri_rws
  val tys = theory_types cheriTheory.inventory


  fun construct remove_let =
      let val cmp = wordsLib.words_compset ()
          val () = if remove_let then computeLib.scrub_const cmp let_tm else ()
          val () = List.app (computeLib.scrub_const cmp) words_opaque
          val () = add_base_datatypes cmp
          val () = add_datatypes tys cmp
          val () = computeLib.add_thms cheri_rws cmp
          val () = computeLib.add_thms extra_thms cmp
          val () = 
              computeLib.add_conv (bitstringSyntax.v2w_tm, 1, bitstringLib.v2w_n2w_CONV)
                                  cmp
      in cmp
      end

in
val cmp = construct true
val full_cmp = construct false
end;

(* This takes a little while.  We probably don't need all of these, but it would
   also be nice to have a more general conversion. *)
local
   val bytes = List.tabulate (8,(fn i => Psyntax.mk_var ("b" ^ int_to_string i, ``:word8``)))
   fun concat_list tms =
       let fun add (tm,NONE) = SOME tm
             | add (tm,SOME tm') = SOME (wordsSyntax.mk_word_concat (tm, tm'))
           val r = foldr add NONE tms
       in Option.valOf r
       end
   fun combine n m = concat_list (List.take (List.drop (bytes,7-n), n-m+1))
   val bytes_tm = combine 7 0
   fun thm n m =
       let val (n_tm, m_tm) = (numSyntax.term_of_int (8*n+7),numSyntax.term_of_int (8*m))
       in wordsLib.WORD_DECIDE ``(^n_tm >< ^m_tm) ^bytes_tm = ^(combine n m)``
       end
   fun count f 0 = [f 0]
     | count f n = (f n)::(count f (n-1))
in val byte_selects = List.concat (count (fn n => count (thm n) n) 7)
end;

fun carries_state_type ty =
    ty = ``:cheri_state`` orelse
    let val (_,r) = pairSyntax.dest_prod ty
    in r = ``:cheri_state``
    end handle _ => false


local
   open wordsTheory
   val low_bit_correct = wordsLib.WORD_DECIDE ``¬(a ⊕ 7w : word3) = a``;

   (* select_*_be adapted from mips_stepTheory, which ends up producing more
      specialised versions.  We can't use REWRITE_CONV for these because we
      need to fill in the free variable in the hypothesis, but to use
      utilsLib.INST_REWRITE_CONV we need to fill in s.MEM, because it won't
      match a free variable there. *)
   val _ = List.app (fn f => f ())
                    [numLib.prefer_num, wordsLib.prefer_word, wordsLib.guess_lengths]

   open lcsymtacs
   infix \\
   val op \\ = op Tactical.THEN;

   val tac =
       wordsLib.Cases_word_value
       \\ simp []
       \\ strip_tac
       \\ blastLib.BBLAST_TAC

in

val select_byte_be = Q.prove(
   `!b:word3 a:word64.
      (7 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) = s.MEM (a + w2w (b ?? 7w))`,
   tac
   )

val select_half_be = Q.prove(
   `!b:word3 a:word64.
     ~word_bit 0 b ==>
     ((15 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 6w)) @@ s.MEM (a + w2w (b ?? 6w) + 1w)) : word16)`,
   tac
   )
val select_half_be = UNDISCH (SPEC_ALL select_half_be)

val select_word_be = Q.prove(
   `!b:word3 a:word64.
     ((1 >< 0) b = 0w:word2) ==>
     ((31 + 8 * w2n b >< 8 * w2n b)
      (s.MEM a @@
       s.MEM (a + 1w) @@
       s.MEM (a + 2w) @@
       s.MEM (a + 3w) @@
       s.MEM (a + 4w) @@
       s.MEM (a + 5w) @@
       s.MEM (a + 6w) @@
       s.MEM (a + 7w)) =
     (s.MEM (a + w2w (b ?? 4w)) @@ s.MEM (a + w2w (b ?? 4w) + 1w) @@
      s.MEM (a + w2w (b ?? 4w) + 2w) @@ s.MEM (a + w2w (b ?? 4w) + 3w)) : word32)`,
   tac
   )
val select_word_be = UNDISCH (SPEC_ALL select_word_be)


val mem_rws =
    [select_byte_be, select_half_be, select_word_be, (*select_pc_be,*)
     (*computeLib.EVAL_RULE byte_address, address_align2,
     StoreMemory_byte, StoreMemory_doubleword, StoreMemory_half, StoreMemory_word,*)
     WORD_XOR_ASSOC, WORD_XOR_CLAUSES,
     low_bit_correct]
end

(* Theorems and information to help build the overall result *)
val let_thm = Tactical.prove(
       ``!u v P t. (u : 'a = v) ==> (P v = t : 'b) ==> (LET P u = t)``,
       metisLib.METIS_TAC []);

val cond_thm = Tactical.prove(
       ``((b = b1) ==> b1 ==> (tt = t : 'a) ==> ((if b then tt else tf) = t)) /\
         ((~b = b2) ==> b2 ==> (tf = t) ==> ((if b then tt else tf) = t))``,
       metisLib.METIS_TAC []);

fun AP_SND th =
    let val (ty1, ty2) = (pairSyntax.dest_prod o type_of o lhs o concl) th
        val snd_tm = inst [alpha |-> ty1, beta |-> ty2] pairSyntax.snd_tm
    in AP_TERM snd_tm th
    end

fun ASSUME_CONJ tm =
    if is_conj tm then
       let val (l,r) = dest_conj tm
           val (l,r) = (ASSUME_CONJ l, ASSUME_CONJ r)
       in CONJ l r
       end
    else ASSUME tm

val update_fns = map rator (utilsLib.update_fns ``:cheri_state``)

val optionSyntax_is_is_some = can optionSyntax.dest_is_some

infixr 3 ANDPERHAPSC;

fun (conv1 ANDPERHAPSC conv2) t =
    let val th1 = conv1 t
    in TRANS th1 (conv2 (rhs (concl th1))) handle UNCHANGED => th1
    end

fun WHENC p conv t =
    if p t then conv t else raise UNCHANGED

(* Some ad-hoc profiling
val lastcall = ref NONE : (Time.time * term) option ref
val worstcall = ref NONE : (int * term) option ref
fun tick tm =
  let val t' = Time.now ()
      val () = case !lastcall of NONE => ()
                               | SOME (t,_) =>
                                 let val d = Time.toSeconds (Time.- (t',t)) in
                                 case !worstcall of
                                     NONE => worstcall := SOME (d,tm)
                                   | SOME (d',_) =>
                                     if d > d' then worstcall := SOME (d,tm) else ()
                                 end
  in lastcall := SOME (t',tm)
  end
*)

fun chomp (hyps,t) =
  let (*val () = tick t*)
      fun cnv cmp = (QCONV o Conv.REPEATC o Conv.CHANGED_CONV)
                       (computeLib.CBV_CONV cmp
                        (* Push conditionals into state updates (especially for memory) to
                           avoid quadratic case-splits.
                           Too expensive to do everywhere, so try a top-level rewrite and
                           only do the whole term if that had an effect. *)
                        THENC ((GEN_REWRITE_CONV TRY_CONV empty_rewrites cond_updates
                                ANDPERHAPSC REWRITE_CONV cond_updates)
                               (* Expose and eliminate trivial aliasing checks early to
                                  prevent quadratic blowup *)
                               ANDPERHAPSC (computeLib.CBV_CONV cmp THENC wordsLib.WORD_ARITH_CONV))
                        (* Can't use INST_REWRITE_CONV due to free vars with a
                           specific meaning. *)
                        THENC REWRITE_CONV hyps
                        THENC utilsLib.INST_REWRITE_CONV (byte_selects@mem_rws))

      (* When we case split on a conditional guard we want to make sure that
         the assumption we produce is in a nice form.  We return an equality
         theorem, which reduces the guard into the term that is returned,
         plus a theorem for rewriting, and a theorem for linking the guard
         term with the assumptions. *)
      fun cnv_guard tm =
          let val th = cnv full_cmp tm
              val tm = rhs (concl th)
          in if optionSyntax_is_is_some tm
             then let val tm' = optionSyntax.dest_is_some tm
                      val v = genvar (optionSyntax.dest_option (type_of tm'))
                      val hy = ASSUME ``^tm' = SOME ^v``
                      val th' = simpLib.SIMP_PROVE (BasicProvers.srw_ss()) [hy] ``IS_SOME ^tm'``
                  in (th, tm, [hy], th')
                  end
             else let val th' = ASSUME_CONJ tm
                  in (th, tm, map ASSUME (hyp th'), th')
                  end
          end
      fun mk_cond t =
          let val (guard,t1,t2) = dest_cond t
              val (guard_eq, guard', guard_hyps, guard_thm)  = cnv_guard guard
              val (nguard_eq, nguard', nguard_hyps, nguard_thm)  = cnv_guard (mk_neg guard)
              val ty = type_of t1
              val cth = INST_TYPE [alpha |-> ty] cond_thm
              val cth = INST [``b:bool`` |-> guard,
                              ``b1:bool`` |-> guard',
                              ``b2:bool`` |-> nguard',
                              mk_var ("tt",ty) |-> t1,
                              mk_var ("tf",ty) |-> t2] cth
              val (th1, th2) = CONJ_PAIR cth
              val gt = MATCH_MP (MATCH_MP th1 guard_eq) guard_thm
              val gf = MATCH_MP (MATCH_MP th2 nguard_eq) nguard_thm
              (* We might have managed to resolve the guard to T or F, so we
                 don't need the F cases; we can drop them by returning ARB *)
              (* TODO: get rid of the T hypothesis which appears when this
                       happens *)
              fun good_hyps th =
                  List.all (fn tm => tm <> boolSyntax.F) (hyp th)
              val t1 = if List.all good_hyps guard_hyps then t1 else arb
              val t2 = if List.all good_hyps nguard_hyps then t2 else arb
          in (t1,t2,guard_hyps,nguard_hyps,gt,gf)
          end
                   
      val thm = cnv cmp t
      val t = rhs (concl thm)
      (* For simplifying terms once we stop exploring; also allowed to deal with let terms *)
      fun final_simp t = (cnv full_cmp
                          (* This catches cases of memory updates that were
                             partially rewritten before, but need more to expose
                             and eliminate the aliasing checks.  TODO: make the
                             general conversion handle this more appropriately *)
                          THENC REWRITE_CONV cond_updates
                          (* Expose and eliminate trivial aliasing checks early to
                             prevent quadratic blowup *)
                          THENC computeLib.CBV_CONV full_cmp
                          THENC wordsLib.WORD_ARITH_CONV) t
      fun no_more t =
          [(hyps,final_simp t)]
      (*val () = print_term t
      val () = print "\n---\n"*)
      val thms =
          if is_let t
          then let val (f,x) = dest_let t
                   val xs = if carries_state_type (type_of x) then chomp (hyps,x) else no_more x
                   val ts = map (fn (hy,x) => (x,chomp (hy,mk_comb (f,rhs (concl x))))) xs
               in List.concat (map (fn (xth,ts') =>
                                       map (fn (hy,tth) =>
                                               (hy,MP (MP (ISPECL [x,rhs (concl xth),f,rhs (concl tth)] let_thm) xth) tth)) ts') ts)
               end
          else if pairSyntax.is_pair t
          then let val (l,r) = pairSyntax.dest_pair t
                   val l' = final_simp l
                   val _ = assert (fn x => x = ``:cheri_state``) (type_of r)
                   val rs = chomp (hyps,r)
               in map (fn (hy,r) => (hy,PairRules.MK_PAIR (l',r))) rs
               end
          else if pairSyntax.is_snd t
          then let val t = pairSyntax.dest_snd t
                   val _ = assert carries_state_type (type_of t)
                   val ts = chomp (hyps,t)
               in map (fn (hy,t) => (hy,AP_SND t)) ts
               end
          else if is_cond t
          then let val (t1,t2,ht,hf,gt,gf) = mk_cond t
                   val ts1 = chomp (ht@hyps, t1)
                   val ts1 = map (fn (hy,th) => (hy,MATCH_MP gt th)) ts1
                   val ts2 = chomp (hf@hyps, t2)
                   val ts2 = map (fn (hy,th) => (hy,MATCH_MP gf th)) ts2
               in ts1@ts2
          end
          else if TypeBase.is_case t
          then let val (_,v,cs) = TypeBase.dest_case t
               (* If we're matching on a variable then it's reasonable to case
                  split *)
               in if is_var v then
                   (* TODO: freshen free variables? *)
                   let fun branch (c,_) =
                           let val hy = ASSUME (mk_eq (v,c))
                               val thm = REWRITE_CONV [hy] t
                               val t' = rhs (concl thm)
                               val rs = chomp (hy::hyps,t')
                               fun fix (hyp',thm') =
                                   (hyp',TRANS thm thm')
                           in map fix rs
                           end
                   in List.concat (map branch cs)
                   end
                  (* If we're not matching on a variable, try to reduce it.  If
                     it gives a variable or a constructor then case split,
                     if it is a conditional break it up and try again,
                     otherwise leave it alone. *)
                  else
                     let val thm = cnv full_cmp v
                         val v' = rhs (concl thm)
                         val thm' = ONCE_REWRITE_CONV [thm] t
                     in if is_cond v'
                        then let val (t1,t2,ht,hf,gt,gf) = mk_cond v'
                                 val th1 = cnv full_cmp t1
                                 val th2 = cnv full_cmp t2
                                 val gt = MATCH_MP gt th1
                                 val gf = MATCH_MP gf th2
                                 fun rw th = TRANS thm' (ONCE_REWRITE_CONV [th] (rhs (concl thm'))) 
                                 val thm1 = rw th1
                                 val thm2 = rw th2
                                 val ts1 = chomp (ht@hyps, rhs (concl thm1))
                                 val ts2 = chomp (hf@hyps, rhs (concl thm2))
                                 fun fix thm (h,thm') = (h,TRANS thm thm')
                             in (map (fix thm1) ts1)@(map (fix thm2) ts2)
                             end
                        else if is_var v' orelse
                           TypeBase.is_constructor v' orelse
                           (is_comb v' andalso TypeBase.is_constructor (fst (strip_comb v'))) then
                           let val rs = chomp (hyps,rhs (concl thm'))
                               fun fix (h,thm'') = (h,TRANS thm' thm'')
                           in map fix rs
                           end
                        else [(hyps,thm')]
                     end
               end
          else if is_comb t
          then let val (f,args) = strip_comb t
                   val f_name = (fst o dest_const) f
               (* Discard cases we don't want *)
               in if f_name = "raise'exception" orelse f_name = "SignalException"
                  then []
                  (* For state updates we want to check the term that's updated
                     for interesting content. *)
                  else if mem f update_fns
                  then let val (upd,s) =
                               case args of [upd,s] => (upd,s)
                                          | _ => failwith ("Bad update: " ^
                                                           Parse.term_to_string t)
                           val rs = chomp (hyps,s)
                           val upd_thm = final_simp upd
                           val upd' = rhs (concl upd_thm)
                           val thm1 = AP_THM (AP_TERM f upd_thm) s
                           fun fix (h,thm) = (h,TRANS thm1 (AP_TERM (mk_comb (f,upd')) thm))
                       in map fix rs
                       end
                  else no_more t
               end
          (* ARB isn't a useful next state - it appears when a case split
             becomes an if-then-else tower *)
          else if is_arb t
          then []
          else no_more t
(*
fun warn_tm tm = if term_size tm > 100 then 
                 let val () = print_term t
                     val () = print "\n --- giving ---\n"
                     val () = print_term tm
                     val () = print "\n === \n"
                 in ()
                 end else ()
fun warn thm = List.app warn_tm (Thm.hyps thm)
val () = List.app (fn (_,thm) => warn thm) thms
*)
  in map (fn (hy,th) => (hy,
                         let val th' = TRANS thm (assert (fn th => lhs (concl th) = t) th)
                                       handle e => (Parse.print_thm thm; Parse.print_thm th; raise e)
                         in th'
                         end
         )) thms
  end

fun eval t =
  map snd (chomp (map ASSUME [``s.CP0.Config.BE``, ``~s.CP0.Status.RE``], t))


val std_hyp =
    [``s.CP0.Config.BE``, ``~s.CP0.Status.RE``,
     ``~s.exceptionSignalled``, (* No MIPS exception *)
     ``s.exception = NoException``, (* No L3 exception *)
     ``s.BranchTo = NONE``]

fun breakup_hex s =
case StringCvt.scanString (Int.scan StringCvt.HEX) s of
    NONE => failwith s
  | SOME n =>
    let val b4 = n mod 256
        val b3 = n div 256 mod 256
        val b2 = n div 256 div 256 mod 256
        val b1 = n div 256 div 256 div 256
    in (b1, b2, b3, b4)
    end

(*fun next h =
    let val (by1,by2,by3,by4) = breakup_hex h
        val instrs =
            [``s.MEM s.PC = ^(wordsSyntax.mk_wordii (by1,8))``,
             ``s.MEM (s.PC + 1w) = ^(wordsSyntax.mk_wordii (by2,8))``,
             ``s.MEM (s.PC + 2w) = ^(wordsSyntax.mk_wordii (by3,8))``,
             ``s.MEM (s.PC + 3w) = ^(wordsSyntax.mk_wordii (by4,8))``]
        val rs = chomp (map ASSUME (instrs@std_hyp), ``NextStateMIPS s``)
        val ths = map snd rs
    in ths
    end

fun next_code s =
  let val h = mips.encodeInstruction s
  in next h
  end
*)

end
