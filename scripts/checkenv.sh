#!/bin/bash

if [ -z "$HOL4_YICES_EXECUTABLE" ]; then
  echo Environment variable HOL4_YICES_EXECUTABLE is not set
else
  if [ -x "$HOL4_YICES_EXECUTABLE" ]; then
    echo Yices OK
  else
    echo "HOL4_YICES_EXECUTABLE is not executable: $HOL4_YICES_EXECUTABLE"
  fi
fi

if type hol; then
  echo HOL OK
else
  echo HOL not found
fi

if type l3mips-cheri-256; then
  echo Simulator OK
else
  echo Simulator not found
fi

if [ `ulimit -v` = unlimited ]; then
  echo WARNING: No memory limit
fi
