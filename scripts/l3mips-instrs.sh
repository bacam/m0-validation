#!/bin/bash

l3mips --trace 1 --cycles 20000 --uart-delay 0 --ignore HI --ignore LO   --format raw "$1" \
  | grep ^instr \
  | grep -v '^instr 0 [0-9]* 9000000040000[0-2][0-9A-F][0-9A-F]' \
  | grep -v '^instr 0 [0-9]* 90000000400003[0-1][0-9A-F]' \
  | grep -v '^instr 0 [0-9]* 9000000040000320' \
  | grep -v '^instr 0 [0-9]* 9000000040000324'
