signature HWTest =
sig
type connection
val connect : unit -> connection
val close : connection -> unit
val reset : connection -> unit
val cpu_id : connection -> string

exception Permanent_failure of string

val run_and_check : connection -> (Arbnum.num * Term.term vector) list -> Term.term -> Thm.thm -> Harness.harness -> string option

val options : (string * ((string -> unit) * (unit -> string))) list

end
